const pug = require('pug');
const fs = require('fs');

// Compile template.pug, and render a set of data
var content = pug.renderFile('html/index2.pug', {
  name: 'Timothy'
});
fs.writeFile('html/index2.html', content, err => {
  if (err) {
    console.error(err);
  } else {
    // file written successfully
  }
});
// "<p>Timothy's Pug source code!</p>"
