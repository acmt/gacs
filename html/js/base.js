
function cross_refs() {
  let h1counter = 0;
  let h2counter = 0;
  let h3counter = 0;
  let thmcounter = 0;
  let defcounter = 0;
  let notcounter = 0;
  const h2s = Array.from(document.querySelectorAll('h1, h2, h3, .thm')).reduce((h2s, h, i) => {
    switch (h.nodeName) {
      case 'H1': h2s[h.id] = ++h1counter; h2counter = 0; break;
      case 'H2': h2s[h.id] = ++h2counter; h3counter = 0; break;
      case 'H3': h2s[h.id] = ++h3counter; break;
      default:
        if (h.classList.contains("definition")) {
          h2s[h.id] = `D ${h1counter}.${++defcounter}`;
        } else if (h.classList.contains("theorem")) {
          h2s[h.id] = `T ${h1counter}.${++thmcounter}`;
        } else if (h.classList.contains("note")) {
          h2s[h.id] = `N ${h1counter}.${++notcounter}`;
        } else if (h.classList.contains("corollary")) {
          h2s[h.id] = `C ${h1counter}.${++thmcounter}`;
        } else if (h.classList.contains("proposition")) {
          h2s[h.id] = `P ${h1counter}.${++thmcounter}`;
        } else if (h.classList.contains("lemma")) {
          h2s[h.id] = `P ${h1counter}.${++thmcounter}`;
        }
    }

    return h2s;
  }, {})

  Array.from(document.querySelectorAll('a[href^="#"]')).forEach(_ => _.textContent = `${h2s[_.href.split('#')[1]]}. ${_.textContent}`)
}


var tlProgress,
  /** @type {anime.AnimeTimelineInstance} */
  myTimeline, tlComp;
function updateUi(anim) {

}

function animate_fig_vs_assoc() {
  /** @type {SVGElement} */
  let fig_vs_assoc = document.getElementById("fig-vs-assoc");
  /** @type {SVGLineElement} */
  let u_arr = fig_vs_assoc.getElementById("fig-vs-assoc-u-arr");
  /** @type {SVGLineElement} */
  let v_arr = fig_vs_assoc.getElementById("fig-vs-assoc-v-arr");
  /** @type {SVGLineElement} */
  let uv_arr = fig_vs_assoc.getElementById("fig-vs-assoc-uv-arr");
  /** @type {SVGLineElement} */
  let u_arr2 = fig_vs_assoc.getElementById("fig-vs-assoc-u-arr2");
  /** @type {SVGLineElement} */
  let v_arr2 = fig_vs_assoc.getElementById("fig-vs-assoc-v-arr2");
  /** @type {SVGForeignObjectElement} */
  let u = fig_vs_assoc.getElementById("fig-vs-assoc-u");
  /** @type {SVGForeignObjectElement} */
  let v = fig_vs_assoc.getElementById("fig-vs-assoc-v");
  /** @type {SVGForeignObjectElement} */
  let uv = fig_vs_assoc.getElementById("fig-vs-assoc-uv");
  /** @type {boolean} */
  let fig_vs_assoc_moving;
  fig_vs_assoc.addEventListener("mousedown", (event) => fig_vs_assoc_moving = true);
  fig_vs_assoc.addEventListener("mouseup", (event) => fig_vs_assoc_moving = false);
  fig_vs_assoc.addEventListener("mousemove", (event) => {
    if (fig_vs_assoc_moving) {
      var rect = fig_vs_assoc.getBoundingClientRect();
      console.log(event);
      u_arr.x2.baseVal.value = u_arr2.x2.baseVal.value = event.clientX - rect.x;
      v_arr.y2.baseVal.value = v_arr2.y2.baseVal.value = event.clientY - rect.y;
      v_arr2.x1.baseVal.value = v_arr2.x2.baseVal.value = u_arr.x2.baseVal.value;
      u_arr2.y1.baseVal.value = u_arr2.y2.baseVal.value = v_arr.y2.baseVal.value;
      uv_arr.x2.baseVal.value = event.clientX - rect.x;
      uv_arr.y2.baseVal.value = event.clientY - rect.y;
      uv.x.baseVal.value = event.clientX - rect.x;
      uv.y.baseVal.value = event.clientY - rect.y;
      u.x.baseVal.value = uv.x.baseVal.value / 2;
      u.y.baseVal.value = 0;
      v.x.baseVal.value = 5;
      v.y.baseVal.value = uv.y.baseVal.value / 2 - 10;
    }
  });
}

document.addEventListener("DOMContentLoaded", (event) => {
  var nestedElement = document.querySelector(".content");
  var dragging = false;
  var playing = true;
  // nestedElement.scrollTo(0, nestedElement.scrollHeight);
  cross_refs();
  var timelineUi = document.querySelector(".timeline");
  /** @type {HTMLElement} */
  tlAudio = timelineUi.querySelector(".audio");
  tlAudio.addEventListener('timeupdate', (event) => {
    var ratio = tlAudio.currentTime / tlAudio.duration;
    myTimeline.seek(myTimeline.duration * ratio);
    // myTimeline.seek(tlAudio.currentTime * 1000);
    for (let i = 0; i < myTimeline.children.length; i += 1) {
      if (myTimeline.children[i].timelineOffset > myTimeline.currentTime) {
        myTimeline.children[i].completed = false;
        myTimeline.children[i].began = false;
      }
    }
  });
  function update(event) {
    var rect = tlComp.getBoundingClientRect();
    var ratio = (event.clientX - rect.x) / rect.width;
    myTimeline.seek(myTimeline.duration * ratio);
  }

  animate_fig_vs_assoc();
  // id, scroll, offset, duration
  var animparts = [
    ["#sec1-vector-spaces", "#sec1-vector-spaces", 1000, 500],
    ["#sec2-outline", "#sec1-vector-spaces", 2200, 500],
    ["#f-vector-space-outline-1", "#sec1-vector-spaces", 3010, 500],
    ["#f-vector-space-outline-2", "#sec1-vector-spaces", 3500, 500],
    ["#f-vector-space-outline-3", "#sec1-vector-spaces", 4000, 500],
    ["#f-vector-space-outline-4", "#sec1-vector-spaces", 4500, 500],
    ["#f-vector-space-outline-5", "#sec1-vector-spaces", 5000, 500],
    ["#f-vector-space-outline-6", "#sec1-vector-spaces", 5500, 500],
    ["#f-vector-space-outline-7", "#sec1-vector-spaces", 6000, 500],
    ["#f-vector-space-outline-8", "#sec1-vector-spaces", 6500, 500],
    ["#f-vector-space-outline-9", "#sec1-vector-spaces", 7000, 500],
    ["#sec2-vector-spaces", "#sec1-vector-spaces", 7500, 500],
    ["#def-vector-space", "#sec1-vector-spaces", 9000, 500],
    ["#def-vector-space-01", "#sec1-vector-spaces", 13500, 500],
    ["#def-vector-space-02", "#sec1-vector-spaces", 19000, 500],
    ["#def-vector-space-03", "#sec1-vector-spaces", 33000, 500],
    ["#def-vector-space-04", "#sec1-vector-spaces", 51000, 500],
    ["#def-vector-space-05", "#sec1-vector-spaces", 59000, 500],
    ["#def-vector-space-06", "#sec1-vector-spaces", 60000, 500],
    ["#def-vector-space-07", "#sec1-vector-spaces", 61000, 500],
    ["#def-vector-space-08", "#sec1-vector-spaces", 65000, 500],
    ["#def-vector-space-09", "#sec1-vector-spaces", 65500, 500],
    ["#def-vector-space-10", "#sec1-vector-spaces", 66000, 500],
    ["#def-vector-space-11", "#sec1-vector-spaces", 66500, 500],
    ["#def-vector-space-12", "#sec1-vector-spaces", 67000, 500],
    ["#fig-vs", "#sec1-vector-spaces", 67500, 500],
    ["#def-r-c-vector-space", "#sld-r-c-vector-spaces", 68000, 500],
    ["#def-lincomb", 100, 68500, 500],
    ["#def-trivial", 100, 69000, 500],
    ["#sec3-examples-vector-spaces", 100, 69500, 500],
    ["#ex-functions", 100, 70000, 500],
    ["#ex-matrices", 100, 70500, 500],
    ["#ex-sequences", 100, 71000, 500],
    ["#ex-infinite-sequences", 100, 71500, 500],
    ["##ex-infinite-seq-1", 100, 71500, 500],
    ["##ex-infinite-seq-2", 100, 71600, 500],
    ["##ex-infinite-seq-3", 100, 71700, 500],
    ["#sec2-subspaces", 100, 72500, 500],
    ["#def-subspaces", 100, 73000, 500],
    ["def-subspace", 100, 73500, 500],
    ["#def-proper-subsp", 100, 74000, 500],
    ["#def-zero-subsp", 100, 74500, 500],
    ["#thm-subspace-nec-suf", 100, 75000, 500],
    ["#prf-subspace-nec-suf", 100, 75500, 500],
    ["#ex-subspace", 100, 76000, 500],
    ["#sec3-lattice-subspaces", 100, 76500, 500],
    ["#def-set-subspaces", 100, 77000, 500],
    ["#thm-setsubsp-poset", 100, 77500, 500],
    ["#prf-setsubsp-poset", 100, 78000, 500],
    ["#lem-intersection-subsp", 100, 78500, 500],
    ["#prf-intersection-subsp", 100, 79000, 500],
    ["#thm-subsp-glb", 100, 79500, 500],
    ["#prf-subsp-glb", 100, 80000, 500],
    ["#thm-vs-not-union", 100, 80500, 500],
    ["#prf-vs-not-union", 100, 81000, 500],
    ["#def-sum", 100, 81500, 500],
    ["#def-sum-infty", 100, 82000, 500],
    ["#thm-sum-lub", 100, 82500, 500],
    ["#prf-sum-lub", 100, 83000, 500],
    ["#thm-set-subsp-complete-lattice", 100, 83500, 500],
    ["#prf-set-subsp-complete-lattice", 100, 84000, 500],
    ["#sec2-direct-sums", 100, 84500, 500],
    ["#sec3-external-direct-sum", 100, 85000, 500],
    ["#def-ext-dir-sum", 100, 85500, 500],
    ["#ex-ext-dir-sum", 100, 86000, 500],
    ["#not-tuple-idx-func", 100, 86200, 500],
    ["#def-dir-prod", 100, 86500, 500],
    ["#def-support", 100, 87000, 500],
    ["#def-ext-dir-sum-inf", 100, 87500, 500],
    ["#def-support", 100, 88000, 500],
    ["#not-dir-prod-dir-sum", 100, 88500, 500],
    ["#sec-int-dir-sum", 100, 89000, 500],
    ["#def-int-dir-sum", 100, 89500, 500],
    ["#def-int-dir-sum-join", 100, 90000, 500],
    ["#def-int-dir-sum-ind", 100, 90500, 500],
    ["#thm-complem-exist", 100, 91000, 500],
    ["#prf-complem-exist", 100, 91500, 500],
    ["#thm-int-direct-equiv", 100, 92000, 500],
    ["#thm-int-direct-equiv1", 100, 92500, 500],
    ["#thm-int-direct-equiv2", 100, 93000, 500],
    ["#thm-int-direct-equiv3", 100, 93500, 500],
    ["#prf-int-direct-equiv", 100, 94000, 500],
    ["#def-sym-matrix", 100, 94500, 500],
    ["#def-skew-matrix", 100, 95000, 500],
    ["#ex-sym-skew", 100, 95500, 500],
    ["#sec2-span-li", 100, 96000, 500],
    ["#def-span", 100, 96500, 500],
    ["#sec3-li", 100, 97000, 500],
    ["#def-li", 100, 97500, 500],
    ["#not-li", 100, 98000, 500],
    ["#def-ess-uniq", 100, 98500, 500],
    ["#thm-li-equiv", 100, 99000, 500],
    ["#thm-li-equiv1", 100, 99500, 500],
    ["#thm-li-equiv2", 100, 100000, 500],
    ["#thm-li-equiv3", 100, 100500, 500],
    ["#prf-li-equiv", 100, 101000, 500],
    ["#def-min-span", 100, 101500, 500],
    ["#def-max-li", 100, 102000, 500],
    ["#thm-li-equivalent-conds", 100, 102500, 500],
    ["#thm-li-equivalent-conds1", 100, 103000, 500],
    ["#thm-li-equivalent-conds2", 100, 103500, 500],
    ["#thm-li-equivalent-conds3", 100, 104000, 500],
    ["#thm-li-equivalent-conds4", 100, 104500, 500],
    ["#prf-li-equivalent-conds", 100, 105000, 500],
    ["#sec3-basis", 100, 105500, 500],
    ["#def-basis", 100, 106000, 500],
    ["#thm-basis-direct-sum", 100, 106500, 500],
    ["#prf-basis-direct-sum", 100, 107000, 500],
    ["#thm-basis-existence", 100, 107500, 500],
    ["#thm-basis-existence1", 100, 108000, 500],
    ["#thm-basis-existence2", 100, 108500, 500],
    ["#thm-basis-existence3", 100, 109000, 500],
    ["#prf-basis-existence", 100, 109500, 500],
    ["#sec2-dimension", 100, 110000, 500],
    ["#thm-li-largest", 100, 110500, 500],
    ["#prf-li-largest", 100, 111000, 500],
    ["#thm-basis-same-order", 100, 111500, 500],
    ["#prf-basis-same-order", 100, 112000, 500],
    ["#thm-basis-same-cardinality", 100, 112500, 500],
    ["#prf-basis-same-cardinality", 100, 113000, 500],
    ["#def-dimension", 100, 113500, 500],
    ["#def-dimension1", 100, 114000, 500],
    ["#def-dimension2", 100, 114500, 500],
    ["#def-dimension3", 100, 115000, 500],
    ["#def-dimension4", 100, 115500, 500],
    ["#thm-basis-direct-sum-span", 100, 116000, 500],
    ["#thm-basis-direct-sum-span1", 100, 116500, 500],
    ["#thm-basis-direct-sum-span2", 100, 117000, 500],
    ["#prf-basis-direct-sum-span", 100, 117500, 500],
    ["#thm-direct-sum-dim", 100, 118000, 500],
    ["#prf-direct-sum-dim", 100, 118500, 500],
    ["#sec2-ordered-basis-coord-mtx", 100, 119000, 500],
    ["#def-ordered-basis", 100, 119500, 500],
    ["#def-coord-mtx", 100, 120000, 500],
    ["#not-coord-map-linear", 100, 120500, 500],
    ["#sec2-col-row-rank", 100, 120800, 500],
    ["#def-col-row-rank", 100, 121000, 500],
    ["#def-col-row-rank1", 100, 121500, 500],
    ["#def-col-row-rank2", 100, 122000, 500],
    ["#def-col-row-rank3", 100, 122500, 500],
    ["#def-col-row-rank4", 100, 123000, 500],
    ["#lem-elem-colrow-op-rank", 100, 123500, 500],
    ["#prf-elem-colrow-op-rank", 100, 124000, 500],
    ["#thm-col-row-rank", 100, 124500, 500],
    ["#prf-col-row-rank", 100, 125000, 500],
    ["#def-rank", 100, 125500, 500],
    ["#def-real-version", 100, 125700, 500],
    ["#sec2-complexification", 100, 126000, 500],
    ["#def-complexification", 100, 126500, 500],
    ["#not-complexif-notation", 100, 127000, 500],
    ["#not-complexif-notation1", 100, 127500, 500],
    ["#not-complexif-notation2", 100, 128000, 500],
    ["#not-complexif-notation3", 100, 128500, 500],
    ["#not-complexif-notation4", 100, 129000, 500],
    ["#def-complex-map", 100, 129500, 500],
    ["#not-complex-map-homomorp", 100, 130000, 500],
    ["#not-complex-map-homomorp1", 100, 130100, 500],
    ["#not-complex-map-homomorp2", 100, 130200, 500],
    ["#not-complex-map-homomorp3", 100, 130300, 500],
    ["#sec3-complexific-dim", 100, 130500, 500],
    ["#thm-complexific-dim", 100, 131000, 500],
    ["#prf-complexific-dim", 100, 131500, 500],
    ["#sec2-exercises", 100, 132000, 500],
    ["#sld-exercises", 100, 132500, 500],
  ];
  myTimeline = anime.timeline();
  animparts.forEach((part) => {
    myTimeline.add({
      targets: part[0], opacity: 1, duration: part[3], easing: 'linear', update: updateUi, begin: (anim) => {
        console.log(part[0]);
        nestedElement.querySelector(part[0]).scrollIntoView({ block: "center", behavior: "smooth", inline: "nearest" })
      }
    }, part[2]);
  });
  myTimeline.restart();
  myTimeline.pause();
  // myTimeline.add({ targets: '#def-r-c-vector-space', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#def-lincomb', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#def-trivial', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#sec3-examples-vector-spaces', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#ex-functions', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#ex-matrices', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#ex-sequences', opacity: 1, duration: 500, easing: 'linear', update: updateUi })
  //   .add({ targets: '#ex-infinite-sequences', opacity: 1, duration: 500, easing: 'linear', update: updateUi });
});