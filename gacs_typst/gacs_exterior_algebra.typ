#import "acmt.typ": *

#show: acmt-theme.with()
/* START OF DOCUMENT */

// Status of the slide:
  // d definition
  // r corollary
  // t theorem
  // l lemma
  // n note
  // p proposition
  // g graphical pictures
  // e example
  // c complete
  // * ongoing

#title-slide(title: "Geometric Algebra", subtitle: "Chapter 3: Exterior algebra", authors: "Anderson Tavares", institution-name: "", date: datetime.today().display())

// Exterior algebra

  // [c,d] Exterior algebra
  #slide[
    == Exterior algebra
    #definition([Exterior algebra])[
      #uncover("2-")[*Exterior algebra* (*Grassman algebra*) $and.big(V)$ of a vector space $V$]
      #uncover("3-")[$ and.big(V) = T(V)/I $]
      // #uncover("4-")[where]
      #uncover("4-",$I=span({cvec(v) times.circle cvec(v) bar.v cvec(v) in V})$)
      #uncover("5-",$=span({cvec(v) times.circle cvec(w) + cvec(w) times.circle cvec(v) bar.v cvec(v),cvec(w) in V})$)

      // #uncover("6-",[and the coset])
      #uncover("7-",[*Exterior product* between $cvec(v)$ and $cvec(w)$:])
      #uncover("6-",[$ cvec(v) wedge cvec(w) = cvec(v) times.circle cvec(w) + I $])
      #uncover("9-",[*Multivector*:])
      #uncover("8-",[$A in and.big(V)$])
    ]
  ]

  // [c,d] Exterior power
  #slide[
    == Exterior power
    #definition([Exterior power])[
      #uncover("2-")[*$k$-Exterior power* of $V$]#uncover("3-")[:]
      #uncover("3-")[quotient space $ and.big^k (V) = (T^(k)(V))/I_k $]
      #uncover("4-")[$I_k=span({cvec(v)_1 times.circle dots.c times.circle cvec(v)_k bar.v cvec(v)_1,dots,cvec(v)_k in V "and" cvec(v)_i=cvec(v)_j "for some" i eq.not j})$]

      #align(center)[#uncover("6-")[*$k$-Blade*] #uncover("5-")[$cvec(v)_1 wedge dots.c wedge cvec(v)_k = cvec(v)_1 times.circle dots.c times.circle cvec(v)_k + I_k$]]
      #pause
      #uncover("7-",[An element $cvec(A)_k in and.big^(k)(V)$ is called])
      #uncover("8-",[*$k$-vector*])
      #uncover("9-",[ of *grade* $k$.])
    ]
    #uncover("10-")[
      #note[
        To add a property $A=0$ to an algebra: define the ideal $span({A})$
        #set list(indent:.8em)
          - For $A=B$, use $A-B=0$
      ]
    ]
  ]

  // [c,d,e] Exterior algebra definition from exterior power (short example)
  #slide[
    == Exterior algebra (exterior power version)
    #definition([Exterior algebra])[
      The *exterior algebra* (*Grassman algebra*) over $V$ is a #text(myred,[_graded algebra_]) of exterior powers of $V$ #pause
      $ and.big (V) = plus.circle.big_k and.big^(k)(V)=and.big^(0)(V)plus.circle and.big^(1)(V)plus.circle dots.c $ #pause
      where $and.big^(i)(V) wedge and.big^(j)(V) = and.big^(i+j)(V)$, i.e., $ "grade"(A_i wedge blade(A)_j) = "grade"(blade(A)_i) + "grade"(blade(A)_j) $
      $
      uncover("4-",k"-blade" arrow.double.long k"-vector" arrow.double.long "multivector")
      // #uncover("5-",[Any $k$-vector is a multivector.])
      // #uncover("6-",[The opposite direction is not true.])
      // #uncover("7-",[A multivector which is not a $k$-vector has a *mixed* grade.])
      $
    ]
    #uncover("5-")[
      #example[
        $ underbrace(A, "multivector" \ and.big(V))
        = underbrace(5, "scalar" \ and.big^(0)(V))
        + underbrace(3cvec(a) + 2cvec(b),"vector" \ and.big^(1)(V))
        + underbrace(4cvec(b) wedge cvec(c) + 10 cvec(a) wedge cvec(c),"bivector" \ and.big^(2)(V)) 
        - underbrace(2 cvec(c) wedge cvec(a) wedge cvec(d) ,"trivector" \ and.big^(3)(V)) $
      ]
    ]
  ]

  // [c,g] Initial geometric view on exterior algebra
  #slide[
    == Initial geometric view on exterior algebra
    /* Put two squares
       - one for V
       - another one for ext(V)
       - display points on V to represent vectors
       - display points on ext(V) to represent vectors, 2-blades, 3-blades, k-vectors, ...
    */
    #grid(columns: (1fr, 1fr), align: center, row-gutter: 10pt,
    [On $V$], [On $ext(V)$],
    [
      #only("1",image("../gacs/figs/ext_alg_blank.svg"))
      #only("2-",image("../gacs/figs/ext_alg_vectors.svg"))
    ],[
      #only("1",image("../gacs/figs/ext_alg_blank.svg"))
      #only("2",image("../gacs/figs/ext_alg_vectors.svg"))
      #only("3",image("../gacs/figs/ext_alg_vectors_bivectors.svg"))
      #only("4",image("../gacs/figs/ext_alg_vectors_bivectors_trivectors.svg"))
      #only("5",image("../gacs/figs/ext_alg_vectors_bivectors_trivectors_scalars.svg"))
      #only("6",image("../gacs/figs/ext_alg_vectors_bivectors_trivectors_scalars_multivectors.svg"))
    ])
  ]

  // [c,d] Exterior algebra definition from alternate property
  #slide[
    == Exterior algebra (alternate property version)
    #definition([Exterior algebra])[
      The *exterior algebra* (*Grassman algebra*) over $V$#uncover("2-")[is an associative algebra $ext(V)$ that 
      - Contains $V$]#uncover("3-")[- Has *exterior product* or *wedge product* $and:and.big(V) times and.big(V) arrow.r and.big(V)$] #uncover("4-")[- Is #text(myred,[_alternating_]) on $V$, i.e., $cvec(v) wedge cvec(v) = 0$]
    ]
    #uncover("5-")[#note([Other properties])[
      #uncover("6-")[- #box(width: 10cm)[#text(myred)[_Associative_]:] $cvec(a) wedge (cvec(b) wedge cvec(c)) = (cvec(a) wedge cvec(b)) wedge cvec(c)$]
      #uncover("7-")[
        - #box(width: 10cm)[#text(myred)[_Bilinear_]:] $(alpha cvec(a) + beta cvec(b)) wedge cvec(c) = alpha cvec(a) wedge cvec(c) + beta cvec(b) wedge cvec(c)$
          #box(width: 10cm)[] $cvec(a) wedge (alpha cvec(b) + beta cvec(c)) = alpha cvec(a) wedge cvec(b) + beta cvec(a) wedge cvec(c)$]
      #uncover("8-")[- #box(width: 10cm)[#text(myred)[_Antisymmetric_]/#text(myred)[_Anticommutative_]:] $cvec(a) wedge cvec(b) = -cvec(b) wedge cvec(a)$]
      #uncover("9-")[- #box(width: 10cm)[#text(myred)[_Identity_]:] $1 wedge cvec(a) = cvec(a) wedge 1 = cvec(a)$]
      #uncover("10-")[- #box(width: 10cm)[#text(myred)[_Zero_]:] $0 wedge cvec(a) = cvec(a) wedge 0 = 0$]
      #uncover("11-")[- #box(width: 10cm)[#text(myred)[_Homogeneous_]:] $alpha wedge cvec(a) = cvec(a) wedge alpha = alpha (1 wedge cvec(a)) = alpha cvec(a)$]
    ]]
  ]

  // [c,g] Associativity and antisymmetry (visual interpretation on $ext(V)$)
  // Not yet on V
  #slide[
    == Visual interpretation of properties on $ext(V)$
    #note([On $ext(V)$])[\
      #grid(columns: (1fr, 1fr), align: center, row-gutter: .3em, [
        #only("1",image("../gacs/figs/ext_alg_vectors.svg"))
        #only("2",image("../gacs/figs/ext_alg_vectors_bivectors.svg"))
        #only("3",image("../gacs/figs/ext_alg_vectors_bivectors_trivectors.svg"))
        #only("4",image("../gacs/figs/ext_alg_vectors_bivectors_trivectors_scalars.svg"))
        #only("5",image("../gacs/figs/ext_alg_associativity.svg"))
        #only("6-",image("../gacs/figs/ext_alg_associativity2.svg"))
      ],[
        #only("7-",image("../gacs/figs/ext_alg_antisymmetry.svg"))
      ],[_Associativity_], uncover("7-")[_Antisymmetry_])
    ]
    #uncover("8-")[
    #note[Interpretation on $V$ to be constructed yet.]
    ]
  ]

  // [c,g] Bilinearity and homogeneity (visual interpretation)
  #slide[
    == Visual interpretation of properties on $ext(V)$
    #note([On $ext(V)$])[\
      #grid(columns: (1fr, 1fr), align: center, row-gutter: .3em, [
        #only("1-",image("../gacs/figs/ext_alg_bilinearity.svg"))
      ],[
        #only("2-",image("../gacs/figs/ext_alg_homogeneity.svg"))
      ],[_Bilinearity_], [_Homogeneity_])
    ]
  ]

  // [c,e] Example of multivectors
  #slide[
    == Example of multivectors
    #example[
      $ (cvec(e)_1+cvec(e)_2) wedge (cvec(e)_1 + cvec(e)_3) 
      uncover("2-",&= cancel(stroke:cp, cvec(e)_1 wedge cvec(e)_1) + cvec(e)_1 wedge cvec(e)_3 + cvec(e)_2 wedge cvec(e)_1 + cvec(e)_2 wedge cvec(e)_3) \ 
      uncover("3-",&= cvec(e)_1 wedge cvec(e)_3 + cvec(e)_2 wedge cvec(e)_1 + cvec(e)_2 wedge cvec(e)_3) \ 
      uncover("4-",&= -cvec(e)_1 wedge cvec(e)_2 + cvec(e)_2 wedge cvec(e)_3 - cvec(e)_3 wedge cvec(e)_1)  $
    ]
    #uncover("5-",[
    #example[
      $ (cvec(e)_1 + cvec(e)_2 ) wedge (cvec(e)_1 wedge cvec(e)_2 + cvec(e)_2 wedge cvec(e)_3 ) 
      uncover("6-",&= cvec(e)_1 wedge (dots) + cvec(e)_2 wedge (dots)) \ 
      uncover("7-",&= cancel(stroke:cp,cvec(e)_1 wedge cvec(e)_1 wedge cvec(e)_2) + cvec(e)_1 wedge cvec(e)_2 wedge cvec(e)_3) \ 
      uncover("8-",&+ #h(.25em) cancel(stroke:cp,cvec(e)_2 wedge cvec(e)_1 wedge cvec(e)_2) + cancel(stroke:cp,cvec(e)_2 wedge cvec(e)_2 wedge cvec(e)_3)) \  
      uncover("9-",&= cvec(e)_1 wedge cvec(e)_2 wedge cvec(e)_3) $
    ]])
  ]

  // [c,d,t] Permutation, cycle, transposition
  #slide[
    == Permutation, transposition, sign
    #definition[
      A *permutation* of a set $S$ is a bijective function from $S$ to $S$.
    ]
    #uncover("2-")[
      #definition[
        A permutation $sigma$ is a *k-cycle* if there are $a_1,dots,a_k$ s.t. $sigma(a_i)=a_(i+1) forall 1 lt.eq i < k, sigma(a_k)=a_1$ and $sigma(a)=a$ if $a in.not {a_1,dots,a_k}$, denoted $sigma=(a_1 a_2 dots a_k)$. A *cycle* means a $k$-cycle for some $k$.
      ]
    ]
    #uncover("3-")[
      #definition[
        A *transposition* is a 2-cycle
      ]
    ]
    #uncover("4-")[
      #theorem[
        Every permutation of $n gt.eq 2$ elements is a product of transpositions
      ]
    ]
    #uncover("5-")[
      #theorem[
        No permutation can be written as a product of both an even and an odd number of transpositions.
      ]
    ]
  ]

  // [c,d] Permutation parity, sign
  #slide[
    == Permutation parity, sign
    
    #uncover("1-")[
      #definition[
        A permutation is *even* or has *even parity* (resp., *odd* or has *odd parity*) if it is the product of an even (resp., odd) number of transpositions
      ]
    ]
    #uncover("2-")[#definition[
      The *sign* of a permutation is defined by
      $ "sg"(pi) = cases(#box(width:1cm,[1]) pi "has even parity",#box(width:1cm,[-1]) pi "has odd parity")$
    ]]
  ]

  // [c,t,r] Antisymmetry, alternate properties for $k$-blades and linear dependence
  #slide[
    == Antisymmetry, alternate properties for $k$-blades and linear dependence
    #theorem([Antisymmetry for $k$-blades])[
      #uncover("2-")[
        If $sigma$ is a permutation of $1,dots,k$, then $ cvec(v)_(sigma(1)) wedge dots.c wedge cvec(v)_(sigma(k)) = "sg"(sigma)cvec(v)_1 wedge dots.c wedge cvec(v)_k. $
      ]
    ]
    #uncover("3-")[
      #corollary[
        $blade(A)_k wedge blade(B)_l = (-1)^(k l) blade(B)_l wedge blade(A)_k$
      ]
    ]
    #uncover("4-")[
      #theorem([Alternate property for $k$-blades])[
        $ cvec(v)_i = cvec(v)_j "for some" i eq.not j quad arrow.double.long quad cvec(v)_1 wedge dots.c wedge cvec(v)_k = 0 $
      ]
    ]
    #uncover("5-")[
      #theorem([Exterior linear dependence])[
      $ cvec(v)_1,dots,cvec(v)_k "are linearly dependent" quad arrow.double.l.r.long quad cvec(v)_1 wedge dots.c wedge cvec(v)_k = 0 $
      ]
    ]
  ]

  // [c,e] Examples of antisymmetry and linear dependency
  #slide[
    == Examples of antisymmetry and linear dependency
    #example[
      $ (cvec(a) wedge cvec(b)) wedge (cvec(c) wedge cvec(d) wedge cvec(e)) =
      uncover("2-",+)(cvec(c) wedge cvec(d) wedge cvec(e)) wedge (cvec(a) wedge cvec(b)) $
    ]
    #uncover("3-")[
      #example[
        $ cvec(a) wedge cvec(b) wedge (2cvec(a) + 3cvec(b))
        uncover("4-", = 0) $
      ]
    ]
    #uncover("5-")[
      #example[
        $ cvec(a) wedge cvec(b) wedge (2cvec(a) wedge cvec(b))
        uncover("6-", = 0) $
      ]
    ]
    #uncover("7-")[
      #example[
        $ & cvec(a) wedge cvec(b) wedge cvec(c) =   &&cvec(b) wedge cvec(c) wedge cvec(a) =  & cvec(c) wedge cvec(a) wedge cvec(b) \ 
        =-& cvec(b) wedge cvec(a) wedge cvec(c) = - &&cvec(a) wedge cvec(c) wedge cvec(b) = -& cvec(c) wedge cvec(b) wedge cvec(a) $
      ]
    ]
  ]

  // [c,d,t,r] Subspace represented by blades, equivalence, complement
  #slide[
    == Subspace represented by blades, equivalence, complement
    #uncover("2-")[
      #definition([Subspace represented by blade])[The  *subspace* of V *represented by* blade $A$ is
        $ V(blade(A))={cvec(v) bar.v cvec(v) wedge blade(A) = blade(A) wedge cvec(v) = 0} < V $
      ]
    ]
    #uncover("3-")[
      #theorem[
        $blade(A)=lambda cvec(v)_1 wedge dots wedge cvec(v)_k quad arrow.r.l.double.long quad span(cvec(v)_1,dots,cvec(v)_k)=V(blade(A)) $
      ]
    ]
    #uncover("4-")[
      #corollary([Blade subspace equivalence])[
        $ span(cvec(v)_1, dots, cvec(v)_k) = span(cvec(w)_1, dots, cvec(w)_k) quad arrow.r.l.double.long quad cvec(v)_1 wedge dots.c wedge cvec(v)_k = lambda cvec(w)_1 wedge dots.c wedge cvec(w)_k $
      ]
    ]
    #uncover("5-")[
      #theorem[(Blade complement)
        $ V(blade(A)_k) < V(blade(A)_l) quad arrow.r.double.long quad exists blade(A)_(l-k): blade(A)_l = blade(A)_k wedge blade(A)_(l-k) $
      ]
    ]
  ]

  // [c,g,d] Geometric interpretation of $V(blade(A))$, attitude, weight
  #slide[
    == Geometric interpretation of $V(blade(A))$
    // Draw zero vector on V
    // Draw point a on ext(V) and point a on V
    // Draw line and call it V(a)
    // Draw point b on ext(V), point b on V and line V(b)
    // Draw point lambda a on ext(V), point lambda a on V and V(lamba a)=V(a)
    // Draw point a^b on ext(V)
    // Draw plane span(a,b) on V and call it V(a^b)
    // #note([Geometric interpretation of $V(A)$])[\
      #grid(columns: (1fr, 1fr), align: center, row-gutter: 10pt,
      [On $V$], [On $ext(V)$],
      [
        #only("1",image("../gacs/figs/ext_alg_blank.svg"))
        #only("2",image("../gacs/figs/ext_alg_subspace_va_1.svg"))
        #only("3",image("../gacs/figs/ext_alg_subspace_va_2.svg"))
        #only("4",image("../gacs/figs/ext_alg_subspace_0_a_va.svg"))
        #only("5",image("../gacs/figs/ext_alg_subspace_0_a_va_2a_v2a.svg"))
        #only("6",image("../gacs/figs/ext_alg_subspace_0_a_va_b_vb.svg"))
        #only("7",image("../gacs/figs/ext_alg_subspace_0_a_b.svg"))
        #only("8",image("../gacs/figs/ext_alg_subspace_0_a_b_vab.svg"))
        #only("9-10",image("../gacs/figs/ext_alg_subspace_0_a_b_vab_3d.svg"))
        #only("11-",image("../gacs/figs/ext_alg_subspace_0_a_b_c_vabc.svg"))
      ],[
        #only("1",image("../gacs/figs/ext_alg_blank.svg"))
        #only("2",image("../gacs/figs/ext_alg_subspace_va_1.svg"))
        #only("3",image("../gacs/figs/ext_alg_subspace_va_2.svg"))
        #only("4",image("../gacs/figs/ext_alg_subspace_0_a_va.svg"))
        #only("5",image("../gacs/figs/ext_alg_subspace_0_a_va_2a_v2a.svg"))
        #only("6",image("../gacs/figs/ext_alg_subspace_0_a_va_b_vb.svg"))
        #only("7-9",image("../gacs/figs/ext_alg_subspace_0_a_b_ab.svg"))
        #only("10",image("../gacs/figs/ext_alg_subspace_0_a_b_ab_spanab.svg"))
        #only("11-",image("../gacs/figs/ext_alg_subspace_0_a_b_c_abc_spanabc.svg"))
      ])#v(-1em)
    // ]
    #uncover("12-", definition[
      *Attitude* of blade $blade(A)=$ #text(myred)[$V(blade(A)) tilde.op span(blade(A))$]
    ])
    #uncover("13", definition[
      *Weight* of blade $blade(A)$ *relative to* blade ($blade(B)=lambda blade(A)$) $=$ #text(myred)[$lambda$]
    ])
  ]

  // [c,d,e,g] Geometric interpretation of blades, attitude, weight, orientation
  #slide[
    == Geometric interpretation of blades
    #uncover("1-", definition[
      *Attitude* of blade $blade(A)$: #text(myred)[$V(blade(A)) tilde.op span(blade(A))$]
    ])
    #uncover("1-", definition[
      *Weight* of blade $blade(A)$ *relative to* blade ($blade(B)=lambda blade(A)$) $=$ #text(myred)[$lambda$]
    ])
    #uncover("2-", definition[
      *Orientation* of blade $blade(A)$ *relative to* blade ($blade(B)=lambda blade(A)$) $=$ #text(myred)[$"sign"(lambda)$]
    ])
    #uncover("3-", example[
      Opposing orientations:
      #uncover("4-")[- $cvec(a)$ and $-cvec(a)$]
      #uncover("5-")[- $cvec(a) wedge cvec(b)$ and $cvec(b) wedge cvec(a)$]
      #uncover("6-")[- $cvec(x) wedge cvec(y) wedge cvec(z)$ and $cvec(y) wedge cvec(x)and cvec(z)$ (e.g., left-handed and right-handed coordinate systems)]
    ])
    #uncover("7-",align(center,image("../gacs/figs/blade_orientation_plane.svg")))
  ]

  // [c,l,t,g] Parallelness
  #slide[
    == Parallelness
    #align(center, image("../gacs/figs/parallel.svg"))
    #v(-.7em)
    #uncover("2-")[
      #lemma([Parallel vectors])[
        $ cvec(x) parallel cvec(a) quad arrow.l.r.double.long quad cvec(x) in "span"(cvec(a)) quad arrow.l.r.double.long quad cvec(x) = lambda cvec(a) quad arrow.l.r.double.long quad cvec(x) in V(cvec(a)) quad arrow.l.r.double.long quad cvec(x) wedge cvec(a) = 0 $
      ]
    ]
    #uncover("3-")[
      #lemma([Vector in a plane])[
        $ cvec(x) in span(cvec(a),cvec(b))=V(cvec(a) wedge cvec(b)) quad arrow.l.r.double.long quad cvec(x) = lambda cvec(a) + mu cvec(b) quad arrow.l.r.double.long quad cvec(x) wedge cvec(a) wedge cvec(b) = 0 $
      ]
    ]
    #uncover("4-")[
      #theorem([Vector in a hypervolume])[Let $blade(A) = cvec(v)_1 wedge dots wedge cvec(v)_k$
        $ cvec(x) in V(blade(A)) quad arrow.l.r.double.long quad cvec(x) = sum_k lambda_k cvec(v)_k quad arrow.l.r.double.long quad cvec(x) wedge blade(A) = 0 $
      ]
    ]
  ]

  // [c,g] Distorting blades (shearing, scaling, rotating)
  #slide[
    == Distorting blades
    #uncover("1-",example([Shearing])[does not change the blade
      #grid(columns: (1fr, 7cm),[
        $ cvec(a) wedge (cvec(b) + alpha cvec(a)) = cvec(a) wedge cvec(b) $
      ],[
        #v(-1.5cm)
        #image("../gacs/figs/ext_alg_shearing.svg")
      ])

      // Draw a picture of parallelogram
      // Distort the parallelogram
    ])
    #uncover("2-",example([Scaling])[produces another blade (a multiple)
    #grid(columns: (1fr, 6cm),
      [$ cvec(a) wedge (alpha cvec(b)) = (alpha cvec(a)) wedge cvec(b) = alpha (cvec(a)and cvec(b)) $],[#v(-2.2em) #image("../gacs/figs/ext_alg_scaling.svg")])
    ])
    #uncover("3-",example([Rotating at same plane])[gets same blade ($c=cos phi, s=sin phi$)
      #grid(columns: (1fr, 6cm),[
        $ & (c cvec(a) + s cvec(b)) wedge (-s cvec(a) + c cvec(b)) &= c^2(cvec(a)and cvec(b)) - s^2(cvec(b) wedge cvec(a)) \ =& (c^2 + s^2)cvec(a) wedge cvec(b) =  cvec(a) wedge cvec(b) $
      ],[#v(-1em) #image("../gacs/figs/ext_alg_rotating.svg")#v(-.5em)])
    ])
  ]

  // [c, e] Distorting blades with general linear transformation
  #slide[
    == Distorting blades
    #example([General linear transformation])[
      $ &(alpha_1 cvec(a) + alpha_2 cvec(b)) wedge (beta_1 cvec(a) + beta_2 cvec(b))
      uncover("2-",= alpha_1 beta_2 (cvec(a) wedge cvec(b)) + alpha_2 beta_1 (cvec(b) wedge cvec(a)))\
      uncover("3-",=& (alpha_1 beta_2 - alpha_2 beta_1) cvec(a) wedge cvec(b))
      uncover("4-",= det(mat(delim:"[",alpha_1,beta_1;alpha_2,beta_2))cvec(a) wedge cvec(b))  $
    ]
  ]

  // [c, g] Solving linear equations
  #slide[
    == Solving linear equations

    #grid(columns: (1fr, .8fr),[
      Expressing a known vector $cvec(x)$ on basis ${cvec(a),cvec(b)}$:
      #uncover("2-", $ cvec(x) = text(myred, alpha) cvec(a) + text(myred, beta) cvec(b) $)

      Solving unknowns $text(myred, alpha), text(myred, beta)$ with exterior products:
      $ uncover("4-",cvec(a) wedge cvec(x))uncover("5-", = text(myred, alpha) cvec(a) wedge cvec(a) + text(myred, beta) cvec(a) wedge cvec(b) = text(myred, beta) cvec(a) wedge cvec(b)) \
        uncover("6-",cvec(x) wedge cvec(b))uncover("7-", = text(myred, alpha) cvec(a) wedge cvec(b) + text(myred, beta) cvec(b) wedge cvec(b) = text(myred, alpha) cvec(a) wedge cvec(b)) $

      #uncover("13-",$ cvec(x) = uncover("14-",(cvec(x) wedge cvec(b))/(cvec(a) wedge cvec(b)))cvec(a) + uncover("15-",(cvec(a) wedge cvec(x))/(cvec(a) wedge cvec(b)))cvec(b) $)

    ],[
      #v(-1cm)
      #only("3-7",box(image("../gacs/figs/ext_alg_solv_lin_eq_01.svg", width: 10cm)))
      #only("8",box(image("../gacs/figs/ext_alg_solv_lin_eq_02.svg", width: 10cm)))
      #only("9",box(image("../gacs/figs/ext_alg_solv_lin_eq_03.svg", width: 10cm)))
      #only("10",box(image("../gacs/figs/ext_alg_solv_lin_eq_03_1.svg", width: 10cm)))
      #only("11",box(image("../gacs/figs/ext_alg_solv_lin_eq_04.svg", width: 10cm)))
      #only("12-",box(image("../gacs/figs/ext_alg_solv_lin_eq_05.svg", width: 10cm)))

    ])
    #grid(columns: (1fr, 1.5fr),[
      #uncover("16-")[
          In Linear algebra:

          $ text(myred, alpha) a_1 + text(myred, beta) b_1 = x_1 \ 
            text(myred, alpha) a_2 + text(myred, beta) b_2 = x_2 $
        ]
    ],[
      #uncover("17-")[
        Cramer's rule:
            $ text(myred, alpha) = (det(mat(delim:"[", x_1, b_1; x_2, b_2)))/(det(mat(delim:"[", a_1, b_1; a_2, b_2))),
            text(myred, beta) = (det(mat(delim:"[", x_1, b_1; x_2, b_2)))/(det(mat(delim:"[", a_1, b_1; a_2, b_2))) $
      ]
    ])
  ]

  // [c, n, g]Intersecting planar lines
  #slide[
    == Intersecting planar lines
    #grid(columns: (10cm, 1fr), [
      #only("1",image("../gacs/figs/inter_plan_lines_01.svg", width: 10cm))
      #only("2",image("../gacs/figs/inter_plan_lines_02.svg", width: 10cm))
      #only("3-4",image("../gacs/figs/inter_plan_lines_03.svg", width: 10cm))
      #only("5",image("../gacs/figs/inter_plan_lines_04.svg", width: 10cm))
      #only("6",image("../gacs/figs/inter_plan_lines_05.svg", width: 10cm))
      #only("7-",image("../gacs/figs/inter_plan_lines_06.svg", width: 10cm))

      #pad(left:1cm,uncover("9-")[
        In linear algebra:

        $hvec(x) = hvec(l) times hvec(m)$
      ])
    ],[
      Lines in $dim(V)=2$:
      $ L_(cvec(p), cvec(u))(lambda) &= cvec(p) + lambda cvec(u) \
        M_(cvec(q), cvec(v))(lambda) &= cvec(q) + lambda cvec(v) $

      #uncover("3-",[
        Intersection point $cvec(x)$:
        $ cvec(x) = alpha cvec(u) + beta cvec(v) $
      ])

      #uncover("4-")[Same as before?]

      #uncover("5-")[
        $ cvec(x) = (cvec(x) wedge cvec(v))/(cvec(u) wedge cvec(v))cvec(u) + (cvec(u) wedge cvec(x))/(cvec(u) wedge cvec(v))cvec(v) $
      ]

      #uncover("6-")[
        #note[$cvec(u) wedge cvec(x) = cvec(u)and cvec(p)$ #uncover("7-")[and $cvec(x) wedge cvec(v) = cvec(q) wedge cvec(v)$]]
      ]

      #uncover("8-")[
        $ cvec(x) = (cvec(q) wedge cvec(v))/(cvec(u) wedge cvec(v))cvec(u) + (cvec(u) wedge cvec(p))/(cvec(u) wedge cvec(v))cvec(v) $
      ]
    ])
  ]

  // [c, d, t] Blade-blade containment
  #slide[
    == Blade-blade containment
    #uncover("2-",definition[
      Blade $blade(B)_l$ *contains* $blade(A)_k$, denoted as $blade(A)_k subset.eq blade(B)_l$ if $V(blade(A)) lt.eq V(blade(B))$
    ])
    #uncover("3-",theorem([Blade-blade containment])[
      $ cvec(a)_1 wedge dots.c wedge cvec(a)_k subset.eq blade(B)_l quad arrow.double.l.r.long quad cvec(a)_i wedge blade(B)_l = 0, quad forall i = 1,dots,k $
    ])
    #uncover("4-", note[not the same as $blade(A) wedge blade(B) = 0$ (any $cvec(a)_i wedge blade(B) = 0$ implies that)])
  ]

  // [c, d, e]Basis and dimension
  #slide[
    == Exterior power basis and dimension
    #uncover("2-",definition[
      If ${cvec(e)_1,dots,cvec(e)_n}$ is a basis for $V$, then 
      #uncover("3-",$ {cvec(e)_(i_1) wedge dots.c wedge cvec(e)_(i_k) bar.v 1 lt.eq i_1 < dots.c < i_k lt.eq n } $)
      #uncover("4-",[is a *basis* for $and.big^(k)(V)$.])
      #uncover("5-",[The *dimension* of $and.big^(k)(V)$ is the binomial coefficient])
      #uncover("6-",[$ dim and.big^(k)(V) = binom(n,k) $])
    ])
    #uncover("7-",example[
      Let ${cvec(e)_1,cvec(e)_2,cvec(e)_3}$ be a basis for $V$. Then:
      #align(center)[
        #tablex(columns:5, auto-vlines: false, align:center+horizon,inset:.3em, auto-lines: false,
          [$k$],vlinex(),[0],[1],[2],[3],hlinex(),
        [basis for $and.big^(k)(V)$],
          uncover("11-",[{1}]),
          uncover("8-",[{$cvec(e)_1,cvec(e)_2,cvec(e)_3$}]),
          uncover("9-",[{$cvec(e)_1 wedge cvec(e)_2, cvec(e)_2 wedge cvec(e)_3, cvec(e)_3 wedge cvec(e)_1$}]),
          uncover("10-",[{$cvec(e)_1 wedge cvec(e)_2 wedge cvec(e)_3$}])
        )
      ]
    ])
  ]

  // [c, d] Basis for exterior algebra
  #slide[
    == Exterior algebra basis and dimension
    #definition[//
      If $B={cvec(e)_1,dots,cvec(e)_n}$ is a basis for $V$#uncover("2-")[, since $ext(V) = plus.big_(k=0)^infinity ext^(k)(V) $]#uncover("3-")[, then its *basis* is]
      #grid(columns: (.7fr, .3fr),[
        #uncover("4-")[$ union.big_(k=0)^infinity underbrace({cvec(e)_(i_1) wedge dots.c wedge cvec(e)_(i_k) bar.v 1 lt.eq i_1 < dots.c < i_k lt.eq n },B_k)
        // = union.big_(k=0)^infinity B_k $
        $]
        #uncover("5-")[where $B_k$ is the basis for $ext^(k)(V)$ generated from $B$.]
        #uncover("6-")[Its *dimension* is
        $ dim(ext(V))uncover("7-",= sum_k dim(ext^(k)(V)))uncover("8-",= sum_k binom(n, k) = 2^n) $]
      ],[
        #uncover("9-",align(center)[
          #v(-1em)
          #show par: set block(spacing: 0em, below: .5em, above: -.5em)
          #set par(leading: 0em)
          #text(myred, style: "italic", weight:"bold", "Pascal triangle")
          #tablex(columns:8, auto-vlines: false, align:center+horizon,inset:(x:.2em,y:.3em), auto-lines: false,
            hlinex(),[],colspanx(6)[grade $k$],$dim$,
            [$n$],vlinex(),[0],[1],[2],[3],[4],[5],vlinex(),$ext(V)$,hlinex(),
            0,1,colspanx(5)[],1,
            1,1,1,colspanx(4)[],2,
            2,1,2,1,colspanx(3)[],4,
            3,1,3,3,1,colspanx(2)[],8,
            4,1,4,6,4,1,[],16,
            5,1,5,10,10,5,1,32
          )
        ])
      ])
    ]
    #definition([Pseudoscalar])[
      *Pseudoscalar* $bl(I)_n in ext^(n)(V) = ee_1 wedge dots wedge ee_n$
    ]
  ]

  // [c, g] Interpretation of the relative weight
  #slide[
    == Interpretation of the relative weight
    #example[If 
      $cvec(a) = alpha cvec(e)_1$ and $cvec(b) = beta (cos (phi) cvec(e)_1 + sin (phi) cvec(e)_2), phi=angle cvec(a),cvec(b)$,
      then

      $ uncover("2-",cvec(a) wedge cvec(b) &= (alpha cvec(e)_1) wedge (beta (cos (phi) cvec(e)_1 + sin (phi) cvec(e)_2)))\
      uncover("3-", &= alpha beta sin(phi) cvec(e)_1 wedge cvec(e)_2) \
      uncover("4-", &= det(mat(delim:"[",alpha, beta cos(phi);0,beta sin(phi))) cvec(e)_1 wedge cvec(e)_2) $

      #uncover("5-",align(center,image("../gacs/figs/ext_alg_parallelogram.svg", width: 12cm)))
    ]
  ]

  // [c, l, t, e] Blade necessary and sufficient condition
  #slide[
    == Necessary and sufficient conditions for blades
    #show par: set block(spacing: 0em, below: .25em, above: .25em)
    #uncover("2-",lemma[
      $A eq.not 0 in and.big^(2)(V) "is a blade" quad arrow.r.long.l.double quad A wedge A = 0 in and.big^(4)(V) $
    ])
    #uncover("3-",example[ Is the bivector $A = 12 cvec(e)_1 wedge cvec(e)_3 + 8 cvec(e)_2 wedge cvec(e)_3$ a blade?
      #uncover("4-",$ A wedge A = & 12 dot.c 8 (cvec(e)_1 wedge cvec(e)_3 wedge cvec(e)_2 wedge cvec(e)_3) + 8 dot.c 12 (cvec(e)_2 wedge cvec(e)_3 wedge cvec(e)_1 wedge cvec(e)_3) \
        =& 0 $)
    ])
    #uncover("5-",example[ Is the bivector $B = cvec(e)_1 wedge cvec(e)_3 + cvec(e)_2 wedge cvec(e)_4$ a blade?
      #uncover("6-",$ B wedge B =&  cvec(e)_1 wedge cvec(e)_3 wedge cvec(e)_2 wedge cvec(e)_4 + cvec(e)_2 wedge cvec(e)_4 wedge cvec(e)_1 wedge cvec(e)_3 \
        =& 2 cvec(e)_1 wedge cvec(e)_3 wedge cvec(e)_2 wedge cvec(e)_4 eq.not 0 $)
    ])
    #uncover("7-",theorem[
      $A eq.not 0 in and.big^(k)(V) "is a blade" arrow.r.long.l.double A wedge A = 0 in and.big^(2k)(V) $
    ])
    #uncover("8-", theorem[
      $A eq.not 0 in and.big^(k)(V) "is a blade" arrow.r.long.l.double V(A) "is a" k"-dimensional subspace"$
    ])
  ]

  // [c,d,e,t] Grade projection operator
  #slide[
    == Grade projection operator

    #uncover("2-",definition[
      *Grade projection* selects the $k$-vector component of a multivector
      $ A=A_0+A_1+dots+A_n arrow.double.r angle.l A angle.r_k = A_k $
    ])
    #uncover("3-",example[
      $ gproj(text(myred,4)
             +text(myblue,cvec(e)_2+3cvec(e)_3)
             +text(mygreen,34cvec(e)_2 wedge cvec(e)_3 + 2cvec(e)_1 wedge cvec(e)_2),2)
        = text(mygreen,34cvec(e)_2 wedge cvec(e)_3 + 2cvec(e)_1 wedge cvec(e)_2)$
      // \textcolor{blue}{4}+\textcolor{red}{e_2+3e_3}+\textcolor{green!30!black}{34e_2\wedge e_3 + 2e_1\wedge e_2} \ran_2
      // \visible<3->{= \textcolor{green!30!black}{34e_2\wedge e_3 + 2e_1\wedge e_2}}
    ])
    #uncover("4-",theorem([Outer product as grade projections])[
      $ A and B = sum_(k=0)^(n)sum_(l=0)^(n) gproj(A,k) wedge gproj(B,l) $
    ])
    #uncover("5-",theorem([Grade raising from grade projection])[
      $ gproj(A wedge B,k) = sum_(i=0)^(k) gproj(A,i) wedge gproj(B, k-i) $
    ])
  ]

  // [c, d]
  #slide[
    == Even/Odd projection operators

    #uncover("2-", definition([Even projection])[Let $dim(V)=n$. The *even projection* of $A$ is
      $ eproj(A) = gproj(A,0) + gproj(A,2) + dots + gproj(A,2floor(n/2)) = sum_(k=0)^(floor(n/2)) gproj(A,2k) $
    ])
    #uncover("3-", definition([Odd projection])[Let $dim(V)=n$. The *odd projection* of $A$ is
      $ oproj(A) = gproj(A,1) + gproj(A,3) + dots + gproj(A,2ceil(n/2)-1) = sum_(k=0)^(ceil(n/2)-1) gproj(A,2k+1) $
    ])
  ]

  // [c, d, t, n] Blade reversion
  #slide[
    == Blade reversion
    #uncover("2-",definition[
      Blade *reversion* of $blade(A)=cvec(a)_1 wedge dots.c wedge cvec(a)_k$ $ rev(blade(A)) = cvec(a)_k wedge cvec(a)_(k-1) wedge dots.c wedge cvec(a)_1 $
    ])
    #uncover("3-",theorem([Reversion is just a multiple])[ for a blade $blade(A)_k$,
      $ rev(blade(A))_k = (-1)^(k(k-1)/2) blade(A)_k $
    ])
    #uncover("4-",note([Sign of blade reversion's weight w.r.t. original one])[
      #v(-.5em)
      #align(center,tablex(columns: 10,inset: .3em,auto-lines: false, [k],vlinex(),0,1,2,3,4,5,6,7,$dots$,hlinex(),[],[+],[+],[-],[-],[+],[+],[-],[-],$dots$))
      #v(-.7em)
    ])
  ]

  // [c, d, t] Multivector reversion
  #slide[
    == Multivector reversion
    #uncover("3-",definition([Multivector reversion])[ The *reversion* of a multivector $A$ is
      $ rev(A) = sum_k (-1)^(1/2k(k-1)) gproj(A,k) $
    ])
    #uncover("4-",corollary([Properties of blade reversion])[
      - $(rev(A))^revalone = A$
      - $(A wedge B)^revalone = rev(B) wedge rev(A)$
    ])
    #uncover("5-",theorem([Reversion commutes with grade projection])[
      $ gproj(A,k)^#move(dy:-.3em,dx:-.7em,revalone) = gproj(rev(A), k) $
    ])
  ]

  // [c, d, t] Grade involution
  #slide[
    == Grade involution

    #uncover("2-",definition[
      Blade *involution* swaps the parity of the grade
      $ invol(blade(A))_k = (-1)^k blade(A)_k $
    ])
    #uncover("3-",theorem([Multivector involution])[ The *involution* of a multivector $A$ is
      $ invol(A) = sum_k (-1)^(k) gproj(A,k) $ #v(-.3em)
    ])
    #uncover("4-",theorem([Properties of involution])[
      - $(A)^#move(dy:.1em,involalone)\ ^(#move(dx:-1.9em,dy:-.1em,involalone)) = invol(A)$
      - $(A wedge B)^involalone = invol(A) wedge invol(B)$
    ])
  ]

  // [c, t, d] Grade involution
  #slide[
    == Grade involution
    #uncover("2-",theorem([Involution and even/odd projections])[
      $ invol(A) = gproj(A,+) - gproj(A,-) $
    ])

    #uncover("3-",note([Sign of blade involution's weight w.r.t. original one])[
      #v(-.5em)
      #align(center,tablex(columns: 10,inset: .3em,auto-lines: false, [k],vlinex(),0,1,2,3,4,5,6,7,$dots$,hlinex(),[],[+],[-],[+],[-],[+],[-],[+],[-],$dots$))
      #v(-.7em)
    ])

    #uncover("4-",theorem([Reversion and involution commute])[
      $ rev(invol(A)) = invol(rev(A)) $
    ])

  ]

  // [c, d, t, n] Clifford conjugation
  #slide[
    == Clifford conjugation

    #uncover("2-",definition([Clifford conjugation])[
      *Clifford conjugation* of a multivector $A$:#v(-.5em)
      $ cconj(A) = rev(invol(A)) = invol(rev(A)) $
    ])
    #uncover("3-", theorem([Properties of Clifford conjugation])[
      - $cconj(cconj(A)) = A$
      - $cconj(gproj(A,k)) = gproj(cconj(A),k)$
      - $cconj(A wedge B) = cconj(B) wedge cconj(A)$
      - $cconj(A) = sum_(k=0)^n (-1)^(k(k+1)/2)gproj(A,k)$
    ])
    #uncover("4-",note([Reversion, Involution and Clifford conjugation for blades])[
      #v(-.5em)
      #align(center,tablex(columns: 3,inset: .4em,auto-lines: false, 
      [*Operation*],vlinex(),[*Relative Weight*],vlinex(),[*Pattern*],hlinex(),
      [Reversion],$(-1)^(k(k-1)\/2)$,$++--++--$,
      [Grade involution],$(-1)^k$,$+-+-+-+-$,
      [Clifford conjungation],$(-1)^(k(k+1)\/2)$,$+--++--+$))
      #v(-.7em)
    ])
  ]

  // Summary
  #slide[
    == Summary
    #set text(size:18pt)

    #align(center,grid(columns: 2, gutter: 1em, align: center, [
      #tablex(columns:2, auto-vlines: false, align:(left+horizon,center+horizon),inset:(x:.2em,y:.4em), auto-lines: false,hlinex(),
        [*Concept*],vlinex(),[*Comments*],hlinex(),
        [Scalar],[$alpha, beta$],
        [Scalar components of vectors],[$alpha_i, beta^i$],hlinex(),
        [Vectors],[$cvec(a), cvec(b)$],
        [Basis vectors (orthonormal)],[$cvec(e)_i$],
        [Basis vectors (non-orthonormal)],[$cvec(b)_i$],hlinex(),
        [General blade],[$blade(A), blade(B)$],
        [General blade of grade $k$],[$blade(A)_k, blade(B)_k$],
        [$k$-vector, not necessarily a blade],[$A_k, B_k$],
        [General multivector],[$A, B$],hlinex(),
        [(Unit pseudoscalars)],[$blade(I)_n, blade(I)$],hlinex(),
      )
    ], [
      #tablex(columns:2, auto-vlines: false, align:(left+horizon,center+horizon),inset:(x:.2em,y:.4em), auto-lines: false,hlinex(),
        [*Concept*],vlinex(),[*Comments*],hlinex(),
        [Vector spaces],[$V, RR^n$],
        [Exterior power],[$ext^(k)(V)$],
        [Exterior algebra],[$ext(V)$],
        [Subspace represented by a blade],[$V(blade(A))$],hlinex(),
        [Exterior product],vlinex(),[$cvec(a) wedge cvec(b)$],
        [Grade projection],[$gproj(A, k)$],
        [Reversion],[$rev(blade(A))$],
        [Grade involution],[$invol(blade(A))$],
        [Clifford conjugation],[$cconj(blade(A))$],hlinex()
      )
    ])
    )
    

    // #tablex(columns:2, auto-vlines: false, align:center+horizon,inset:(x:.2em,y:.4em), auto-lines: false,
    // [*Operator*],[*Notation*],hlinex(),
    // )
  ]

