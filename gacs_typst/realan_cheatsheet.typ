#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Real Analysis Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)
#show_thm_legend #v(-1em)
#set heading(numbering: "1.1.")
#show outline.entry.where(
  level: 1
): it => {
  v(8pt, weak: true)
  strong(it)
}
#text(size:8.2pt,outline(depth:2,indent:1em, fill:none))
