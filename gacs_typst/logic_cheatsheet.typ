#import "base.typ": *
#show: thmrules

#show: doc => conf(
  title: [
    Logic Cheatsheet $copyright$ 2024 CC BY 4.0.
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)
