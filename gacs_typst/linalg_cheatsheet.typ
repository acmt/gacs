#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Linear Algebra Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  noproof: true,
  doc,
)

#show_thm_legend #v(-0em)
#set heading(numbering: "1.1.")
#show outline.entry.where(
  level: 1
): it => {
  v(8pt, weak: true)
  strong(it)
}
#let proof(..args) = myproof(..args)
#block(inset:(right:.5em),text(size:8.2pt,outline(depth:2,indent:1em, fill:none)))

//#text(size:8.2pt,outline(depth:2,indent:1em, fill:none))
= Vector spaces
  == Vector spaces
    #definition[*Vector space* $(V, +, dot.c)$
      over field $FF$ ($FF$-*space*) of *scalars*:
            - $V$ is a set of *vectors* $cvec(v) in V$
            - *Vector addition* $+:V times V arrow.r V$ (abelian group):
                + #box(width:4.36cm)[_Associativity_:] $cvec(u)+(cvec(v)+cvec(w))=(cvec(u)+cvec(v))+cvec(w)$
                + #box(width:6.35cm)[_Commutativity_:] $cvec(u)+cvec(v)=cvec(v)+cvec(u)$
                + #box(width:4.375cm)[_Identity_:] $exists 0 in V: cvec(v)+0=cvec(v), forall cvec(v) in V$
                + #box(width:3.5cm)[_Reverse_:] $forall cvec(v) in V, exists - cvec(v) in V: cvec(v) + (-cvec(v)) = 0$
            - *Scalar multiplication* $dot.c: FF times V arrow.r V$:
              5. #box(width:6.25cm)[_Scalar/Field compatibility_:] $alpha(beta cvec(v))=(alpha beta)cvec(v)$
              6. #box(width:7.5cm)[_Identity_:] $1 cvec(v) = cvec(v)$
              7. #box(width:5.4cm)[_Distributivity w.r.t. vector addition_:] $alpha(cvec(u)+cvec(v))=alpha cvec(u) + alpha cvec(v)$
              8. #box(width:5.45cm)[_Distributivity w.r.t. field addition_:] $(alpha+beta)cvec(v) = alpha cvec(v) + beta cvec(v)$

        #v(.5em)
        Axioms 1-4: abelian group under addition, 5-8: $FF$-module.
      ]
    #figure(caption: [Illustration of some vector space properties: commutativity, associativity, identity, reverse, scalar compatibility])[
      #image("../gacs/figs/linalg_vector_space.svg")
    ]
    #definition[*Real (Complex) vector space*: $FF=RR (FF=CC) $]
    #definition[
      *Linear combination* of ${#h(-.1em)cvec(v)_i#h(-.1em)}_(i=0)^n#h(0em) in#h(0em) V$ with *coefficients* $alpha_i#h(0em) in#h(0.1em) FF $
      $ alpha_1 cvec(v)_1+alpha_2 cvec(v)_2+dots.c+alpha_n cvec(v)_n $
      ]
    #definition[*Trivial* linear combination: $forall i, alpha_i = 0$
      (*Nontrivial* otw)]
    === Examples of vector spaces
      #example[
        $FF^FF$: set of all functions $f:FF arrow FF$ $ (f+g)(x)=f(x)+g(x) quad "and" quad (alpha f)(x) = alpha (f(x)) $
        ]
      #example[
        $cal(M)_(m,n)(FF)$: set of all $m times n$ matrices over $FF$
        ]
      #example[
        $FF^n$: set of all ordered sequences $(a_1,dots,a_n)$: $ (a_1,dots,a_n) + (b_1, dots,b_n)=(a_1+b_1,dots,a_n+b_n) \ c(a_1,dots,a_n) = (c a_1,dots,c a_n) $
        ]
      #example[
        Set $"Seq"(FF)$ of all infinite sequences over $FF$ is a vector space: $ (s_n)+(t_n)=(s_n+t_n) quad alpha(s_n)=(alpha s_n). $
        Also, the set $c_0$ of all sequences of complex numbers that converge to 0 is a vector space, as is the set $ell^infinity$ of all bounded complex sequences. The set $ell^p$ ($p > 0$) of all complex sequences $(s_n)$ for which $sum_(n=1)^infinity |s_n|^p < infinity$ is a vector space.
        ]
  == Subspaces
    #definition[ *Subspaces*
      - #box(width:2.7cm)[*Subspace*:] $S subset.eq V "and" S "is a vector space" arrow.double.l.r.long S subspace V$
      - #box(width:2.7cm)[*Proper subspace*:] $S subspace V "and" S eq.not V arrow.double.l.r.long S < V$
      - #box(width:2.7cm)[*Zero subspace*:] ${0}$
      ]

    #theorem[
      $S subspace V arrow.double.l.r forall vv,ww in S,alpha,beta in FF: alpha vv+beta ww in S$
      ]
    #proof()["$arrow.double$: immediate. "$arrow.double.l$": Since $S subset.eq V$, only closure under $+$ and $dot.c$ should be checked: $forall vv,ww in S, alpha, beta in FF$ then
    $ alpha vv + beta ww in S arrow.double 1 dot.c vv + 1 dot.c ww in S "and" alpha dot.c vv + 0 dot.c ww in S. $#v(-1.3em)
    ]
    #example()[
      Let $V(n,2)$ be a vector space of all binary $n$-tuples. The *weight* $cal(W)(vv)$ of $vv in V(n,2)$ is the number of nonzero coordinates in $vv$.
      ]
    === The lattice of subspaces
      #definition[$cal(S)(V)$: set of all subspaces of $V$.]
      #theorem[
        $cal(S)(V)$ is a #text(myred)[_poset_] under set inclusion.
        - _Smallest element_: ${0}$
        - _Largest element_: $V$
        ]
      #proof()[#todo]
      #lemma[_Intersection is subspace_:
        $S,T h2 lt.eq h2 V arrow.double S sect T = "glb"{S,T} h2 lt.eq h2 V $.
        ]
      #proof()[#todo]
      #theorem[${S_i : i in K} arrow.double sect.big_(i in K) S_i 
        = "glb"{S_i : i in K}$
        ]
      #proof()[#todo]
      #theorem[A vector space is not the union of a finite number 
        of proper subspaces.]
      #proof()[Let $V=S_1 union dots union S_n$ where $S_1 subset.eq.not S_2 union dots union S_n$ and $S_i subspace V$. Let $ww in S_1 without (S_2 union dots union S_n), vv in.not S_1$. Consider the line through $vv$ parallel to $ww$, i.e., the infinite set $L={vv+ alpha ww : alpha in FF}$. Since $ww in S_1$, then $alpha ww + vv in S_1 "and" arrow.double vv in S_1$, a contradiction.
      Suppose $alpha_1 ww h1 + h1 vv in S_i$ and $alpha_2 ww h1 + h1 vv in S_i$, for $i h1 gt.eq h1 2$, where $alpha_1 h1 eq.not h1 alpha_2$. Then it is a contradiction because: $ (alpha_1 ww + v)-(alpha_2 ww + v)=(alpha_1 - alpha_2)ww in S_i arrow.double ww in S_i. $#v(-1.3em)]
      #definition[*Sum* of $S,T < V$: $S+T={s+t : s in S, t in T} $]
      #definition[*Sum* of ${S_i : i in K, S_i subspace V}$:
          $ sum_(i in K) S_i = {s_1 + dots + s_n : s_j in union.big_(i in K) S_i} $
        ]
      #theorem[If $forall i in K, S_i subspace V$, then $ sum_(i in k) S_i
        = "lub"{S_i : i in K} subspace V$
        ]
      #proof()[#todo]
      #theorem[
          $(cal(S)(V), subset.eq)$ is a complete lattice: minimum ${0}$, maximum $V$
          $ "glb"{S_i mid(|) i in K}=sect.big_(i in K) S_i wide "lub"{S_i mid(|) i in K}=sum_(i in K) S_i $
        ]
      #proof()[#todo]
  == Direct sums
    === External direct sums
      #definition[
        *External direct sum* of $V_1, dots, V_n$,
        $ V=V_1 plus.square dots.c plus.square V_n={(vv_1,dots,vv_n): vv_i in V, i=1,dots,n} $
        is a vector space under operations ($forall alpha in FF$)
        - $(uu_1,dots,uu_n)+(vv_1,dots,vv_n)=(uu_1+vv_1,dots,uu_n+vv_n)$
        - $alpha(vv_1, dots,vv_n)=(alpha vv_1, dots, alpha vv_n)$

      ]
      #example()[$FF^n=FF plus.square dots.c plus.square FF$]
      #definition[
        *Direct product* of $cal(F)={V_i:i in K}$: 
        $ product_(i in K) V_i = {f: K arrow union.big_(i in K) V_i: f(i) in V_i} $
      ]
      #definition[
        *Support* of $f: K arrow union_i V_i$: $"supp"(f)={i in K: f(i) eq.not 0}$
      ]
      #definition[
        *External direct sum* of $cal(F)={V_i:i in K}$: 
        $ plus.circle.big_(i in K)^"ext" V_i = {f: K arrow union.big_(i in K) V_i: f(i) in V_i, f "has finite support"} $
      ]
      #definition[
        $V^K h1 = h1 {f h1:K arrow V} quad (V^K)_0 h1 =h1 {f h1 in h1  V^K h1: |"supp"(f)|<infinity }$
      ]
      #note[#v(-1.4em)$ product_(i in K) V = V^K wide plus.circle.big_(i in K)^("ext") V = (V^K)_0 $]
    === Internal direct sums
      #definition[
        (*Internal*) *direct sum* of a family $cal(F) = {S_i: i in I}$ of *direct summands* $S_i$ is a vector space $ V=plus.circle.big cal(F) =plus.circle.big_(i in I) S_i $ which satisfies the following properties:
        + #box(width:4cm)[(_Join of the family_):] $V = sum_(i in I) S_i$
        + #box(width:4cm)[(_Independence of the family_)] $forall i in I: S_i sect (sum_(j eq.not i) S_j) = {0}$
        If $cal(F)={S_1,dots,S_n}$ is a finite family, then $V=S_1 plus.circle dots.c plus.circle S_n$. If $V = S plus.circle T$, then $T$ is a *complement* of $S$ in $V$.
      ]
      #theorem[
        $S subspace V arrow.double exists T subspace V: V = S plus.circle T$
      ]
      #proof()[#todo]
      #theorem[The following are equivalent for $cal(F)={S_i subspace V: i in I, S_i eq.not S_j "for" i eq.not j}$
        + (_Independence of the family_) $forall i in I: S_i sect (sum_(j eq.not i) S_j) = {0}$
        + (_Uniqueness of expression for 0_) $forall vv_i eq.not 0 in S_i: sum_i vv_i eq.not 0 $
        + (_Uniqueness of expression_) $forall vv h1 in h1 V,  exists ! {cvec(s)_i h1 in h2 S_i}h1: vv=cvec(s)_1 + dots.c + cvec(s)_n $
        Hence, $sum_(i in I) S_i$ is direct iff any of 1.-3. works.
      ]
      #proof()[#todo]
      #definition[$"Sym" lt.eq cal(M)_n$: set of all symmetric $n times n$ matrices.]
      #definition()[$"Skew" lt.eq cal(M)_n$: set of all skew-symmetric $n times n$ matrices.]
      #example()[
        $A h1 = h1 1/2(A h2 + h2 A^top)h1 + h1 1/2(A h2 - h2 A^top) h2 = h2 B h2 + h2 C, B  h2 in h2  "Sym", C  h2 in h2  "Skew" $

        $ cal(M)_n = "Sym" + "Skew" $
        If $S,S' in "Sym", T,T' in "Skew"$ and $S+T=S'+T'$, then 
        $ U h1 = h1 S h1 - h1 S' h1 = h1 T' h1 - h1 T h1  in h1  "Sym" sect "Skew" h1 h2 arrow.double h2 h2 U h1 = h2 0 h2 h1 arrow.double h2 h1  S h1 = h1 S',T h1 = h1 T' $
        Thus,
        $cal(M)_n = "Sym" plus.circle "Skew" $.
      ]
  == Spanning sets and linear independence

    #definition[
      *Span*: $span(S) = {alpha_1 cvec(v)_1+dots.c+alpha_n cvec(v)_n: alpha_i in K, cvec(v)_i in S} $
      ]

    === Linear independence

    #definition[
      *Linear independent* (*LI*) set ${cvec(v)_1,dots,cvec(v)_n} subset V$ satisfies:
      $ alpha_1 cvec(v)_1 + dots.c + alpha_n cvec(v)_n = 0 arrow.double.l.r.long alpha_1=dots.c=alpha_n=0 $
      #align(center)[Not LI $arrow.double.r.long$ *linearly dependent*]
    ]
    #note[
      $S$ is LI $arrow.double 0 in.not S $
    ]
    #definition[
      $0 eq.not vv in V$ is an *essentially unique* linear combination in $S subset V$ means $ exists ! {cv(s)_1, dots, cv(s)_n} subset S, cv(s)_i eq.not cv(s)_j, alpha_i in FF: vv=alpha_1 cv(s)_1 + dots.c + alpha_n cv(s)_n $
      i.e., $ #h(-.1em) vv h1 in h1 span(S)$ and if $vv h1 = h1 sum_(i=1)^n h0 alpha_i cv(s)_i h1 = h1 sum_(i=1)^m h0 beta_i cv(t)_i$ where $alpha_i  ,h1 beta_i h1 eq.not h1 0$, $cv(t)_i eq.not cv(t)_j, cv(s)_i eq.not cv(s)_j$, then $m h1 = h1 n$, and after reordering $b_i t_i$'s, we have $alpha_i h1 = h1 beta_i$ and $cv(s)_i h1= h1 cv(t)_i, forall i=1,dots,n.$ (stronger than $alpha_i cv(s)_i h1 = h1 beta_i cv(t)_i$)
    ]
    #theorem[The following are equivalent for ${0} eq.not S subset V$:
      + $S$ is LI
      + Every $vv eq.not 0 in span(S)$ is an essentially unique linear combination of vectors in $S$.
      + No vector in $S$ is a linear combination of other vectors in $S$
    ]
    #proof()[#todo]
    #theorem[
      The following statements are equivalent for $S subset V$:
      - $S$ is LI and spans $V$.
      - Every $vv eq.not 0 in V$ is an essentially unique linear combination of vectors in $S$
      //- $forall cvec(v) in V, exists! cvec(v)_1,dots,cvec(v)_n in S, alpha_1,dots,alpha_n in FF:$
      // $ vv = alpha_1 cvec(v)_1+dots.c+ alpha_n cvec(v)_n $
      - $S$ is a minimal spanning set.
      - $S$ is a maximal linearly independent set.
    ] <t:basis>
    #proof()[#todo]
    === Basis
    #definition[
      $S$ is a *basis* for $V$ ($S lt.dot V$) if it satisfies one (and hence all) conditions on @t:basis
    ]
    #theorem[
      $S h1 = h1 {vv_1,h1  dots, vv_n} h1 lt.dot h1 V arrow.double.l.r V h0 = h1 span(vv_1) h1  plus.circle h1  dots.c  h1  plus.circle  h1 span(vv_n)$.
    ]
    #proof()[#todo]
    #theorem[Let $I subset V$ be LI in $V$ and $I subset.eq S subset V$ s.t. $span(S) = V$. Then $exists B lt.dot V$ for which $I subset.eq B subset.eq S$. In particular,
      + Any vector space, except ${0}$, has a basis.
      + Any LI set in $V$ is contained in a basis.
      + Any spanning set in $V$ contains a basis.
    ]
    #proof()[#todo]

  == The dimension of a vector space

    #theorem()[
      ${vv_1,dots,vv_n} subset V$ are LI and $span({cv(s)_1, dots,cv(s)_m})=V arrow.double n lt.eq m.$
    ]
    #proof()[#todo]
    #corollary()[
      $V$ has a finite spanning set $arrow.double (B_1 #h(-.05cm),h0 B_2 h1 lt.dot h1 V h1 arrow.double h1 |B_1|h1=h1 |B_2|)$.
    ]
    #proof()[#todo]
    #theorem()[
      #box(width: 3.87cm)[$V$ is a vector space] $arrow.double (B_1 #h(-.05cm),h0 B_2 h1 lt.dot h1 V h1 arrow.double h1 |B_1|h1=h1 |B_2|)$.
    ]
    #proof()[#todo]
    #definition[ *Dimension of a vector space*
      - *Dimension*: cardinality $kappa$ of any basis for $V$. If $dim V = kappa$, then $V$ is $kappa$-*dimensional*.
      - $dim {0} = 0$
      - *Finite dimensional*: ${0}$ or has a finite basis
      - *Infinite dimensional*: not finite.
    ]

    #theorem[
      Let $V$ be a vector space
      + If $cal(B)$ is a basis for V and if $cal(B)=cal(B)_1 union cal(B)_2$ and $cal(B)_1 sect cal(B)_2 = emptyset$, then
      $ V=span(cal(B)_1) plus.circle span(cal(B)_2) $
      + Let $V=S plus.circle T$. If $cal(B)_1$ is a basis for $S$ and $cal(B)_2$ is a basis for $T$, then $cal(B)_1 sect cal(B)_2 = emptyset$ and $B = cal(B)_2 union cal(B)_2$ is a bssis for $V$.
    ]
    #proof()[#todo]
    #theorem()[Let $S,T subspace V$. Then $dim(S) + dim(T) = dim(S+T)+dim(S sect T)$. In particular, $dim(S plus.circle T) = dim(S)+dim(T)$]<t:sum_dim>
    #proof()[#todo]
  == Ordered bases and coordinate matrices

    #definition[*Ordered basis* for $V$ ($dim V h1 = h1 n$): ordered $n$-tuple $(vv_1,dots,vv_n), vv_i in V$, for which the set ${vv_1,dots,vv_n}$ is a basis for $V$.]

    #definition()[*Coordinate map* $phi.alt_cal(B)h0 :h1 V h1 arrow h1 FF^n$ for ordered basis $(vv_1 h0 ,h0 dots,h0 vv_n)$
    $ vv=alpha_1 vv_1 + dots.c + alpha_n vv_n quad arrow.double quad  phi.alt_cal(B)(vv)=mtx(b)_(cal(B)) = mat(delim:"[", alpha_1,dots,alpha_n)^top $ 
    where $mtx(vv)_cal(B)$ is the *coordinate matrix* of $vv$ w.r.t. $cal(B)$]
    #note[
      $phi.alt_(cal(B))(alpha_1 vv_1 + dots.c + alpha_n vv_n)= alpha_1 phi.alt_(cal(B))(vv_1) + dots.c + alpha_n phi.alt_(cal(B))(vv_n)$
    ]
  == The row and column spaces of a matrix

    #definition()[Let $A$ be a $m times n$ matrix over $FF$
    - #box(width:3cm)[*Row space* of $A$] ($"rs"(A) < FF^n$): span of rows of $A$.
    - #box(width:3cm)[*Column space* of $A$] ($"cs"(A) < FF^m$): span of columns of $A$.
    - #box(width:3cm)[*Row rank*:] $"rrk"(A) = dim "rs"(A)$
    - #box(width:3cm)[*Column rank*:] $"crk"(A) = dim "cs"(A)$ 
    ]
    #lemma()[Let $A in cal(M)_(m,n)$. Then elementary column (resp., row) operations do not affect $"rrk"(A)$ (resp., $"crk"(A)$).]
    #proof()[#todo]
    #theorem()[If $A in cal(M)_(m,n)$, then $"rrk"(A)="crk"(A)$]
    #proof()[#todo]
    #definition()[*Rank* of $A$: $"rk"(A)="rrk"(A)="crk"(A)$]


    === The complexification of a real vector space

      #definition()[The set $V^CC=V times V$, with addition 
      $ (uu,vv)+(xx,yy)=(uu+xx,vv+yy) $ and scalar multiplication over $CC$ (for $alpha,beta in RR$)
      $ (alpha+beta i)(uu,vv)=(alpha uu - beta vv, alpha vv + beta uu) $ is a complex vector space called the *complexification* of $V$.]
      #note[_Notation for $V^CC$_: $V^CC={uu+vv i: uu,vv in V}$ with
      - #box(width: 2cm)[Addition:] $(uu + vv i)+(xx + yy i) = (uu + xx) + (vv + yy)i $ 
      - #box(width: 2cm)[Scalar mult.:] $(alpha + beta i)(uu + vv i) = (alpha uu - beta vv) + (alpha vv + beta uu)i $
      - $zz=uu+vv i arrow$ *real part*$:uu, $ *imaginary part*: $vv$.
      - $zz=0 arrow.double.r.l uu,vv=0$
      ]

      #definition()[*Complexification map* $"cpx": V arrow V^CC$: $"cpx"(vv)=vv+0 i$.]
      #note[It's a group homomorphism: $ "cpx"(0)=0+0i quad "cpx"(uu plus.minus vv = "cpx"(uu)plus.minus "cpx"(vv)) $ It is also injective: $"cpx"(uu)="cpx"(vv) arrow.double.l.r uu = vv$\
      It also preserves multiplication by real scalars (for $alpha in RR$)
      $ "cpx"(alpha uu) = alpha uu + 0i = alpha(uu + 0i) = alpha "cpx"(uu) $]

    === The dimension of $V^(CC)$

    #theorem()[If $cal(B)={vv_j: j in I}$ is a basis for $V$ over $RR$, then the *complexification* of $cal(B)$, $"cpx"(cal(B)){vv_j + 0i: vv_j in cal(B)}$ is a basis for $V^CC$ over $CC$. Hence, $dim(V^CC)=dim(V)$ ]
    #proof()[#todo]
= Linear Transformations
  #definition[
    #let boxwidth=3.5cm
      *Linear transformation*: function $tau:V arrow.r W$ where:
      $ tau[alpha xx +beta yy] = alpha tau[xx]+  beta tau[yy], quad forall alpha, beta in FF,xx,yy in V  $
      $cal(L)(V, W)$ is the set of all linear transformations $tau:V arrow.r W$.
        - #box(width: boxwidth)[*Linear operator*:] $tau:V arrow.r V$ ($cal(L)(V)=cal(L)(V,V)$)
          - #box(width: boxwidth)[*Real operator*:] $tau$ linear operator and $FF=RR$
          - #box(width: boxwidth)[*Complex operator*:] $tau$ linear operator and $FF = C $
        - #box(width: boxwidth)[*Linear functional*:] $tau: V arrow.r FF$.
        - #box(width: boxwidth)[*Dual space*:] $V^ast$: set of all linear functionals on $V$.
    ]
  #definition[Other terms for linear transformations
    #let boxwidth=3.5cm
    - #box(width: boxwidth)[*Homomorphism*:] for linear transformation
    - #box(width: boxwidth)[*Endomorphism*:] for linear operator
    - #box(width: boxwidth)[*Monomorphism*:] for injective linear transformation
    - #box(width: boxwidth)[*Epimorphism*:] for surjective linear transformation
    - #box(width: boxwidth)[*Isomorphism*:] for bijective linear transformation
    - #box(width: boxwidth)[*Automorphism*:] for bijective linear operator
    ]
  #theorem()[
    + $cal(L)(V,W)$ is a vector space under $tau+sigma$ and $alpha tau$ 
    + $sigma in cal(L)(U,V), tau in cal(L)(V,W) arrow.double tau sigma in cal(L)(U,W)$
    + $tau in cal(L)(V,W)$ bijective $arrow.double tau^(-1) in cal(L)(W,V)$
    + $cal(L)(V)$ is an algebra under function composition. Identity and zero maps $iota, 0 in cal(L)(V)$ are the multiplicative and additive identities, resp.
    ]
  #proof()[#todo]
  #theorem()[If $cal(B) h1 =h1 {vv_i h0: i h1 in h1 I}$ is a basis for $V$, we can define $tau in cal(L)(V,W)$ by only specifying $tau vv_i, forall vv_i in cal(B)$ and extending $tau$ to $V$ by linearity:
    $ tau(alpha_1 vv_1 + dots.c + alpha_n vv_n) = alpha_1 tau vv_1 + dots.c + alpha_n tau vv_n $
  ]
  #proof()[#todo]
  #note[$tau in cal(L)(V,W) and S < V arrow.double tau|_S in cal(L)(S,W)$]

  == The kernel and image of a linear transformation

    #definition()[Let $tau in cal(L)(V,W)$
      - *Kernel* and *image* of $tau$:
      $ ker(tau)={vv in V: tau vv = 0} wide im(tau)={tau vv: v in V} $
      - *Nullity* and *rank* of $tau$:
      $ "null"(tau) = dim ker (tau) wide "rk"(tau)= dim im(tau) $
    ]
    #theorem()[ Let $tau in cal(L)(V,W)$. Then
      + #box(width:1.7cm)[$tau$ surjective] $arrow.l.r.double im(tau)=W$
      + #box(width:1.7cm)[$tau$ injective] $arrow.l.r.double ker(tau)={0}$
    ]
    #proof()[#todo]

  == Isomorphisms

    #definition()[
      If an *isomorphism* (bijective $tau in cal(L)(V,W)$) exists, then $V,W$ are *isomorphic*, written $V isom W$.
    ]

    #theorem()[
      Let $tau in cal(L)(V,W)$ be an isomorphism. Let $S subset.eq V$. Then
      + $V=span(S) arrow.l.r.double W=span(tau S)$
      + $S$ is LI in $V arrow.double.l.r tau S$ is LI in $W$
      + $S$ is a basis for $V arrow.double.r.l tau S$ is a basis for $W$.
    ]
    #proof()[#todo]

    #theorem()[
      $tau in cal(L)(V,W)$ is an isomorphism iff there is a basis $cal(B)$ for $V$ s.t. $tau cal(B)$ is a basis for $W$.
    ]
    #proof()[#todo]

    #theorem()[$V isom W arrow.double.l.r dim V = dim W$]
    #proof()[#todo]
    #definition()[$(FF^B)_0={(f:B arrow F):"supp"(f)< infinity}$]
    #definition()[Define $(delta_(ww):B arrow F) in (FF^B)_0$ as $delta_(ww)(vv)=delta_(ww = vv). $]
    #theorem[${delta_(ww) in (FF^B)_0:ww in B} lt.dot (FF^B)_0$.]
    #proof()[#todo]
    #theorem()[If $dim V(FF) = n in NN$, then $V isom FF^n $. In general, if $|B|=kappa$ and $dim V(FF)=kappa$, then $V isom (FF^B)_0$ ]<t:space_sequence_isomorphic>
    #proof()[#todo]

  == The rank plus nullity theorem

    #theorem()[ Let $tau in cal(L)(V, W)$
      + $ker(tau)^c isom im(tau)$
      + *The rank plus nullity theorem*
      $ dim ker(tau)) + dim im(tau) = dim V $
      $ "rk"(tau) + "null"(tau) = dim V $
      ]<t:rank_nullity_thm>
    #proof()[#todo]
    #corollary()[
      Let $dim V = dim W < infinity$. Then $tau in cal(L)(V,W)$ is injective iff it is surjective.
    ]<t:same_dim_injective_surjective>
    #proof()[#todo]
  == Linear transformations from $FF^n$ to $FF^m$

    #theorem()[
      + $A in cal(M)_(m,n(FF)) arrow.double tau_A = A vv in cal(L)(FF^n, FF^m)$
      + $tau in cal(L)(FF^n, FF^m) arrow.double tau = tau_A$, where $A=[tau e_1 | dots.c | tau e_n]$
      ] <t:matrix_lin_transf>
      #proof()[#todo]
    #definition()[The matrix $A$ in @t:matrix_lin_transf is called the *matrix* of $tau$.]

    #theorem()[Let $A in cal(M)_(m,n)(FF)$
      + $tau_A: FF^n arrow FF^m$ is injective iff $"rk"(A) = n$
      + $tau_A: FF^n arrow FF^m$ is surjective iff $"rk"(A) = m$
      ]
      #proof()[#todo]
  == Change of basis matrices

    #definition()[
      *Change of basis/coordinates operator*: $phi.alt_(cal(C),cal(B))=phi.alt_(cal(C))phi.alt_(cal(B))^(-1)$ for ordered basis $cal(C), cal(B)$ of $V$.
    ]
    #theorem()[If $cal(B)=(bb_1,h0 dots,bb_n)$ and $cal(C)$ are ordered bases for $V$, then $phi.alt_(cal(C),cal(B))$ is an automorphism for $FF^n$, with matrix $M_(cal(C),cal(B)) h1= h1 ([bb_1]_(cal(C))|dots.c|[bb_n]_cal(C))$. Hence $[vv]_cal(C)=M_(cal(C),cal(B))[vv]_cal(B)$ and $M_(cal(B),cal(C))=M_(cal(C),cal(B))^(-1)$]
    #proof[#todo]
    #theorem()[If $A=M_(cal(C),cal(B))$, then one unknown ($A$, $cal(B)$ or $cal(C)$) can be uniquely determined from other two.
      ]
    #proof[#todo]

  == The matrix of a linear transformation

    #theorem()[ If $tau in cal(L)(V,W)$ and $cal(B),cal(C)$ are ordered bases for $V,W$, then:
      $ [tau vv]_cal(C)=[tau]_(cal(C),cal(B))[vv]_cal(B) wide [tau]_(cal(C),cal(B))=([tau bb_1]_cal(C)|dots.c|[tau bb_n]_cal(C)) $
      If $V=W$ and $cal(B)=cal(C)$, then $[tau]_(cal(C),cal(B))=[tau]_cal(B)$ and $[tau vv]_cal(B)=[tau]_cal(B)[vv]_cal(B).$
    ]
    #proof[#todo]
    #definition()[*Matrix* of $tau$ *with respect to bases* $cal(B)$ and $cal(C)$: $[tau]_(cal(C),cal(B))$]

    #theorem()[
      let $V,W$ be finite-dimensional vector spaces over $FF$, with ordered bases $cal(B)=(bb_1,dots,bb_n)$ and $cal(C)=(cc_1,dots,cc_m)$, respectively.
      + The map $mu:cal(L)(V,W)arrow cal(M)_(m,n)(FF)$ defined by $mu(tau)=[tau]_(cal(C),cal(B))$ is an isomorphism and so $cal(L)(V,W) isom cal(M)_(m,n)(FF)$. Hence $ dim cal(L)(V,W)=dim cal(M)_(m,n)(FF)=m n $
      + If $sigma in cal(L)(U,V)$ and $tau in cal(L)(V,W)$ and if $cal(B), cal(C)$ and $cal(D)$ are ordered bases for $U, V$, and $W$, resp., then $[tau sigma]_(cal(D), cal(B))=[tau]_(cal(D),cal(C))[sigma]_(cal(C), cal(B))$
    ] <t:matrix_of_product>
    #proof[#todo]
    #note[@t:matrix_of_product (2) is the main reason for definition of matrix multip.]

  == Change of bases for linear transformations

    #theorem()[
      Let $tau in cal(L)(V,W)$ and let $(cal(B), cal(C)), (cal(B)', cal(C)')$ be pairs of ordered bases of $V,W$, resp. Then, $[tau]_(cal(C)', cal(B)')=M_(cal(C'),cal(C))[tau]_(cal(C), cal(B))M_(cal(B), cal(B)')$.
    ]
    #proof[#todo]
    #corollary()[
      If $tau in cal(L)(V,W)$ and $cal(B),cal(C) $ are ordered bases for $V$, then 
      $ [tau]_cal(C) = M_(cal(C),cal(B))[tau]_cal(B) M_(cal(C),cal(B))^(-1) $
    ]
    #proof[#todo]

  == Equivalence of matrices


    #definition()[Two matrices $A,B$ are *equivalent* if there exist invertible matrices $P, Q$ s.t. $B=P A Q^(-1)$]

    #theorem()[
      $A,B in cal(M)_(m,n)$ are equivalent iff they represent the same $tau in cal(L)(V,W) $ but possibly w.r.t different ordered bases. In this case, $A,B$ represent the same $S subset cal(L)(V,W)$.
    ]
    #proof[#todo]


  == Similarity of matrices


    #definition()[
      Two matrices $A,B$ are *similar*, $A tilde B$, if there exists an invertible matrix $P$ s.t. $B = P A P^(-1)$. *Similarity classes* are equivalence classes associated with similarity.
    ]
    #theorem()[$A,B in cal(M)_n$ are similar iff they represent the same $tau h1 in h1 cal(L)(V)$ but possibly w.r.t different ordered bases. In this case, $A,B$ represent the same $S subset cal(L)(V)$.]
    #proof[#todo]
  == Similarity of operators

    #definition()[$tau, sigma in cal(L)(V)$ are *similar*, $tau tilde sigma$, if there exists an automorphism $phi.alt in cal(L)(V)$ s.t. $sigma=phi.alt tau phi.alt^(-1)$. *Similarity classes* are equivalence classes associated with similarity.]

    #theorem()[
      $tau, sigma in cal(L)(V)$ are similar iff there is a matrix $A$ that represents both operators, but w.r.t. possibly different ordered bases. In this case, $tau,sigma$ are represented by the same $S subset cal(M)_n$.
    ]
    #proof[#todo]


  == Invariant subspaces and reducing pairs
    #definition()[$S subset V$ is *invariant under* $tau in cal(L)(V)$ or $tau$-*invariant* if $tau S subset.eq S$, i.e., if $tau|_S in cal(L)(S)$.]

    #definition()[$(S, h1 T)$ *reduces* $tau h1 in h1 cal(L)(V)$ if $V h1= h1 S h1 plus.circle h1 T$ and $S,T$ are $tau$-invar.]

    #definition()[If $(S,T)$ reduces $tau$, then $tau=tau|_S plus.circle tau|_T$ is the *direct sum* of $tau|_S, tau|_T$.]



  == Projection operators
    #definition()[Let $V=S plus.circle T$. Then $rho_(S,T):V h1 arrow h1 V$, where $rho_(S,T)(cv(s)+cv(t))=cv(s)$ for $cv(s) in S, cv(t) in T$ is called *projection* onto $S$ *along* $T$.]

    #theorem()[Let $rho in cal(L)(V)$
      + $V=S plus.circle T arrow.double rho_(S,T)+rho_(T,S)=iota$
      + $rho=rho_(S,T) arrow.double im(rho)=S, ker(rho)=T$ and $V=im(rho) plus.circle ker(rho)$
      + $vv in im(rho) arrow.double.l.r rho vv = vv$
      + If $sigma in cal(L)(V)$, $V=im(sigma)plus.circle ker(sigma)$ and $sigma|_(im(sigma))=iota$, then $sigma$ is projection onto $im(sigma)$ along $ker(sigma)$.
    ]
    #proof[#todo]
    #definition()[$tau in cal(L)(V)$ is *idempotent* if $tau^2 = tau$]
    #theorem()[$tau in cal(L)(V)$ is a projection iff it is idempotent.]
    #proof[#todo]
    === Projections and Invariance

      #theorem()[
        $S < V$ is $tau$-invariant iff $exists rho=rho_(S,T): rho tau rho = tau rho$
      ]
      #proof[#todo]

      #theorem()[$(S,T)$ reduces $tau in cal(L)(V)$ iff $tau$ commutes with $rho_(S,T)$.]
      #proof[#todo]

    === Orthogonal projections and resolutions of the identity

      #definition()[Two projections $rho, sigma in cal(L)(V)$ are *orthogonal*, $rho perp sigma$, if $rho sigma = sigma rho = 0$]
      #note[$rho perp sigma arrow.double.l.r im(rho) subset.eq ker(sigma) and im(sigma) subset.eq ker(rho)$]

      #definition()[A *resolution of the identity* on V is $rho_1+dots.c+rho_k=iota$, where $rho_i perp rho_j$ for $i eq.not j$.]

      #theorem()[
        + If $sum_i^k rho_i=iota$ is a resolution of the identity, then $V=sum_i^k im(rho_i)$ and $rho_i$ is projection onto $im(rho_i)$ along $ker(rho_i)=plus.circle.big_(j eq.not 1) im(rho_j)$
        + Conversely, if $V=plus.circle.big_j^k S_i$ and if $rho_i$ is projection onto $S_i$ along $plus.circle.big_(j eq.not i) S_j$, then $sum_i^k rho_i=iota$ is a resolution of the identity.
      ]
      #proof[#todo]

    === The algebra of projections

      #theorem()[Let $rho, sigma$ be projections on $V(FF)$, with $"char"(FF) eq.not 2$.
        + $rho + sigma$ is a projection iff $rho perp sigma$, where, $ im(rho+sigma)=im(rho) plus.circle im(sigma) wide ker(rho+sigma)=ker(rho) sect ker(sigma) $
        + $rho - sigma$ is a projection iff $rho sigma = sigma rho = sigma$, where, 
        $ im(rho - sigma) = im(rho) sect ker(sigma) wide ker(rho-sigma)=ker(rho) plus.circle im(sigma) $
        + $rho sigma$ is a projection if $rho sigma = sigma rho$, where
        $ im(rho sigma) = im(rho) sect im(sigma) wide ker(rho sigma)=ker(rho) + ker(sigma) $
      ]
      #proof[#todo]


  == Topological vector spaces

    === The definition
      #definition()[
        $(V,cal(T))$, where $cal(T)$ is a topology on vector space $V$, is a *topological vector space* if addition $A(vv,ww)=vv+ww$ and scalar multiplication $M(alpha,v)=alpha vv$ are continuous functions.
      ]

    === The standard topology on $RR^n$

      #definition()[
        $RR^n$ is a topological vector space under the *standard topology*, with the set of *open rectangles*
        $cal(B)={I_1 times dots times I_n: I_i$'s are open intervals in $RR}$ as a base of the topology, which is also induced by the Euclidean metric on $RR^n$.
      ]

    === The natural topology on $V$

      #definition()[
        The *natural topology* $cal(T)$ on the real vector space $V$ of dimension $n$ is a topology where all linear functionals are continuous.
      ]
      #theorem()[For each real vector space $V$ of dimension $n$, there is a unique natural topology determined by the fact that the coordinate map $phi.alt:V arrow RR^n$ is a homeomorphism, where $RR^n$ has the standard topology induced by the Euclidean metric.]
      #proof[#todo]


  == Linear operators on $V^(CC)$

    #definition()[*Complexification* of $tau$: $tau^(CC)(uu + vv i) = tau(uu) + tau(vv)i$]

    #theorem()[If $tau, sigma in cal(L)(V)$, then
    + $(alpha tau)^CC = alpha tau^CC, alpha in RR$
    + $(tau + sigma)^CC = tau^CC + sigma^CC$
    + $(tau sigma)^CC = tau^CC sigma^CC$
    + $[tau vv]^CC= tau^CC(vv^CC)$
    ]
    #proof[#todo]

    #theorem()[Let $tau in cal(L)(V(RR))$ and $basis(cal(B)) = V$. Then $[tau^CC]_("cpx"(cal(B)))=[tau]_(cal(B))$]
    #proof[#todo]


= The isomorphism theorems
  == Quotient spaces

#definition()[
  Let $S < V$ and $v h1  in h1  V$. Then $[vv]=vv h1 + h1 S = {vv+s h0 :s in S}$ is called a *coset* of $S$ in $V$, and $vv$ is a *coset representative* for $vv h1 + h1 S$.
]
#theorem[
  $[xx] = [vv] arrow.double.r.l xx in [vv]$
]
#proof[#todo]
#theorem()[Let $S < V$. The binary relation $uu equiv vv arrow.double.l.r uu-vv in S$ is an equivalence relation on $V$, whose equiv. classes are $[vv]h0 =h0 vv h0 +h0 S$.]
#proof[#todo]

#definition()[Let $S < V$. The set $V\/S = {vv+S:vv in V}$ is the *quotient space* of $V$ modulo $S$. The *zero* vector in $V\/S$ is $[0]=0+S=S$.]
#theorem()[$V\/S$ is a vector space under well-defined operations 
$ alpha(uu+S)=alpha uu +S "and" (uu + S)+(vv+S)=(uu+vv)+S $]
#proof[#todo]


=== The natural projection and the correspondence theorem
#definition()[*Canonical* (or *natural*) *projection* of $V$ onto $V\/S$: 
$ pi_S:V arrow V h0 \/S quad pi_(S)(vv)=vv+S=[vv] $]

#theorem()[
  $pi_S in epi(V, V\/S)$ and $ker(pi_S)=S$.
]<t:canonical_proj_quot_space_epi>
#proof()[
  Linearity ($pi=pi_S$):
  #set par(leading: .3em)
  $ pi(alpha uu h0 + h0 beta vv)& h1= h1(alpha uu h0 + h0 beta vv) h0 + h0 S h1 = h1 alpha(uu h0 + h0 S) h0 + h0 beta(vv h0 + h0 S) h1= h1 alpha pi(uu) h0 + h0 beta pi(vv) $
  Clearly $im(pi_S)=V\/S$ so it is surjective (an epimorphism). Also,
  $ vv  h1 in h1  ker(pi) arrow.double.l.r pi(vv) h1 = h1 0_(V\/S) arrow.double.l.r vv h1 + h1 S  h1 = h1  S  arrow.double.l.r vv  h1 in  h1 S  arrow.double.l.r  ker(pi) h1 = h1 S. $#v(-1.4em)
  ]
#theorem([_Correspondence theorem_])[
  Let $S h1 lt.eq h1 V$. Then if $f(T)h1=h1 T\/S$,$forall S lt.eq T subspace V$, where $T\/S subspace V\/S$, then $f$ is an order-preserving (w.r.t. $subset.eq$), one-to-one correspondence between ${T:S lt.eq T subspace V}$ and ${U:U subspace V\/S}$
]
#proof()[Let $U subspace V$, $X={uu h1 + h1 S : uu in U}$, and $T h1 = h1 union.big_(uu in U) (uu h1+h1 S)$. Then $xx,yy in T arrow.double xx+S,yy+S in X$. As $X subspace V \/ S $, then $ alpha xx+S, (xx+yy)+S in X arrow.double alpha xx, xx+yy in T arrow.double S lt.eq T subspace V. $
Moreover, $cv(t)h1+h1 S h1 in h1 T\/S arrow.double cv(t) h1 in h1 T arrow.double cv(t)h1 + h1 S h1 in h1 X$. Conversely, $uu h1 + h1 S h1 in h1 X arrow.double uu in T arrow.double uu+S in T\/S$. Thus, $X=T\/S$ and $f$ is surjecive.
]


== The universal property of quotients and the first isomorphism theorem
#theorem()[
  Let $S h1 lt.eq h1 V$ and $tau h1 in h1 cal(L)(V,W)$ s.t. $S h1 subset.eq h1 ker(tau)$. Then there is a unique $ tau' h1 in h1 cal(L)(V#h(-.1em)\/S, W)$ where $tau' compose pi_S=tau$. Moreover, $ker(tau')=ker(tau)\/S$ and $im(tau')=im(tau)$.
]<t:quotient_space_lin_transf>
#proof()[
  Let $tau' h0 in h1 cal(L)(V\/S,W)$ s.t. $tau' h0 compose pi_S=tau$, i.e., $tau'(vv h1 + h1 S)=tau vv$. So,
  #set par(leading: .3em)
  $ vv+S = uu + S arrow.double vv - uu in S arrow.double tau(vv - uu)=0 arrow.double  tau vv = tau uu. $
  Thus, $tau'(vv+S)=tau'(uu+S)$ and it is well defined. Also,
  $ im(tau')={tau'(vv + S): vv in V}={tau vv: vv in V}=im(tau) $
  and 
  $ ker(tau')&={vv+S:tau'(vv+S)=0}={vv+S: tau vv = 0}\ &={vv+S: vv in ker(tau)}=ker(tau)\/S $
  
  The uniqueness of $tau'$ is evident. #v(-1.3em)
  ]
#theorem([_The first isomorphism theorem_])[
  Let $tau h1 in h1 cal(L)(V,W), S h1 = h1 ker(tau)$. Then $tau' h0 in h1 cal(L)(V\/S, W)$ defined by $tau'(vv+S)=tau vv$ is injective and $ V\/S isom im(tau). $
]<t:first_isomorphism_thm>
#proof()[By @t:quotient_space_lin_transf, $ker(tau')=S\/S={0} arrow.double tau'$ is injective. Let $sigma=tau|_(V,im(tau))$ (a surjection) and $sigma' compose pi_S = sigma$. By @t:canonical_proj_quot_space_epi, $sigma'$ is surjective, and $sigma'=tau'|_(V\/S, im(tau))$, so $sigma'$ is an isomorphism.]

== Quotient spaces, complements and codimension
#theorem()[Let $S subspace V$. Then $V=S plus.circle T arrow.double T isom V\/S$.]<t:complement_quot_space>
#proof()[Apply @t:first_isomorphism_thm to $tau=rho_(T,S):V arrow T$.]

#corollary()[Let $S subspace V$. Then $dim V= dim S + dim (V\/S)$.]
#proof()[By @t:complement_quot_space and @t:sum_dim]
#definition()[*Codimension* of $S$ in $V$ ($"codim"_(V)(S)$): $dim(V\/S)$]
#note[$dim V < infinity arrow.double "codim"_(V)(S)=dim V - dim S$]


== Aditional isomorphism theorems

#theorem([_The second isomorphism theorem_])[Let $S,T subspace V$. Then
$ (S+T)\/ T isom S\/(S sect T) $
]<t:second_isomorphism_thm>
#proof()[Let $tau:(S+T)arrow S\/(S sect T)$ s.t. $tau(cv(s)+cv(t))=cv(s)+(S sect T)$. Prove that $tau$ is an epimorphism with kernel $T$. Then apply @t:first_isomorphism_thm]

#theorem([_The third isomorphism theorem_])[Let $S lt.eq T subspace V$. Then $ (V\/S)\/(T\/S) isom V\/T $]<t:third_isomorphism_thm>
#proof()[Let $tau: V\/S arrow V \/ T$ be defined by $tau(vv+S)=vv+T$. Prove that $tau$ is an epimorphism. Then apply @t:first_isomorphism_thm]
#theorem()[Let $S h1 lt.eq h1 V$, $V h2 = h2 V_1 oplus V_2 $ and $S h2 =h2 S_1 oplus S_2$ with $S_i h1 lt.eq h1 V_i$. Then 
 $ V/S=(V_1 oplus V_2)/(S_1 oplus S_2) isom V_1/S_1 plus.square V_2/S_2 $
]
#proof()[Let $tau: V arrow (V_1\/S_1)plus.square(V_2\/S_2)$ be defined by $ tau(vv_1+vv_2)=(vv_1+S_1, vv_2 + S_2). $
Since $V=V_1 oplus V_2$ is direct, $tau$ is well defined. Prove that $tau$ is an epimorphism with kernel $S_1 oplus S_2$. Then apply @t:first_isomorphism_thm.
]

== Linear functionals

#definition()[*Linear functional* (or *functional*) on $V$#h(-.1em): map $f h0 in h0 cal(L)(V #h(-.14em) (FF),h0 FF)$]
#definition()[*Algebraic dual space* of $V$: $V^ast=cal(L)(V(FF),FF)$.]

#theorem()[For any $f in V^ast$: 
  $dim(ker(f))=dim(V)-1$
]
#proof()[
  By @t:rank_nullity_thm and because $dim im(f)=dim FF=1$.
]
#theorem()[
  + $forall 0 eq.not vv in V, exists f in V^ast: f(vv) eq.not 0$
  + $vv in VV=0 arrow.double.l.r forall f in V^ast, f(vv)=0$
  + $(f in V^ast) and (f(xx) eq.not 0) arrow.double V=span(xx) oplus ker(f)$
  + For nonzero $k,g in V^ast$, $ker(f)=ker(g) arrow.double.l.r exists lambda in FF: f=lambda g$
]<t:dual_properties>



== Dual bases

#definition()[
  Let $cal(B)={vv_i h0:i in I} lt.dot V$. For $i in I$, define $vv_i^ast in V^ast$ s.t.
  $ vv_i^(ast)(vv_j)=delta_(i = j). $
]
#theorem()[Let $cal(B)={vv_i:i in I} lt.dot V$.
+ $cal(B)^ast={v_i^ast:i in I}$ is LI
+ $dim V < infinity arrow.double B^ast lt.dot V^ast$.
] <t:dual_basis>
#proof()[
  For all $i_k$, $0=sum_(j=1)^n alpha_(i_j)vv^ast_(i_j)(vv_(i_k))=sum_(j=1)^n alpha_(i_j)delta_(i_j=i_k)=alpha_(i_k)$. For any $f in V^ast$, then $sum_j f(vv_j)vv^ast_j(vv_i)=sum_j f(vv_j)delta_(i = j)=f(vv_i)$. So, $f=sum f(vv_j)vv^ast_j in span(B^ast)$. Hence $B^ast lt.dot V^ast$.
]
#definition()[$B^ast$ in @t:dual_basis is the *dual basis* of $V^ast$.]

#theorem()[$dim V lt.eq dim V^ast$ with equality iff $dim V < infinity$.]
#proof()[
  If $cal(B)lt.dot V$ and if $V=V(FF)$ then by @t:space_sequence_isomorphic, $V isom (FF^cal(B))_0$ and $V^ast isom FF^cal(B)$ #todo
]


== Reflexivity
#definition()[*Double* (*algebraic*) *dual space* $V^(ast ast)=cal(L)(V^ast, FF)$]
#definition()[*Evaluation at* $vv in V$: $overline(vv):V^ast arrow FF$ where $overline(vv)(f) = f(vv)$.]
#theorem()[$overline(vv)$ is linear, hence $overline(vv) in V^(ast ast)$.]
#proof()[$overline(v)(alpha f h0 + h0 beta g) h1=h1 (alpha f  h0 + h0  beta g)(vv)h1=h1 alpha f(vv)  h0 + h0  beta g(vv)h1=h1 alpha overline(v)(f) h0 + h0 beta overline(v)(g)$.]
#definition()[*Canonical* (or *natural*) *map* from $V$ to $V^(ast ast)$: $tau vv = overline(vv)$]
#theorem()[$tau h0:h1 V h1 arrow h1 V^(ast ast)$, with $tau vv h1 = h1 overline(vv)$, is a monomorphism. If $dim V h1 < h1 infinity$, then $tau$ is an isomorphism.]
#proof()[$tau$ is linear since, for all $f in V^(ast ast)$
$ overline(alpha uu + beta vv)(f)=f(alpha uu + beta vv)=alpha f(uu) + beta f(vv)=(alpha overline(uu)+beta overline(vv))(f) $
$ker(tau)={0}$ (i.e., $tau$ is injective) because (last equality uses @t:dual_properties):
$ tau vv = 0 = overline(vv) arrow.double forall f in V^(ast)(overline(vv)(f)=f(vv)=0) arrow.double vv=0 $
As $dim(V^(ast ast))h1=h1 dim(V^ast)h1=h1 dim(V)$ and by @t:same_dim_injective_surjective, $dim V<infinity$ implies that $tau$ is also surjective, hence an isomorphism.
]

== Annihilators
#definition()[*Annihilator* $M^0$ of $0 eq.not M subset.eq V$: 
$ M^0={f in V^ast : f(M)={0}} $]
#theorem()[$M^0 subspace V^ast$.]
#theorem()[
  + If $M,N subset.eq V$, then $M subset N arrow.double N^0 subset.eq M^0$
  + $dim V < infinity arrow.double forall 0 eq.not M subset.eq V (tau: span(M) isom M^(00))$. In particular, $S subspace V arrow.double S^(00) isom S$.
  + $S,T subspace V arrow.double (S sect T)^0 = S^0 + T^0$ and $(S+T)^0=S^0 sect T^0$.
]
#proof()[#todo]

=== Annihilators and direct sums
#definition()[
  *Extension by* 0: extending $f in T^ast$ to $overline(f) in V^ast=(S oplus T)^ast$ by setting $overline(f)(S)=0$.
]
#note[
  $overline(f)in S^0$.
]
#theorem()[Let $V=S oplus T$.
  + The extension by 0 map is an isomorphism from $T^ast$ to $S^0$ and so 
  $ T^ast isom S^0 $
  + $dim V < infinity arrow.double dim(S^0)=codim_(V)(S)=dim V - dim S$
]

#theorem()[$(S oplus T)^ast = S^0 oplus T^0$]


== Operator adjoints
#definition()[
  *Operator adjoint* of $tau in cal(L)(V,W)$: $tau^times: W^ast arrow V^ast$ s.t. $ tau^(times)(f)=f compose tau = f tau quad arrow.double quad forall vv in V: [tau^(times)(f)](vv)=f(tau v) $
]
#theorem([_Properties of the Operator Adjoint_])[
  + For $tau, sigma in cal(L)(V, W)$ and $a,b in F$: $(alpha tau + beta sigma)^times = alpha tau^times + beta sigma^times$
  + For $sigma in cal(L)(V,W)$ and $tau in cal(L)(W,U)$, $(tau sigma)^times = sigma^times tau^times$
  + For any invertible $tau in cal(L)(V)$, $(tau^(-1))^times = (tau^times)^(-1)$
]

#theorem()[If $V,W$ are finite-dimensinal vector spaces and $tau in cal(L)(V,W)$. If $V^(ast ast), W^(ast ast)$ are identified resp. with $V,W$ using the natural maps, then $tau^(ast ast)$ is identified with $tau$.]
#proof()[#todo]
#theorem()[
  Let $tau in cal(L)(V,W)$. Then
  + $ker(tau^ast) = im(tau)^0$
  + $im(tau^ast) = ker(tau)^0$

]
#proof()[#todo]

#corollary()[
  Let $tau in cal(L)(V,W)$ where V,W are finite dimensional. Then $"rk"(tau)="rk"(tau^ast)$
]

#proof()[#todo]
#theorem()[Let $tau in cal(L)(V,W)$, where $V,W$ are finite dimensional. If $B basis V, C basis W, B^ast basis V^ast, C^ast basis W^ast$, then:
$ [tau^ast]_(C^ast, B^ast)=([tau]_(B^ast,C^ast)\)^top $
]
#proof()[#todo]


= Modules I: Basic Properties
= Modules II: Free and Noetherian Modules
= Modules over a Principal Ideal Domain

= The structure of a linear operator
#theorem()[]


== The module associated with a linear operator
#theorem()[]

=== Submodules and invariant subspaces
#theorem()[]

=== Orders and the minimal polynomial

#definition()[]

#theorem()[]

#definition()[]
#theorem()[]

=== Summary



== The primary cyclic decomposition of V

#definition()[]

#theorem()[]

#theorem()[]



== The characteristic polynomial
#definition()[]

#theorem()[]

#definition()[]


== Cyclic and indecomposable modules
#theorem()[]

=== Indecomposable modules

#theorem()[]

#theorem()[]
=== Companion matrices

#definition()[]

#theorem()[]



== The big picture

#theorem()[]


== The rational canonical form

#definition()[]

#theorem()[]

=== Invariant factor version

#theorem()[]
#definition()[]
#theorem()[]

=== The determinant form of the characteristic polynomial

#lemma()[]

#theorem()[]

=== Changing the base field

#theorem()[]

#theorem()[]



= Eigenvalues and eigenvectors



== Eigenvalues and eigenvectors

#definition()[]

#theorem()[]

#theorem()[]

#theorem()[]

=== The trace and the determinant

#definition()[]

#theorem()[]

#definition()[]



== Geometric and algebraic multiplicities
#definition()[]

#theorem()[]



== The Jordan Canonical Form

#theorem()[]


== Triangularizability and Schur's Theorem

#definition()[]

#theorem()[]

=== The real case

#theorem()[]
#definition()[]

#theorem()[]

=== Unitary Triangularizability


== Diagonalizable operators
#definition()[]

#theorem()[]

#theorem()[]

=== Spectral resolutions

#theorem()[]



= Real and complex inner product spaces

#definition()[]

#lemma()[]

#theorem()[]


== Norm and distance

#theorem()[]

#theorem()[]

#definition()[]

#theorem()[]


== Isometries

#definition()[]

#theorem()[]


== Orthogonality

#definition()[]

#theorem()[]

#definition()[]

#theorem()[]


== Orthogonal and orthonormal sets

#theorem()[]

=== Gram-Schmidt Orthogonalization

#theorem()[]

#theorem()[]

=== The QR factorization

#theorem()[]

=== Hilbert and Hamel bases

#definition()[]

#theorem()[]


== The projection theorem and best approximations

#theorem()[]

#theorem()[]

#theorem()[]

=== Characterizing orthonormal bases
#theorem()[]


== The Riesz representation theorem
#definition()[]

#theorem()[]


= Structure theory of normal operators

== The adjoint of a linear operator

#theorem()[]

#theorem()[]

#theorem()[]

=== The operator adjoint and the Hilbert space adjoint

#theorem()[]
== Orthogonal projections

#definition()[]

#theorem()[]

=== Orthogonal resolutions of the identity

#definition()[]

#theorem()[]


== Unitary diagonalizability
#definition()[]

#theorem()[]


== Normal operators

#definition()[]

#theorem()[]

=== The spectral theorem for normal operators
#theorem()[]
==== The real case
#theorem()[]


== Special types of normal operators
#definition()[]


== Self-adjoint operators
#theorem()[]


== Unitary operators and isometries
#theorem()[]

#theorem()[]

#theorem()[]

=== Unitary similarity

#definition()[]

#theorem()[]

=== Reflections

#theorem()[]

#theorem()[]


== The structure of normal operators

#theorem()[]

#theorem()[]


== Functional calculus
=== Commutativity
#theorem()[]

#theorem()[]



== Positive operators

#definition()[]

#theorem()[]

#theorem()[]


== The polar decomposition of an operator

#theorem()[]

#corollary()[]

#theorem()[]


= Metric vector spaces: the theory of bilinear forms

== Bilinear maps
#definition()[
  *Bilinear* map $f:U times V arrow W$: linear in both variables:
  $ f(alpha uu + beta uu', gamma vv + mu vv')&=(alpha gamma) f(uu, vv)+(alpha mu)f(uu,vv')\  &+(beta gamma)f(uu',vv)+ (beta mu)f(uu',vv') $
] <d:bilinear_map>
#definition()[$hom_(FF)(U\,V\;W)={(f:U times V arrow W):f "is bilinear"}$]

== Symmetric, skew-symmetric and alternate forms

#definition()[
  *Bilinear form*: bilinear map $b in hom_(FF)(V\,V\;FF)$:
  1. #box(width: 4.5cm)[*Symmetric*:] $b(xx,yy)=b(yy,xx), forall xx,yy in V$
  2. #box(width: 4.5cm)[*Skew-symmetric* (*antisym.*):] $b(xx,h1 yy)h1 =h1 -b(xx,h1 yy), forall xx,yy h1 in h1  V $
  3. #box(width: 4.5cm)[*Alternate* (*alternating*):] $b(xx,xx)=0, forall xx in V$
]
#definition()[*Bilinear form space*: $(V,b)$ where b is bilinear.
- *Metric vector space*: $b$ is symmetric, antisymmetric or alternate.
- *Orthogonal geometry*: $b$ is symmetric.
- *Sympletic geometry*: $b$ is alternate.
]
//#definition()[*Metric vector space*: bilinear form space where ]

#note[A bilinear form is a 2-form (see #todo).]// cite multilinear map

#theorem()[If $"char"(FF)=2$ then $ "alternate" arrow.double "symmetric" arrow.double.l.r "antisymmetric" $]
#theorem()[If $"char"(FF) eq.not 2$ then $"alternate" arrow.double.l.r "antisymmetric"$. Only the zero form $b(xx,yy)=0, forall xx,yy in V$ is both alternate and symmetric.]
#proof()[For an alternating form over any field:
  $ 0 = b(xx+yy,xx+yy)=b(xx,yy)+b(yy,xx) arrow.double b(xx,yy)=b(yy,xx)$
]
== The matrix of a bilinear form
#definition()[]

#theorem()[]
=== The discriminant of a form


== Quadratic forms

  #definition[
    *Quadratic space* $(V,Q)$: vector space $V$ and *quadratic form* $Q:V arrow FF$ where:
    - $Q(alpha vv) = alpha^2 Q(vv), forall alpha in FF, vv in V$
    - $b_(Q)(uu,vv) h0 = h0 Q(uu h0 + h0 vv)h0 -h0 Q(uu)h0 -h0 Q(vv)$ is a (symmetric) bilinear form
  ]
  #definition()[*Set of quadratic forms* $ cal(Q)(V)={(Q:V arrow FF):Q "is a quadratic form"} $]
  #definition[
    *Polarization* of $Q$: symmetric bilinear form ($forall vv,ww in V$): $ B_(Q)(vv,ww):=1/2 b_(Q)(uu,vv) $
  ]
  #theorem()[For any bilinear form $b$, $Q(vv)=b(vv,vv)$ is a quadratic form.]
  #proof()[
    + $Q(alpha vv)=b(alpha vv, alpha vv) = alpha^2 b(vv,vv)=alpha^2 Q(vv)$, and
    + $b_(Q)(uu, h1 vv)
       h1 =h1 b(uu h0 +h0 vv,h1 uu h0 +h0 vv)h1 -h1 b(uu,h1 uu)h1 -h1 b(vv, h1 vv)
       h1 =h1 b(uu,h1 vv)h1 +h1 b(vv,h1 uu) $
    #v(-1.4em)
  ]
  #definition()[*Symmetrization* of bilinear form $b$: $B_Q$ with $Q(vv)h0 =h0 b(vv,h0 vv)$]
  #theorem()[For a symmetric bilinear form $b$, $B_Q h1=h1 b$ where $Q(vv)h0 =h0 b(vv,h1 vv)$]
  #corollary()[$hom^plus_(FF)(V\,V\;FF) isom cal(Q)(V)$ with .]
  #theorem[Let $(ee_1,dots,ee_n) h1 basis h1 (V, inner(dot,dot))$ and $Q(vv)=inner(vv,vv)$. Then:
    $ (ee_1,dots,ee_n) in cal(P)^(perp)(V) arrow.double.l.r Q(ee_i + ee_j) h1 = h1 Q(ee_i) h1 + h1 Q(ee_j), forall i h1 eq.not h1 j. $
  ]
  #theorem[If $"char" FF h1 eq.not h1 2$, then there exists an orthogonal basis of $(V,Q)$.]
  #theorem()[
    If $(ee_1,dots,ee_n)_(#h(-.1em) perp) h0 basis h1 V(FF)$ and $Q(ee_i) h1 > h1 0$ (or $Q(ee_i) h1 < h1 0$), $forall i$, then 
    $ Q(vv) = 0 arrow.double vv = 0. $
  ]
  #theorem([_Sylvester's Law of Inertia_])[
  // #show math.equation: set block(below:0.1em, above:0.1em)
  // #show par: set block(below:.2em)
  // #set block(below:.9em)
    If $E,F h1 basis h1 V #h(-.05cm) (RR)$ are orthogonal and
    #set par(leading: .3em)
    $ E^plus  &:= {ee in E: Q(ee) > 0} quad & F^plus  &:= {ee in F: Q(ee) > 0} \ 
      E^minus &:= {ee in E: Q(ee) < 0} & F^minus &:= {ee in F: Q(ee) < 0} \ 
      E^0     &:= {ee in E: Q(ee) = 0} & F^0     &:= {ee in F: Q(ee) = 0} $
    then $|E^plus|=|F^plus|$, $|E^minus|=|F^minus|$ and $span(E^0)=span(F^0)$.
  ]

  #definition()[*Signature* of $(V,Q)$: unique tuple $(|E^plus|, |E_minus|, |E^0|)$. $(V,Q)$ is *degenerate* iff $E^0 eq.not emptyset$.]
  #theorem()[If $E,F basis V(CC)$ are orthogonal and 
  #set par(leading: .3em)
    $ E^times  &:= {ee in E: Q(ee) eq.not 0} quad & F^plus  &:= {ee in F: Q(ee) eq.not 0} \ 
      E^0     &:= {ee in E: Q(ee) = 0} & F^0     &:= {ee in F: Q(ee) = 0} $
    then $|E^times|=|F^times|$ and $span(E^0)=span(F^0)$. Furthermore:
    $ E^0 =emptyset arrow.double exists (ee_1,dots,ee_n)_nperp basis V:forall i(Q(ee_i)=1) $
  ]

  #definition()[$RR^(s,t,u)$ means the real vector space with signature $(s,t,u)$ (likewise for $CC$). $RR^(s,t)=RR^(s,t,0)$. $RR^s=RR^(s,0)$.
    #columns(2)[
      + *Euclidean*: $RR^(n)$
      + *Antieuclidean*: $RR^(0,n)$
      + *Lorentzian*: $RR^(1,n)$
      #colbreak()
      4. *Antilorentzian*: $RR^(n,1)$
      + *Minkowski*: $RR^(3,1)$ or $RR^(1,3)$.
    ]
  ]

#definition[
  *Complex conjugate* of a *vector space*: $cconj(V)=V$ where $ +_(cconj(V))=+_V quad "and" quad dot_(cconj(V))(alpha,vv) = dot_V (cconj(alpha),vv)=cconj(alpha)vv  quad forall alpha in FF $
]
#definition()[
  *Sesquilinear form*: $b: $
]


== Orthogonality
#definition()[]

#definition()[]

#definition()[]

#theorem()[]


#theorem()[]

=== Orthogonal and symplectic geometries
== Linear functionals
#theorem()[]

#theorem()[]


== Orthogonal complements and orthogonal direct sums

#definition()[]

#theorem()[]

#theorem()[]


== Isometries

#definition()[]

#theorem()[]
== Hyperbolic spaces
#definition()[]
== Nonsingular completions of a subspace
#definition()[]
#theorem()[]

#theorem()[]

=== Extending isometries to nonsingular completions

#theorem()[]


== The Witt theorems: a preview
== The classification problem for metric vector spaces

== Symplectic geometry
=== The classification of sympletic geometries
#theorem()[]

#theorem()[]

=== Witt's extension and cancelation theorems

#theorem()[]

#theorem()[]

=== The structure of symplectic group: symplectic transvections

#definition()[]

#theorem()[]

#theorem()[]

#theorem()[]

== The structure of orthogonal geometries: orthogonal bases
=== Orthogonal bases
#lemma()[]

#theorem()[]

#corollary()[]

== The classification of orthogonal geometries: canonical forms 

=== Algebraically closed fields

#theorem()[]

#theorem()[]
=== The real field $RR$

#theorem()[]

#theorem()[]

=== Finite fields

#theorem()[]

#definition()[]

#theorem()[]

#theorem()[]

#theorem()[]


== The orthogonal group

=== Rotations and reflections

#theorem()[]

=== Symmetries

#theorem()[]

#theorem()[]


== The Witt theorems for orthogonal geometries
#theorem()[]

#theorem()[]


== Maximal hyperbolic subspaces of an orthogonal geometry
=== Maximal totally degenerate subspaces

#theorem()[]

=== Maximal hyperbolic subspaces

#theorem()[]

=== The anisotropic decomposition of an orthogonal geometry

#theorem()[]

= Metric spaces


== The definition

#definition()[]


== Open and closed sets
#definition()[]
#definition()[]

#theorem()[]
#definition()[]

== Convergence in a metric space
#definition()[]

#theorem()[]


== The closure of a set
#definition()[]
#definition()[]
#theorem()[]

== Dense subsets
#definition()[]

== Continuity

#definition()[]

== Complements
== Isometries
== The completion of a metric space

= Hilbert spaces
== A brief review
== Hilbert spaces
== Infinite series
== An approximation problem
== Hilbert bases
== Fourier expansions
== A characterization of Hilbert bases
== Hilbert dimension
== A characterization of Hilbert spaces
== The Riesz representation theorem

= Tensor products

== Universality
#definition()[
  Let $A$ be a set and $cal(S)$ be a family of sets. Let $ cal(F)h1=h1 {g h0:A h0 arrow h0  X: X in cal(S)} quad "and" quad  cal(H)h1=h1{tau:X arrow Y:X,Y in cal(S)}. $
  + $cal(H)$ contains the identity map $iota_S:X arrow X, forall X in cal(S)$
  + $cal(H)$ is closed under composition of functions, an associative op.
  + $forall tau in cal(H), f in cal(F)h0: tau compose f in cal(F)$.
  $cal(H)$ is the *measuring family* and $tau h1 in h1 cal(H)$ is a *measuring function*.
  A pair $(S, f: A arrow S)$, where $S in cal(S), f in cal(F)$ has the *universal property* for $cal(F)$ *as measured by* $cal(H)$, or is a *universal pair* for $(cal(F), cal(H))$, if for every $g: A arrow X$ in $cal(F)$, there is a unique $tau: S arrow X$ in $cal(H)$ for which $g=tau compose f$, or equivalently, any $g in cal(F)$ can be *factored through* $f$. The unique measuring function $tau$ is the *mediating morphism* for $g$.
]
#theorem()[
  Let $(S, f:A arrow S)$ and $(T, g:A arrow T)$ be universal pairs for $(cal(F), cal(G))$. Then there is a bijective measuring function $mu in cal(H)$ for which $mu S = T$. In fact, the mediating morphisms (of $f$ w.r.t $g$, and vice versa) are isomorphisms.
]<t:universal_pair_isomorphisms>
#proof()[
  Let $tau:S arrow T$ and $sigma: T arrow S$ be mediating morphisms s.t. $g=tau compose f$ and $f = sigma compose g$. Hence, $g = (tau compose sigma) compose g$ and $f = (sigma compose tau) compose f$. Both $(sigma compose tau):S arrow S$ and the identity map $iota: S arrow S$ are mediating morphisms for $f$. By uniqueness, then $sigma h2 compose h2 tau h1 = iota$. Similarly $tau compose sigma=iota$. So $sigma,tau$ are inverses.
]
== Tensor products
#definition()[
  $"Vect"(FF)$: family of all vectors spaces over $FF$.
]
#definition()[
  Let $U,V$ be vector spaces over $FF$, $cal(S)="Vect"(FF),cal(H)=cal(L)$
  $ cal(F)=union.big_W {hom_(FF)(U\,V\;W):W in "Vect"(FF)} $

  A pair $(T, t in cal(L)(U times V, T))$ is *universal for bilinearity* if it is universal for $(cal(F),cal(H))$, i.e., for ever bilinear map $f:U h1 times h1 V h1 arrow h1 W$, there is a unique *mediating morphism* $tau in cal(L)(T,W)$ for $f$ where $ f=tau compose t. $
]

#definition([Tensor product via bilinearity])[
  *Tensor product* of vector spaces $U(FF),V(FF)$: any universal pair $(T,t:U times V arrow T)$ for bilinearity, where $T$, denoted by $U otimes V$, are sometimes referred to as the tensor product. The map $t$ is called the *tensor map* and each $t(uu,vv)=uu otimes vv in U otimes V$ are called *tensors*. Each $uu otimes vv in im(t)$ is called *decomposable* tensor.
]
#note[By @t:universal_pair_isomorphisms, there is a unique (up to isomorphism) tensor product of vector spaces.]
#definition[
  *Free vector space* over field $FF$ generated by set $A$: 
  $ "Free"(A)={sum_(i=1)^n lambda_i a_i: lambda_i in FF, a_i in A, n < infinity} $
]

#definition([_Tensor product via quotient space_])[
  Let $U,V$ be vector spaces over $FF$. Let $FF_(U times V):= "Free"(U times V)$ the free vector space over $FF$ generated by $U times V$. Let $S lt.eq FF_(U times V) $ be spanned by 
  $ (uu + alpha ww, vv + beta xx)-(uu,vv)-alpha(ww,vv)-beta(uu,xx)-(alpha beta)(ww,xx) $ with $alpha,beta in FF, uu,ww in U, vv,xx in V$.
  Let $U otimes V:= F_(U times V)\/S$ and $t h0: U h1 times h1 V arrow U otimes V$ be the map $t(uu,vv)=uu otimes vv = (uu,vv)+S$.// The pair $(cal(P)\/cal(R))$ is the *tensor product* of $V$ and $W$. We write $V otimes W$ for $cal(P)\/cal(R)$, and $vv otimes ww$ for $mu(vv,ww).$
]
#theorem[
  The pair $(U otimes V, t: U times V arrow U otimes V)$ is the tensor product of $U$ and $V$.
]
#proof()[#todo]

=== Bilinearity on $U times V$ Equals Linearity on $U otimes V$
#theorem()[Let $(U  h1 otimes h1  V #h(-.2em), t h0 : U h1 times h1 V arrow U otimes V)$ be the tensor product. Then,
  $ phi.alt: hom(U\,V\;W) isom cal(L)(U otimes V, W) $
  where $phi.alt f$ is the only linear map where $ f  =  phi.alt f compose  t, quad forall f  in  hom(U\,V\;W) $
]
// #theorem()[
//   $mu in hom(V,W\;cal(P)\/cal(R))$.
// ]<t:tensor_product_mu_bilinear>
// #proof()[
//   Let $pi = pi_(cal(P)\/cal(R))$. Since $cal(R) subset.eq ker\(pi)$:

//   $ 0 h0 &= h0 pi((uu h0 + h0 alpha vv, ww h0 + h0 beta xx) h0 - h0 (uu,vv) h0 - h0 alpha(vv,ww) h0 - h0 beta(uu,xx) h0 - h0 (alpha beta)(vv,xx)) \
//   &=h0 pi((uu h0 + h0 alpha vv, ww h0 + h0 beta xx))-pi((uu,vv))-alpha pi((vv,ww))-beta pi((uu,xx))\ &thick thick -(alpha beta)pi((vv,xx)) \
//   &=mu(uu h0 + h0 alpha vv, ww h0 + h0 beta xx)-mu(uu,ww)-alpha mu(vv,ww)-beta mu(uu,xx)\ 
//   &thick thick -(alpha beta)mu(vv xx)
//    $#v(-1.2em)]
// #theorem([_Universal property of tensor products_])[
//   Let $V,W,M$ be vector spaces over $FF$. Let $V otimes W=cal(P)\/cal(R)$ be the tensor product. For any $forall phi.alt in hom (V,W;M), exists! med overline(phi.alt) in cal(L)(V otimes W, M)$, s.t. $forall vv h1 in h1 V, ww h1 in h1 W$, $overline(phi.alt)(vv otimes ww)=phi(vv, ww)$. Moveover, any $overline(sigma) in cal(L)(V otimes W, M)$ arises in this way.
// ] <t:vec_tensor_map>
// #proof()[
//   Since  By @t:tensor_product_mu_bilinear, $phi$
// ]
== When is a tensor product zero?
  #theorem()[
    If ${uu_1,dots,uu_n} subset U$ are LI and $vv_1,dots,vv_n in V$, then 
    $ sum uu_i otimes vv_i = 0 quad arrow.double quad vv_i = 0, forall i $
    In particular, $uu otimes vv arrow.double.l.r uu=0 $ or $ vv=0$.
    ]
  #proof[
    #todo
    ]
== Coordinate matrices and rank
== Characterizing Vectors in a Tensor Product

== Defining linear transformations on a tensor product
== The tensor product of linear transformations
== Change of base field
== Multilinear maps and iterated tensor products
== Tensor spaces
== Special multilinear maps
== Graded algebras
== The symmetric and antisymmetric tensor algebras
== The determinant

= Positive solutions to linear systems: convexity and separation

== Convex, closed and compact sets
== Convex hulls
== Linear and affine hyperplanes
== Separation

= Affine geometry

== Affine geometry 
== Affine combinations
== Affine hulls
== The lattice of flats
== Affine independence
== Affine transformations
== Projective geometry

= Singular values and the Moore-Penrose inverse

== Singular values
== The Moore-Penrose generalized inverse
== Least squares approximation

= An introduction to algebras

== Motivation
== Associative algebras
== Division algebras

#definition[
  *Inner product space* $(V, angle.l dot.c, dot.c angle.r)$: vector space $V$ and *inner product* $angle.l dot.c,dot.c angle.r:V times V arrow.r FF$:

  - #box(width: 4.48cm)[_Linearity in the right argument_:] $angle.l alpha cvec(x)+beta cvec(y),cvec(z) angle.r #h(.1em)=#h(.1em) alpha angle.l cvec(x), cvec(y) angle.r + beta angle.l cvec(y),cvec(z) angle.r$
  - #box(width: 4.48cm)[_Hermitian symmetry_:] $angle.l cvec(x), cvec(y) angle.r=overline(inner(yy,xx))$.
  - #box(width: 4.48cm)[_Positive definiteness_:] $angle.l cvec(x),cvec(x) angle.r gt.eq 0 $, $ angle.l cvec(x),cvec(x) angle.r = 0 arrow.l.r.double.long cvec(x)=0$
]
#definition[ *Inner product for sets*
- #box(width: 4.48cm)[_Vector/set_:] $angle.l cvec(v), Y angle.r = {angle.l cvec(v), cvec(yy) angle.r bar.v cvec(y) in Y}$
- #box(width: 4.48cm)[_Set/set_:]    $inner(X, Y)=union.big_(xx in X) inner(xx, Y)$
]

#definition[
  *Normed space* $(V, norm(dot.c))$: space $V$ and *norm* $norm(dot.c): arrow.r FF$ with: 
  - #box(width: 4.48cm)[_Triangle inequality_:] $norm(cvec(x)+cvec(y)) lt.eq norm(cvec(x)) + norm(cvec(y))$ 
  - #box(width: 4.48cm)[_Absolute homogeneity_:] $norm(alpha cvec(x)) = |alpha|norm(cvec(x))$ 
  - #box(width: 4.48cm)[_Positive definiteness_:] $norm(cvec(x))=0 arrow.r.double.long cvec(x) = 0$ 
]
#definition[
  *Norm induced by* $angle.l dot.c,dot.c angle.r: norm(cvec(x)) = sqrt(inner(cvec(x), cvec(x))) $
]
#definition[
  *Unit vector*: $norm(vv) = 1$
]
#definition[
  *Metric space*: vector space $(V, d)$ and *metric* (or *distance*) $d:V times V arrow.r FF$ with conditions: 
  - #box(width: 3.28cm)[_Triangle inequality_:] $forall cvec(x),cvec(y),cvec(z) in V, d(cvec(x),cvec(y)) lt.eq d(cvec(x),cvec(z)) + d(cvec(z),cvec(y))$ 
  - #box(width: 3.28cm)[_Symmetry_:] $forall cvec(x),cvec(y) in V, d(cvec(x),cvec(y))=d(cvec(y),cvec(x))$ 
  - #box(width: 3.28cm)[_Positive definiteness_:] $forall cvec(x),cvec(y) in V, d(cvec(x),cvec(y)) lt.eq 0$ and \ #h(3.38cm) $d(cvec(x),cvec(y))=0 arrow.r.l.double cvec(x)=cvec(y)$
]

#definition[*Orthogonality*: let $A,B subset V$ and $K$ an index set
    - #box(width:2cm)[_Vector/vector_: ] $aa perp bb arrow.double.l.r inner(aa,bb)=0$
    - #box(width:2cm)[_Vector/set_: ] $aa perp B arrow.double.l.r forall bb in B, aa perp bb$
    - #box(width:2cm)[_Set/set_: ] $A perp S arrow.double.l.r forall aa in A, aa perp B$
    - #box(width:2cm)[_Set_:] $A={aa_i}_(i in K)$ is *orthogonal* if $aa_i perp aa_j$ for $i eq.not j$
  ]
#definition[$A={aa_i}_(i in K)$ is *orthonormal* if $A$ is orthogonal and $forall i in K, aa_i$ is a unit vector.
]
#definition[
  *Orthogonal complement* of $X subset.eq V$: $ X^perp = {cvec(v) in V bar.v cvec(v) perp X} $
]

#definition[
  #let boxwidth=4cm
    *Linear transformation*: $tau:V arrow.r W$ where:
    $ tau(alpha u +beta v ) = alpha tau(cvec(u))+  beta tau( cvec(v) ) $
      - #box(width: boxwidth)[*Linear operator*:] $tau:V arrow.r V$
        - #box(width: boxwidth)[*Real operator*:] $tau$ linear operator and $FF=RR$
        - #box(width: boxwidth)[*Complex operator*:] $tau$ linear operator and $FF = C $
      - #box(width: boxwidth)[*Linear functional*:] $tau: V arrow.r FF$.
      - #box(width: boxwidth)[*Dual space*:] $V^ast$: set of all linear functionals on $V$.
  ]
#definition[#let boxwidth=4cm
  - #box(width: boxwidth)[*Homomorphism*:] for linear transformation
  - #box(width: boxwidth)[*Endomorphism*:] for linear operator
  - #box(width: boxwidth)[*Monomorphism*:] for injective linear transformation
  - #box(width: boxwidth)[*Epimorphism*:] for surjective linear transformation
  - #box(width: boxwidth)[*Isomorphism*:] for bijective transformation
  ]

#definition[

]

//
// Partial Orthogonality
//

#definition[
  Set $X$ is *partially orthogonal* to $Y$, i.e,
  $ X pperp Y arrow.double.r.l exists xx eq.not 0 in X: forall yy in Y inner(xx,yy)=0 $
]
#definition[
  Let $X,Y<V$ be nonzero subspaces, $p=dim X, q=dim Y$ and $m=min{p,q}$. Their *principal angles* are $0 lt.eq theta_1 lt.eq dots.c lt.eq theta_m lt.eq pi/2$ and orthornormal basis $cal(B)_X=(xx_1,dots,xx_p)$ and $cal(B)_Y=(yy_1,dots,yy_q)$ are *associated principal bases*, formed by *principal vectors* if, $forall 1 lt.eq i lt.eq p $ and $1 lt.eq j lt.eq q$
  $ inner(xx_i, yy_j) = delta_i^j cos theta_i $
]
#theorem[ For any $X, Y < V:$
  - $X pperp Y arrow.double.r.l dim X > dim Y$ or a principal angle of $V,W$ is $pi/2$.
  - $X pperp W arrow.double.r.l dim V > dim P_Y (X) $
  - If $dim X = dim Y$ then $X pperp Y arrow.double.r.l Y pperp X$
]


#definition[*(Associative) algebra*:
  non null set $A$ with *addition*, *multi-plication*, and *scalar multiplication* operations:
  - $A$ is a _vector space_ over $FF$ under addition and scalar multipli.
  - $A$ is a _ring with identity_ under addition and multiplication
  - $alpha(aa bb)=(alpha aa)bb=aa(alpha bb), forall alpha in FF, aa,bb in V$
  ]

#colbreak()