#import "acmt.typ": *

#show: acmt-theme.with()

/* START OF DOCUMENT */

// Status of the slide:
  // d definition
  // r corollary
  // t theorem
  // l lemma
  // n note
  // p proposition
  // g graphical pictures
  // e example
  // c complete
  // * ongoing

  #title-slide(title: "Geometric Algebra", subtitle: "Chapter 6: Intersection and Union of Subspaces", authors: "Anderson Tavares", institution-name: "", date: datetime.today().display())

// #let mybox(name, title, fill, stroke) = {thmbox(name, title, fill: fill, radius:0cm, inset: (x: .5em, y: .5em), above:-1em, below:-1em, stroke: (left:stroke + 2pt), titlefmt:titlefmt(stroke)).with(numbering: none)}
// #let theorem = mybox("theorem", "Theorem", rgb("#ddffdd"), rgb("#006600"))
// #let proposition = mybox("proposition", "Proposition", rgb("#ffeebb"), rgb("#777700"))
// #let corollary = mybox("corollary", "Corollary", rgb("#ddffdd"), rgb("#006600"))

// #let lemma = mybox("lemma","Lemma", rgb("#eeffee"), rgb("#009900"))
// #let definition = mybox("definition", "Definition", rgb("#ffeeee"), rgb("#990000"))
// #let example = mybox("example", "Example", rgb("#eeeeff"), rgb("#000099"))
// #let proof = thmproof("proof", "Proof").with(numbering: none)
// #let note = thmplain("note", "Note", inset:(left:0em,right:0em)).with(numbering: none)
#let hquad = h(.5em)
#let oidx = $cal(I)$
#let ord(elem)=$overline(elem)$
#let idx(elem) = $bold(elem)$// $#text(font:"Latin Modern Sans",weight: "regular", elem)$
#let uidx = $cal(M)$
#let signord = $sigma$
#let setminus(aelem,belem) = $aelem #h(0cm) without #h(0cm) belem$
#show par: set block(spacing: 0em, inset: 0em, outset: 0em)
#show math.equation: set block(below: 0.5em, above: 0.5em, inset: 0em, outset: 0em)
// #set block(above: .7em, below: .7em, inset: 0em, outset: 0em)
// #show par: set block(spacing: .0em, above: 0em, below: 0em, outset: .0em, inset: 0em)
// #show math.equation: set block(spacing: .0em, above: .0em, below: .0em, outset: .0em, inset: 0em)

// [c, d, e] Multi-index formalism
#slide[
  == Multi-index formalism

  #definition[
    #uncover("2-")[
      - For *grade* $p$ and $q$ *dimensions*:
        #uncover("3-")[- #box(width:6.5cm)[*Unordered indices*:]]
        #v(-.2cm)
        #uncover("4-")[$ 
          uidx_p^q #h(0em) uncover("5-",= {(i_1,dots,i_p) in NN^p: i_j lt.eq q, i_j eq.not i_k "if" j eq.not k} #h(.2cm))
          uncover("6-",#uidx^q = union.big_(p=0)^q uidx_p^q) #h(.5cm)
          uncover("7-",#uidx=union.big_(q=0)^infinity uidx^q)
        $]
        #uncover("9-")[- #box(width:6.5cm)[*Ordered indices*:]] // $#box(width:1cm,align(right,$oidx_p^q$)) = {(i_1,dots,i_p) in NN^p: 1 lt.eq i_1 < dots < i_p lt.eq q}$
        #v(-.2cm)
        #uncover("10-")[$ 
          #box(width:1cm,align(right,$oidx_p^q$))uncover("11-", = {(i_1,dots,i_p) in NN^p: 1 lt.eq i_1 < dots < i_p lt.eq q} #h(.5cm))
          uncover("12-",#box(width:1cm, align(right,$oidx^q$)) = union.big_(p=0)^q oidx_p^q)  #h(.5cm)
          uncover("14-",#box(width:1cm,align(right,$oidx$))=union.big_(q=0)^infinity oidx^q)
        $]
      ]
      #uncover("15-")[- #box(width: 6.5cm)[_Case_ $p=0$:] $#box(width:1cm, align(right,$oidx_0^q$))=uidx_0^q={emptyset}$]
    // #tablex(columns: (.55fr, .48fr), auto-lines: false,[
    //   - For $q$ dimensions:
    //     - #box(width: 6.5cm)[*Ordered indices*:] $#box(width:1cm, align(right,$oidx^q$)) = union.big_(p=0)^q oidx_p^q$
    //     - #box(width: 6.5cm)[*Unordered indices*:] $#box(width:1cm, align(right,$uidx^q$)) = union.big_(p=0)^q uidx_p^q$
    //   ],vlinex(),[
    //   - For _any_ dimension and _any_ grade:
    //     $#box(width:1cm,align(right,$oidx$))=union.big_(q=0)^infinity oidx^q$\
    //     $#box(width:1cm,align(right,$uidx$))=union.big_(q=0)^infinity uidx^q$
    // ])
      #uncover("16-")[- #box(width: 6.5cm)[*Grade* and *sum*:] $#box(width:1cm, align(right,$forall idx(i)$))=(i_1,dots,i_p), quad |idx(i)|=p quad norm(idx(i))=i_1+dots+i_p $]
      #uncover("17-")[- #box(width: 6.5cm)[*Juxtaposition*:] $#box(width:1cm, align(right,$idx(i)$))=(i_1,dots,i_p)=i_1dots i_p$]
      #uncover("18-")[- #box(width: 6.5cm)[_Common indices_:] $idx(i),idx(j),idx(k) in oidx quad idx(r),idx(s),idx(t) in uidx$]
  ]
  #uncover("4-",example[
    $(2,1,4)in uidx_3^4 quad uncover("8-",(7,dots,1)in uidx) quad uncover("10-",(1,2,4)in oidx_3^4) quad uncover("13-",(2)\,(3,4,5) in oidx^5)$
  ])
]

// [c, d] Multi-index formalism: operations on indices
#slide[
  == Multi-index formalism: operations on indices
  #definition[
    #uncover("2-")[
      - For $idx(r), idx(s) in uidx quad idx(r)=r_1,dots,r_(p_r) quad idx(s)=s_1,dots,s_(p_s) $
        // - *Union* $idx(r) union idx(s) = i_1 dots i_k, i_j in idx(r)$
        #uncover("3-")[- #box(width:7.5cm)[*Difference*:] $setminus(idx(r), idx(s)) in uidx =$ removing from $idx(r)$ any indices of $idx(s)$]
        #uncover("4-")[- #box(width:7.5cm)[*Disjoint*:] $idx(r) sect idx(s)=emptyset arrow.double.l.r r_i eq.not s_j$ (no common indices)]
        #uncover("5-")[- #box(width:7.5cm)[*Concatenation*:] $idx(r)idx(s)=r_1 dots r_(p_r)s_1,dots,s_(p_s) in uidx$ (if disjoint)]
        #uncover("6-")[- #box(width:7.5cm)[*Subsequence*:] $idx(r) subset idx(s) quad arrow.l.r.double.long quad r_i in idx(r) arrow.double r_i in idx(s)$]
        #uncover("7-")[- #box(width:7.5cm)[*Ordering* $idx(r)$:] $overline(idx(r)) in oidx$ (permutation *sign*: $signord_idx(r)$ ($signord_emptyset=1$))]
        #uncover("8-")[- #box(width:7.5cm)[*Cartesian product*] $idx(r)times idx(s)={(r,s): r in idx(r), s in idx(s)}$]
        #uncover("9-")[- #box(width:7.5cm)[*Larger indices*:] $idx(r) > idx(s)={(r,s) in idx(r)times idx(s): r > s}$]
    ]

    #uncover("10-")[
      - For $idx(i),idx(j) in oidx$
        #uncover("11-")[- #box(width:7.5cm)[*Intersection*] $idx(i) sect idx(j) = i_1 dots i_k in oidx, i_j in idx(i) "and" i_j in idx(j) $]
        #uncover("12-")[- #box(width:7.5cm)[*Union*] $idx(i) union idx(j) = i_1 dots i_k in oidx, i_j in idx(i) "or" i_j in idx(j)$]
        #uncover("13-")[- #box(width:7.5cm)[*Symmetric difference*:] $idx(i) triangle.t idx(j) = (setminus(idx(i),idx(j))) union (setminus(idx(j),idx(i)))$]
        #uncover("14-")[- #box(width:7.5cm)[*Complement* ($idx(i)in oidx^q $):] $idx(i)'=setminus((1,dots,q),idx(i))$]
    ]
  ]
]

#slide[
  == Multi-index formalism
  #theorem[Let $idx(r),idx(s) in uidx$ and $idx(i),idx(j),idx(k) in oidx$ be pairwise disjoint
    #uncover("2-")[1. $signord_(idx(r)idx(s)) = (-1)^(|idx(r)||idx(s)|)signord_(idx(s)idx(r))$]
    #uncover("4-")[2. $signord_(idx(r)idx(s)) = signord_idx(r)signord_(overline(idx(r))idx(s))$]
    #uncover("6-")[3. $signord_(idx(i)idx(j)) = (-1)^(|idx(i) > idx(j)|)$]
    #uncover("8-")[4. $signord_(idx(i)idx(i)') = (-1)^((|idx(i)|(|idx(i)|+1))/(2)+norm(idx(i)))$]
    #uncover("10-")[5. $signord_(idx(i)idx(j)idx(k)) = signord_(idx(i)idx(j))signord_(idx(i)idx(k))signord_(idx(j)idx(k))$]
  ]
  #uncover("3-",proof[
    #uncover("3-")[1. $|s|$ transpositions for each $r_i in idx(r)$.]
    #uncover("5-")[2. First order $idx(r)$, then order $ord(idx(r))idx(s)$.]
    #uncover("7-")[3. $j_1 in idx(j)$ swaps with all $i > j_1$, and so on.]
    #uncover("9-")[4. $|idx(i) > idx(i)'|=(i_1-1) + (i_2-2)+dots.c + (i_p - p)=norm(idx(i))-(p(p+1))/(2)$.#v(.2em)]
    #uncover("11-")[5. $signord_(idx(i)idx(j)idx(k))=signord_(idx(i)idx(j))signord_(ord(idx(i)idx(j))idx(k)), quad signord_(ord(idx(i)idx(j))idx(k)) = (-1)^(|idx(i)idx(j)>idx(k)|) = (-1)^(|idx(i)>idx(k)|+|idx(j)>idx(k)|) = signord_(idx(i)idx(k))signord_(idx(j)idx(k))$.]
  ])
]

#slide[
  == Grassmann algebra
  #definition[
    #uncover("2-")[*Grassman's exterior algebra* of subspace $S < V_FF$:]
    #uncover("3-")[graded algebra
    $ ext S = limits(plus.circle.big)_(p in ZZ) ext^p S = underbrace(ext^0 S, FF ("scalars")) + underbrace(ext^1 S, S("vectors")) + underbrace(ext^2 S, 2"-vectors") + dots $]
    #uncover("4-")[with $ext^p S={0}$ if $p in.not {0,dots,dim S}$.]
    #uncover("6-")[Any $M in ext S$ is a *multivector*]
    #uncover("7-")[, where $M=sum_p M_p$ and $M_p in ext^p S$ is a *$p$-vector* of *grade* $|M_p|=p$.]
    #uncover("8-")[The *exterior product* $wedge:ext^i S times ext ^ j S arrow ext^(i+j) S$ is bilinear, associative and alternate:]
    #uncover("9-")[$ A in ext^p S "and" B in ext^q S arrow.double.long A wedge B = (-1)^(p q) B wedge  A in ext^(p+q) S $]
  ]
  #uncover("5-")[
    #example[#v(-.9cm)
      $ underbrace(A, "multivector" \ and.big(V))
      = underbrace(5, "scalar" \ and.big^(0)(V))
      + underbrace(3cvec(a) + 2cvec(b),"vector" \ and.big^(1)(V))
      + underbrace(4cvec(b) wedge cvec(c) + 10 cvec(a) wedge cvec(c),"bivector" \ and.big^(2)(V)) 
      - underbrace(2 cvec(c) wedge cvec(a) wedge cvec(d) ,"trivector" \ and.big^(3)(V)) $
    ]
  ]
]
#slide[
  
  #theorem[For $cvec(u), cvec(v) in S$, $cvec(u) wedge cvec(v)=-cvec(v)wedge cvec(u)$, so $cvec(v)wedge cvec(v)=0$]
  #definition[
    $cvec(v)_1,dots,cvec(v)_q in S, idx(r)=r_1 dots r_p in uidx_p^q thick med arrow.double.long thick  cvec(v)_idx(r)=v_(r_1)wedge dots.c wedge v_(r_p) "and" v_emptyset=1$
  ]
  #definition([Indicator func.])[
    $[|cvec(P)|]=1$ if proposition $cvec(P)$ is true, otherwise $[|cvec(P)|]=0$
  ]
  #theorem[
    $idx(r),idx(s) in uidx^q arrow.double cvec(v)_idx(r) wedge cvec(v)_idx(s) = [|idx(r) sect idx(s) = emptyset|]cvec(v)_(idx(r)idx(s))=[|idx(r) sect idx(s) = emptyset|]signord_(idx(r)idx(s))cvec(v)_(overline(idx(r)idx(s)))$
  ]
  #corollary[
    $idx(i),idx(j) in oidx^q arrow.double [|idx(i) sect idx(j) = emptyset|]signord_(idx(i)idx(j)) cvec(v)_(idx(i) union idx(j))$
  ]
  #definition([Basis])[
    $cal(B)_S={cvec(v)_i}_(i=1)^q arrow.double cal(B)_(ext^(p) S) = {cvec(v)_idx(i)}_(i in oidx_p^q)$ and $cal(B)_(ext V)={cvec(v)_idx(i)}_(i in oidx^q)$
  ]
  #note[$S={0} arrow.double cal(B)_(ext S) = cal(B)_(ext^0 S) = {1}$]
  #theorem[
    $S < T arrow.double.long ext S < ext T$
  ]
]

#slide[
  == Grassmann algebra
  #example[$cvec(v)_2 wedge cvec(v)_(13)=cvec(v)_(213)=-cvec(v)_(123)$ and $cvec(v)_(13)wedge cvec(v)_(23)=0$]
  #example[$cal(B)_S=(cvec(v)_1,cvec(v)_2,cvec(v)_3) arrow.double cal(B)_(ext^2 S) = (cvec(v)_(12), cvec(v)_(13), cvec(v)_23), cal(B)_(ext S)=(1, cvec(v)_1, cvec(v)_2, cvec(v)_3, cvec(v)_(12), cvec(v)_(13), cvec(v)_23, cvec(v)_(123))$]
  #definition([Grade projection])[
    $M in ext V #h(.5em) arrow.double hquad (M)_p in ext^p V hquad arrow.double hquad M = sum_p (M)_p$
  ]
  #definition[$M_p in ext^p S$ is a $p$*-vector* or *Homogeneous of grade $p$*]
  #definition[$bl(M)_p=cvec(v)_(1 dots p) in ext^p S$ is a $p$*-blade* or a *simple* $p$*-vector*]
  #theorem[$bl(M)_p =cvec(v)_(1 dots p) eq.not 0 arrow.l.r.double cvec(v)_1,dots,cvec(v)_p$ are linearly independent]
  #definition([Space of a blade])[$lambda in ext^0 V arrow.double [lambda]={0}$ and 
    $ [bl(M)_p eq.not 0]#h(0em)=#h(.2em)span{cvec(v)_1,dots,cvec(v)_p}#h(0.1em)=#h(0.1em){cvec(v) in V: cvec(v) in bl(M)_p#h(0.1em)=0} $
  ]
  #theorem[$S=[bl(M)]arrow.double.l.r ext^p S=span(bl(M))$]
]
#slide[
  == Subblades, attitude, weight, orientation
  #definition[$bl(A)$ is *subblade* of $bl(B)$ if $[bl(A)] < [bl(B)]$]
  #definition[$bl(A),bl(B)$ have same *attitude* if $bl(A)=lambda bl(B)$, where $|lambda|$ is the *weight* of $bl(A)$ w.r.t. $bl(B)$ and $"sign"(lambda)=1$ means $bl(A),bl(B)$ have same orientation.]
  #definition[$bl(A),bl(B)$ have same *orientation* if $bl(A)=lambda bl(B)$]
]

#slide[
  == Inner product
  #definition([Inner product])[
    $ inner(bl(A)_p, bl(B)_p)=det(inner(cvec(a)_i, cvec(b)_j)) "with" inner(kappa, lambda)=overline(kappa)lambda "and" inner(bl(A)_m, bl(B)_n)=0 "if" m eq.not n $
  ]
  #theorem[$cal(B)_S$ is orthonormal $arrow.double cal(B) ext S$ and $cal(B) ext^p S$ are orthonormal]
  #definition([Norm])[
    $norm(M)=sqrt(inner(M, M))$
  ]
  #note[$FF=RR$: $norm(M)$ is the $p$-dimensional volume of parallelotope spanned by $cvec(v)_1,dots,cvec(v)_p$]
  #note[$FF=CC$: $norm(M)^2$ is the $2p$-dimensional volume spanned by $cvec(v)_1,i cvec(v)_1,dots,cvec(v)_p,i cvec(v)_p$]
]

#slide[
  == Outermorphism

  #definition[
    Any linear map $tau:V arrow.bar W$ extends to an *outermorphism*, a linear $tau: ext V arrow ext W$ with $tau(M wedge N)=tau M wedge tau N$ for $M,N in ext V$
  ]
  #theorem[
    
  ]
]

125 34
q=5

000

1+2+3 =6-3*4/2 = 6-6=0

0+0+1=1

