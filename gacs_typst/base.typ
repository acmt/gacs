#import "@preview/ctheorems:1.1.2": *
#import "@preview/tablex:0.0.8": tablex, gridx, hlinex, vlinex, colspanx, rowspanx


#let wedge = $#h(0.05em) and #h(0.1em) $
#let vee = $#h(0.05em) or #h(0.1em) $
// #let wedge = $and$
#let span(elem) = $angle.l elem angle.r$
#let gproj(elem, exp) = {
  if exp == none [
    $(elem)$
  ] else [
    $(elem)_exp$
  ]
}
#let myred = color.rgb("#990000")
#let mylightred = color.rgb("#ffaaaa")
#let mygreen = color.rgb("#006600")
#let myblue = color.rgb("#000099")
#let h0 = h(0.0em)
#let h1 = h(0.1em)
#let h2 = h(0.2em)
#let rfloor = $\u{230B}$
#let lfloor = $\u{230A}$
#let rceil = $\u{2309}$
#let lceil = $\u{2308}$
#let oidx = $cal(I)$
#let uidx = $cal(M)$
#let cconj(elem) = $overline(elem)$ //$ accent(elem, macron, size:#(200%+.25cm))$
#let eproj(elem) = gproj(elem, $+$)
#let oproj(elem) = gproj(elem, $-$)
#let cp = (paint: red,thickness: 1pt)
#let revalone = scale(x:130%, y: 110%, $tilde$)
#let involalone = scale(x:150%,y:150%,$hat$)
#let rev(symb) = {$accent(symb, tilde, size:#calc.max(50%+.3cm))$}
#let invol(symb) = {$accent(symb, hat, size:#(50%+.3cm))$}
#let cvec(elem)=$bold(upright(elem))$
#let mt(elem)=$bold(upright(elem))$
#let hvec(elem)=$cvec(tilde(elem, size:#100%))$
#let uvec(elem)=$cvec(hat(elem, size:#160%))$
#let blade(elem)=$elem$
#let ct(elem)=$elem^ast$
#let ins(elem)=$lfloor elem rfloor$
#let ous(elem)=$lceil elem rceil$
#let bl(elem) = $blade(elem)$
#let ord(elem)=$overline(elem)$
#let pperp = [#h(0.5em)#scale(y:80%,move(dy:-.2em,[|]))#h(-.5em)#move(dy:.2em, scale(x:130%,[$tilde$]))#h(0.2em)]
#let npperp = $pperp #h(-.3cm) slash #h(.15cm)$
#let signord = $sigma$
#let idx(elem)=$cvec(elem)$// $#text(font:"Latin Modern Sans", weight: "regular", elem)$
#let cv=cvec
#let uu=$cv(u)$
#let vv=$cv(v)$
#let todo=box(rect(stroke: black)[todo])
#let ww=$cv(w)$
#let zz=$cv(z)$
#let hid(elem)={}
#let subspace=$lt.eq$
#let otimes = $#h(0.1em)times.circle #h(0.1em)$
#let oplus = $#h(0.1em)plus.circle #h(0.1em)$
#let ee = $cvec(e)$
#let ff = $cvec(f)$
#let xx = $cvec(x)$
#let aa = $cvec(a)$
#let bb = $cvec(b)$
#let cc = $cvec(c)$
#let lcont = $rfloor$
#let lc = scale(80%,$#h(-.3em) angle.right.rev #h(-.3em) $)
#let rc = scale(80%,$#h(-.25em) angle.right #h(-.3em) $)
#let rcont = $lfloor$
#let ext = $scripts(and.big)$
#let hidden-slide(body)={}
#let inner(val1,val2)=$angle.l val1,val2 angle.r$
#let uni-colors = state("uni-colors", (:))
#let uni-short-title = state("uni-short-title", none)
#let uni-short-author = state("uni-short-author", none)
#let uni-short-date = state("uni-short-date", none)
#let uni-progress-bar = state("uni-progress-bar", true)
#let psn = $bl(I)_n$
#let ps(pwr) = $bl(I)_pwr$
#let psninv = $bl(I)_n^(-1)$
#let psinv(pwr) = $ps(pwr)^(-1)$
#let dual(elem) = $elem^ast$
#let undual(elem) = $elem^ast.circle$
#let myline = line(length: 28%, stroke:  (paint:myred, dash: "dashed"))
#let isom = $approx$
#let yy = $cvec(y)$
#let adj(func)=$func^ast$
#let implies=$quad arrow.double.long quad$
#let setminus(aelem,belem) = $aelem #h(0cm) without #h(0cm) belem$
#let normal = $lt.tri.eq$
#let gen(elem) = $angle.l elem angle.r$
#let mtx(elem) = $bracket.double.l elem bracket.double.r$
#let titlefmt(color) = {
  let f(body) = {text(fill:color, weight: "bold", body)}
  return f
}
#let ld(elem)=$attach(elem,tl:star)$
#let rd(elem)=$attach(elem,tr:star)$
#let lrd(elem)=$attach(elem,tr:star, tl:star)$
#let ldd(elem, blade)=$attach(elem,tl:star_blade)$
#let rdd(elem, blade)=$attach(elem,tr:star_blade)$
#let lrdd(elem, blade1, blade2)=$attach(elem,tr:star_blade1, tl:star_blade2)$
#let epi="epi"

#let ad=$"ad"$
#let tad=$invol("ad")$
#let color_t = rgb("#006600")
#let color_p = rgb("#777700")
#let color_c = rgb("#006600")
#let color_d = rgb("#990000")
#let color_l = rgb("#009900")
#let color_e = rgb("#000099")
#let color_n = rgb("#000000")

#let basis = $lt.dot$
#let obasis(elem) = $basis(elem)_perp$
#let nperp=$accent(perp, hat, size:#150%)$
#let mybox(name, title, fill, stroke) = {thmbox(name, title, fill: fill, radius:0cm, inset: (x: .3em, y: .5em), stroke: (left:stroke + 2pt, top:(paint:stroke,thickness:.0pt,dash:"solid")), spacing:0em, titlefmt:titlefmt(stroke), padding: (top: 0em, bottom: 0em), separator: [#h(0.1em)],base_level:1).with(numbering: "1.")}
#let codim="codim"
#let myproof = {

}

#let theorem = mybox("theorem", "T", rgb("#ddffdd"), color_t).with(numbering:"1.")
#let proposition = mybox("theorem", "P", rgb("#ffeebb"), color_p)
#let corollary = mybox("theorem", "C", rgb("#ddffdd"), color_c)

#let lemma = mybox("theorem","L", rgb("#eeffee"), color_l)
#let definition = mybox("definition", "D", rgb("#ffeeee"), color_d).with(numbering:"1.")
#let example = mybox("example", "E", rgb("#eeeeff"), color_e)
#let note = mybox("note", "N", rgb("#f1f1f1"), color_n)

#let show_thm_legend = rect(stroke: .1pt)[#text(color_d)[*D.*]efinition #text(color_t)[*T.*]heorem #text(color_c)[*C.*]orollary #text(color_e)[*E.*]xample #text(color_n)[*N.*]ote #text(color_p)[*P.*]roof #text(color_l)[*L.*]emma]
#let myproof = thmproof("proof", "Proof", inset:(x: .3em, y: .5em, top:0.2em), spacing:0em, padding: (top:0em, bottom: 0em))
#let conf(
  title: none,
  authors: (),
  abstract: [],
  noproof: false,
  doc,
) = {
  let authors_oneline = authors.map(author => [
        #author.name -
        #link("mailto:" + author.email)
    ]).join(",")
  // Set and show rules from before.
  set page(paper:"a4", margin: (top:1cm, rest:.5cm), header:[
    #grid(columns:(.3fr, .4fr, .3fr), align:(left, center, right), inset:(bottom:.0em), gutter:0em,
      align(
        left+horizon,text(weight:"bold",[
          #title
        ])
      ),
      [#authors_oneline],
      [#locate(loc=>{counter(page).at(loc).at(0)})]
    )

    #line(length: 100%,) #v(.6em)
  ],
    header-ascent: 0%, numbering: "1", number-align: top)

  let count = authors.len()
  let ncols = calc.min(count, 3)

  // set par(leading: .4em)
  // show list: set align(left)
  // show enum: set align(left)
  show par: set block(below:.5em)
  set block(below:.3em)
  set heading(numbering: "1.1.")
  show enum: set block(below: .8em)

  // show par: set block(below:.5em, above:.5em)
  // show list: set block(below: .5em)
  // show enum: set block(below: .5em)
  show math.equation: set block(below:0.3em, above:0.6em)
  show heading.where(level: 1): set text(size: 12pt)
  show heading.where(level: 1): set block()
  show heading.where(level: 2): set text(size: 11pt)
  set text(size: 10pt)

  columns(2, gutter:.1em, doc)
}