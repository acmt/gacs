= Greek letters
#columns(4,gutter: 0em, [
      + Alpha: $A alpha$
      + Beta: $B beta$
      + Gamma: $ Gamma gamma$
      + Delta: $ Delta delta$
      + Epsilon: $E epsilon epsilon.alt$
      + Zeta: $Z zeta$
      #colbreak()
      7. Eta: $H eta$
      + Theta: $ Theta theta$
      + Iota: $I iota$
      + Kappa: $K kappa$
      + Lambda: $ Lambda lambda$
      + Mu: $M mu$
      #colbreak()
      13. Nu: $N nu$
      + Xi: $ Xi xi$
      + Omicron: $Omicron omicron$
      + Pi: $ Pi pi$
      + Rho: $R rho$
      + Sigma: $ Sigma sigma.alt sigma$
      #colbreak()
      19. Tau: $T tau$
      + Upsilon: $ Upsilon upsilon$
      + Phi: $ Phi phi$
      + Chi: $X chi$
      + Psi: $ Psi psi$
      + Omega: $ Omega omega$
])
= Logic
#definition[
  #tablex(columns:2,align:(right, left),
  [Truth],[$$],
  [Idempotence],[],
  [Idempotence],[],
  [Excluded Middle],[],
  [Consistency],[],
  [Double Negative],[],
  [Commutativity of And],[],
  [Commutativity of Or],[],
  [Commutativity of Iff],[],
  [Associativity of And],[],
  [Associativity of Or],[],
  [De Morgan],[],
  [],[],
  [Distribution],[],
  [],[],
  [],[],
  [],[],
  [Definition of T],[],
  [Definition of F],[],
  [Absorption],[],
  [Absorption],[],
  [Alt Def of Equivalence],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  [],[],
  )

]
= Set theory
#definition[
  #tablex(columns:3, inset:0.2em, stroke:gray+.5pt, align:(right, center, left), auto-lines:false, [*Concept*],[*Expression*],[*Details*],hlinex(),
  [- *Set*],[$A$],[${a_1,dots,a_n}$],
  [- *Empty set*],[$emptyset$],[${}$],
  [- *Subset*],[$A subset.eq B$],[$x in A arrow.double x in B$],
  [- *Proper subset*],[$A subset B$],[$A subset.eq B wedge exists x in B: x in.not A$],
  [- *Superset*],[$A supset.eq B$],[$B subset.eq A$],
  [- *Proper superset*],[$A subset B$],[$B subset A$],
  [- Set *equality*],[$A = B$],[$A subset.eq B and B subset.eq A$],
  [- *Union*],[$A union B$],[${x: x in A or x in B}$],
  [],[$union.big_i^n A_i$],[$A_1 union dots union A_n$],
  [- *Intersection*],[$A sect B$],[${x: x in A and x in B}$],
  [],[$sect.big_i^n A_i$],[$A_1 sect dots sect A_n$],
  [- *Difference*],[$A - B$],[$x: x in A and x in.not B$],
  [],[$A backslash B$],[],
  [- *Symmetric diff*],[$A triangle.t B$],[$x: x in A arrow.double.l.r x in.not B$],
  [- *Complement*],[$A^c$ or $macron(A)$],[${x: x in.not A}=U-A$],
  [- *Cartesian product*],[$A times B$],[${(a,b): a in A, b in B}$],
  [],[$times.big_i A_i$],[$A_1 times dots times A_n$],
  [- *Power set*],[$cal(P)(A)$ or $2^A$],[${B : B subset.eq A}$],
  [- *Cardinality*],[$|A|$],[\# of elements],hlinex()
  )
]




#definition[ *Partially ordered set* or *poset* $(P,lt.eq)$:
    + #box(width:2.5cm)[*Reflexivity*:] #box(width:2cm,$forall a in P$), $a lt.eq a$
    + #box(width:2.5cm)[*Antisymmetry*:] #box(width:2cm,$forall a,b in P$), $a lt.eq b "and" b lt.eq a arrow.double a = b$
    + #box(width:2.5cm)[*Transitivity*:] #box(width:2cm,$forall a,b,c in P$), $a lt.eq b "and" b lt.eq c arrow.double a lt.eq c$
]
#definition[
  An element $m in P$ is *maximal* if $p in P, m lt.eq p arrow.double m = p $.\ An element $n in P$ is *minimal* if $p in P, p lt.eq n arrow.double p = n $
]
#definition[
  The *maximum* (*largest*, *top*) of poset $P$, if it exists is $M in P$ s.t.
  $ p in P arrow.double p lt.eq M. $
  The *minimum* (*least*, *smallest*, *bottom*) of $P$, if it exists, is $N in P$ s.t.
  $ p in P arrow.double N lt.eq p $
]

#colbreak()

= Linear algebra

#definition[*Vector space*
  over field $FF$ of *scalars*: tuple $(V, +, dot.c)$ s.t.:
        - $V$ is a set of *vectors* $cvec(v) in V$
        - *Vector addition* $+:V times V arrow.r V$ (abelian group):
            + #box(width:4.36cm)[_Associativity_:] $cvec(u)+(cvec(v)+cvec(w))=(cvec(u)+cvec(v))+cvec(w)$
            + #box(width:6.35cm)[_Commutativity_:] $cvec(u)+cvec(v)=cvec(v)+cvec(u)$
            + #box(width:4.375cm)[_Identity_:] $exists 0 in V: cvec(v)+0=cvec(v), forall cvec(v) in V$
            + #box(width:3.5cm)[_Reverse_:] $forall cvec(v) in V, exists - cvec(v) in V: cvec(v) + (-cvec(v)) = 0$
        - *Scalar multiplication* $dot.c: FF times V arrow.r V$:
          5. #box(width:6.25cm)[_Scalar/Field compatibility_:] $alpha(beta cvec(v))=(alpha beta)cvec(v)$
          6. #box(width:7.5cm)[_Identity_:] $1 cvec(v) = cvec(v)$
          7. #box(width:5.4cm)[_Distributivity w.r.t. vector addition_:] $alpha(cvec(u)+cvec(v))=alpha cvec(u) + alpha cvec(v)$
          8. #box(width:5.45cm)[_Distributivity w.r.t. field addition_:] $(alpha+beta)cvec(v) = alpha cvec(v) + beta cvec(v)$
      Axioms 1-4: abelian group under addition, 5-8: $FF$-module.
    ]

#definition[ *Subspaces*
  - *Subspace*: $S subset.eq V "and" S "is a vector space" arrow.double.l.r.long S lt.eq V$
  - *Proper subspace*: $S lt.eq V "and" S eq.not V arrow.double.l.r.long S < V$
  - *Zero subspace*: ${0}$
  ]
#definition[
  *Linear combination* of ${#h(-.1em)cvec(v)_i#h(-.1em)}_(i=0)^n#h(0em) in#h(0em) V$ with *coefficients* $alpha_i#h(0em) in#h(0.1em) FF $
  $ alpha_1 cvec(v)_1+alpha_2 cvec(v)_2+dots.c+alpha_n cvec(v)_n $
  ]
#definition[*Trivial* linear combination: $forall i, alpha_i = 0$ (*Nontrivial* otw)]
#definition[
  *Span*: $"span"{S} = {alpha_1 cvec(v)_1+dots.c+alpha_n cvec(v)_n: alpha_i in K, cvec(v)_i in S} $
  ]

#definition[
  *Linear independent* (*LI*) vectors $cvec(v)_1,dots,cvec(v)_n in V$:
  $ alpha_1 cvec(v)_1 + dots.c + alpha_n cvec(v)_n = 0 arrow.double.l.r.long alpha_1=dots.c=alpha_n=0 $
  #align(center)[Not LI $arrow.double.r.long$ *linearly dependent*]
]
#theorem[
  The following statements are equivalent:
  - $S$ is LI and spans $V$.
  - $forall cvec(v) in V, exists! cvec(v)_1,dots,cvec(v)_n in S, alpha_1,dots,alpha_n in FF:$
  $ vv = alpha_1 cvec(v)_1+dots.c+ alpha_n cvec(v)_n $
  - $S$ is a minimal spanning set.
  - $S$ is a maximal linearly independent set.
]
#definition[
  $S$ is a *basis* for $V$ if it satisfies one (and hence all) conditions on *T* 1.1.
]

#definition[ *Dimension of a vector space*
  - *Dimension*: cardinality $kappa$ of any basis for $V$. If $dim V = kappa$, then $V$ is $kappa$-*dimensional*.
  - $dim {0} = 0$
  - *Finite dimensional*: ${0}$ or has a finite basis
  - *Infinite dimensional*: not finite.
]

#definition[
  *Inner product space* $(V, angle.l dot.c, dot.c angle.r)$: vector space $V$ and *inner product* $angle.l dot.c,dot.c angle.r:V times V arrow.r FF$:

  - #box(width: 4.48cm)[_Linearity in the right argument_:] $angle.l alpha cvec(x)+beta cvec(y),cvec(z) angle.r #h(.1em)=#h(.1em) alpha angle.l cvec(x), cvec(y) angle.r + beta angle.l cvec(y),cvec(z) angle.r$
  - #box(width: 4.48cm)[_Hermitian symmetry_:] $angle.l cvec(x), cvec(y) angle.r=overline(inner(yy,xx))$.
  - #box(width: 4.48cm)[_Positive definiteness_:] $angle.l cvec(x),cvec(x) angle.r gt.eq 0 $, $ angle.l cvec(x),cvec(x) angle.r = 0 arrow.l.r.double.long cvec(x)=0$
]
#definition[ *Inner product for sets*
- #box(width: 4.48cm)[_Vector/set_:] $angle.l cvec(v), Y angle.r = {angle.l cvec(v), cvec(yy) angle.r bar.v cvec(y) in Y}$
- #box(width: 4.48cm)[_Set/set_:]    $inner(X, Y)=union.big_(xx in X) inner(xx, Y)$
]

#definition[
  *Normed space* $(V, norm(dot.c))$: space $V$ and *norm* $norm(dot.c): arrow.r FF$ with: 
  - #box(width: 4.48cm)[_Triangle inequality_:] $norm(cvec(x)+cvec(y)) lt.eq norm(cvec(x)) + norm(cvec(y))$ 
  - #box(width: 4.48cm)[_Absolute homogeneity_:] $norm(alpha cvec(x)) = |alpha|norm(cvec(x))$ 
  - #box(width: 4.48cm)[_Positive definiteness_:] $norm(cvec(x))=0 arrow.r.double.long cvec(x) = 0$ 
]
#definition[
  *Norm induced by* $angle.l dot.c,dot.c angle.r: norm(cvec(x)) = sqrt(inner(cvec(x), cvec(x))) $
]
#definition[
  *Unit vector*: $norm(vv) = 1$
]
#definition[
  *Metric space*: vector space $(V, d)$ and *metric* (or *distance*) $d:V times V arrow.r FF$ with conditions: 
  - #box(width: 3.28cm)[_Triangle inequality_:] $forall cvec(x),cvec(y),cvec(z) in V, d(cvec(x),cvec(y)) lt.eq d(cvec(x),cvec(z)) + d(cvec(z),cvec(y))$ 
  - #box(width: 3.28cm)[_Symmetry_:] $forall cvec(x),cvec(y) in V, d(cvec(x),cvec(y))=d(cvec(y),cvec(x))$ 
  - #box(width: 3.28cm)[_Positive definiteness_:] $forall cvec(x),cvec(y) in V, d(cvec(x),cvec(y)) lt.eq 0$ and \ #h(3.38cm) $d(cvec(x),cvec(y))=0 arrow.r.l.double cvec(x)=cvec(y)$
]

#definition[*Orthogonality*: let $A,B subset V$ and $K$ an index set
    - #box(width:2cm)[_Vector/vector_: ] $aa perp bb arrow.double.l.r inner(aa,bb)=0$
    - #box(width:2cm)[_Vector/set_: ] $aa perp B arrow.double.l.r forall bb in B, aa perp bb$
    - #box(width:2cm)[_Set/set_: ] $A perp S arrow.double.l.r forall aa in A, aa perp B$
    - #box(width:2cm)[_Set_:] $A={aa_i}_(i in K)$ is *orthogonal* if $aa_i perp aa_j$ for $i eq.not j$
  ]
#definition[$A={aa_i}_(i in K)$ is *orthonormal* if $A$ is orthogonal and $forall i in K, aa_i$ is a unit vector.
]
#definition[
  *Orthogonal complement* of $X subset.eq V$: $ X^perp = {cvec(v) in V bar.v cvec(v) perp X} $
]

#definition[
  #let boxwidth=4cm
    *Linear transformation*: $tau:V arrow.r W$ where:
    $ tau(alpha u +beta v ) = alpha tau(cvec(u))+  beta tau( cvec(v) ) $
      - #box(width: boxwidth)[*Linear operator*:] $tau:V arrow.r V$
        - #box(width: boxwidth)[*Real operator*:] $tau$ linear operator and $FF=RR$
        - #box(width: boxwidth)[*Complex operator*:] $tau$ linear operator and $FF = C $
      - #box(width: boxwidth)[*Linear functional*:] $tau: V arrow.r FF$.
      - #box(width: boxwidth)[*Dual space*:] $V^ast$: set of all linear functionals on $V$.
  ]
#definition[#let boxwidth=4cm
  - #box(width: boxwidth)[*Homomorphism*:] for linear transformation
  - #box(width: boxwidth)[*Endomorphism*:] for linear operator
  - #box(width: boxwidth)[*Monomorphism*:] for injective linear transformation
  - #box(width: boxwidth)[*Epimorphism*:] for surjective linear transformation
  - #box(width: boxwidth)[*Isomorphism*:] for bijective transformation
  ]

#definition[

]

//
// Partial Orthogonality
//

#definition[
  Set $X$ is *partially orthogonal* to $Y$, i.e,
  $ X pperp Y arrow.double.r.l exists xx eq.not 0 in X: forall yy in Y inner(xx,yy)=0 $
]
#definition[
  Let $X,Y<V$ be nonzero subspaces, $p=dim X, q=dim Y$ and $m=min{p,q}$. Their *principal angles* are $0 lt.eq theta_1 lt.eq dots.c lt.eq theta_m lt.eq pi/2$ and orthornormal basis $cal(B)_X=(xx_1,dots,xx_p)$ and $cal(B)_Y=(yy_1,dots,yy_q)$ are *associated principal bases*, formed by *principal vectors* if, $forall 1 lt.eq i lt.eq p $ and $1 lt.eq j lt.eq q$
  $ inner(xx_i, yy_j) = delta_i^j cos theta_i $
]
#theorem[ For any $X, Y < V:$
  - $X pperp Y arrow.double.r.l dim X > dim Y$ or a principal angle of $V,W$ is $pi/2$.
  - $X pperp W arrow.double.r.l dim V > dim P_Y (X) $
  - If $dim X = dim Y$ then $X pperp Y arrow.double.r.l Y pperp X$
]


#definition[*(Associative) algebra*:
  non null set $A$ with *addition*, *multi-plication*, and *scalar multiplication* operations:
  - $A$ is a _vector space_ over $FF$ under addition and scalar multipli.
  - $A$ is a _ring with identity_ under addition and multiplication
  - $alpha(aa bb)=(alpha aa)bb=aa(alpha bb), forall alpha in FF, aa,bb in V$
  ]

#colbreak()
