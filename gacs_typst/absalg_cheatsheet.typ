#import "base.typ": *
#show: thmrules

#show: doc => conf(
  title: [
    Abstract Algebra Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)
#show_thm_legend #v(-1em)
= Introduction to Groups

== Operators, semigroups, monoids
#definition[
  *Binary function*: $A times B arrow C$
]
#definition[
  *Operator*: $compose: A #h(0.2em)times#h(0.2em) dots.c#h(0.2em) times#h(0.2em) A #h(0.2em)arrow#h(0.2em) A$, and set $A$ is *closed under* $compose$.
]
#definition[
  *Binary operator*: both binary function and operator.
]
#definition[
  *Magma* $(G,compose)$: set $G$ *under* binary operator $compose h1: G h2 times h2 G h2 arrow h2 G$
]
#note[We sometimes write $G$ to refer to $(G, compose)$ ]
#note[We write $a b$ or $a + b$ (for addition) to refer to $a compose b$]
#definition[
  Let $(G, compose)$ be a magma.
  - $compose$ is *associative* if $forall a,b,c in A: (a b) c  = a (b  c)$
  - $compose$ is *comutative* if $ forall a,b h1 in h1 A h0: a b = b a$ ($a$ and $b$ *commute*).
  - $e in A$ is a *left* (*right*) *identity* if $forall a in A #h(0em): e a h1 = h1 a$ (resp. $a e h1 = h1 a$)
  - $e in A$ is an *identity* if it is a left and right identity.
]

#theorem[ The identity of a binary operator, if it exists, is unique.]

#definition[
  $a_1  dots  a_n$ *needs no parentheses* if no matter what choices of multiplications of adjacent factors are made, the resulting elements of $G$ are equal. 
]

#theorem([Generalized Associativity])[
  If $compose$ in a magma is associative, then any expression $a_1 a_2 dots a_n$ needs no parentheses.
]


#definition[
  *Semigroup* $(G,compose)$: magma under associative $compose$.
]
#definition[
  Let $a$ in a semigroup. Then, $a^1=a$ and $a^(n+1) = a^n a$ $(n gt.eq 1)$.
]
#corollary[
  Let $(G,compose)$ be a semigroup, $a in G$ and let $m,n in NN^+$. Then 
  $ a^m  a^n = a^(m+n)=a^n a^m  quad "and" quad (a^m)^n=a^(m n)=(a^n)^m $
]
#definition[
  A *monoid* is a semigroup with an identity.
]
#definition[
  Let $a in G$ in a monoid $(G,compose)$. Then $b in G$ is a *left* (*right*) *inverse* of $a$ if $b  a h2=h2 e$ (resp. $a  b h2 =h2 e$), and an *inverse* of $a$ (deno-ted by $a^(-1)$) if it is both left and right, and $a$ is called *invertible*.
]
#theorem[
  In a monoid $(G, compose)$, the inverse of $a h2 in h2 G$, if it exists, is unique.
]
#theorem[
  For an invertible $a$ in a monoid, $\(a^(-1)\)^(-1) = a$.
]
#theorem[
  For invertible $a,b$ in a monoid, $\(a  b\)^(-1) = b^(-1)  a^(-1)$.
]
#theorem[
  If $a$ is invertible in a monoid, then $a^(-n) = \(a^(-1)\)^n$
]
#theorem([*Cancellation Law*])[
  Let $G$ be a monoid and $a,b,c in G$, with $a$ invertible. If either $a  b=a  c$ or $b  a = c  a$ then $b = c$
]
#corollary[
  Let $G$ be a monoid and $a,b in G$, with $a$ invertible. Then
  $ exists !c in G: a  c=b quad "and" quad exists !d in G: d  a = b $
]
== Groups

#definition[
  *Group*: monoid where all elements are invertible.
]
#definition[
  *Trivial group*: ${e}$
]
#definition[
  *Abelian* group $(G, compose)$: group where $compose$ is commutative.
]
#example[
  The set of all invertible $n times n$ matrices over $FF$ is called the *general linear group* $  "GL"_(n)(FF)$, a group under matrix mult. The identity matrix $I_n$ is the identity. If $A,B in "GL"_n(FF)$, then $ A B (B^(-1) A^(-1))=A (B B^(-1)) A^(-1) = A I_n A^(-1)=A A^(-1)=I_n $
  and, similarly,#h(-0.1em) $(B^(-1)#h(-0.1em) A^(-1))A B h1 = h1 I_n$. Thus, #h(-0.1em) $A B h2 in h2 "GL"_n(FF)$.\ #h(-0.0em) If $A h2 in h2 "GL"_n(FF)$, then $(A^(-1))^(-1)=A$, so $A^(-1)in "GL"_n(FF)$
]
#theorem[
  If $(G,compose)$ is a semigroup, if it has a left identity and if any $a in G$ has a left inverse, then $(G, compose)$ is a group.
]
#definition[
  *Direct product* of groups $(G, *), (H, compose)$: Cartesian product $G times H$ along with the operation $diamond.small$ where $forall g_i h1 in h1 G, h_i h1 in h1 H, i=1,2$:
  $ (g_1,h_1) diamond.small (g_2, h_2) = (g_1*g_2, h_1 compose h_2) $
]
#theorem[
  The direct product of two groups is a group
]
== Order
#definition[
  *Order* of a group G: cardinality $|G|$.
]
#definition[
  *Finite* (*infinite*) group $(G, compose)$: $|G|$ is finite (resp. infinite).
]
#definition[
  A *group table* for a finite group $({a_1,dots,a_n}, compose)$ is the $n times n$ matrix $M$ where $(M)_(i,j)=a_i  a_j$:
  #align(center,tablex(columns: 5, auto-lines:false, stroke:.4pt, align:center, inset:(top:0em, rest:0.4em),
  $$, vlinex(), $a_1$, $a_2$, $dots$, $a_n$,hlinex(),
  $a_1$, $a_1  a_1$, $a_1  a_2$, $dots.c$, $a_1  a_n$,
  $a_2$, $a_2  a_1$, $a_2  a_2$, $dots.c$, $a_2  a_n$,
  $dots.v$, $dots.v$, $dots.v$, $dots.down$, $dots.v$,
  $a_n$, $a_n  a_1$, $a_n  a_2$, $dots.c$, $a_n  a_n$,
  ))
]

#definition[
  A group $G$ is *cyclic* if exists $a in G$ s.t. any $b in G$ is a power of $a$. We say that $G$ is *generated* by $a$, and write $G=gen(a)$.
]

#theorem[
  Every cyclic group is abelian
]

#definition[
  In a group $G$, the *order* of $a in G$, denoted $|a|$, is the smallest $n in NN^*$ s.t. $a^n=e$. If $n < infinity$, then $a$ has *finite order* is finite, otherwise $a$ has *infinite order*.
]

#theorem[
  Let $G$ be a group, $a in G$ and $i,j in ZZ$. Then
  + If $|a| = infinity$, then $a^i=a^j arrow.double.l.r.long i=j$
  + If $|a| < infinity$, then $a^i = a^j arrow.double.l.r.long i equiv j thick (mod n)$
]
#let divides=$#h(0.1em) divides #h(0.1em)$
#corollary[
  Let $G$ be a group and $a h2 in h2 G$ have order $n h1 < h1 infinity$. For any $i h1 in h2 ZZ$:
  + $a^i = e arrow.double.long.l.r n divides i$
  + $|a^i| = n\/gcd(i,n)$
]

#definition[
  If $G$ is a group, and $a,b in G$, then $b$ is a *conjugate* of $a$ if
  $ exists c in G: b=c^(-1)a c $
]
#theorem[
  In any group, conjugate elements have the same order.
]

== Subgroups
#definition[In a group $G$, $H h1 subset.eq h1 G$ is a *subgroup*, denoted by $H h1 lt.eq h1 G $, if it is a group under $compose$. If $H h1 subset h1 G$, then H is a *proper* subgroup $(H h1 < h1 G).$]

#theorem[
  Let G be a group and $H subset.eq G$. Then $H lt.eq G $ iff:
  + $e_G h1 in h1 H$ ($H$ contains the identity)
  + $forall a,b h2 in h2 H h0: a  b h2 in h2 H$ ($H$ is closed under $compose$)
  + $forall a h2 in h2 H h0: a^(-1) h1 in h1 H$ ($H$ contains all inverses)
] <t:subgroup_necessary_sufficient>
#note[Condition 1. of @t:subgroup_necessary_sufficient can be replaced by: 1. $H eq.not emptyset $]

#definition[
  The *cyclic subgroup generated by* $a in G$ of a group $G$ is
  $ gen(a) = {a^n: n in ZZ} $
]
#theorem[
  If $G$ is a group and $a in G$, then $gen(a)$ is a subgroup of $G$.
]
#definition[
  *Center* of group $G$: $Z(G)={z in G: a z = z a, forall a in G}$.
]

#theorem[
  If $G$ is a group, then $Z(G) lt.eq G$.
]
#theorem[
  Let $G$ be a group and $H subset.eq G$. Then $H lt.eq G$ iff
  + $e in H$
  + $forall a,b in H: a b^(-1) in H$
] <t:subgroup_necessary_sufficient_2>
#note[We can replace condition 1. in @t:subgroup_necessary_sufficient_2 by 1. $H eq.not emptyset$]

#theorem[
  Let $G$ be a group and $H subset.eq G$ be finite. Then $H lt.eq G$ iff
  + $e in H$
  + $forall a,b in H: a b in H$
]

== Cyclic Groups

#theorem[
  Let $G=gen(a)$ be cyclic. If $|a|=infinity$, then all powers of $a$ are distinct. If $|a|h2 =h2 n h2 < h1 infinity$, then $G h1 = h1 {e, a, a^2, dots,a^(n-1)}$ and $|a|h2 =h2 |gen(a)|$.
]

#theorem[
  Every subgroup of a cyclic group is cyclic.
]

#corollary[
  Let $G h2= h2 gen(a)$, where $|a| h2 = h2 n h2 < h2 infinity$ and $H h2  lt.eq h2  G$. Then $|H|$ divi-des $n$. If $m|n$, $m in NN^+$, then $exists !H lt.eq G: |H|=m$, namely $gen(a^(n\/m))$.
]

#definition[
  *Euler phi-function*: function $phi:NN arrow NN$, where $ phi(n)=|{i in NN^+:i lt.eq n "and" gcd(i,n)=1}| $
]

#theorem[
  For any $n in NN^+$, $|U(n)|=phi(n)$.
]

#theorem[
  Let $G=gen(a)$ be a cyclic group of order $n$ and $m in NN^+$, s.t. $m|n$. Then $|{a in G:|a|=m}|=phi(m)$
]

#theorem[
  Let p be a prime number and $m,n in NN^+$.
  + $phi(p^n) = p^n-p^(n-1)$
  + if $gcd(m,n)=1$, then $phi(m n) = phi(m)phi(n)$
]

== Cosets and Lagrange's Theorem

#definition[
  Let $G$ be a group and $H lt.eq G$. If $a,b in G$, $a$ is *congruent to* $b$ *modulo* $H$, written $a equiv b h2 (mod H)$, if $a^(-1)b in H$
]

#lemma[Let $G$ be a group and $H lt.eq G$. Then congruence modulo $H$ is an equivalence relation on $G$.]
#lemma[Let $G$ be a group and $H h2  lt.eq h2  G$. If $a h2 in h2  G$, then $[a] h1 = h1 {a h h0: h h1  in h1  H}$]
#definition[Let $G$ be a group, $H lt.eq G$ and $a in G$. The *left coset* of $a$ w.r.t. $H$ is [a], denoted $a H$ (_additive group_: $a h1 + h1 H h1 = h1 {a h1 + h1 h h0 :h h1  in h1  H}$).]
#theorem[Let G be a group and $H lt.eq G$. Then the left cosets of H in G partition G. In particular:
+ each $a in G$ is in exactly one left coset, namely $a H$; and
+ if $a,b in G$, then either $a H = b H $ or $a H sect b H = emptyset$.]
#theorem([Lagrange's Theorem])[Let G be a finite group and $H lt.eq G$. Then $|H|$ divides $|G|$.]
#definition[*Index* of $H lt.eq G$ in group $G$: $[G:H]=|{a H:a in G}|$]
#corollary[If $G$ is a finite group and $H lt.eq G$, then $[G:H]=|G|\/|H|$.]
#corollary[Let $G$ be a finite group, and $a in G$. Then $|a|$ divides $|G|$.]
#corollary[Every group of prime order is cyclic.]
#definition[Let $G$ be a group and $H lt.eq G$. Then for any $a in G$, the *right coset* of $a$ w.r.t. $H$ is $H a = {h a: h in H}$ (additive group: $H+a={h+a:h in H}$).]

= Factor groups and homomorphisms

== Normal subgroups


#definition[*Normal subgroup* $N h0 normal h1 G arrow.double.l.r (N h1 lt.eq h1 G)h1 and h1 (a N h1 = h1 N a, forall a h1 in h1 G)$.]
#theorem[$H lt.eq G$ and $[G:H]=2 arrow.double.long H normal G$ ($H$ is normal in G).]
#definition[Let $H lt.eq G$. Then, $forall a in G, a^(-1) H a = {a^(-1) h a: h in H}$]
#theorem[$H lt.eq G$ and $a in G arrow.double.long a^(-1) H a lt.eq G$.]
#theorem[Let $H lt.eq G$. The following are equivalent:
+ $H normal G$.
+ $a^(-1)h a in H, forall h in H, a in G$
+ $a^(-1)H a subset.eq H, forall a in G$
+ $a^(-1)H a = H, forall a in G$]
#definition[ $H,K lt.eq G arrow.double.long H K={h k: h in H, k in K}$]
#theorem[ If $H,K lt.eq G$ are finite, then $|H K|=|H||K|\/|H sect K|$]
#theorem[ Let $H,K lt.eq G$. Then
+ $(H normal G) or (K normal G) arrow.double.long H K lt.eq G$
+ $(H normal G) and (K normal G) arrow.double.long H K normal G $.
]

== Factor Groups
#definition[Let $N normal G$. The *factor group* (or *quotient group*) $G\/N$ equals ${a N: a in G}$ under the operation $(a N)(b N)=a b N$.]
#theorem[$N normal G arrow.double.long |G\/N|=[G:N]$]
#theorem[Let $N normal G$ and $a in G$. Then
+ If $G$ is abelian, then so is $G\/N$
+ If $G$ is cyclic, then so is $G\/N$
+ If $|a|=m < infinity$, then $|a N|$ divides $m$]
#theorem[$$]
#theorem[Let $G$ be any group. If $G\/Z(G)$ is cyclic, then $G$ is abelian.]
#corollary[]

== Homomorphisms

#definition[
  Let $(G, *), (H, compose)$ be semigroups. A function $f:G arrow H$ is a *homomorphism* if, $forall a,b in G$:
  $ f(a * b) = f(a) compose f(b)$
]
#definition[
  *Isomorphism*: bijective homomorphism.
]
#definition[
  Groups $G,H$ are *isomorphic*, denoted by $G tilde.equiv H$, if there is an isomorphism $f:G arrow H. $
]
#theorem[
  Let $f:G arrow H$ be an homomorphism
  + Identity maps to identity: $f(e_G) = e_H$
  + Inverse maps to inverse: $f(a^(-1))=f(a)^(-1)$
  + Power maps to power: $f(a^(n))=f(a)^n$
]
== Isomorphisms

#definition[]

#theorem[]

#theorem[]

#corollary[]

#lemma[]

#corollary[]

#theorem[]

#theorem[]

#corollary[]

#theorem[]

== The isomorphism theorem for groups

#theorem[]

#theorem[]

#theorem[]

== Automorphisms

#definition[]
#theorem[]
#theorem[]
#definition[]
#lemma[]
#theorem[]
#theorem[]

= Direct Products and the Classification of Finite Abelian Groups

== Direct products

#definition[]
#theorem[]
#definition[]
#lemma[]
#lemma[]
#theorem[]

== The fundamental theorem of finite abelian groups

#definition[]
#lemma[]
#lemma[]
#lemma[]
#lemma[]
#theorem[]
#corollary[]
#theorem[]
#theorem[]
#corollary[]

== Elementary divisors and invariant factors
#definition[]
#definition[]
#lemma[]
#theorem()[]
#theorem()[]
#definition()[]
#theorem()[]


== A word about infinite abelian groups
#definition()[]
#theorem()[]
#definition()[]
#definition()[]

= Symmetric and alternating groups


== The symmetric group and cycle notation
#theorem()[]
#corollary()[]
#definition()[]
#theorem()[]
#definition()[]
#theorem()[]
#theorem()[]
#theorem()[]

== Transpositions and the alternating group
#definition()[]
#theorem()[]
#lemma()[]
#theorem()[]
#definition()[]
#theorem()[]
#definition()[]
#theorem()[]


== The simplicity of the alternating group

#definition()[]
#theorem()[]
#lemma()[]
#corollary()[]
#corollary()[]
#theorem()[]
#corollary()[]

= The Sylow theorems
== Normalizers and centralizers
#definition()[]
#theorem()[]
#definition()[]
#theorem()[]
#theorem()[]

== Conjugacy and the class equation
#theorem()[]
#definition()[]
#lemma()[]
#theorem()[]
#corollary()[]
#corollary()[]
#definition()[]
#definition()[]
#theorem()[]
#theorem()[]


== The three Sylow theorems
#definition()[]
#theorem()[]
#theorem()[]
#theorem()[]

== Applying the Sylow theorems
#theorem()[]
#corollary()[]
#corollary()[]
#theorem()[]
#theorem()[]
#theorem()[]

== Classification of the groups of small order
#theorem()[]
#theorem()[]
#theorem()[]

= Introduction to rings

== Rings
#definition()[]
#definition()[]
#definition()[]
#definition()[]
#theorem()[]

== Basic properties of rings
#theorem()[]
#theorem()[]
#corollary()[]
#theorem()[]
== Subrings

#definition()[]
#theorem()[]
#definition()[]
#theorem()[]
#definition()[]

== Integral domains and fields
#definition()[]
#definition()[]
#theorem()[]
#definition()[]
#theorem()[]
#definition()[]
#lemma()[]
#theorem()[]
#theorem()[]
#theorem()[]
#definition()[]
#theorem()[]

== The characteristic of a ring
#definition()[]
#theorem()[]
#corollary()[]
#theorem()[]
#theorem()[]


= Ideals, factor rings and homomorphisms

== Ideals
#definition()[]
#theorem()[]
#theorem()[]
#corollary()[]
#theorem()[]
#theorem()[]
#definition()[]
#theorem()[]

== Factor rings
#definition()[]
#theorem()[]
#theorem()[]
#theorem()[]

== Ring homomorphisms
#definition()[]
#definition()[]
#theorem()[]
#theorem()[]
#theorem()[]


== Isomorphisms and automorphisms
#definition()[]
#theorem()[]
#theorem()[]
#definition()[]
#theorem()[]
#theorem()[]
#definition()[]
#theorem()[]

== Isomorphism theorems for rings
#theorem()[]
#corollary()[]
#corollary()[]
#theorem()[]
#theorem()[]


== Prime and maximal ideals
#definition()[]
#theorem()[]
#definition()[]
#theorem()[]
#theorem()[]



= Special types of domains
== Polynomial rings
#definition()[]
#definition()[]
#theorem()[]
#corollary()[]
#theorem()[]
#corollary()[]
#theorem()[]


== Euclidean domains
#definition()[]
#definition()[]
#definition()[]
#definition()[]
#definition()[]
#theorem()[]
#corollary()[]
#corollary()[]
#definition()[]
#lemma()[]
#theorem()[]
#theorem()[]
#theorem()[]

== Principal ideal domains
#definition()[]
#theorem()[]
#theorem()[]
#definition()[]
#lemma()[]
#definition()[]
#theorem()[]
#theorem()[]

== Unique factorization domains
#definition()[]
#theorem()[]
#theorem()[]
