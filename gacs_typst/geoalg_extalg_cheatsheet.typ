#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Geometric Algebra Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)
#let proof(..args)=myproof(..args)

= Exterior Algebra

#definition[$k$-*Exterior power*:
  quot. algebra $ext^k V = T^(k)(V)\/I_k$ where\
  $ I_k #h(0em)=#h(0em) span({cv(v)_1#h(-.2em) otimes dots.c otimes cv(v)_k#h(0em): cv(v)_1,dots,#h(0em)cv(v)_k#h(0.1em) in #h(0.1em) V#h(-0.0em), cv(v)_i#h(0em)=#h(0em) cv(v)_j "for some"  i #h(0em)eq.not#h(0em) j}) $
  ]
#note[$ext^0 V = FF, ext^1 V = V$]
#definition[#box(width:2cm)[$k$-*Blade*]
  $bl(A)=vv_1 wedge dots wedge vv_k  = vv_(1 dots k) = vv_1 otimes dots.c otimes vv_k + I_k$.
  ]
#definition[#box(width:2cm)[$k$-*Vector*]
  $G=sum_i bl(A)_i in ext^k V, bl(A)_i in ext^k V$.
  ]
#definition([_Notation on blades_])[
  + Set of all k-blades: $cal(B)_k := {vv_(1 dots k) in ext^k V}$
  + Set of all blades: $cal(B):=union.big_(k=0)^infinity cal(B)_k $
  + Nonzero blades: $cal(B)^ast = cal(B) without {0}$
]
#lemma[Let $V #h(-.1em) (FF)#h(-.05cm),M #h(-.1em) (FF)$ be vector spaces. For any $k$-linear, alternating map $phi.alt h0: V^k h1 arrow h1 M, exists! overline(phi.alt) in cal(L)\(ext^(k) V, M\)$ s.t. $overline(phi.alt)(vv_(1 dots k)) h1 = h1 phi.alt(vv_1,h0 dots,h0 vv_k)$ for all $vv_1,h0 dots,h0 vv_k h1 in h1 V$. Moreover, any $overline(sigma) h1 in h1 cal(L)\(ext^k V, M\)$ arises in this way.]<l:vec_map_ext_map>
#proof()[
  Since $phi.alt$ is $k$-linear, by @tavares_linalg24 T 11.4, $exists! rev(phi.alt) h1 in h1 cal(L)(T^(k)(V), M)$ s.t. $forall vv_1,dots,vv_k h1 in h1 V, rev(phi.alt)(vv_1 otimes dots otimes vv_k) h1 =h1 phi.alt(vv_1,dots,vv_k) $. By @tavares_linalg24 T 3.6, $I_k h1 subset.eq h1 ker(rev(phi.alt)) arrow.double.l.r exists overline(phi.alt) in cal(L)(T^(k)(V)\/I_k, M)$ s.t. $forall vv in T^(k)(V)(overline(phi.alt)(vv + I_k)=rev(phi.alt)(vv))$. As $phi.alt$ is alternating, $I_k h1 subset.eq h1 ker(rev(phi.alt))$ and we are done.
]
#theorem()[There is a natural bilinear map $ext^k V times ext^l V arrow ext^(k+l) V$ s.t.
$ (vv_(1 dots k),ww_(1 dots l)) arrow.bar vv_(1 dots k) wedge ww_(1 dots l) $
]
#proof()[The map $ f_(ww_(1 dots l))(vv_1, dots,vv_k)= vv_(1 dots k)wedge ww_(1 dots l) $ is $k$-linear and alternating.
By @l:vec_map_ext_map we get a linear map $overline(f)_(#h(-.2em)ww_(1 dots l)) #h(-.2em) (#h(-.1em)vv_(1 dots k)) h0 = h0 vv_(1 dots k)wedge ww_(1 dots l)$, i.e., $overline(f)_(ww_(1 dots l))(B_1)h0=h0 B_1 wedge ww_(1 dots l), forall B_1 h0 in h0 ext^k V$ . By the same argument, for each $B_1 in ext^k V$, we get a linear map 
$ overline(f)_(B_1)(B_2)=B_1 wedge B_2, forall B_2 in ext^l V. $
The map $overline(f)(B_1, B_2)=overline(f)_(B_1)(B_2)$ is linear in the second argument. Since 
$ overline(f)(sum_i alpha_i C_i, B_2)
h1 =h1 (sum_i alpha_i C_i)wedge B_2
h1 =h1 sum_i alpha_(i)(C_i wedge B_2)
h1 =h1 sum_i alpha_i overline(f)(C_i, B_2)$,
it is therefore bilinear.
]
#definition[*Grassman's exterior algebra* (v2):
  graded algebra
  $ ext V = scripts(plus.circle.big)_k ext^k V = ext^0 V plus.circle ext^1 V plus.circle dots $
  with *exterior product* $wedge: ext^p V times ext^q V arrow ext^(p + q) V$
  ]
#definition[#box(width:2cm)[*Multivector*] $M=sum_k G_k in ext V, G_k in ext^k V$.]
#definition[*Grassmann's exterior algebra* (v1):
  $ext V = T(V)\/I$ s.t.\
  #align(center,[$T(V)$: Tensor algebra on $V quad I$: two-sided ideal where \
  $I=span({cv(v) otimes cv(v)#h(0em): cv(v) #h(0.1em) in #h(0.1em) V})=span({cv(v) otimes cv(w)+cv(w)otimes cv(v)#h(0em): cv(v),cv(w) #h(0.1em) in #h(0.1em) V})$])
  ]

#definition[*Exterior/Wedge/Outer product*:
  $vv wedge cv(w) = vv otimes cv(w) + I$]

#example[#v(-1.3em)
  $ #h(.7em)underbrace(M, "multivector" \ and.big(V))
        = underbrace(5, "scalar" \ and.big^(0)(V))
        + underbrace(3cvec(a) + 2cvec(b),"vector" \ and.big^(1)(V))
        + underbrace(4cvec(b) wedge cvec(c) + 10 cvec(a) wedge cvec(c),"bivector" \ and.big^(2)(V))
        - underbrace(2 cvec(c) wedge cvec(a) wedge cvec(d) ,"trivector" \ and.big^(3)(V)) $
]
#figure(caption: [Illustration of exterior algebra and its elements])[
  #grid(columns: 2, row-gutter: .4em, image("../gacs/figs/ext_alg_vectors_bigger.svg", width: 100%)
  ,image("../gacs/figs/ext_alg_vectors_bivectors_trivectors_scalars_multivectors_bigger.svg", width: 100%), $V$,$ext V$)
]

// #note[_multivector_
//    #h(0cm)=#h(0cm) $sum_k lambda_k (k$_-vector_$) #h(0cm)=#h(0cm) sum_k lambda_k (sum_j lambda_j (k$-_blades_$))$
//   ]
#definition[*Grassman's exterior algebra* (v3):
   associative alg. $ext h0 V$ s.t.:
  + Contains $V$
  + *Exterior/Wedge/Outer product* $wedge: ext V times ext V arrow ext V$
  + Alternating on $V$, i.e., $vv wedge vv = 0$
  ]
  #let wii = 3cm
#note[Some inherited properties for vectors
  #h(1cm)#list([
    #box(width: wii)[_Associative_:] $aa wedge (bb wedge cc) = (aa wedge bb) wedge cc$
    ],[
      #box(width: wii)[_Bilinear_:] $(alpha aa + beta bb) wedge cc = alpha aa wedge cc + beta bb wedge cc$
      #box(width: wii)[] $aa wedge (alpha bb + beta cc) = alpha aa wedge bb + beta aa wedge cc$
    ],[#box(width: wii)[_Antisymmetric_:] $aa wedge bb = -bb wedge aa$
    ],[#box(width: wii)[_Identity_:] $1 wedge aa = aa wedge 1 = aa$
    ],[#box(width: wii)[_Zero_:] $0 wedge aa = aa wedge 0 = 0$
    ],[#box(width: wii)[_Homogeneous_:] $alpha wedge aa = aa wedge alpha = alpha (1 wedge aa) = alpha aa$
    ])
  ]
#theorem[_Antisymmetry in a blade_:
  $ vv_(sigma(1)) wedge dots.c wedge vv_(sigma(k)) = "sg"(sigma) vv_(1 dots k)$
]
#corollary[*N* 1.2 is identical for $k$-vectors (incl. blades), except
  - #box(width: 3cm)[_Antisymmetric_:] $A wedge B = (-1)^(|A||B|) B wedge A$
]
#lemma[
  _Alternate_ property for $k$-blades $vv_(1 dots k)$
  $ vv_i=vv_j "for some" i eq.not j arrow.double.long vv_(1 dots k) = 0 $
]
#note[
  $forall k in NN: 0 in ext^k V$ (zero is a scalar, vector, bivector, etc...)
]
#proposition()[
  $S lt.eq V arrow.double ext S lt.eq ext V$
]
== Linear independency
#theorem([_Exterior linear dependency_])[
  $ vv_1,dots,vv_k "are linear dependent" arrow.l.r.double.long vv_(1 dots k) = 0 $
]
#proof()[]
#corollary([_Vector on hyperplane_])[
  $xx in span({vv_1,dots,vv_k}) arrow.double.r.l xx wedge vv_(1 dots k) = 0$
]
#proof()[]
#corollary([_Parallel vectors_])[
  $xx parallel yy arrow.double.r.l xx wedge yy=0$
]
#note[#box(width: 2.2cm)[for $idx(r),idx(s) in uidx^n$,] $vv_idx(r) wedge vv_idx(s) = delta_(idx(r) sect idx(s) = emptyset)vv_(idx(r)idx(s)) = delta_(idx(r) sect idx(s) = emptyset) sigma_(idx(r)idx(s))vv_(overline(idx(r)idx(s)))$
]
#note[#box(width: 2.2cm)[for $idx(i),idx(j) in oidx^n$,] $vv_idx(i)wedge vv_idx(j) = delta_(idx(i)sect idx(j)=emptyset)sigma_(idx(i)idx(j))vv_(idx(i)union idx(j))$.]

== Grades
#definition[#box(width:3cm)[*Grade*:] $|A|=k "if" A in ext^k V$]
#definition[#box(width:2.8cm)[*Grade projection*:] $(sum_k A_k)_i=A_i$]
#example[
  $ gproj(4
          +cvec(e)_2+3cvec(e)_3
          +34cvec(e)_(23) + 2cvec(e)_(12),2)
    = 34cvec(e)_(23) + 2cvec(e)_(12)$

  ]#definition[#box(width:2.8cm)[*Even projection*:]
  $eproj(A) = sum_(k=0)^(floor(n/2)) gproj(A,2k) $
  ]
#definition[#box(width:2.8cm)[*Odd projection*:]
  $oproj(A) = sum_(k=0)^(ceil(n/2)-1) gproj(A,2k+1)$
  ]
#theorem[ _Outer prod. + grade projection_:
  $ A wedge B = #h(0em) limits(sum)_(k=0)^n limits(sum)_(l=0)^n (A)_k wedge (B)_l$
]
#proof[#todo]
#theorem[ _Grade raising property_:
  $(A wedge B)_k=sum_(i=0)^k (A)_i wedge (B)_(k-i)$
]
#proof[#todo]
#theorem[ $A = eproj(A)+oproj(A) in ext V$]
#proof[#todo]

== Blade spaces
#definition[#box(width:2.2cm)[Blade *space*] $[bl(A)]
  #h(0cm)=#h(0cm){vv in V#h(0cm): vv wedge bl(A)#h(0.05cm)=#h(0.05cm)bl(A)wedge vv = 0}<V$.
  ]

#theorem[_Blade space equivalence_
  $bl(A) #h(0em)=#h(0em) lambda vv_(1 dots k) #h(0.2em)arrow.double.l.r#h(0.1em) span({vv_1#h(-.1em),dots,vv_k})#h(0em)=#h(0em) [bl(A)] $
]
#proof[#todo]
#theorem[$ext^k$ _is an isomorphism on spaces_: $ext^k [bl(A)_k] = span(bl(A)_k) $]
#proof[#todo]
#theorem[_Blade complement_
   $[bl(A)_k]<[bl(A)_l] arrow.double exists bl(A)_(l-k)#h(0cm):bl(A)_l=bl(A)_k wedge bl(A)_(l-k)$
]
#proof[#todo]
#theorem[ _Necessary_ and _sufficient_ conditions for a blade
  $ A eq.not 0 in ext^k V "is a blade" arrow.double.l.r A wedge A = 0 in ext^(2k) V arrow.double.l.r [A] lt.eq V $
]
#proof[#todo]
#definition[ Let blades $bl(B)=lambda bl(A)$.
  - #box(width: 4.8cm)[*Attitude* of $bl(A)$] $[bl(A)] $
  - #box(width: 4.8cm)[*Weight* of $bl(A)$ *relative to* $bl(B)$] $lambda$
  - #box(width: 4.8cm)[*Orientation* of $bl(A)$ *relative to* $bl(B)$] $"sign"(lambda)$ (1:same, -1:different)
  ]

#definition[#box(width:3.3cm)[*Blade containment*:]
  $bl(A) subset.eq bl(B)$ if $[bl(A)] lt.eq [bl(B)]$
]
#theorem([_Cond. for containment_])[
  $aa_(1 dots k) h1 subset.eq h1  bl(B) arrow.double.l.r forall i h1= h1 1,dots,k h0: aa_i wedge bl(B) h1 = h1 0 $
]
#proof[#todo]
#definition[#box(width:4.1cm)[*Blade union* (*join*):] $[bl(A) union bl(B)]=[bl(A)]+[bl(B)]$]
#definition[#box(width:4.1cm)[*Blade intersection* (*meet*):] $[bl(A) sect bl(B)]=[bl(A)]sect[bl(B)]$]

== Basis and dimension
#definition[*Basis*:
  $quad cal(B)_V#h(0cm)=#h(0cm){ee_i}_(i=1)^n
  #h(0.2cm)#h(0.1cm)
  cal(B)_(ext^k V)#h(0cm)=#h(0cm){ee_idx(i)}_(idx(i)in oidx_k^n)
  #h(0.2cm)#h(0.1cm)
  cal(B)_(ext V)#h(0cm)=#h(0cm){ee_idx(i)}_(i in oidx^n) $
  ]
#example[
  #move(dy:-1.3em,dx:3em,align(center)[
    #tablex(columns:5, auto-vlines: false, align:center+horizon,inset:.3em, auto-lines: false,
      [$k$],vlinex(),[0],[1],[2],[3],hlinex(),
      [basis for $ext^(k)(V)$],
      [{1}],
      [{$cvec(e)_1,cvec(e)_2,cvec(e)_3$}],
      [{$cvec(e)_(12), cvec(e)_(23), cvec(e)_(13)$}],
      [{$cvec(e)_(123)$}]
    )#v(-1.4em)
  ])
]
#definition[ *Dimension*: $ quad dim ext^k V #h(0em)= |cal(B)_(ext^k V)| quad dim ext V = |cal(B)_(ext V)|$]
#theorem[ $dim ext^k V = binom(n, k)$ and $dim ext V = sum_k binom(n, k) = 2^n$]
#proof[#todo]
#definition[ *Pseudoscalar* $ps(k) = ee_(1 dots k) in ext^k V$ if $cal(B)_V = {ee_i}_(i=1)^n$]
== Some operations
#definition[Blade *reversion*:
  $bl(A)=aa_(idx(i)) arrow.double rev(bl(A))=aa_(#h(.1em)rev(idx(i)))$]
#definition[$k$-Vector *reversion*:
  $A_k=sum_i bl(A)_i arrow.double rev(A)_k=sum_i rev(bl(A))_i$]
#theorem[_Antisymmetric prop. of reversion_: $rev(A)_k = (-1)^((k(k-1))/2)A_k$]
#proof[#todo]
#note[#v(-.45cm)
  #align(center,tablex(columns: 10,inset: (top:.5em, rest:.3em),auto-lines: false, $k$,vlinex(),0,1,2,3,4,5,6,7,$dots$,hlinex(),$rev(A)\/A$,[+],[+],[-],[-],[+],[+],[-],[-],$dots$))
]
#definition[Multivector *reversion*
   $rev(A)=sum_k (#h(-.0em)A)^revalone_k$
  ]
#theorem[
  $rev(A)=sum_k (-1)^((k(k-1))/2)(A)_k$
]
#proof[#todo]
#theorem[
  $(rev(A))^revalone = A, quad (A wedge B)^revalone = rev(B) wedge rev(A), quad (A)^revalone_k = \(rev(A)\)_k$
]
#proof[#todo]

#definition[Blade *involution*
   $ invol(bl(A))_k = (aa_(1 dots k))^involalone = (-aa_1)wedge dots wedge (-aa_k) = (-1)^k bl(A)_k $
  ]
#definition[Multivector *involution*
   $invol(A) = sum_k (A)^involalone_k = sum_k (-1)^k (A)_k $
  ]
#theorem[
  $invol(A) = eproj(A) - oproj(A)$
]
#proof[#todo]
#theorem[
  $\(invol(A)\)^involalone = A "and" (A wedge B)^involalone = invol(A)wedge invol(B)$
]
#proof[#todo]
#note[#v(-.45cm)
  #align(center,tablex(columns: 10,inset: .3em,auto-lines: false, [k],vlinex(),0,1,2,3,4,5,6,7,$dots$,hlinex(),[$invol(bl(A))\/bl(A)$],[+],[-],[+],[-],[+],[-],[+],[-],$dots$))
]
#theorem[
  _Reversion and involution commute_: $invol(rev(A))=rev(invol(A))$
]
#proof[#todo]
#definition[$M^(involalone k)$ (Composing $k$ involutions) and $breve(M) = M^(involalone n+1)$]

#definition[*Clifford conjugation*
   $cconj(A)=rev(invol(A))=invol(rev(A))$
  ]
#theorem[_Properties of Clifford conjugation_ #v(.3em)
  #grid(columns:(.3fr, .7fr), row-gutter: .5em,
    [- $cconj(cconj(A))=A$],
    [- $cconj(A wedge B) = cconj(B) wedge cconj(A)$],
    [- $cconj((A)_k) = (cconj(A))_k$],
    [- $cconj(A) = sum_(k=0)^n (-1)^((k(k+1))/2)(A)_k$]
  )
]
#proof[#todo]
#note[
  #align(center,move(dx:0em,dy:-1.5em,tablex(columns: 3,inset: (x:.2em, y:.3em), auto-lines: false,
      [*Operation*],vlinex(),[*Relative Weight*],vlinex(),[*Pattern*],hlinex(),
      [Reversion],$(-1)^(k(k-1)\/2)$,$++--++--$,
      [Grade involution],$(-1)^k$,$+-+-+-+-$,
      [Clifford conjug.],$(-1)^(k(k+1)\/2)$,$+--++--+$)))#v(-.6cm)
]
== Outermorphisms
#definition([Outermorphism])[
      A _linear_ function $f:ext(V) arrow ext(W)$ is an *outermorphism* if it is also an #text(myred)[_unital algebra homomorphism_]:
      $ tau(M wedge N) &= tau M wedge tau N, quad forall M,N in ext(V) $
  ]
#note[Extending linear transformations into outermorphisms
  $
    sigma: V arrow W wide tau[aa] = sigma[aa] wide tau[aa wedge bb] = sigma[aa] wedge sigma[bb]
  $
]
#note[Extracting linear transformations from outermorphisms
  $ sigma=tau|_(ext^1 V) $
]
#theorem[ $tau alpha = alpha$ and $tau(bl(A)_1 wedge dots.c wedge bl(A)_k) = tau bl(A)_1 wedge dots.c wedge tau bl(A)_k$
  ]
#proof[#todo]
#theorem([Outermorphisms properties])[
      - #box(width:4.1cm)[_Blades map to blades_:] $[tau bl(A)]=tau([bl(A)]) lt.eq V$
      - #box(width:4.1cm)[_Grades are preserved_:] $tau (gproj(M,k))=gproj(tau M,k)$ and \ #v(.5em)
      #h(4.5cm) $tau (ext^p S)=ext^p tau S$
      - #box(width:4.1cm)[_Preservation of factorization_:] $bl(C) subset.eq bl(A) sect bl(B) arrow.double tau bl(C) subset.eq tau bl(A) sect tau bl(B)$
      - #box(width:4.1cm)[_Not homogeneous_:] $lambda tau$ ($lambda eq.not 1$) is not an outermorphism
  ]
#proof[#todo]

= Metric spaces

== Inner and scalar products

#definition[*Inner product*:
   bilinear form $inner(dot,dot)#h(0em):ext^k#h(0cm) V #h(0cm)times#h(0cm) ext^k#h(0cm) V #h(0.1em)arrow#h(0.1em) FF$, s.t.
  $ inner(aa_(1 dots k),bb_(1 dots k))=det(inner(aa_i, bb_j)) quad "and" quad inner(alpha, beta)= overline(alpha) beta $
  ]
#definition[*Scalar product*
   $A ast B = inner(rev(A),B)$, for $A,B in ext^k V$
  ]

#definition[
  *Norm* $norm(dot)$ *induced by* $inner(dot,dot)$: $norm(A)=sqrt(inner(A,A))=sqrt(rev(A) ast A)$
]
#note[In the real case, $norm(aa_(1 dots k))$ is the $k$-dimensional volume of the parallelotope spanned by $aa_1,dots,aa_k$. In the complex case, $norm(A)$ gives the $2p$-dimensional volume of that spanned by $aa_1,i aa_1,dots,aa_k,i aa_k.$]

#note[
  For the rest of the text, $V,W,...$ are inner product spaces.
]

== Contractions
// #definition[
//   *Orthogonal* blades: $inner(bl(A),bl(B))=0 arrow.double.long.l.r A perp B $
// ]
#align(center)[
  #box(width: 30%,image("../gacs/figs/ext_alg_m_left_contraction.svg", width: 100%))
  #box(width: 30%, image("../gacs/figs/ext_alg_m_right_contraction.svg", width: 100%))
]
#definition[
  Only the *left* ($M lc N$) and *right* ($ N rc M$) *contractions* of a
  *contractor* $M h0 in h0 ext h0 V$ and a *contractee* $N h0  in h1  ext h0 V$, satisties $forall #h(-.1em) X h0  in h0  ext h0 V$
  $ inner(L,M lc N) = inner(M wedge L, N) quad "and" quad inner(L,N rc M) = inner(L wedge M, N) $
]

#theorem()[
  For $(vv_1,dots, vv_n)_nperp basis V$ and $idx(i),idx(j)in oidx^n$
  $ vv_idx(i) lc vv_idx(j) = delta_(idx(i) subset idx(j)) sigma_(idx(i)(idx(j) without idx(i))) vv_(idx(j)without idx(i))
  quad "and" quad
  vv_(idx(j)) rc vv_idx(i) = delta_(idx(i)subset idx(j)) sigma_((idx(j)without idx(i))idx(i))vv_(idx(j) without idx(i)) $
] <t:contraction_blades>

#proof[
  $ forall idx(k) in oidx^n: inner(vv_idx(k),vv_idx(i) lc vv_idx(j))=inner(vv_idx(i) wedge vv_idx(k), vv_idx(j)) eq.not 0 arrow.double.l.r idx(i) sect idx(k)=emptyset$ and $idx(j)=overline(idx(i)idx(k))$ (so $idx(i) h1 subset h1 idx(j)$ and $idx(k)=idx(j) without idx(i)$), in which case $inner(vv_idx(k),vv_idx(i) lc vv_idx(j)) h1= h1 sigma_(idx(i)(idx(j)without idx(i)))$.
]

#note[$lambda lc M = overline(lambda)M$ and $M lc lambda = overline(kappa)lambda$ where $kappa = (M)_0$.]

#example[For $(vv_1,dots,vv_4)_nperp, thick thick M h1= h1 vv_4+i vv_(234), thick thick N h1 = h1 (i-3)vv_(34)+vv_(124)$:
$ M lc N & h1= h1 (i-3)vv_4 lc vv_(34) + vv_4 lc vv_124 - i(i-3)vv_(234) lc vv_(34)-i vv_(234) lc vv_(124) \
& h1=h1 (3-i) vv_4 lc vv_(43) + vv_4 lc vv_(412) + 0 + 0 = (3-i)vv_3 + vv_(12) \
N rc M & h1 =h1 (i-3)vv_3 + vv_(12) "and" N lc M = M rc N = (1-3i)vv_2 $]

#theorem()[Let $G in ext^p V, H in ext^q V$ and $M, N in ext V$
  + #box(width:3.3cm)[_Grade decreasing_:] $G lc H, H lc G in ext^(q-p) V$
  + #box(width:3.3cm)[_Equal grade_:] If $p=q$ then $G lc H = H rc G = inner(G, H)$
  + #box(width:3.3cm)[_Contracting too much_:] If $p > q$ then $G lc H = H rc G =0 $
  + #box(width:3.3cm)[_Contraction/involution_:]$ G  lc  M h0 = h0 M^(involalone h0 p) lc invol(G) quad h2 h2 M lc H  h1 = h1 H rc M^(involalone(q+1))$
  + #box(width:3.3cm)[_Containment_:] $forall S h1 < h1 V:N h1 in h1 ext S arrow.double M lc N in ext S$.
  + $(M lc N)^involalone = invol(M) lc invol(N), (M lc N)breve = breve(M) lc breve(N)$, but $(M lc N)^revalone = rev(N) rc rev(M)$
]
#proof[
  (1-3) Follow from @t:contraction_blades and linearity, with $G=sum_(idx(i) in oidx_(p)^n) h1 lambda_idx(i) vv_idx(i)$ and $H=sum_(idx(j) in oidx_(q)^n) h1 kappa_idx(j) vv_idx(j)$ for $ lambda_idx(i),kappa_idx(j) in FF$. (5) Likewise, extending $(v_1,dots,v_r)_nperp basis S$ to $V$, so $M=sum_(idx(i) in oidx^n) lambda_idx(i) vv_idx(i)$ and $N=sum_(idx(j) in oidx^r) kappa_idx(j) vv_idx(j)$. (6) $inner(L, (M lc N)^revalone) = inner(rev(L), M lc N) h1 = h1 inner(M wedge rev(L), N) = inner((M wedge rev(L))^revalone, rev(N))= inner(L wedge rev(M), rev(N)) = inner(L, rev(N) rc rev(M))$. Likewise for $hat$ and $breve$, but without swapping $L$ and $M$.
]

#theorem()[
  Let $vv, ww_1,dots,ww_q in V, H in ext^p V$ with $p lt.eq q$, and $L,M,N in ext V$. Then:
  + #box(width: 3cm)[_Contractor split_:] $(L wedge M) lc N = M lc (L lc N) $
  + #box(width: 3cm)[_Blade contractee_:] $H lc ww_(1 dots q) = sum_(idx(i) in oidx_p^q) sigma_(idx(i)idx(i)') inner(H, ww_idx(i)) ww_(idx(i)')$
  + #box(width: 3cm)[_Vector/blade contrac._:] $vv lc ww_(1 dots q) = sum_(i=1)^q (-1)^(i-1)inner(vv,ww_idx(i))ww_((i)')$
  + #box(width: 3cm)[_Contractee split_:] $vv lc (M wedge N) = (vv lc M) wedge N + invol(M) wedge(vv lc N) $
] <t:contractee_split>
#proof[#todo]
#corollary([_Blade contractor_])[ For $vv_1 ,dots,vv_p in V$, $M in ext V$
  $ vv_(1 dots p) lc M = vv_p lc (dots.c lc (vv_1 lc M)dots.c) $
]
#proof[#todo]
#corollary([_Adjoint of @t:contractee_split iv_])[ For $vv in V$ and $M,N in ext V$:
  $ vv wedge (M lc N) = (M rc vv) lc N + invol(M) lc (vv wedge N) $
]
#proof[#todo]
#corollary()[
  For $vv in V$ and $M in ext V$:
  $  vv wedge (vv lc M) + vv lc (vv wedge M) = norm(vv)^2 M $
]
#proof[#todo]
#note[By convention, $tau M lc tau N = (tau M) lc (tau N)$]
=== Contraction inverse

#definition()[If $norm(G) eq.not 0$ for $G in ext^k V$, then its  *inverse* w.r.t. $med thick lc thick $ is  $ G^(-1) h1 =h1 G\/norm(G)^2. $]

#theorem()[$G lc G^(-1) = 1$]
#proof[
  $G lc G^(-1)=G lc G/inner(G,G)=inner(G,G)/inner(G,G)=1$
]


=== Orthogonality
#corollary([_Orthogonal complement_])[
  Let $S,T h0 < h0 V$, $M h0 in h0 ext h0 S$ and $N h0 in h0 ext h0 T$
  + If $L thick h2 in ext (T^perp) thick h2$ then $L thick h2 lc (M wedge N)=(L lc M) wedge N$
  + If $H in ext^(p)(S^perp)$ then $H lc (M wedge N)= quad h2  M^(involalone p) wedge (H lc N)$
]
#proof[#todo]

#corollary()[
  $vv wedge M = vv lc M = 0 arrow.double.l.r vv = 0 "or" M = 0$
]
#proof[#todo]

#corollary()[
  For $0 eq.not vv in V$ and $M in ext V$:
  + $vv wedge M = 0 arrow.double.l.r M = vv wedge N$ for $N in ext V$. In particular, $N=(vv lc M)/(norm(vv)^2)$
  + $vv lc M = 0 arrow.double.l.r M = vv lc N$ for $N in ext V$. In particular, $N = (v wedge M)/(norm(vv)^2)$
]
#proof[#todo]
#corollary()[
  If $M eq.not 0, {vv in V: vv wedge M = 0} perp {ww in V: ww lc M = 0}.$
]
#proof[#todo]
#theorem()[
  For disjoint $S,T < V$, $vv in V, thick thick 0 h1 eq.not h1  M h1 in h1 ext S, thick thick 0 h1 eq.not h1 N h1 in h1 ext T$ $ vv lc (M wedge N) = 0 arrow.double.l.r vv lc M = vv lc N = 0 $
]
#proof[#todo]

#theorem([_Contractions and orthogonality_])[ For $vv in V$ and $M in ext V $,
  $ vv lc M = 0 arrow.double.l.r M in ext([vv]^perp) $
]
#proof[#todo]
#corollary()[
  $[bl(B)]={vv in V: vv lc bl(B) = 0}^perp$, for a blade $bl(B)$.
]
#proof[#todo]
=== Partial orthogonality
#definition[
  For $S,T lt.eq V$, $S$ is *partially orthogonal* to $T$ ($S pperp T$) if there is a nonzero $cv(s) in S$ s.t. for all $cv(t) in T: inner(cv(s),cv(t))=0$.
]
#theorem()[
  $S pperp T arrow.double.r.l T^perp sect S eq.not {0}$
]
#proof[#todo]
#note[For a blade $bl(B) eq.not 0$, $[bl(B)] pperp S arrow.double.l.r Rho_(S)bl(B) = 0$]
#theorem()[
  For blade $bl(B) h1 eq.not h1 0$ and $S h1 < h1 V$, 
  $ [bl(B)] pperp S arrow.double.l.r h1 forall M h1 in h1  ext h0 S h0: bl(B) lc M h1 = h1 0. $
]
#proof[#todo]

#corollary()[
  $S h1 lt.eq h1 V, M h1 in h1 ext S, N h1 in h1 ext (S^perp) arrow.double M lc N h1 = h1 macron(lambda)N$ for $lambda h1= h1 (M)_0$.
]
#proof[#todo]

== Projection
#definition()[*Orthogonal projection* $Rho_(cal(S))h0:ext V arrow cal(S)$, where $cal(S)lt.eq ext V$]
#proposition[
  $S lt.eq V arrow.double Rho_(S)=\[Rho_(ext(S))\]$
]
#definition()[
  If $M in ext V$, then $Rho_M = Rho_span(M)$
]
#definition[
  The *orthogonal projection* of blade $bl(A) in ext^p V$ onto an invertible blade $bl(B) in ext^q V$ is $Rho_bl(B) bl(A) = (bl(A) lc bl(B)) lc bl(B)^(-1) $
]
#theorem[
  $p=q arrow.double Rho_bl(B) bl(A) = inner(bl(B),bl(A))/norm(bl(B))^2 bl(B)$
]
#proof[#todo]
#theorem[
  $Rho_bl(B)$ is an outermorphism
]
#let binv = $bl(B)^(-1)$
#let blb = $bl(B)$
#proof[#v(-1.3em)
  $ P_bl(B)(bl(A)) wedge P_bl(B)(bl(C)) &= P_bl(B)(bl(A)) wedge ((bl(C) lcont bl(B))lcont binv)      \
&= (P_bl(B)[bl(A)] lcont (bl(C) lcont blb)) lcont binv \
&= ((P_bl(B)[bl(A)]wedge bl(C)) lcont blb)lcont binv \
&= -((bl(C) wedge P_bl(B)[bl(A)])lcont blb) lcont binv \
&= - (bl(C) lcont (P_bl(B)[bl(A)] lcont blb)) lcont binv \
    &= - (bl(C) lcont ((bl(A) lcont blb) wedge (binv lcont blb)))lcont binv \
&= - (bl(C) lcont (bl(A) lcont blb))lcont binv \
&= - ((bl(C) wedge bl(A))lcont blb)lcont binv \
&= ((bl(A) wedge bl(C)) lcont blb) lcont binv \
&= P_bl(B)(bl(A) wedge bl(C)) $#v(-1em)]

#theorem()[
  $Rho_B$ is a projection, i.e., $Rho_B compose Rho_B = Rho_B$
]
#proof[#todo]


=== Left and right contractions
#theorem([_Left/right contractions together_])[ For $L,M,N in ext V$:
  $ L lc (M rc N) = (L lc M) rc N $
]
#proof[#todo]
#note[In general, $L rc (M lc N) eq.not (L rc M) lc N$, for $ L, M,N in ext V$.]

#theorem()[
  Let $bl(A), bl(B)$ be blades, and $M, N in ext V$.
  + If $[bl(A)]subset.eq [bl(B)]$ then $(bl(A) rc M) lc bl(B) = Rho_(bl(A)) (M)wedge (bl(A) lc bl(B))$
  + If $N in ext[bl(B)]$ then $(M rc N) lc bl(B) = N wedge (M lc bl(B))$
]
#proof[#todo]

#corollary()[ For a unit blade $bl(B)$ and $M in ext V$,
  $ bl(B) rc M lc bl(B) = Rho_(bl(B)) (M) $
]
#proof[#todo]
#corollary()[For $M,N in ext V$ and a blade $bl(B) eq.not 0$:
$ M = N lc bl(B) arrow.double M=(bl(B) rc M)/(norm(bl(B))^2)lc bl(B) $]
#proof[#todo]
== Geometric interpretation

#note()[Consider blades $A in ext^p V$ and $B in ext^q V$.]
#definition()[
  $bl(B)=bl(B)_parallel wedge bl(B)_perp$ is a *projective-orthogonal* (*PO*) *factorization* w.r.t. $bl(A)$ if $bl(B)_parallel$ and $bl(B)_perp$ are subblades of $bl(B)$ of grades $m=min{p,q}$ and $q-m$, resp., with $[bl(B)_perp] h1 perp h1 [A]$ and $[bl(B)_perp] h1 perp h1 [bl(B)_perp]$.
]
#note[
  $Rho_(bl(B))(bl(A))=Rho_(bl(B)_parallel)(bl(A)) = (inner(bl(B)_parallel, bl(A)))/(norm(bl(B)_parallel)^2) bl(B)_parallel$ ($=0$ if $p > q = m$).
]
#theorem()[$bl(A) lc bl(B) = inner(bl(A), bl(B)_parallel)bl(B)_perp$]

#definition()[$Theta_(S,T)=cos^(-1)(norm(Rho_(bl(B)) (bl(A))))/(norm(bl(A)))$ is the *asymmetric angle* of $S=[bl(A)]$ with $T=[bl(B)]$.]

#theorem()[
  + $Theta_(S,T)=0 arrow.double.l.r S subset.eq T$
  + $Theta_(S,T)=pi\/2 arrow.double.l.r S pperp T$
  + $p=q arrow.double Theta_(S,T)=Theta_(T,S)=cos^(-1)(|inner(bl(A),bl(B))|)/(norm(bl(A))norm(bl(B)))$
]
#note[In general $Theta_(#h(-.1em) S,T) h0 eq.not h0 Theta_(T,S)$, reflected by contraction asymmetry.]
#theorem()[If $A,B eq.not 0$, then
  $bl(A) lc bl(B) = 0 arrow.double.l.r [bl(A)] pperp [bl(B)]$. If $bl(A) lc bl(B) eq.not 0$, it is the only (p-q)-blade s.t:
  + $[bl(A) lc bl(B)]=[bl(A)]^perp sect [bl(B)]$
  + $norm(bl(A) lc bl(B)) = norm(Rho_(bl(B))) norm(bl(B)) = norm(bl(A))norm(bl(B)) cos Theta_([bl(A)],[bl(B)])$
  + $Rho_(bl(B))(bl(A))wedge (bl(A) lc bl(B))$ has the orientation of $bl(B)$
] <t:lc_norm>
#corollary()[
  For right contraction, repeat @t:lc_norm, except
  3. $(bl(B) rc bl(A)) wedge Rho_(bl(B))(bl(A))$ has the orientation of $bl(B)$
]
#note()[@t:lc_norm ii works for all blades, as $[A] h1 perp h1 [B] arrow.double.l.r Theta_([bl(A)],[bl(B)])=pi/2$]

== Exterior and interior product operators

#definition()[(*Left*) *exterior* and *interior products by* $M in ext V$ are defined, resp., by $e_(M)(N)=M wedge N$ and $iota_(M)(M lc N)$ for $N in ext V$.]
#note[In $N$, both are linear. In $M$, $e_M$ is linear and $iota_M$ is conjugate-linear.]

#theorem()[For $M,N in ext V$
  $ e_M e_N = e_(M wedge N) quad "and" quad  iota_M iota_N = iota_(N wedge M) h2 ("note the order") $
]
#corollary()[
  If $M$ is odd, or a nonscalar blade, then $e_M^2=iota_M^2=0$, so
  $ im e_M subset.eq ker e_M "and" im iota_M subset.eq ker iota_M $
]
#proof[#todo]
#theorem()[For $L,M,N in ext V$
  + $M lc (M wedge N) = 0 arrow.double.l.r M wedge N = 0$.
  + $M wedge (M lc N) = 0 arrow.double.l.r M lc N = 0$.
  + $L = M lc N arrow.double.l.r L = M lc (M wedge K)$ for some $K in ext V$.
  + $L = M wedge N arrow.double.l.r L = M wedge (M lc K)$ for some $K in ext V$.
]
#proof[#todo]
#definition()[
  For $S h1 < h1 V$ and $M h1 in h1 ext V$, let $M wedge ext S h1 = h1 {M wedge N h0: N h1 in h1 ext V}$.
]
#theorem()[For any blade $bl(B)$, $im e_bl(B)=bl(B) wedge ext([bl(B)]^perp)$ and if $bl(B) eq.not 0$, then $im iota_bl(B)=ext([bl(B)]^perp)$.
]
#proof[#todo]
#theorem()[
  For a unit blade $bl(B)$ and $M in ext V$
  + $B lc (B wedge M) = Rho_(im iota_bl(B))(M) = Rho_([bl(B)]^perp)(M)$
  + $bl(B) wedge (bl(B) lc M) = Rho_(im e_(bl(B)))(M)$
]
#proof[#todo]
#corollary()[Let $bl(B) eq.not 0$ be a blade, and $M in ext V$
  + If $M = bl(B) lc N$ for $N in ext V$, then $M = bl(B)lc (bl(B wedge M))/(norm(bl(B))^2)$
  + If $M = bl(B) wedge N$ for $N in ext V$ then $M=bl(B)wedge (bl(B)lc M)/(norm(bl(B))^2)$
]
#proof[#todo]
#corollary()[
The restricted maps $ext ([bl(B)]^perp)$ #h(.2cm) #box(move(dy:.6em,dx:.2em,pad(bottom:.0cm,stack(
    dir: ttb,
    [#v(-1.1em)$text(size:#8pt,e_bl(B))$],
    [#v(-.0em)#scale(x:200%,$#h(-.05cm) arrow.long$)],
    [#v(-.2em)#scale(x:200%,$#h(-.05cm)arrow.long.l$)],
    [#v(-.3em)$text(size:#8pt,iota_bl(B))$]
  )))) #h(.3cm) $bl(B) wedge ext([bl(B)]^perp)$ are mutually inverse isometries, for a unit blade $bl(B)$.
  #v(.1cm)
]
#proof[#todo]
== Higher Order Leibniz Rule

#theorem()[For $vv_1,dots,vv_p in V$ and $M,N in ext V$
$ vv_(1 dots p) lc (M wedge N) = sum_(idx(i) in oidx^p) sigma_(idx(i)idx(i)') (vv_idx(i)' lc M^(involalone|idx(i)|)) wedge(vv_i lc N) \
  vv_(1 dots p) wedge (M lc N) = sum_(idx(i) in oidx^p) sigma_(idx(i)idx(i)') (M^(involalone |idx(i)'|) rc vv_i) wedge (vv_idx(i)' wedge N)
  $
]
#proof[#todo]
== Adjoint operator

#theorem[
  The outermorphism of the adjoint $adj(tau): W arrow V$ of a linear transformation $tau: V arrow W$ is the adjoint of its outermorphism, i.e., $ inner(M,adj(tau) N)= inner(tau M, N) , forall M in ext V, forall N in ext W $
]
// #theorem[
//   If $tau$ is an isometry, then $adj(tau) compose tau = tau compose adj(tau) = iota$
// ]

== Outermorphisms and contractions
#theorem[
  The outermorphism of the inverse (assuming it exists) is the inverse of the outermorphism, i.e.,
  $tau^(-1) compose tau = iota$
]
#proof[#todo]

#theorem()[$tau(tau^(ast)M lcont N)=M lcont tau N$, for $M in ext S$ and $N in ext T$]
#proof[#todo]
#corollary()[$tau$ is invertible $h1 arrow.double h1 tau(M lcont N)h1= h1 (adj(tau))^(-1)M lc tau N$, for $M,h0 N h1 in h1 ext V$]
#proof[#todo]
#corollary()[$tau$ is an isometry $arrow.double.l.r tau(M lc N) = tau M lc tau N$ for $M,N in ext V$.]
#proof[#todo]
== Determinant formulas

For $bl(A)=aa_(1 dots p), bl(B)=bb_(1,dots,q)$, let $mt(A) in cal(M)_(p,q)=(inner(aa_i, bb_j)), mt(B) in cal(M)_(q,q)=(inner(bb_i,bb_j))$ and $mt(M) in cal(M)_(p+q,p+q)=mat(mt(0),mt(A);ct(mt(A)),B)$. Let also $mt(W) in cal(M)_(n,q)=(lambda_(i j))$ if $ww_j=sum lambda_(i j)uu_i$ for an arbitrary basis $(uu_1,dots,uu_n)$. Let $mt(A)_(idx(i),idx(j))$ be the submatrix of $mt(A)$ formed by columns with indices in $idx(j)$ and rows with indices in $idx(i)$, and $mt(A)_(:,:)=mt(A)$.


#theorem()[If $p lt.eq q$ then
$ A lc B = sum_(idx(j) in oidx^q_p) sigma_(idx(j)idx(j)')det(mt(A)_(:,idx(j)))ww_idx(j)' = sum_(idx(i) in oidx^n_(q-p)) det mat(delim:"[",mt(A);mt(W)_(idx(i),:))uu_i $]
#proof[#todo]
#theorem()[$norm(bl(A) lc bl(B))=sqrt(|det mt(M)|)$]
#proof[#todo]

#corollary()[If $B eq.not 0$, then $norm(A lc B)=sqrt(det mt(B)det(mt(A)mt(B)^(-1)ct(mt(A))))$]
#proof[#todo]

#bibliography("library.bib", style: "ieee")