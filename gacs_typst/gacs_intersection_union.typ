#import "acmt.typ": *

#show: acmt-theme.with()

/* START OF DOCUMENT */

// Status of the slide:
  // d definition
  // r corollary
  // t theorem
  // l lemma
  // n note
  // p proposition
  // g graphical pictures
  // e example
  // c complete
  // * ongoing

  #title-slide(title: "Geometric Algebra", subtitle: "Chapter 6: Intersection and Union of Subspaces", authors: "Anderson Tavares", institution-name: "", date: datetime.today().display())

  // [c, g] The phenomenology of intersection
  #hidden-slide[
    == The phenomenology of intersection
    #uncover("2-")[*_Intersection_* $f[bl(A),bl(B)], quad bl(A),bl(B) in ext^2(V)$]
    #uncover("3-")[
      - $f[bl(A),bl(B)] in ext^text(myred,1)(V)$
        #uncover("4-")[- $"grade"(bl(A) wedge bl(B)) = "grade"(bl(A)) + "grade"(bl(B))$]
        #uncover("5-")[- $#box(width:4.2cm,$"grade"(bl(A) lcont bl(B))$) = "grade"(bl(B)) - "grade"(bl(A))$]
    ]
    #uncover("6-")[
    - Planes coincide ($bl(A) = lambda bl(B)$)
      #uncover("7-")[- $f[bl(A),bl(B)] in ext^text(myred,2)(V)$]
      #uncover("8-")[
        - Linear $f$:
          #uncover("9-")[- $"grade"(bl(A)) = "grade"(f[bl(A)])$]
          #uncover("10-")[- $f[bl(A)uncover("11-", + delta bl(D)), lambda bl(A)] uncover("12-", != f[bl(A), lambda bl(A)] + f[delta bl(D), lambda bl(A)])$]
      ]
    ]
    #uncover("13-")[- Intersection and union on $ext^(k)(V)$]
      #uncover("15-")[- Weight and orientation not preserved]
      #uncover("16-")[- *_Meet_* $sect$ and *_join_* $union$]

    #align(right)[
      #v(-13cm)
      #only("2-13",image("../gacs/figs/ext_alg_meet_join_00.svg", width: 8cm))
      #only("14-",image("../gacs/figs/ext_alg_meet_join_01.svg", width: 8cm))
      // #only("3-",image("../gacs/figs/ext_alg_meet_join_02.svg"))
    ]
  ]

  #slide[
    == Partially ordered set
    #uncover("2-",definition([Poset])[*Partially ordered set* or *poset* $(P,lt.eq)$:
    + #box(width:5cm)[*Reflexivity*:] #box(width:4cm,$forall a in P$), $a lt.eq a$
    + #box(width:5cm)[*Antisymmetry*:] #box(width:4cm,$forall a,b in P$), $a lt.eq b "and" b lt.eq a arrow.double a = b$
    + #box(width:5cm)[*Transitivity*:] #box(width:4cm,$forall a,b,c in P$), $a lt.eq b "and" b lt.eq c arrow.double a lt.eq c$
    ])
    #uncover("3-",definition[
      An element $m in P$ is *maximal* if $p in P, m lt.eq p arrow.double m = p $.\ An element $n in P$ is *minimal* if $p in P, p lt.eq n arrow.double p = n $
    ])
    #uncover("4-",definition[
      The *maximum* (*largest*, *top*) of poset $P$, if it exists is $M in P$ s.t.
      $ p in P arrow.double p lt.eq M. $
      The *minimum* (*least*, *smallest*, *bottom*) of $P$, if it exists, is $N in P$ s.t.
      $ p in P arrow.double N lt.eq p $
    ])
  ]

  #slide[
    #set block(spacing: 1em, above: 1em, below: 0em)
    #show par: set block(spacing: .5em, above: .5em, below: .5em, outset: 0em)
    == Partially ordered set
    #uncover("2-",definition[An element $u in P$ is an *upper bound* of $a,b in P$ if $a lt.eq #h(.1cm) u "and" b lt.eq #h(.1cm) u$.
    The *least upper bound* $"lub"(a,b)$, it it exists, is the unique smallest upper bound for $a,b in P$
    ])
    #uncover("3-",definition[
      An element $l in P$ is an *lower bound* of $a,b in P$ if $l  lt.eq a "and" l lt.eq b$.
      The *greatest lower bound* $"glb"{a,b}$, it it exists, is the unique largest lower bound for $a,b in P$
    ])
    #uncover("4-",definition[
      An element $u in P$ is an *upper bound* for subset $S$ if $forall s in S, s lt.eq u$. An element $l in P$ is an *lower bound* for subset $S$ if $forall s in S, l lt.eq s$.
    ])
    #uncover("5-",definition[*Totally/Linearly ordered set* or *toset*: poset s.t. $ forall x,y in P, x lt.eq y "or" y lt.eq x. $ Any totally ordered subset of a poset $P$ is a *chain* of $P$. A toset on $P$ where all its subsets has a least element is a *well ordering*.
    ])
  ]
  #slide[
    == Lattice
    #uncover("2-",definition[*Lattice* is a poset $P$ where every pair of elements has a #text(myred)[_lub_] and a #text(myred)[_glb_].])
    #uncover("3-",theorem[Every toset is a lattice])
    #uncover("4-",definition[If lattice $P$ has a maximum and minimum, and every $S subset.eq P$ has a #text(myred)[_lub_] and a #text(myred)[_glb_], then $P$ is a *complete lattice*.])
  ]

  #slide[
    == The lattice of subspaces
    #uncover("2-",theorem[
      The set $cal(S)(V)$ of all subspaces of $V$ is a #text(myred)[_poset_] under set inclusion.
      - _Smallest element_: ${0}$
      - _Largest element_: $V$
    ])

    #uncover("3-",theorem[
      If $S,T$ are subspaces, then $S sect T$ is a subspace $ S sect T = "glb"{S,T} $
    ])
    #uncover("4-",corollary[If ${S_i bar i in K}$ is a collection of subspaces, then
      $ sect.big_(i in K) S_i = "glb"{S_i bar i in K} $
    ])
    #uncover("5-",definition[The *sum* $S+T$ of subspaces $S,T < V$ is defined by $ S+T={s+t bar s in S, t in T} $])
  ]
  #slide[
    == The lattice of subspaces
    #uncover("2-",definition[The *sum* of a collection ${S_i bar i in K}$ is
    $ sum_(i in K) S_i = {s_1 + dots + s_n mid(|) s_j in union.big_(i in K) S_i} $])
    #uncover("3-",theorem[The sum of any collection of subspaces of $V$ is a subspace of $V$:
    $ S+T="lub"{S,T} wide sum_(i in K) S_i= "lub"{S_i mid(|) i in K} $])
    #uncover("4-",theorem[
      $(cal(S)(V), subset.eq)$ is a complete lattice, with minimum ${0}$, maximum $V$
      $ #text(myred,"meet")="glb"{S_i mid(|) i in K}=sect.big_(i in K) S_i quad "and" quad #text(myred,"join")="lub"{S_i mid(|) i in K}=sum_(i in K) S_i $
    ])
  ]
  #slide[
    == Intersection through outer factorization
    #definition[The *meet* of blades $bl(A),bl(B) in ext(V)$ is the 1D subspace $lambda bl(M)$ where:
    $ V(bl(A)) sect V(bl(B)) = "glb"{V(bl(A)), V(bl(B))} = V(lambda bl(M)) $]
    #definition[The *join* of blades $bl(A),bl(B) in ext(V)$ is the 1D subspace $lambda bl(J)$ where:
    $ V(A) + V(B) = "lub"{V(bl(A)), V(bl(B))} = V(lambda J) $]
    #note[_join_ and _meet_ are defined only by attitude, not weight or orientation.]
    #theorem[For any pair of blades $A,B in ext(V)$, either there is a 1D space $lambda bl(M)$ such that $A = A'wedge lambda M$ and $B=lambda M wedge B'$ or .]
  ]
  #slide[
    == Intersection through outer factorization
    // #theorem([Division algorithm])[Let $bl(A), bl(B) in ext(V)$ where $"grade"(bl(B)) > 0$. Then there exist unique blades $bl(Q), bl(R) in ext(V)$ for which
    //   $ bl(A) = bl(Q) wedge bl(B) + bl(R) $
    //   where $bl(R)=0$ or $0 lt.eq "grade"(bl(R)) < "grade"(bl(B))$
    // ]
    // #definition[
    //   If $bl(R)=0$, then $bl(B)$ *divides* $bl(A)$ and we write $bl(B)|bl(A)$
    // ]
    // #definition([Greatest common divisor])[*Greatest common divisor* of $bl(A)$ and $bl(B)$, denoted by $"gcd"(bl(A),bl(B))$ is the unique blade $bl(Q)$ for which
    // - $bl(Q)|bl(A)$ and $bl(Q)|bl(B)$
    // - If $bl(R)|bl(A)$ and $bl(R)|bl(B)$, then $bl(R)|bl(Q)$
    // Furthermore, there exist blades $bl(F)$ and $bl(G)$ over $ext(V)$ for which:
    // $ "gcd"(bl(A),bl(B))= bl(F)wedge bl(A) + bl(G)wedge bl(B) $
    // ]
    #grid(columns: (.7fr, .3fr),[
      #definition[M divide A if exists A' such that ]
      #definition([Greatest common divisor of blades\*])[*Greatest common divisor* (*gcd*) of A and B is a blade M where A wedge M]
      #definition([Geometric intersection])[M lcd(A, B)
        $A sect B = M$
      ]
      - Geometric intersection: $A$
      $ bl(A) = bl(A)'wedge bl(M) "and" bl(B)=bl(M) wedge bl(B)' $
      $ bl(A) union bl(B) = bl(A)' wedge bl(M) wedge bl(B)' "and" bl(A) sect bl(B) = bl(M) $
    ],[
      #align(right)[
      #only("2-13",image("../gacs/figs/ext_alg_meet_join_00.svg", width: 8cm))
      #only("14-",image("../gacs/figs/ext_alg_meet_join_01.svg", width: 8cm))
      // #only("3-",image("../gacs/figs/ext_alg_meet_join_02.svg"))
    ]
    ])
  ]

  // Relationships between meet and join
  #slide[
    == Relationships between meet and join
  
    $ bl(B)' = bl(M)^(-1) lcont bl(B) "and" bl(A)' = bl(A)rcont bl(M)^(-1) $

    $ a' = a-m, b'=b-m, j=a+b-m, m+j=a+b $

    $ J=A union B = A wedge (M^(-1)lcont B) = (A rcont M^(-1))wedge B $

    $ 1 = J ast J^(-1) = (A wedge (M^(-1)lcont B)) ast J^(-1) = A ast ((M^(-1)lcont B)lcont J^(-1)) = (A' wedge M) ast (M^(-1) wedge (B lcont J^(-1))) = A' ast (B lcont J^(-1))  $

    $ 1 = (B lcont J^(-1)) ast (A rcont M^(-1)) = (M^(-1) wedge (B lcont J^(-1))) ast A = (M^(-1) wedge (B lcont J^(-1))) lcont A $

    $ M = A sect B = (B lcont J^(-1)) lcont A $

    $ M lcont J^(-1) = (B lcont J^(-1)) wedge (A lcont J^(-1)) $

    #theorem([Dual meet])[$dual((A sect B)) = dual(B) wedge dual(A) $]
  ]

  // Using meet and join
  #slide[
    == Using meet and join
    $ A sect B &= 1/sqrt(3)((ee_1 wedge ee_2)lcont (ee_3 wedge ee_2 wedge ee_1))lcont ((ee_1 + ee_2)wedge (ee_2 + ee_3)) \
    &= 1/sqrt(3) ee_3 lcont ((ee_1 + ee_2)wedge (ee_2 + ee_3)) \
    &= -1/sqrt(3)(ee_1 + ee_2) = -sqrt(2/3)((ee_1+ee_2)/sqrt(2))  $ 

    $ dual(A) times dual(B) = dual((dual(A) wedge dual(B))) = -dual((dual(B) wedge dual(A))) = undual((dual(B)wedge dual(A))) = dual(B) lcont A = A sect B $

  ]

  // Join and meet are mostly linear
  #slide[
    == Join and meet are mostly linear

    $ M = a sect B = (B lcont psinv(3))lcont A = inner(dual(B), a) = inner(b, a) $

    $ M = (B lcont psinv(2))lcont a = beta a $
  ]

  // Quantitative properties of the meet
  #slide[
    == Quantitative properties of the meet

    $ B sect A &= dual((dual(A) wedge dual(B))) \
    &= (-1)^((j-a)(j-b))dual((dual(B) wedge dual(A))) \
    &= (-1)^((j-a)(j-b))A sect B
    $
  ]

  // Linear transformation of meet and join
  #slide[
    == Linear transformation of meet and join

    $ f[A union B] = f[A] union f[B] $
    $ f[A sect B] = f[A] sect f[B] $

    $ f[A sect B] = (f[B] lcont f[J]^(-1)) lcont f[A] $


  ]

  #slide[
    == Offset subspaces
  ]