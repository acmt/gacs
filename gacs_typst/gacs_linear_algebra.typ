#import "acmt.typ":*

#show: acmt-theme.with()
#title-slide(title:"Linear Algebra", subtitle: "Chapter 1: Vector Spaces", authors: ("Anderson Tavares"), institution-name: "adf")

// Vector space
  // [c, d] vector space
  #hidden-slide[
    == Vector space
    #definition[
      *Vector space* over a field $K$ of *scalars*: $(V, +, dot.c)$#pause
        - Set $V$ of *vectors* $cvec(v) in V$#pause
        - *Vector addition* $+:V times V arrow.r V$ (abelian group):#pause
            - #box(width:11cm)[_Associativity_:] $cvec(u)+(cvec(v)+cvec(w))=(cvec(u)+cvec(v))+cvec(w)$ #pause
            - #box(width:11cm)[_Commutativity_:] $cvec(u)+cvec(v)=cvec(v)+cvec(u)$ #pause
            - #box(width:11cm)[_Identity_:] $exists 0 in V: cvec(v)+0=cvec(v), forall cvec(v) in V$ #pause
            - #box(width:11cm)[_Reverse_:] $forall cvec(v) in V, exists - cvec(v) in V: cvec(v) + (-cvec(v)) = 0$ #pause
        - *Scalar multiplication* $dot.c: K times V arrow.r V$:#pause
          - #box(width:11cm)[_Scalar/Field compatibility_:] $alpha(beta cvec(v))=(alpha beta)cvec(v)$ #pause
          - #box(width:11cm)[_Identity_:] $1 cvec(v) = cvec(v)$ #pause
          - #box(width:11cm)[_Distributivity w.r.t. vector addition_:] $alpha(cvec(u)+cvec(v))=alpha cvec(u) + alpha cvec(v)$ #pause
          - #box(width:11cm)[_Distributivity w.r.t. field addition_:] $(alpha+beta)cvec(v) = alpha cvec(v) + beta cvec(v)$ #pause
      Axioms 1-4: abelian group under addition, 5-8: $K$-module.#pause
    ]
  ]
  #slide[
    == Vector space
    #figure(caption: [Illustration of some vector space properties: commutativity, associativity, identity, reverse, scalar compatibility])[
      #image("../gacs/figs/linalg_vector_space.svg")
    ]
  ]

  // [c, d] Subspace, linear combination, span
  #slide[
    == Subspace, linear combination, span
    #definition[
      - *Subspace*: $S subset.eq V "and" S "is a vector space" arrow.double.l.r.long S lt.eq V$ #pause
      - *Proper subspace*: $S lt.eq V "and" S eq.not V arrow.double.l.r.long S < V$ #pause
      - *Zero subspace*: ${0}$#pause
    ]
    #definition[
      *Linear combination* with *coefficients* $alpha_i in K "and" cvec(v)_i in V$#pause
      $ alpha_1 cvec(v)_1+alpha_2 cvec(v)_2+dots.c+alpha_n cvec(v)_n $ #pause
      *Trivial*: $forall i, alpha_i = 0$ #uncover("7-")[(*Nontrivial* otherwise)] #pause #pause
    ]
    #definition[
      *Span*: $ angle.l S angle.r = "span"(S) = {alpha_1 cvec(v)_1+dots.c+alpha_n cvec(v)_n mid alpha_i in K, cvec(v)_i in S} $
    ]
  ]

  // [c, d, t] Linear independence, basis
  #slide[
    == Linear independence, basis
    #definition[
      *Linear independent* vectors $cvec(v)_1,dots,cvec(v)_n in V$: #pause
      $ alpha_1 cvec(v)_1 + dots.c + alpha_n cvec(v)_n = 0 arrow.double.l.r.long alpha_1=dots.c=alpha_n=0 $ #pause
      Not linearly independent #uncover("4-")[$arrow.double.r.long$ *linearly dependent*] #pause #pause
    ]
    #theorem[
      The following statements are equivalent:#pause
      - $S$ is linearly independent and spans $V$.#pause
      - $forall cvec(v) in V, exists! cvec(v)_1,dots,cvec(v)_n in S, alpha_1,dots,alpha_n in K:$#pause
      $ v = alpha_1 cvec(v)_1+dots.c+ alpha_n cvec(v)_n $#pause
      - $S$ is a minimal spanning set.#pause
      - $S$ is a maximal linearly independent set. #pause
      If $S$ satisfies one (and hence all) those conditions, then $S$ is a #uncover("12-")[*basis*  for $V$]
    ]
  ]

  // [c, d] Dimension, inner product
  #slide[
    == Dimension, inner product
    #definition[
      - *Finite dimensional*: ${0}$ or has a finite basis (otw, *infinite dimensional*) #pause
      - *Dimension*: cardinality $kappa$ of any basis for $V$ ($kappa$-dimensional: $dim V=kappa$) #pause
      - $dim {0} = 0$ #pause
    ]

    #definition[
      *Inner product space* $(V, angle.l dot.c, dot.c angle.r)$: vector space $V$ and *inner product* $angle.l dot.c,dot.c angle.r:V times V arrow.r K$: #pause

      - #box(width: 10cm)[_Linearity in the first argument_:] $angle.l alpha cvec(x)+beta cvec(y),cvec(z) angle.r = alpha angle.l cvec(x), cvec(y) angle.r + beta angle.l cvec(y),cvec(z) angle.r$.#pause
      - #box(width: 10cm)[_Hermitian symmetry_:] $angle.l cvec(x), cvec(y) angle.r=angle.l cvec(y),cvec(x) angle.r^*$. #pause
      - #box(width: 10cm)[_Positive definiteness_:] $angle.l cvec(x),cvec(x) angle.r gt.eq 0 $, $ angle.l cvec(x),cvec(x) angle.r = 0 arrow.l.r.double.long cvec(x)=0$.
    ]
    #definition[
      *Inner product* of sets $X, Y$: $angle.l X, Y angle.r = {angle.l cvec(x),cvec(y) angle.r bar.v cvec(x) in X, cvec(y) in Y}$ \ #box(width:8.7cm)[] of vector $cvec(v)$ and set $S$: $angle.l cvec(v), S angle.r = {angle.l cvec(v), cvec(x) angle.r bar.v cvec(x) in S}$ 
    ]
  ]

  // [c, d] Linear transformation
  #slide[
    == Linear transformation
    #definition[
    *Linear transformation*: $tau:V arrow.r W$ where:
    $ tau(alpha u +beta v ) = alpha tau(cvec(u))+  beta tau( cvec(v) )$ #pause
      - #box(width: 7cm)[*Linear operator*:] $tau:V arrow.r V$ #pause
        - #box(width: 7cm)[*Real operator*:] $tau$ linear operator and $K=RR$ #pause
        - #box(width: 7cm)[*Complex operator*:] $tau$ linear operator and $K = C $ #pause
      - #box(width: 7cm)[*Linear functional*:] $tau: V arrow.r K$. #pause
      - #box(width: 7cm)[*Dual space*:] $V^ast$: set of all linear functionals on $V$. #pause
      - #box(width: 7cm)[*Homomorphism*:] for linear transformation #pause
      - #box(width: 7cm)[*Endomorphism*:] for linear operator #pause
      - #box(width: 7cm)[*Monomorphism*:] for injective linear transformation #pause
      - #box(width: 7cm)[*Epimorphism*:] for surjective linear transformation #pause
      - #box(width: 7cm)[*Isomorphism*:] for bijective transformation 
    ]

  ]
  
  // [c, d] Norm, normed space, metric, metric space
  #hidden-slide[
    == Norm, normed space, metric, metric space
    #definition[
      *Normed space* $(V, norm(dot.c))$: vector space $V$ and *norm* $norm(dot.c): arrow.r K$ with conditions: #pause
      - #box(width: 7cm)[_Triangle inequality_:] $norm(cvec(x)+cvec(y)) lt.eq norm(cvec(x)) + norm(cvec(y))$ #pause
      - #box(width: 7cm)[_Absolute homogeneity_:] $norm(alpha cvec(x)) = |alpha|norm(cvec(x))$ #pause
      - #box(width: 7cm)[_Positive definiteness_:] $norm(cvec(x))=0 arrow.r.double.long cvec(x) = 0$ #pause
    ]
    #show math.equation: set block(spacing: .8em)
    #show par: set block(spacing: .8em)
    #definition[
      The *norm induced by the inner product* $angle.l dot.c,dot.c angle.r$ is $ norm(cvec(x)) = sqrt(inner(cvec(x), cvec(x))) $
    ]
    #show math.equation: set block(spacing: 1em)
    #show par: set block(spacing: 1em)
    #definition[
      *Metric space*: vector space $(V, d)$ and *metric* (or *distance*)\ $d:V times V arrow.r K$ with conditions: #pause
      - #box(width: 7cm)[_Triangle inequality_:] $forall cvec(x),cvec(y),cvec(z) in V, d(cvec(x),cvec(y)) lt.eq d(cvec(x),cvec(z)) + d(cvec(z),cvec(y))$ #pause
      - #box(width: 7cm)[_Symmetry_:] $forall cvec(x),cvec(y) in V, d(cvec(x),cvec(y))=d(cvec(y),cvec(x))$ #pause
      - #box(width: 7cm)[_Positive definiteness_:] $forall cvec(x),cvec(y) in V, d(cvec(x),cvec(y)) lt.eq 0 "and" d(cvec(x),cvec(y))=0 arrow.r.l.double.long cvec(x)=cvec(y)$
    ]
  ]

  // [*, t] Inner product-angle relationship
  #hidden-slide[
    #theorem([Inner product-angle relationship])[
      $ cos (angle cvec(a),cvec(b)) = (angle.l cvec(a), cvec(b) angle.r)/(norm(cvec(a))norm(cvec(b))) $
    ]
  ]

  // [*, d] Orthogonality
  #slide[
    == Orthogonality
    #definition([Orthogonality])[
      - #box(width:12cm)[*Orthogonal* vectors $cvec(a)$ and $cvec(b)$, $cvec(a) perp cvec(b)$:] $angle.l cvec(a), cvec(b) angle.r = 0$ #pause
      - #box(width:12cm)[*Orthogonal* subsets $X$ and $Y$, $X perp Y$:] $angle.l X, Y angle.r = {0}$, i.e., $angle.l cvec(x),cvec(y) angle.r = 0, forall cvec(x) in X, cvec(y) in Y$. We write $cvec(v) perp X$ in place of ${cvec(v)} perp X$. #pause
      - *Orthogonal complement* of $X subset.eq V$: $ X^perp = {cvec(v) in V bar.v cvec(v) perp X} $
    ]
  ]

  // [*, d, t] Cosets, Quotient space
  #hidden-slide[
    == Cosets, Quotient space
    #theorem[
      Let $S lt.eq V$. The binary relation #pause
      $ cvec(u) equiv cvec(v) arrow.l.r.double.long cvec(u)-cvec(v) in S $ #pause
      is an equivalence relation on $V$, whose equivalence classes are the *cosets* #pause
      $ [cvec(v)]=cvec(v)+S={cvec(v)+cvec(s) bar.v cvec(s) in S} $ #pause
      of $S$ in $V$. #pause
    ]
    #definition[
      *Quotient space* of $V$ modulo $S$: vector space $V\/S$ of all cosets of $S$ in $V$, where #pause
      $ alpha(cvec(u)+S)&=alpha cvec(u)+S \ (cvec(u)+S)+(cvec(v)+S)&=(cvec(u)+cvec(v))+S $ #pause

      Zero vector of $V\/S$=$0+S=S$.
    ]
  ]