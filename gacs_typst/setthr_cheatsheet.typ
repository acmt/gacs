#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Set Theory Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)
#show_thm_legend #v(-1em)
#set heading(numbering: "1.1.")
#show outline.entry.where(
  level: 1
): it => {
  v(8pt, weak: true)
  strong(it)
}
#text(size:8.2pt,outline(depth:2,indent:1em, fill:none))

= Axioms of Set Theory

== Axioms of Zermelo-Fraenkel

#definition([_Axiom of Extensionality_])[$X,h0 Y h1$have same members $ h0 arrow.double h0 X h0 =h0 Y$:
  $ forall A forall B (forall X (X in A arrow.double.l.r X in B)) arrow.double X=Y $]<d:axiom_extensionality>

#definition([_Axiom of Pairing_])[Any two objects $a,b$ have a pair ${a,b}$:
  $ forall a forall b exists C forall D :D in C arrow.double.l.r (D=A or D=B) $
]

#definition([_Axiom Schema of Separation_])[A subclass of a set is a set:
  $ forall A exists S forall x (x in S arrow.l.r.double (x in A and phi(p))) $
  For every set $A$ there exists a set $S$ that consists of all elements $x in A$ s.t. formula $phi(p)$ holds.
]
#definition([_Axiom of Union_])[For any $X$ there exists a set $Y=union.big X$:
  $ forall A exists B forall c(c in B arrow.double.l.r exists D(c in D and D in A)) $
]
#definition([_Axiom of Power Set_])[Every set X has a power set $cal(P)(X)$:
  $ forall X exists cal(P) forall S (S in cal(P) arrow.double.l.r forall s(s in S arrow.double s in X)) $
]
#definition([_Axiom of Infinity_])[There exists an infinite set
$ exists I(&exists o(o in I and not exists n: n in o) and \ &forall x (x in I arrow.double exists y(y in I and forall a(a in y arrow.double.l.r (a in x or a = x))))) $
This formula can be abbreviated as:
$ exists I(emptyset in I and forall x(x in I arrow.double (x union {x}) in I)) $
]
#definition([_Axiom Schema of Replacement_])[The image $phi[A]$ of $A$ is a set.
  $ forall A ([forall x h1 in h1 A exists !y h0:phi(x,y,A)] h1 arrow.double h1 exists B forall y[y h1 in h1 B arrow.double.l.r exists x h1 in h1 A h0: phi(x,y,A)]) $
]
#definition([_Axiom of Regularity_])[
  $ forall x (x eq.not emptyset arrow.double exists y(y in x and y sect x = emptyset)) $
]<d:axiom_regularity>
#definition([_Axiom of Choice_])[]<d:axiom_choice>
#definition()[Axioms @d:axiom_extensionality to @d:axiom_regularity define the *Zermelo-Fraenkel* axiomatic set theory *ZF*; *ZFC* denotes *ZF* plus @d:axiom_choice]
== Why Axiomatic Set Theory?
#definition([_Axiom Schema of Comprehension_ - False])[]
#definition([_Russel's Paradox_])[

]

#definition[*Power set* $cal(P)(X)={S h0: S subset.eq X}$: set of all subsets of set $X$.]


== Language of Set Theory, Formulas
== Classes



= Ordinal Numbers

#definition()[*Partial ordering* of set $P$: binary relation $lt.eq$ on $P$ s.t.:
  + #box(width:2.5cm)[*Reflexivity*:] #box(width:2cm,$forall a in P$), $a lt.eq a$
  + #box(width:2.5cm)[*Antisymmetry*:] #box(width:2cm,$forall a,b in P$), $a lt.eq b "and" b lt.eq a arrow.double a = b$
  + #box(width:2.5cm)[*Transitivity*:] #box(width:2cm,$forall a,b,c in P$), $a lt.eq b "and" b lt.eq c arrow.double a lt.eq c$
]<d:po>
#definition()[*Strict partial ordering* of set $P$: binary relation $<$ on $P$ s.t.:
  + #box(width:2.5cm)[*Irreflexivity*:] #box(width:2cm,$forall a in P$), $not(a lt a)$
  + #box(width:2.5cm)[*Asymmetry*:] #box(width:2cm,$forall a,b in P$), $a lt b arrow.double not(b lt a)$
  + #box(width:2.5cm)[*Transitivity*:] #box(width:2cm,$forall a,b,c in P$), $a lt b "and" b lt c arrow.double a lt c$
]<d:spo>
#theorem()[
  Asymmetric $arrow.double$ Irreflexive
]
#corollary()[
  Transitivity $arrow.double$ (Asymmetric $arrow.double.l.r$ Irreflexive)
]<c:asymmetric_irreflexive>
#note[Due to @c:asymmetric_irreflexive, @d:spo can omit either i. or ii. but not both.]
#theorem()[For any partial ordering $lt.eq$ on $P$, there is a unique associated strict partial ordering $lt$ on $P$. The converse is also true. Therefore, @d:po and @d:spo are equivalent.]
#definition[ *Partially ordered set* or *poset* $(P,lt.eq)$, $(P,lt)$ or $(P,lt,lt.eq)$
]
#definition()[
  #v(-1.2em)$ #h(1cm)  a lt.not b&= not(a lt b) 
   & quad a > b&= b < a
   & quad a gt.not b&= not(b < a) \
   
   a lt.eq.not b&= not(a lt.eq b) 
   & quad a gt.eq b&= b lt.eq a 
   & quad a gt.eq.not b&= not(b lt.eq a) $
]
#definition()[For a poset $(P, <)$ and $X eq.not emptyset subset.eq P$, then $a in P$ is:
  + a *maximal* element of $X$ if $a in X and forall x in X: a lt.not x$;
  + a *minimal* element of $X$ if $a in X and forall x in X: x lt.not a$;
  + the *greatest* element of $X$ if $a in X and forall x in X: x lt.eq a$;
  + the *least* element of $X$ if $a in X and forall x in X: a lt.eq x$;
  + an *upper bound* of $X$ if $forall x in X: x lt.eq a$;
  + a *lower bound* of $X$ if $forall x in X: a lt.eq x$;
  + the *supremum* of $X$ if
  + the *infimum* of $X$
]


#definition[
  An element $m in P$ is *maximal* if $p in P, m lt.eq p arrow.double m = p $.\ An element $n in P$ is *minimal* if $p in P, p lt.eq n arrow.double p = n $
]
#definition[
  The *maximum* (*largest*, *top*) of poset $P$, if it exists is $M in P$ s.t.
  $ p in P arrow.double p lt.eq M. $
  The *minimum* (*least*, *smallest*, *bottom*) of $P$, if it exists, is $N in P$ s.t.
  $ p in P arrow.double N lt.eq p $
]




= Cardinal Numbers

= Real Numbers

= The Axiom of Choice and Cardinal Arithmetic

= The Axiom of Regularity

= Filters, Ultrafilters and Boolean Algebras

= Stationary Sets

= Combinatorial Set Theory

= Measurable Cardinals

= Borel and Analytic Sets

= Models of Set Theory