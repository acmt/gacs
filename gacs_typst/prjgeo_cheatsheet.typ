#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Projective Geoemtry Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)

#show_thm_legend #v(-1em)
#set heading(numbering: "1.1.")
#show outline.entry.where(
  level: 1
): it => {
  v(8pt, weak: true)
  strong(it)
}
#text(size:8.2pt,outline(depth:2,indent:1em, fill:none))

// [1] Casas-Alvero, E. Analytic Projective Geometry
// [2] 
= Projective spaces and linear varieties
== Projective spaces
#definition[
  ($FF$)-*Projective space* $(PP, E, pi)$ over field $FF$:
  - $PP$ is a set of *projective points* or *elements*
  - $E$, is a vector space over $FF$, $dim E  gt.eq 2$
  - $pi: E without {0} arrow PP$ is onto and $forall vv,ww in E without {0}$
    $ vv=lambda ww, lambda eq.not 0 arrow.double.l.r pi(vv)=pi(ww) $
  If $p=pi(vv)=[vv]$, then $p$ is *represented by* its *representative* $vv$.

  $(E, pi)$ is a *projective structure* of $PP$
]
#definition[
  *Projective space*: quotient space $PP=(E without {0})\/tilde$, where $tilde$ is the equivalence relation on $E without {0}$
  $ vv tilde ww arrow.l.r.double exists lambda in FF_*: vv = lambda ww $
]

#theorem[
  $dim PP=dim E-1$
]
#definition[$dim PP_n=n$]
#example[

]
#definition[_Common Subspaces_ $S < PP$:
  #tablex(columns: 5, auto-lines:false, inset:0.3em, stroke:.5pt, align:center,
  [_Name_],vlinex(),[*proj. point*],[*proj. line*],[*proj. plane*],[*proj. volume*],hlinex(),
  [$dim S$],[0],[1],[2],[3])
]


#definition[
  $(E,pi)$ is *equivalent* to $(E',pi')$ if $E tilde^Phi E'$ s.t. $pi = pi' compose Phi$.
]

== Linear varieties

#definition[
  Let  $F<E$. Then, $L = pi(F without {0})=[F] subset PP_n$ is a *linear variety* (or *projective subspace*), *defined by* $F$.
]
#theorem[
  $L=[F]  arrow.double.long F without{0} = pi^(-1)(L)$
]

#let codim="codim"
#definition[Let $L<PP_n$ be a linear variety. 
  - #box(width: 3.1cm)[*Dimension* of $L$:] #box(width: 1.5cm, $dim L$)$ = dim F-1$.
  - #box(width: 3.1cm)[*Codimension* of $L$:] #box(width: 1.5cm, $codim L$)$ = n - dim L$
]
#example[
  - $dim (emptyset=[{0}])=-1$
  - $dim (PP_n=[E])=n$
  - $dim ({p}=[span(vv)]) = 0$
]

#corollary[
  $dim(L)=0 arrow.double L={p}$ (We denote $p$ and ${p}$ as the same)
]
#definition[
  $cal(S)_(d+1)(E)$: set of all linear subspaces of $E$ of dimension $d+1$
]
#definition[
  $cal(S)(E)=union.big_d^(dim E) S_(d)(E)$: Set of all linear subspaces of $E$.
]
#definition[
  $cal(L V)_(d)(PP_n)$: set of all linear varieties of $PP_n$ of dimension $d$
]

#definition[
  $cal(L V)(PP_n)=union.big_d^n cal(L V)_(d)(PP_n)$: Set of all linear varieties of $PP_n$.
]

#corollary[
  The map $rho:cal(S)_(d+1)(E) arrow cal(L V)_(d)(PP_n), rho(F)=[F]$ is bijective.
]

#definition[
  The structure *induced* or *subordinated on* $L=[F]$ ($dim L > 0$) by $PP_n$ is $(L,pi_(|F without {0})$) with $pi_(|F without {0}): F without {0} arrow L$
]
#lemma[
  $L_1=[F_1],L_2=[F_2] in cal(L V)(PP_n) arrow.double (L_1 subset.eq L_2 arrow.double.l.r F_1 subset.eq F_2)$
]

#proposition[For any $L_1,L_2 in cal(L V)(PP_n)$
  1. $L_1 subset.eq L_2 arrow.double dim L_1 lt.eq dim L_2$
  2. $L_1 subset.eq L_2$ and $dim L_1 = dim L_2 arrow.double L_1 = L_2$
]
#note[#v(-1.2em)
  $ emptyset subset.eq L subset.eq PP_n arrow.double -1 lt.eq dim L lt.eq n $  
$ dim L = -1 arrow.double.l.r L=emptyset wide dim L = n arrow.double.l.r L=PP_n $]

== Incidence of linear varieties

#proposition([Intersection])[
  If $L_1=[F_1],L_2=[F_2] in cal(L V)(PP_N)$, then $ L_1 sect L_2 = [F_1 sect F_2] in cal(L V)(PP_n) $
]

#proposition[ If $L_1,L_2 in cal(L V)(PP_n)$ then $L_1 sect L_2 in cal(L V)(PP_n)$, and any $T in cal(L V)(PP_n)$ contained in $L_1$ and $L_2$ is contained in $L_1 sect L_2$, which is the only linear variety satisfying those properties.
]
#note[$L_1 sect L_2=inf{L_1,L_2}$ for a poset $(cal(L V)(PP_n), subset.eq)$ ]

#definition[
  *Join* $L_1 or L_2 = [F_1 + F_2]$ a.k.a. LV *spanned* by $L_1,L_2$.
]
#proposition[
  If $L_1,L_2 in cal(L V)(PP_n)$ then $L_1 or L_2 in cal(L V)(PP_n)$, and any $T in cal(L V)(PP_n)$ containing $L_1$ and $L_2$ contains in turn $L_1 or L_2$, which is the only linear variety satisfying those properties.
]
#note[$L_1 or L_2=sup{L_1,L_2}$ for poset $(cal(L V)(PP_n), subset.eq)$.]
#corollary[
  If $L_1, L_2, L'_1, L'_2 in cal(L V)(PP_n)$ then $ L_1 subset.eq L'_1, L_2 subset L'_2 arrow.double.long L_1 or L_2 subset.eq L'_1 or L'_2 $
]
#corollary[If $L_i=[F_i],i=1, dots, k$:
  - $L_1 sect dots.c sect L_k = [F_1 sect dots sect F_k]$
  - $L_1 or dots.c or L_k = [F_1 + dots + F_k]$
]
#proposition()[
  If $L_1,dots,L_k in cal(L V)(PP_n)$:
  1. $L_1 sect dots.c sect L_k subset.eq L_i, forall i in NN_k^+$.\ $T subset.eq L_i, forall i in NN_k^+ arrow.double T subset.eq L_1 sect dots.c sect L_r$
  2. $L_1 or dots.c or L_k supset.eq L_i forall i in NN_k^+$.\ $T supset.eq L_i, forall i in NN_k^+ arrow.double T supset.eq L_1 or dots.c or L_r$
]
#theorem([Grassman formula])[
  For any $L_1,L_2 in cal(L V)(PP_n)$
  $ dim L_1 + dim L_2 = dim L_1 sect L_2 + dim L_1 or L_2 $
]
#corollary[
  If $L in cal(L V)(PP_n)$ and $p in.not L$ is a point of $PP_n$, then
  $ dim L or p = dim L + 1 $
]
#corollary[
  If $L in cal(L V)(PP_n)$ and $H supset.eq.not L$ is a hyperplane of $PP_n$, then
  $ dim L sect H = dim L - 1 $
]
#corollary[_Below assume different (or disjoint) elements:_
  + In $PP_n$, point $or$ point $=$ line
  + In $PP_n$, point $or$ line $=$ plane
  + In $PP_2$, line $sect$ line $=$ point
  + In $PP_3$, line $sect$ plane $=$ point
  + In $PP_3$, plane $sect$ plane $=$ line
  + In $PP_3$, line $or$ line $=$ $text(size:#11pt,cases(gap:#0.2em,#[whole space #h(.08cm) iff line $sect$ line $=$ $ emptyset$]#h(.66cm) (bold("skew lines")),#[plane #h(.85cm) iff line $sect$ line $=$ point] (bold("coplanar lines"))))$
]
#show: thmrules

#corollary[If $L_1, L_2 in cal(L V)(PP_n)$, then:
$ dim L_1 + dim L_2 > n-1 arrow.double.long L_1 sect L_2 eq.not emptyset $
]

#corollary[If $L_1, L_2 in cal(L V)(PP_n)$, any two conditions imply the third one:
- $L_1 sect L_2 = emptyset$
- $L_1 or L_2 = PP_n$
- $dim L_1 + dim L_2 = n-1$]<c:supplementary>

#definition[Linear varieties satisfying @c:supplementary are called *supplementary* (one *supplementary to* the other)]

#note[$L_1#h(0cm)=#h(0cm) [F_1], L_2#h(0cm)=#h(0cm) [F_2]$ are supplementary iff $E=F_1 plus.circle F_2$]

#corollary[
  If $L_1,dots,L_k in cal(L V)(PP_n), k gt.eq 2$, then 
  $ n-dim(L_1 sect dots sect L_m) lt.eq sum_(i=1)^k (n-dim L_i) $
  and equality holds iff $(L_1 sect dots sect L_(i-1)) or L_i = PP_n, forall i=2,dots,k$
]

#definition[Point p *belongs to* or *lies on* $L in cal(L V)(PP_n)$ if $p in L$. In this case, $L$ *goes* or *passes through* $p$.]
#definition[Intersection of linear varieties is also called a *section* of either of them by the other.]
#definition[*Collinear* points $p_1,p_2$ lying on line $L$]
#definition[*Coplanar* points $p_1,p_2$ lying on plane $P$]
#definition[$L_1,L_2$ *meet* or are *concurrent* if $L_1 sect L_2 eq.not emptyset$.

If $p in L_1 sect L_2$, then they are *concurrent* or *meet at* $p$.

If $p in.not L_1 sect L_2$, then they *misses* $p$.]

== Linear independence of points
#proposition[
  Let $p_0, dots, p_k$ be points of $PP_n$. Then
  + $dim p_0 or dots.c or p_k lt.eq k$, and
  + $dim p_0 or dots.c or p_k = k arrow.double.r.l forall i=NN_(1:k), p_i eq.not p_0 or dots.c or p_((i-1))$.
] <p:linear_independence>

#note[The order of points does not matter for  @p:linear_independence]

#definition[
  Points $p_0,dots,p_k in PP_n$ are *linearly independent* (LI) iff $dim p_0  h1 or h1 dots.c h1 or h1 p_k h1 = h1 k$. Otherwise, they are *linearly dependent* (LD).
]

#proposition[The following claim works regardless of ordering of points:

  $ p_0,dots,p_k "are independent" arrow.double.long.r.l forall i in NN_k^+, p_i in.not p_0 or dots.c or p_(i-1) $
]

#corollary[
  Points of a subset of a set of LI points are LI.
]

#note[
  $d+1$ points spanning $L$ with $dim L=d$ are independent. Conversely, $d+1$ independent points in $L$ with $dim L=d$ span it.\
  This, if $d+1$ independent points of $L$ with $dim L=d$, belong to $L'$, then $L subset L'$.
]

#theorem[
  Let points $p_0,dots,p_k in L in cal(L V)(PP_v)$ with $dim L=d$. Then
  + $m lt.eq d$
  + $exists p_(m+1),dots,p_d: p_0,dots,p_d$ are independent
]

#corollary[
  Any $L in cal(L V)(PP_n)$ has a supplementary
]
#corollary[
  If $L',L in cal(L V)(PP_n), L' subset.eq L, m=dim L', d=dim L$, then $exists L_i in cal(L V)(PP_n), i=m+1,dots,d-1$ s.t. $dim L_i = i$ and
  $ L' subset L_(m+1) subset dots subset L_(d-1) subset L $
]

#proposition[
  Any maximal subset of independent points of a finite set of points ${p_0,dots,p_k}$ spans $p_0 or dots.c or p_k$
]

#lemma[
  Let $p_i=[vv_i], i=0,dots,k$. $p_0,dots,p_k$ are independent iff $vv_0, dots, vv_k$ are linearly independent.
]

== Projectivities
#definition[
  Let $(PP_n, E, pi), (PP'_n, E', pi')$ be projective spaces over $FF$ and $phi:E arrow E'$ be an isomorphism. The map $f:PP_n arrow PP'_n$, where $f(p=[vv])=[phi(vv)]$ is called the *projectivity induced* or *represented by*  its *representative* $phi$, written $f=[phi]$. If $phi$ is not relevant, then $f$ is simply called *projectivity* or *homography*.
]
#note[If there is a projectivity $f#h(.0em):PP_n #h(0em) arrow PP'_m$, then $m=n$ and $FF=FF'$]

#definition[ A pair of *homographic projective spaces* is a triple $(PP_n, PP'_n, f)$ where $f:PP_n arrow PP'_n$ is a projectivity]

#lemma[
  For isomorphisms $phi,psi:E arrow E'$,
  $ [phi]= [psi] arrow.double.l.r.long phi = lambda psi "for some" lambda eq.not 0 in FF. $
]

#theorem[
  + $"Id"_(PP_n)=["Id"_E]$
  + If $f=[phi]:PP_n arrow PP'_n$ and $g=[psi]:PP'_n arrow PP''_n$ are projectivities, then $g compose f=[psi compose phi]$; in particular, $g compose f$ is a projectivity.
  + If $f=[phi]$ is a projectivity, then it is bijective and its inverse map is the projectivity induced by $phi^(-1), f^(-1)=[phi^(-1)]$.
]

#theorem()[
  + $(E,pi),(E',pi')$ are equivalent iff $"Id"_(PP)h0:(PP,E,pi)arrow (PP, E', pi')$ is a projectivity
  + If $f:PP arrow overline(PP)$ into $(overline(PP), overline(EE),overline(pi))$ is a projectivity for both $(E,pi),(E',pi')$, then they are equivalent.
]

== Projective invariance

#theorem()[
  If $f=[phi]:PP_n arrow overline(PP)_n$ is a projectivity and $L=[F]$ is a linear variety of $PP_n$, then:
  + $f(L) in cal(L V)(overline(PP)_n)$ where $phi(F):f(L)=[phi(F)]$. In particular, $dim f(L)= dim L$.
  + If $dim L h1 > h1 0$, then $f|_L h0:L h1 arrow h1 f(L)$ is induced by $phi|_F h0:F h1 arrow h1 phi(F)$.
]
#note[]
#corollary()[
  If $f=[phi]:PP_n arrow overline(PP)_n$ is a projectivity and $L_1, L_2$ are linear varieties of $PP_n$, then
  + $L_1 subset.eq L_2 arrow.double.l.r  f(L_1) subset.eq f(L_2)$,
  + $f(L_1 sect L_2) = f(L_1) sect f(L_2)$
  + $f(L_1 or L_2) = f(L_1) or f(L_2)$.
]

#note[]
#corollary()[
  If $f=[phi]:PP_n arrow overline(PP)_n$ is a projectivity, then
  $ q_0, dots,q_m in PP_n "are LI" arrow.double.l.r f(q_0),dots,f(q_m) "are LI". $
]
#definition()[*Linear figure*: finite set $F in cal(L V)(PP_n)$, classified in a number of subsets.]
#definition()[A class of linear figures is *projective* iff it is invariant by projectivities.]
#note[Classes of linear figures defined by conditions formulated in terms of dimension, inclusion, intersection, join and independence of points, are projective. Intersections and unions of projective classes are projective classes.]

#definition()[A (*complete*) *quadrilateral* is the linear figure of $PP_2$ with four lines (*sides*), no three of which are concurrent, and six points (*vertices*) for the intersections of $binom(4,2)$ pairs of different sides. Two vertices are *opposite* iff no side contains both. The join of opposite vertices is a *diagonal*. A quadrilateral has three different diagonals.]

#definition()[A *quadrivertex* (or *quadrangle*) is the linear figure of $PP_2$ with four points (*vertices*), no three of which are aligned and six different lines (*sides*) spanned the $binom(4,2)$ pairs of different vertices. Two sides are *opposite* iff they share no vertex. The intersection of opposite sides is a *diagonal point*. A quadrivertex has three different diagonal points.]

== Pappus' and Desargues' theorems

#theorem()[The three diagonals of a complete quadrilateral are not concurrent]
#theorem()[The three diagonal points of a quadrivertex are not aligned]
#theorem([Desargues])[Let $A B C$, $A'B'C'$ be triangles of $PP_2$, and $(a,b,c),(a',b',c')$ be sides opposite to $(A,B,C),(A' #h(-.2em),B'#h(-.2em),C')$, resp. 
Assume $A eq.not A', B eq.not B', C eq.not C', a eq.not a', b eq.not b'$, and $ c eq.not c'$. 
If $A A'#h(-.1em),B B'#h(-.1em), C C'#h(-.1em)$ are concurrent, then $a h1 sect h1  a', b h1  sect h1  b', c h1  sect h1  c'$ are aligned.]

#theorem([Pappus])[
  Let $r,s$
]
== Projection, section and perspectivity

= Projective coordinates and cross ratio

== Projective references
== Projective coordinates
== Change of coordinates
== Absolute coordinate
== Parametric equations
== Implicit equations
== Incidence with coordinates
== Determination and matrices of a projectivity
== Cross ratio
== Harmonic sets
== Projective classification

= Affine geometry

= Duality

== The space of hyperplanes

== Bundles of hyperplanes

== The principle of duality

== Hyperplane coordinates

== The dual of a projectivity

== Biduality

== Duals of linear varieties and bunches

= Projective transformations

== Complex extension of a real projective space

