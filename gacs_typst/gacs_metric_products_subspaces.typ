#import "acmt.typ": *

#show: acmt-theme.with()

/* START OF DOCUMENT */

// Status of the slide:
  // d definition
  // r corollary
  // t theorem
  // l lemma
  // n note
  // p proposition
  // g graphical pictures
  // e example
  // c complete
  // * ongoing



  #title-slide(title: "Geometric Algebra", subtitle: "Chapter 4: Metric products of subspaces", authors: "Anderson Tavares", institution-name: "", date: datetime.today().display())


// Metric products of subspaces
  // [c, n, d, t] Scalar product
  #slide[
    == Scalar product
    #uncover("2-",note[Assume a inner product space $(V, inner(dot.c, dot.c))$ with an induced norm $norm(dot.c)=sqrt(inner(v,v))$.])

    #uncover("3-",definition([Scalar product])[
      *Scalar product* $ast: ext^(k)(V) times ext^(k)(V) arrow K $:
      $ uncover("4-",alpha ast beta &= alpha beta) \
      uncover("5-",bl(A) ast bl(B)
        // = det(mat(delim:"[", aa_1^top;dots.v;aa_n^top) mat(delim:"[", bb_1,dots,bb_n)) 
        &= mat(delim:"|",
          inner(aa_1, bb_k), inner(aa_1, bb_(k-1)),dots,inner(aa_1, bb_1);
          inner(aa_2, bb_k), inner(aa_2, bb_(k-1)),dots,inner(aa_2, bb_1);
          dots.v,dots.v,dots.down,dots.v;
          inner(aa_k, bb_k), inner(aa_k, bb_(k-1)),dots,inner(aa_k, bb_1);
        )
      ) \
      uncover("6-",bl(A) ast bl(B)&=0 "if grade"(bl(A))eq.not"grade"(bl(B))) $
    ])
    #uncover("7-",theorem([Symmetries of the scalar product])[
      $ bl(B) ast bl(A) = bl(A) ast bl(B) = rev(bl(A)) ast rev(bl(B)) $
    ])
  ]
  // [c, d, n] Squared norm of a subspace
  #slide[
    == Squared norm of a subspace <square_norm_subspace>
    #uncover("2-",definition[
      *Squared norm* of blade $bl(A)$: $ norm(bl(A))^2 = A ast rev(bl(A)) $
    ])
    #uncover("3-",note[
      #set list(indent:.8em)
      #uncover("4-")[- *1-Blades* (*vectors*): $aa ast bb = det[inner(aa, bb)] = inner(aa, bb) arrow.double aa ast aa =$ length of $aa$]
      #uncover("5-")[- *2-Blades*: 
        #columns(2, [
          $ uncover("6-",norm(bl(A))^2 &= (aa_1 wedge aa_2)ast(aa_1 wedge aa_2)^tilde) \
                    uncover("7-", &= (aa_1 wedge aa_2)ast(aa_2 wedge aa_1)) \  
                    uncover("8-", &= mat(delim:"|", inner(aa_1, aa_1), inner(aa_1,aa_2);
                                  inner(aa_2,aa_1), inner(aa_2,aa_2))) \
                    uncover("9-", &= inner(aa_1,aa_1)inner(aa_2,aa_2)-inner(aa_1,aa_2)^2) $
          #colbreak()
          $ uncover("10-", norm(bl(A))^2 &= inner(aa_1,aa_1)inner(aa_2,aa_2)-inner(aa_1,aa_2)^2) \ 
            uncover("11-", &= norm(aa_1)^2 norm(aa_2)^2(1-cos^2 phi)) \ 
            uncover("12-", &= (norm(aa_1)norm(aa_2)sin phi)^2) $
          #uncover("13-")[- Squared area of parallelogram $aa_1,aa_2$]
          #uncover("14-")[- Any basis for $bl(A)$ gives same norm]
        ])
      ]
    ])
  ]

  // [c, n] Squared norm of a subspace (k-blades)
  #slide[
    == Squared norm of a subspace
    #note([cont])[
      #set list(indent:.8em)
      #uncover("2-",list[
        *$k$-Blades*:
        #uncover("3-")[- $k$-volume of $bl(A) = aa_1 wedge dots wedge aa_k$ is proportional to $det([aa_1, dots, aa_k])$]
        #uncover("4-")[- $norm(bl(A))^2 
             uncover("5-",= bl(A) ast rev(bl(A))) 
             uncover("6-",= det\(underbrace([bl(A)]^top [bl(A)],"Gram matrix")\))
             uncover("7-",= det([bl(A)]^top)det([bl(A)])) 
             uncover("8-",= det([bl(A)])^2)$]
      ])
      $ uncover("5-",mat(delim:"|",
          inner(aa_1, aa_1), inner(aa_1, aa_2),dots,inner(aa_1, aa_k);
          inner(aa_2, aa_1), inner(aa_2, aa_2),dots,inner(aa_2, aa_k);
          dots.v,dots.v,dots.down,dots.v;
          inner(aa_k, aa_1), inner(aa_k, aa_2),dots,inner(aa_k, aa_k);
        ))uncover("6-",= det(underbrace(mat(delim:"[", aa_1^top;dots.v;aa_n^top),bl(A)^top)#move(dy:1.5cm,$ underbrace(#move(dy:-1.2cm,$ mat(delim:"[", aa_1,dots,aa_n) $), bl(A))$))) $
    ]
  ]

  // [c,t] Angle between blades
  #slide[
    == Angle between blades

    #uncover("2-",theorem([Angle between blades])[#v(-.5em)
      $ cos phi = (bl(A) ast rev(bl(B)))/(norm(bl(A))norm(bl(B))) $ #v(-.2em)
    ])
    #uncover("3-",grid(columns: (1fr, 0fr),[
      #note[
        #uncover("3-")[- *1-blades* (vectors): $cos phi = inner(aa,bb)\/(norm(aa)norm(bb))$]
        #uncover("4-")[
          - *2-blades*: $A=aa_1 wedge aa_2$, $B=bb_1 wedge bb_2$
                    $ uncover("6-",aa_1 wedge aa_2 &= aa wedge cvec(c) "where" inner(aa,cvec(c)) = 0\, norm(cvec(c))=1 \ 
                      bb_1 wedge bb_2 &= bb wedge cvec(c) "where" inner(bb,cvec(c)) = 0) \
                    uncover("7-",bl(A) * rev(bl(B)) &= (aa_1 wedge aa_2) * (bb_2 wedge bb_1) = (aa wedge cvec(c))*(cvec(c)wedge bb)) \
                    uncover("8-",
                    &= mat(delim:"|", inner(aa,bb), inner(aa,cvec(c));inner(cvec(c),bb), inner(cvec(c),cvec(c))))
                    uncover("9-",= mat(delim:"|", inner(aa,bb), 0; 0, 1))
                    uncover("10-",= inner(aa,bb))
                    uncover("11-",= norm(aa)norm(bb)cos phi)
                    uncover("12-",= norm(bl(A))norm(bl(B))cos phi) $
                    #v(-.3cm)
                    #uncover("13-")[because $norm(bl(A))^2 = norm(aa wedge cvec(c))^2 uncover("14-",= inner(aa,aa)inner(cvec(c),cvec(c))-inner(aa,cvec(c))^2)
                    uncover("15-", = inner(aa,aa))
                    uncover("16", = norm(aa)^2)$]
        ]
      ]
    ],[
      #move(dx:-6cm)[
        #align(center,[
          #only("3",image("../gacs/figs/ext_alg_angle_vector.svg",     width:11cm))
          #only("4",image("../gacs/figs/ext_alg_angle_bivector.svg",   width:11cm))
          #only("5-",image("../gacs/figs/ext_alg_angle_bivector_2.svg", width:11cm))
        ])
      ]
    ]))
    
  ]

  // #set footnote.entry(clearance: 0em, indent: 0em)
  // [c, n] Angle between blades (k-blade case)
  #slide[
    == Angle between blades
    #uncover("1-",theorem([Angle between blades])[#v(-.5em)
      $ cos phi = (bl(A) ast rev(bl(B)))/(norm(bl(A))norm(bl(B))) $ #v(-.2em)
    ])
    #note[
      - *$k$-blades*: take out common factors. Then:
        #uncover("2-")[- _Only scalars are left_: $uncover("3-",bl(A)=lambda bl(B))uncover("4-",arrow.double phi=0)uncover("5-",arrow.double cos phi = 1)uncover("6-",arrow.double bl(A) ast rev(bl(B))=norm(bl(A))norm(bl(B))).$]
        #uncover("7-")[- _One vector in each term_: same as 2-blade case]
        #uncover("8-")[
          - _Totally disjoint subblades of grade_ $ gt.eq 2$
            #uncover("9-")[- at least two rotations in orthogonal 2-blades
                             - Riesz, M. Clifford Numbers and Spinors. Kluwer Academic, 1993.]
            #uncover("10-")[- $cos phi = cos alpha_1 cos alpha_2 dots$]
            #uncover("11-")[- $alpha_i = 90 degree$ for some i $uncover("12-", arrow.double cos phi = 0)uncover("13-", arrow.double bl(A) ast bl(B) = 0)$]
          ]
    ]
  ]
  
  // [c, d, t] Orthogonal blades
  #slide[
    == Orthogonal $k$-blades

    #definition([Orthogonal $k$-blades])[
      $bl(A), bl(B) in ext^(k)(V)$ are *orthogonal*, $bl(A) perp bl(B)$, if $ bl(A) ast bl(B) = 0 $
    ]
    #uncover("2-",theorem[
      $ V(bl(A)) perp V(bl(B)) arrow.r.double.long bl(A) perp bl(B) $
    ])
    #uncover("3-",align(center, image("../gacs/figs/ext_alg_orthogonal_line_plane.svg")))
  ]

  // [c,g,d,t] Contraction (implicit definition)
  #slide[
    == Contraction
    #only("1",align(center,image("../gacs/figs/ext_alg_left_contraction.svg", width: 7.5cm)))
    #only("2-",align(center,image("../gacs/figs/ext_alg_left_contraction_2.svg", width: 7.5cm)))

    #uncover("3-",definition([Implicit definition of contraction])[*Left contraction* is a function \ $lcont:ext^(k)(V) times ext^(l)(V)arrow ext^(l-k)(V)$ that must satisfy
        $ (bl(X) wedge bl(A)) ast bl(B) = bl(X) ast (bl(A) lcont bl(B)) $
    ])
    #uncover("4-",theorem[
      $"grade"(bl(A) lcont bl(B)) = "grade"(bl(B))-"grade"(bl(A))$
    ])
  ]

  // [c, t] Contraction (explicit definition)
  #slide[
    == Contraction
    #show par: set block(spacing: 1em, below: .6em, above: 1em)
    // #show math.equation: set block(spacing: -1em, inset: -1em, above: .8em, below: -2em)
    #theorem([Explicit definition of contraction])[\
      (*Left*) *contraction* $lcont:ext^(k)(V) times ext^(l)(V)arrow ext^(l-k)(V)$ with properties:
      $ uncover("2-",alpha lcont bl(B))uncover("3-", &= alpha bl(B)) \ 
        uncover("4-",bl(B) lcont alpha)uncover("5-",&= 0 "if" "grade"(bl(B))>0) \
        uncover("6-",aa lcont bb )uncover("7-",&= inner(aa,bb)) \
        uncover("8-",aa lcont (bl(B) wedge bl(C)))uncover("9-", &= (aa lcont bl(B)) wedge bl(C) + invol(bl(B))and(aa lcont bl(C))) \
        uncover("10-",(bl(A) wedge bl(B))lcont bl(C))uncover("11-", &= bl(A) lcont(bl(B) lcont bl(C)))
        $
    ]

    #uncover("12-",theorem([Bilinear property of contration])[
      $ uncover("13-",(bl(A)+bl(B))lcont bl(C) = bl(A) lcont bl(C) + bl(B) lcont bl(C)) \ 
      uncover("14-",bl(A) lcont (bl(B)+bl(C)) = bl(A) lcont bl(B) + bl(A) lcont bl(C)) \
      uncover("15-",(alpha bl(A))lcont bl(B) = alpha(bl(A)lcont bl(B)) = bl(A) lcont (alpha bl(B))) $
    ])
  ]

  // [c, n, e] Contraction of blades
  #slide[
    == Contraction of blades
    #note([Computing contraction of blades])[
      $ bl(A)_k lcont bl(B)_l = (bl(A)_(k-1) wedge aa)lcont bl(B)_l = bl(A)_(k-1)lcont(aa lcont bl(B)_l)$
    ]
  
    #example[
      $ uncover("2-",(ee_1& wedge ee_2) lcont (ee_1 wedge ee_3 wedge ee_2)) \
      uncover("3-",&= ee_1 lcont (ee_2 lcont (ee_1 wedge ee_3 wedge ee_2))) \
      uncover("4-",&= ee_1 lcont ((ee_2 lcont ee_1) wedge (ee_3 wedge ee_2) - ee_1 wedge (ee_2 lcont (ee_3 wedge ee_2)))) \
      uncover("5-",&= ee_1 lcont ((ee_2 lcont ee_1) wedge(ee_3 wedge ee_2) - ee_1 wedge((ee_2 lcont ee_3)wedge ee_2 - ee_3 wedge (ee_2 lcont ee_2))))\
      uncover("6-",&= ee_1 lcont (ee_1 wedge ee_3))\
      uncover("7-",&= (ee_1 lcont ee_1) wedge ee_3 - ee_1 wedge (ee_1 lcont ee_3))\
      uncover("8-",&= ee_3)
      $
    ]
  ]

  // [c,t,n,e] Contraction of blades (recursive way)
  #slide[
    == Contraction of blades
    #show par: set block(spacing: 0em, below: .5em, above: 0em)
    #uncover("2-",theorem([Recursive contraction onto blades])[
      $ x lcont (a_1 wedge a_2 wedge dots.c wedge a_k) = sum_(i=1)^k (-1)^(i-1) a_1 wedge a_2 wedge dots.c wedge (x lcont a_i) wedge a_k $
    ])
    #uncover("3-",note([For 2-blades])[
      $ xx lcont (aa_1 wedge aa_2) = inner(xx, aa_1)aa_2 - inner(xx, aa_2)aa_1 $
    #v(-.5em)])
    #example[
       $ uncover("4-",(ee_1& wedge ee_2) lcont (ee_1 wedge ee_3 wedge ee_2)) \
        uncover("5-",&= ee_1 lcont (ee_2 lcont (ee_1 wedge ee_3 wedge ee_2))) \ 
        uncover("6-",&= ee_1 lcont \()only("8-12",0)only("10-12",+ 0)only("12",+)uncover("12-",ee_1 wedge ee_3\))\
        uncover("13-",&=)uncover("15-18",ee_3)uncover("17", + 0)\
        &only("6-17",#line(length: 28%, stroke:  (paint:myred, dash: "dashed")) #linebreak())
        &only("7-8", +text(myred,(ee_2 lcont ee_1))wedge ee_3 wedge ee_2)only("9-10", -ee_1 wedge text(myred,(ee_2 lcont ee_3)) wedge ee_2)only("11-12", +ee_1 wedge ee_3 wedge text(myred,(ee_2 lcont ee_2)))only("14-15",+text(myred,(ee_1 lcont ee_1))ee_3)only("16-17",-ee_1 text(myred,(ee_1 lcont ee_3)))
        $

    ]

  ]

  // [c, t, g] Geometric interpretation of contration
  #slide[
    == Geometric interpretation of contration

    #theorem([Other properties of contraction])[
      #columns(2)[
      #uncover("2-")[- $bl(A)$, $bl(B)$ are blades $arrow.double bl(A)lcont bl(B)$ is as well]
      #uncover("3-")[- $bl(A)lcont bl(B) subset.eq bl(B)$]// because $bl(A)lcont bl(B)=(bl(A)'wedge aa)lcont bl(B) = bl(A)'lcont(aalcont bl(B))

        // - $aalcont bl(B)$ contains only vectors of $bl(B)$.
        // - Even if $bl(A)lcont bl(B)=0$, 0 belongs to any space.
      #uncover("4-")[- $xx lcont bl(B) = 0 arrow.double.long.l.r xx perp V(bl(B))$]
      #uncover("5-")[- $bl(A) lcont bl(B) perp bl(A)$]
      #colbreak()
      #uncover("6-")[- $norm(bl(A)lcont bl(B)) tilde norm(bl(A))norm(bl(B))cos angle (bl(A),P_(bl(B))[bl(A)]) $]
      #uncover("7-")[- $norm(bl(A)lcont bl(B))=0$ if $"grade"(bl(A)) gt "grade"(bl(B))$]
      #uncover("8-")[- $(bl(A) lcont bl(B))hat = invol(bl(A)) lcont invol(bl(B))$]
      #uncover("9-")[- $(bl(A) lcont bl(B))^tilde = rev(bl(B)) rcont rev(bl(A))$]
      ]
    ]
    #align(center,[
      #only("1-3",image("../gacs/figs/ext_alg_contraction.svg", width: 10cm))
      #only("4",image("../gacs/figs/ext_alg_orthogonal_line_plane.svg", width: 10cm))
      #only("5-",image("../gacs/figs/ext_alg_contraction_orthogonal.svg", width: 10cm))
    ])
  ]

  // [c, e] Geometric interpretation of contration (example)
  #slide[
    == Geometric interpretation of contration
    #example[
      $cvec(x) = norm(cvec(x)) (ee_1 cos phi + ee_3 sin phi)$ and $bl(B) = norm(bl(B)) ee_1 wedge ee_2$
      $ uncover("2-",cvec(x) lcont bl(B))uncover("3-", &= norm(cvec(x))norm(bl(B)))uncover("4-",\[(ee_1 lcont (ee_1 wedge ee_2))cos phi)uncover("5-", + (ee_3 lcont (ee_1 wedge ee_2)) sin phi \] ) \
      uncover("6-",&= (norm(cvec(x))norm(bl(B))cos phi) ee_2)
       $

      #align(center,image("../gacs/figs/ext_alg_contraction_basis.svg"))
    ]
  ]

  // [c,g,d,t] The right contraction
  #slide[
    == Right contraction
    #align(center,image("../gacs/figs/ext_alg_right_contraction.svg"))
    #v(-.5cm)
    #uncover("2-",definition([Implicit definition of right contraction])[
      $ bl(B) ast (bl(A) wedge bl(X)) = (bl(B) rcont bl(A)) ast bl(X) $
    ])
    #uncover("3-",theorem[
      $ bl(B) rcont bl(A) = (rev(bl(A))lcont rev(bl(B)))^tilde = (-1)^a(b+1) bl(A) lcont bl(B) quad (a="grade"(bl(A)), b="grade"(bl(B)))$
    ])
    #uncover("4-",theorem([Grade reducing property])[
      $ "grade"(bl(B) rcont bl(A)) = "grade"(bl(B))-"grade"(bl(A))$
    ])
    #uncover("5-",theorem[
      $(bl(B) rcont bl(A))hat = invol(bl(B)) rcont invol(bl(A))$
    ])
  ]

  // [c, n, t] Nonassociativity of the contraction
  #slide[
    == Nonassociativity of the contraction
    #uncover("2-",note([Contraction is not associative])[
      $ uncover("3-",bl(A) lcont (bl(B) lcont bl(C)) eq.not (bl(A) lcont bl(B))lcont bl(C)) $
      #uncover("4-")[- $bl(A) lcont(bl(B) lcont bl(C)) = (bl(A) wedge bl(B))lcont bl(C)$ (see Theorem "Explicit definition of contraction")]
    ])
    #uncover("5-",theorem[
      If $bl(A) subset.eq bl(C)$, then $(bl(A) lcont bl(B))lcont bl(C) = bl(A) wedge (bl(B) lcont bl(C))$
    ])

    #uncover("6-",note(text(myred,[_duality formulas_])))
  ]

  // [c, d, n] Blade inverse
  #slide[
    == Blade inverse
    #show par: set block(spacing: 1em, above: 0em, below: .4em)
    #show math.equation: set block(spacing: 0em, inset: 0em, above: .4em, below: .4em)
    // #set block(spacing: 0em)
    $ uncover("2-",bl(A) lcont bl(A)^(-1) = 1) \
      uncover("3-",bl(B) perp bl(A))uncover("4-", quad arrow.double.long quad bl(A) lcont (bl(A)^(-1) + alpha bl(B)))uncover("5-", = bl(A) lcont bl(A)^(-1) + alpha bl(A) lcont  bl(B))uncover("6-", = 1) $
    #uncover("7-",definition([Blade inverse])[
    $ uncover("8-",bl(A)_k^(-1))
      uncover("9-",= rev(bl(A))_k/norm(bl(A)_k)^2)uncover("10-",=(-1)^(k(k-1)\/2) bl(A)_k/norm(bl(A)_k)^2) $
    ])
    #uncover("11-",note([Checking inverse for contraction])[
      $ uncover("12-",bl(A)_(k) lcont bl(A)_(k)^(-1))
        uncover("13-",= bl(A)_(k) lcont (rev(bl(A))_k)/(norm(bl(A))^2))
        uncover("14-",=(bl(A)_(k) lcont rev(bl(A))_k)/(bl(A)_(k) ast rev(bl(A))_k))
        uncover("15-",= (bl(A)_(k) ast rev(bl(A))_k)/(bl(A)_(k) ast rev(bl(A))_k))
        uncover("16-", = 1) $
    ])#v(-1cm)
    #uncover("17-",note([Inverse of common blades])[
      #grid(columns:(.25fr, .25fr, .25fr, .25fr),
      [#uncover("17-")[*Vectors*\ $aa^(-1)=aa\/norm(aa)^2$]],
      [#uncover("18-")[*2-Blades*\ $(ee_1 wedge ee_2 wedge ee_3)^(-1) \ = ee_3 wedge ee_2 wedge ee_1 \ = -ee_1 wedge ee_2 wedge ee_3$]],
      [#uncover("19-")[*3-blades*\ $(ee_1 wedge ee_2)^(-1)\ = ee_2 wedge ee_1 \ = -ee_1 wedge ee_2$]],
      [#uncover("20-")[*Pseudoscalars*\ $bl(I)_n^(-1)=rev(bl(I))_n$]])
    ])
  ]

  // [c, d, e, t, g] Orthogonal complement and duality
  #slide[
    == Orthogonal complement and duality
    #uncover("2-",definition([Dualization])[The *dualization* function $*:ext^(k)(V) arrow ext^(n-k)(V)$ maps $bl(A)_k$ to its *dual*:
      $ uncover("3-",bl(A)_k^ast = bl(A)_k lcont bl(I)_n^(-1)) $
    ])
    #uncover("4-",example[ If $aa = alpha ee_1$ and $bl(I)_2 = ee_1 wedge ee_2$, then
      #grid(columns:(.7fr, .3fr),[
        $ uncover("5-",aa^ast)
          uncover("6-", &=& alpha ee_1 lcont(ee_2 wedge ee_1))
          uncover("7-", &= -alpha ee_2) \ 
          uncover("8-",(aa^ast)^ast)
          uncover("9-", &=& -alpha ee_2 lcont(ee_2 wedge ee_1))
          uncover("10-", &= -alpha ee_1)
          uncover("11-", &= -aa)
        $
      ],[
        #colbreak()
        #v(-1.5cm)#uncover("12-",align(center,image("../gacs/figs/ext_alg_dual.svg", width: 7cm)))
      ])
    ])
    #uncover("13-",theorem[
      $ (A_k^ast)^ast = (-1)^(n(n-1)\/2) A_k$
    ])
    #uncover("14-",theorem[
      $V(dual(bl(A))) = V(bl(A))^perp$
    ])
  ]

  // [c, t, p, n] Orthogonal complement and duality (proof double dualization)
  #slide[
    == Orthogonal complement and duality
    #uncover("1-",theorem[
      $ (A_k^ast)^ast = (-1)^(n(n-1)\/2) A_k$
    ])
    #uncover("2-",proof[#v(-1cm)
      $ (bl(A)_k^ast)^ast 
        uncover("3-", &= (bl(A)_k lcont psninv)lcont psninv) 
        uncover("4-",= bl(A)_k wedge (psninv lcont psninv)) \ 
        uncover("5-",&= (-1)^(n(n-1)\/2)bl(A)_k wedge (psn lcont psninv ))\
        uncover("6-",&= (-1)^(n(n-1)\/2)bl(A)_k wedge 1) \
        uncover("7-",&= (-1)^(n(n-1)\/2)bl(A)_k) $ #v(-1.5cm)
    ])
    #uncover("8-", note[#v(-1cm)#align(center,tablex(align:center, columns:9, auto-lines:false,
      [n],vlinex(),0,1,2,3,4,5,6,7,hlinex(),
      [double-dual sign],[+],[+],[-],[-],[+],[+],[-],[-]))])
    #uncover("9-", theorem[
      $(alpha bl(A) + beta bl(B))^ast = alpha dual(bl(A)) + beta dual(bl(B))$
    ])
    #uncover("10-", note[
      If $bl(A)_n = lambda bl(I)_n$, then $dual(bl(A)_n) = lambda$
    ])
  ]

  // [c, n, t, d, t] Dualization and undualization
  #slide[
    == Dualization and undualization
    #uncover("2-",note([Dualization in 3D])[#v(-1cm) #align(center,image("../gacs/figs/ext_alg_dual_3d.svg", width: 10cm))]) #v(-.8cm)
    #uncover("3-",note[#v(-.8cm)
      $ dual(aa) &= aa lcont psinv(3) = (a_1 ee_1 + a_2 ee_2 + a_3 ee_3) lcont (ee_3 wedge ee_2 wedge ee_1) \
      &= -a_1 ee_2 wedge ee_3 - a_2 ee_3 wedge ee_1 - a_3 ee_1 wedge ee_2 $#v(-.5cm)
    ])
    #uncover("4-",theorem[
      $bl(A)$ and $dual(bl(A))$ have the same coefficients w.r.t. basis ${bl(B)_i}$ and ${dual(bl(B)_i)}$ resp.
    ])
    #uncover("5-",definition([Undualization])[*Undualization* $ast.circle$ of a blade $bl(A)_k$ outputs its *undual*
      $ undual(bl(A)) = bl(A) lcont bl(I)_n $
    ])
    #uncover("6-",theorem[
      $(undual(bl(A)))^ast.circle = bl(A)$
    ])
  ]

  // Duality relationships
  #slide[
    == Duality relationships
    #theorem([Duality properties])[
      $ uncover("2-",(bl(A) wedge bl(B))^ast &= bl(A) lcont(bl(B)^ast)) \
        uncover("3-",(bl(A) lcont bl(B))^ast &= bl(A) wedge (bl(B)^ast)) $
    ]

    #uncover("4-",theorem[
      $xx in V(bl(A)) quad arrow.l.r.double.long quad xx lcont dual(bl(A)) = 0 $
    ])
    #uncover("5-",corollary[
      $cvec(y) in V(dual(bl(A))) quad arrow.l.r.double.long quad cvec(y) lcont bl(A) = 0 $
    ])
    #uncover("6-",note[In linear algebra: *normal* vector $cvec(n)$ for hyperplane $xx dot.c cvec(n) = 0$])
  ]

  // [c, d, n] Orthogonal projection of a vector
  #slide[
    == Orthogonal projection of a vector
    #only("2",align(center,image("../gacs/figs/ext_alg_projection_01.svg")))
    #only("3-",align(center,image("../gacs/figs/ext_alg_projection_02.svg")))
    

    #uncover("4-",definition([orthogonal projection of a vector])[
      *Orthogonal projection* of vector $xx$ onto $bl(B)$
      $ P_(bl(B))[dot.c]:V arrow V, quad P_(bl(B))[xx] = (xx lcont bl(B))lcont bl(B)^(-1) $
    ])
    #uncover("5-",note([Nonlinearity])[#text(myred)[*linear*] in $xx$, #text(myblue)[*nonlinear*] in $bl(B)$.])
  ]

  // [c, t, p] Orthogonal projection of a vector (idempotent, perp killed)
  #slide[
    == Orthogonal projection of a vector
    #uncover("2-",theorem([Projection is idempotent])[
      $uncover("3-",P_(bl(B))[P_(bl(B))[xx]] &= P_(bl(B))[xx]) $
    ])
    #uncover("3-",proof[#v(-1.7em)
      $ P_(bl(B))[P_(bl(B))[xx]]
      uncover("4-",&= (((xx lcont bl(B))lcont bl(B)^(-1))lcont bl(B))lcont bl(B)^(-1)) \
      uncover("5-",&= ((xx lcont bl(B))lcont bl(B)^(-1))and(bl(B) lcont bl(B)^(-1))) \
      uncover("6-",&= ((xx lcont bl(B))lcont bl(B)^(-1)) and 1)\
      uncover("7-",&= P_(bl(B))[xx])
      $#v(-.5cm)
    ])#v(-.5cm)
    #uncover("8-",theorem[Let $xx = xx_perp + xx_parallel$, #uncover("9-")[where $xx_perp lcont bl(B) = 0$]#uncover("10-")[ and $xx_parallel lcont bl(B) eq.not 0$. Then]
      $ uncover("11-",P_(bl(B))[xx] = xx_parallel) $
    ])
    #uncover("12-",proof[
      $P_(bl(B))[xx] 
      uncover("13-",= (xx_perp lcont bl(B)) lcont bl(B)^(-1) + (xx_parallel lcont bl(B))lcont bl(B)^(-1))
      uncover("14-",= 0 + xx_parallel wedge (bl(B)lcont bl(B)^(-1)))
      uncover("15-", = xx_parallel) $
    ])
  ]

  // [c, d, t] Orthogonal projection of a blade
  #slide[
    == Orthogonal projection of a blade
    #uncover("2-",definition([orthogonal projection of a blade])[
      *Orthogonal projection* of blade $bl(X)$ onto $bl(B)$
      $ P_(bl(B))[bl(X)] = (bl(X) lcont bl(B)) lcont bl(B)^(-1) $
    ])

    #uncover("3-",theorem[
      $bl(X) lcont bl(B) = P_(bl(B))[bl(X)] lcont bl(B) $
    ])
    #uncover("4-",note[
      - $bl(A)=bl(X) lcont bl(B)$ is _dual by_ $bl(B)$ to the projection of $bl(X)$ onto $bl(B)$
      #uncover("5-")[- _Contraction_: linear in both $bl(X)$ and $bl(B)$]
      #uncover("6-")[- _Projection_: linear only in $bl(X)$]
    ])
    #uncover("7-",definition([orthogonal projection of a blade])[
      $ P_(bl(B))[bl(X)] = (bl(X) lcont bl(B)^(-1))lcont bl(B) $
    ])
  ]

  // [c, g, d] The 3D cross product
  #slide[
    == The 3D cross product
    // #note([Uses of cross product])[
    //   #uncover("2-")[- Normal vectors]
    //   #uncover("3-")[- Rotational velocities]
    //   #uncover("4-")[- Intersecting planes]
    // ]
    #align(center)[
      #only("2-5", image("../gacs/figs/ext_alg_cross_normal.svg"))
      #only("6-9", image("../gacs/figs/ext_alg_cross_normal_velocity.svg"))
      #only("10-", image("../gacs/figs/ext_alg_cross_normal_velocity_intersectplane.svg"))
    ]

    #grid(columns: (.8fr, .8fr, 1fr), align:center, 
    uncover("2-")[
      *Normal vectors*\
      #uncover("3-")[Representing planes]\
      #uncover("4-")[Only in 3D]\
      #uncover("5-")[$t(aa times bb) eq.not t(aa) times t(bb) $]
    ],
    uncover("6-")[
      *Rotational velocities*
      #uncover("7-")[Rotation axis $aa$]\
      #uncover("8-")[Only in 3D]\
      #uncover("9-")[5D $V^((4,1))$]
    ],
    uncover("10-")[
      *Intersecting planes*\
      #uncover("11-")[Line of intersection]\
      #uncover("12-")[Only in 3D]\
      #uncover("13-")[$cancel("Other subspaces")$]
    ])

    #uncover("14-",definition([Cross product in 3D])[
      $ aa times bb uncover("15-", = (aa wedge bb)^ast)uncover("16-", = (aa wedge bb) lcont I_3^(-1)) $
    ])
  ]

  // [c, n, e] The 3D cross product
  #slide[
    == The 3D cross product
    #uncover("2-",note[
      $ a times b = mat(delim:"|", ee_1, ee_2, ee_3;a_1,a_2,a_3;b_1,b_2,b_3) $
    ])
    #uncover("3-",example[
      // $aa=(a_1,a_2,a_3), bb=(b_1, b_2, b_3)$, basis $(ee_1, ee_2, ee_3)$
      $ a times b &= (a_2 b_3 - a_3 b_2)ee_1 
        &+& (a_3 b_1 - a_1 b_3) ee_2 
        &+& (a_1 b_2 - a_2 b_1) ee_3 \ 
        a wedge b &= (a_2 b_3 - a_3 b_2)ee_2 wedge ee_3 
        &+& (a_3 b_1 - a_1 b_3) ee_3 wedge ee_1
        &+& (a_1 b_2 - a_2 b_1) ee_1 wedge ee_2  $
    ])
    #uncover("4-",note[
      $dual(ee_2 wedge ee_3)=(ee_2 wedge ee_3)lcont (ee_3 wedge ee_2 wedge ee_1) = ee_1$
      #uncover("5-")[- $a wedge b$ does not depend on the metric]
      #uncover("6-")[- $a wedge b$ works in any space (not only 3D)]
    ])
  ]

  // [c, g] The 3D cross produt
  #slide[
    == The 3D cross product

    #only("2-8", align(center,image("../gacs/figs/ext_alg_cross_normal_velocity_intersectplane.svg")))
    #only("9-", align(center,image("../gacs/figs/ext_alg_cross_normal_velocity_intersectplane2.svg")))
    #uncover("3-")[- *_Normal vectors_*: $aa wedge bb$ directly representing the plane.]
    #show par: set block(spacing: 0em, below: .0em, above: 0em)
    #show math.equation: set block(above: .5em)
    #uncover("4-")[- *_Velocities_*:
      $ aa times xx
        uncover("5-", = (aa wedge xx)^ast)
        uncover("6-", = -(xx wedge aa)^ast)
        uncover("7-", = -xx lcont dual(aa))
        uncover("8-", = text(myred, xx lcont bl(A)) quad bl(A) = undual(aa))
      $
    ]
    #uncover("10-")[- *_Intersecting planes_*: 
      $ aa times bb 
        uncover("11-",= (dual(bl(A)) wedge dual(bl(B))) lcont psinv(3))
        uncover("12-",= dual(bl(B)) lcont ((bl(A) lcont psinv(3))lcont ps(3))) 
        uncover("13-",= dual(bl(B)) lcont bl(A)) $
    ]
  ]

  // [c, t] Application: reciprocal frames
  #slide[
    == Application: reciprocal frames

    $ text(size:#30pt, only("1",cancel("coordinates"))only("2-","coordinates")) uncover("3-", quad arrow.long.double quad xx = sum_(i=1)^n x_i bb_i) $
    #v(-1cm)


    #align(center)[
      #only("4", image("../gacs/figs/ext_alg_reciprocal_basis_01.svg"))
      #only("5-9", image("../gacs/figs/ext_alg_reciprocal_basis_02.svg"))
      #only("10-", image("../gacs/figs/ext_alg_reciprocal_basis_03.svg"))
    ]

    #uncover("4-")[- Basis ${bb_i}^n_(i=1)$ orthonormal $quad arrow.double.long quad x_i = inner(xx, bb_i)$]

    #uncover("6-",definition([Reciprocal vector])[The *reciprocal vector* of $bb_i$ is the vector $bb^i$ s.t.:
      $ uncover("7-",bb^i = dual(bl(B)_i) = (-1)^(i-1) (bb_1 wedge bb_2 wedge dots.c wedge breve(bb)_i wedge dots.c wedge bb_n)lcont psninv) $
      #uncover("8-")[where $bb_i wedge bl(B)_i = bb_1 wedge dots.c wedge bb_n = psn$]
      #uncover("9-")[ and $bb_j wedge bl(B)_i = 0$ for $i eq.not j$.]
      #uncover("11-")[The set ${bb^i}_(i=1)^n$ form a basis for $V$]
      #uncover("12-")[called the *reciprocal basis* of ${bb_i}_(i=1)^n$.]
    ])
  ]

  // [c, t, p] Application: reciprocal frames
  #slide[
    == Application: reciprocal frames
    #definition([Reciprocal vector])[The *reciprocal vector* of $bb_i$ is the vector $bb^i$ s.t.:
      $ bb^i = dual(bl(B)_i) = (-1)^(i-1) (bb_1 wedge bb_2 wedge dots.c wedge breve(bb)_i wedge dots.c wedge bb_n)lcont psninv $
      where $bb_i wedge bl(B)_i = bb_1 wedge dots.c wedge bb_n = psn$ and $bb_j wedge bl(B)_i = 0$ for $i eq.not j$.
      The set ${bb^i}_(i=1)^n$ form a basis for $V$ called the *reciprocal basis* of ${bb_i}_(i=1)^n$.
    ]
    #uncover("2-",theorem[${bb_i}^n_(i=1)$ and ${bb^i}^n_(i=1)$ are mutually orthonormal])
    #uncover("3-",proof[
      $uncover("3-",inner(bb_i, bb^j))
        uncover("4-",= bb_i lcont (bl(B)_j lcont psninv))
        uncover("5-",= (bb_i wedge bl(B)_j) lcont psninv)
        uncover("6-",= delta_i^j psn lcont psninv)
        uncover("7-",= delta_i^j) $
    ])
    #uncover("8-",theorem[$xx = sum_i x^i bb_j$ where $x^i=inner(xx,bb^i)$])
    #uncover("9-",proof[
      $uncover("9-",inner(xx, bb^i))
        uncover("10-", = inner(\(sum_j x^j bb_j\), bb^i))
        uncover("11-", = sum_j x^j inner(bb_j, bb_i))
        uncover("12-", = sum_j x^j delta_i^j)
        uncover("13-", = x^i) $
    ])
    #uncover("14-", theorem[
      $bb_i = plus.minus bb^i$ if ${bb_i}^n_(i=1)$ is an orthonormal basis.
    ])
  ]

