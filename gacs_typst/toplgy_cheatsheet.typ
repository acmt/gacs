#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Topology Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)
#show_thm_legend #v(-1em)
#set heading(numbering: "1.1.")
#show outline.entry.where(
  level: 1
): it => {
  v(8pt, weak: true)
  strong(it)
}
#text(size:8.2pt,outline(depth:2,indent:1em, fill:none))

= Topological spaces

== Topology
#definition[*Power set* $cal(P)(X)$: set of all subsets of a non-empty set $X$.]
#definition[*Topological space* $(X,cal(T))$: non-empty set $X$ with a *topology* $cal(T)subset.eq cal(P)(X)$ which satisfies:
  + $X, emptyset in cal(T)$
  + ${S_i}_(i in I) subset cal(T) arrow.double union.big_(i in I) S_i in cal(T)$ (finite or infinite index set $I$)
  + $S_1,S_2 in cal(T) arrow.double S_1 sect S_2 in cal(T)$
] <d:topological_space>
#theorem([Generalization of @d:topological_space iii])[${S_1,dots,S_n} subset cal(T) arrow.double sect.big_i^n S_i in cal(T)$]
#example[

]
#example[]
#example[]

#example()[]
#definition()[
  Let $(X, cal(T))$ be a topological space. Then $(X, cal(T))$ is a
  + *discrete space* if $cal(T)$ is a *discrete topology*, i.e., $cal(T)h1=cal(P)(X)$.
  + *indiscrete space* if $cal(T)$ #h(-.1em) is an *indiscrete topology*, i.e., $cal(T)#h(-.2em)= h0 {X, h1 emptyset}$.
]


#theorem()[If $(X,cal(T))$ is a topological space and $forall x in X: {x} in cal(T)$, then $cal(T)$ is the discrete topology.]
== Open sets, Closed Sets, and Clopen Sets
#definition()[$S subset.eq X$ is an *open set* in $(X,cal(T))$ if $S in cal(T)$.]
#definition()[$S subset.eq X$ is a *closed set* in $(X,cal(T))$ if $X without S in cal(T)$.]
#definition()[$S subset.eq X$ is a *clopen set* in $(X, cal(T))$ if both $S, X without S in cal(T)$.]
#theorem[$X,h0 emptyset,h1 limits(union.big)_(i in I) h0 S_i$, and $limits(sect.big)_i^n h1 S_i$ (if $S_i$ #h(-.1em) is open) are open sets in $(X #h(-.2em), h1 cal(T))$.]
#theorem[$X,h0 emptyset,h1 limits(sect.big)_(i in I) h0 S_i$, and $limits(union.big)_i^n h1 S_i$ (if $S_i$ #h(-.1em) is closed) are closed sets in $(X #h(-.2em), h1 cal(T))$.]

== Finite-Closed Topology

#definition()[*Finite-closed topology* or *cofinite topology*: all closed subsets are $X$ and all finite subsets of $X$, i.e., all open subsets are only $emptyset$ and all $S subset X$ with finite complements.]



= The Euclidean Topology


== Euclidean Topology

#definition()[$S subset RR$ is open in the *Euclidean topology on* $RR$ if $ forall x in S: exists a,b in RR, a < b: x in (a,b) subset.eq S $]
#note[If no topology on $RR$ is cited, we refer to Euclidean topology.]
#theorem[
  + The Euclidean topology is a topology
  + In the Euclidean topology $cal(T)$ on $RR$, $forall r,s in RR: (r,s) in cal(T)$.
  + $forall r in RR, (r,infinity), (-infinity, r)$ are open sets in $RR$.
  + $forall a,b in RR, a < b: [a,b]$ is not open in $RR$, but it is closed in $RR$.
  + $forall a in RR: {a}$ is closed in $RR$.
  + $ZZ$ is closed in $RR$
  + $QQ$ is neither closed nor open in $RR$.
  + $RR, emptyset$ are the only clopen subsets in $RR$.
]

== Basis for a Topology

#theorem()[
  $S subset RR$ is open iff it is a union of open intervals
]

#definition()[A collection $cal(B)$ of open subsets of $X$ is a *basis* for $cal(T)$ if $ forall S in cal(T): S=union.big_i B_i, "where" B_i in cal(B). $
]
#note[$cal(T)$ is a basis for $cal(T)$.]

#theorem()[If $cal(B)$ is a basis for $cal(T)$ and $cal(B) subset.eq cal(C) subset.eq cal(T)$, then $cal(C)$ is a basis for $cal(T)$.]

#theorem[
  $cal(B) subset.eq cal(P)(X)$ is a basis for a topology on $X$ iff 
  + $X=union.big_(B in cal(B)) B$ and 
  + $B_1,B_2 in cal(B) arrow.double B_1 sect B_2=union.big_i B_i, "where" B_i in cal(B)$
]

== Basis for a Given Topology

#theorem()[
  A collection $cal(B)$ of open subsets of $X$ is a basis for $cal(T)$ iff $ forall U in cal(T): forall x in U: exists B in cal(B): x in B subset.eq U $
]

#theorem()[Let $cal(B)$ be a basis for $(X, cal(T))$. $U subset.eq X$ is open iff $ forall x in U: exists B in cal(B): x in B subset.eq U $]

#note[@t:bases_same_topology (below) tells when two bases define the same topology.]
#theorem()[
  Let $cal(B)_1, h1 cal(B)_2$ be bases for $cal(T)_(#h(-.2em)1), h1 cal(T)_(#h(-.2em)2)$, resp., on $X h1 eq.not h1 emptyset$. $cal(T)_(#h(-.2em)1) h0 = h1 cal(T)_(#h(-.2em)2)$ iff
  + $forall B in cal(B)_1, forall x in B h0: exists B' in cal(B)_2 h0: x in B' subset.eq B $ and
  + $forall B in cal(B)_2, forall x in B h0: exists B' in cal(B)_1 h0: x in B' subset.eq B $
] <t:bases_same_topology>

= Limit Points
== Limit Points and Closure

== Neighbourhoods
== Connectedness

= Homeomorphisms
== Subspaces
== Homeomorphisms
== Non-Homeomorphic Spaces

= Continuous Mappings
== Continuous Mappings
== Intermediate Value Theorem

= Metric Spaces
== Metric Spaces
== Convergence of Sequences
== Completeness
== Contraction Mappings
== Baire Spaces

= Compactness
== Compact Spaces
== The Heine-Borel Theorem

= Finite Products
== The Product Topology
== Projections onto Factors of a Product
== Tychonoff's Theorem for Finite Products
== Products and Connectedness
== Fundamental Theorem of Algebra

= Countable Products
== The Cantor Set
== The Product Topology
== The Cantor Space and the Hilbert Cube
== Urysohn's Theorem
== Peano's Theorem

= Tychonoff's Theorem
== The Product Topology For All Products
== Zorn's Lemma
== Tychonoff's Theorem
== Stone-C̆ech Compactification

= Quotient Spaces
== Quotient Spaces
== Identification Spaces
== Möbius Strip, Klein Bottle and Real Projective Space
== The Stone-Weierstrass Theorem
== The Weierstrass Approximation Theorem
== The Stone-Weierstrass Theorem
