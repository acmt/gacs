#import "base.typ": *
#show: thmrules

#show: doc => conf(
  title: [
    Geometric Algebra Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)

#show_thm_legend

#grid(columns: 2,column-gutter: .3em, [
    #tablex(columns:2, auto-vlines: false, align:(left+horizon,center+horizon),inset:(x:.2em,y:.3em), auto-lines: false,
        hlinex(stroke:.5pt),
        [Scalar],vlinex(stroke:.5pt),[$alpha, beta$],
        [Coefficients],[$alpha_i, beta^i$],hlinex(stroke:.5pt),
        [Vectors],[$cvec(a), cvec(b)$],
        [Orthonormal Basis vectors],[$cvec(e)_i$],
        [General Basis vectors],[$cvec(b)_i$],hlinex(stroke:.5pt),
        [Blade],[$blade(A), blade(B)$],
        [Blade of grade $k$],[$blade(A)_k, blade(B)_k$],
        [$k$-vector],[$G, H$],
        [General multivector],[$M, N$],hlinex(stroke:.5pt),
        [(Unit pseudoscalars)],[$blade(I)_n, blade(I)$],hlinex(stroke:.5pt),
      )
  ],[
    #tablex(columns:2, auto-vlines: false, align:(left+horizon,center+horizon),inset:(x:.2em,y:.3em), auto-lines: false,
        hlinex(stroke:.5pt),
        [Vector spaces],vlinex(stroke:.5pt),[$V, RR^n$],
        [Exterior power],[$ext^(k)V$],
        [Exterior algebra],[$ext V$],
        [Blade space],[$[A]$],hlinex(stroke:.5pt),
        [Exterior product],[$cvec(a) wedge cvec(b)$],
        [Grade projection],[$gproj(A, k)$],
        [Reversion],[$rev(blade(A))$],
        [Grade involution],[$invol(blade(A))$],
        [Clifford conjug.],[$cconj(blade(A))$],hlinex(stroke:.5pt)
      )
  ])

= Exterior algebra
#definition[*Grassmann's Exterior Algebra* (v1):
  $ext V = T(V)\/I$ s.t.\
  $T(V)$: Tensor algebra on $V quad I$: two-sided ideal where\
  $I=span({cv(v) otimes cv(v)#h(0em): cv(v) #h(0.1em) in #h(0.1em) V})=span({cv(v) otimes cv(w)+cv(w)otimes cv(v)#h(0em): cv(v),cv(w) #h(0.1em) in #h(0.1em) V})$
  ]
#definition[*Exterior/Wedge/Outer product*
  $vv wedge cv(w) = vv otimes cv(w) + I$]

#definition[$k$-*Exterior power*:
  quot. algebra $ext^k V = T^(k)(V)\/I_k$ where\
  $ I_k #h(0em)=#h(0em) span{cv(v)_1#h(-.2em) otimes dots.c otimes cv(v)_k#h(0em): cv(v)_1,dots,#h(0em)cv(v)_k#h(0.1em) in #h(0.1em) V#h(-0.0em), cv(v)_i#h(0em)=#h(0em) cv(v)_j "for some"  i #h(0em)eq.not#h(0em) j} $
  ]
#definition[
  $k$-*Blade* $bl(A)_k=vv_1 wedge dots wedge vv_k  = vv_(1 dots k) = vv_1 otimes dots.c otimes vv_k + I_k$
]
#definition[
  $k$-*Vector* $G=sum lambda_i bl(A)_i in ext^k V$ of *grade* $|A|=k$.
]
#definition[
  *Grassman's Exterior Algebra* (v2): graded algebra
  $ ext V = scripts(plus.circle.big)_k ext^k V = ext^0 V plus.circle ext^1 V plus.circle dots $
  with *exterior product* $wedge: ext^p V times ext^q V arrow ext^(p + q) V$
]
#note[
  _multivector_ #h(0cm)=#h(0cm) $sum_k lambda_k (k$_-vector_$) #h(0cm)=#h(0cm) sum_k lambda_k (sum_j lambda_j (k$-_blades_$))$
]
#example[#v(-1.3em)
  $ #h(.7em)underbrace(M, "multivector" \ and.big(V))
        = underbrace(5, "scalar" \ and.big^(0)(V))
        + underbrace(3cvec(a) + 2cvec(b),"vector" \ and.big^(1)(V))
        + underbrace(4cvec(b) wedge cvec(c) + 10 cvec(a) wedge cvec(c),"bivector" \ and.big^(2)(V))
        - underbrace(2 cvec(c) wedge cvec(a) wedge cvec(d) ,"trivector" \ and.big^(3)(V)) $
]

#definition[
  *Grassman's exterior Algebra* (v3): associative alg. $ext h0 V$ s.t.:
  + Contains $V$
  + *Exterior/Wedge/Outer product* $wedge: ext V times ext V arrow ext V$
  + Alternating on $V$, i.e., $vv wedge vv = 0$
]
#note[Some properties for vectors
  #let wii = 3cm
  #h(1cm)#list([
    #box(width: wii)[_Associative_:] $aa wedge (bb wedge cc) = (aa wedge bb) wedge cc$
    ],[
      #box(width: wii)[_Bilinear_:] $(alpha aa + beta bb) wedge cc = alpha aa wedge cc + beta bb wedge cc$
      #box(width: wii)[] $aa wedge (alpha bb + beta cc) = alpha aa wedge bb + beta aa wedge cc$
    ],[#box(width: wii)[_Antisymmetric_:] $aa wedge bb = -bb wedge aa$
    ],[#box(width: wii)[_Identity_:] $1 wedge aa = aa wedge 1 = aa$
    ],[#box(width: wii)[_Zero_:] $0 wedge aa = aa wedge 0 = 0$
    ],[#box(width: wii)[_Homogeneous_:] $alpha wedge aa = aa wedge alpha = alpha (1 wedge aa) = alpha aa$
    ])
]
#theorem[_Antisymmetry in a blade_:
  $ vv_(sigma(1)) wedge dots.c wedge vv_(sigma(k)) = "sg"(sigma) vv_(1 dots k)$
]
#corollary[*N* 1.2 is identical for $k$-vectors (incl. blades), except
  - #box(width: 3cm)[_Antisymmetric_:] $A wedge B = (-1)^(|A||B|) B wedge A$
]
#theorem[
  _Alternate_ property for $k$-blades $vv_(1 dots k)$
  $ vv_i=vv_j "for some" i eq.not j arrow.double.long vv_(1 dots k) = 0 $
]

#theorem[
  _Exterior linear dependency_
  $ vv_1,dots,vv_k "are linear dependent" arrow.l.r.double.long vv_(1 dots k) = 0 $
]

#definition[
  *Space* of a blade $[bl(A)]#h(0cm)=#h(0cm){vv in V#h(0cm): vv wedge bl(A)#h(0.05cm)=#h(0.05cm)bl(A)wedge vv = 0}<V$.
]

#theorem[_Blade space equivalence_
  $bl(A) #h(0em)=#h(0em) lambda vv_(1 dots k) #h(0.2em)arrow.double.l.r#h(0.1em) span{vv_1#h(-.1em),dots,vv_k}#h(0em)=#h(0em) [bl(A)] $
]

#theorem[$ext^k$ _is an isomorphism on spaces_: $ext^k [bl(A)_k] = span(bl(A)_k) $]


#theorem[_Blade Complement_
   $[bl(A)_k]<[bl(A)_l] arrow.double exists bl(A)_(l-k)#h(0cm):bl(A)_l=bl(A)_k wedge bl(A)_(l-k)$
]
#definition[ Let blades $bl(B)=lambda bl(A)$.
  - #box(width: 4.8cm)[*Attitude* of $bl(A)$] $[bl(A)] $
  - #box(width: 4.8cm)[*Weight* of $bl(A)$ *relative to* $bl(B)$] $lambda$
  - #box(width: 4.8cm)[*Orientation* of $bl(A)$ *relative to* $bl(B)$] $"sign"(lambda)$ (1:same, -1:different)
]
#lemma[
  $xx parallel aa arrow.double.l.r xx in span{aa} arrow.double.l.r xx =lambda aa arrow.double.l.r xx in [aa] arrow.double.l.r xx wedge aa = 0 $
]
#lemma[
  $xx in span{aa,bb}#h(0em)=#h(0em) [aa wedge bb] arrow.double.l.r xx = lambda aa + mu bb arrow.double.l.r xx wedge aa wedge bb = 0$
]

#theorem[
  $xx in [bl(A)=vv_(1 dots k)] arrow.double.r.l xx = sum_k lambda_k vv_k arrow.double.l.r xx wedge bl(A) = 0$
]
#definition[ *Blade containment*:
  $bl(A) subset.eq bl(B)$ if $[bl(A)] lt.eq [bl(B)]$
 ]
#theorem[
  $aa_(1 dots k) = bl(A) subset.eq bl(B) arrow.double.l.r aa_i wedge bl(B) = 0, forall i=1,dots,k$
]
#definition[ *Unordered indices*:
  $ uidx_k^n #h(0em) = {(i_1,dots,i_k) in NN^k: i_j lt.eq n, i_j eq.not i_k "if" j eq.not k} $
  $#h(.8cm) uidx_0^n={emptyset} wide
   #uidx^n = union.big_(p=0)^n uidx_k^n wide
   #uidx=union.big_(n=0)^infinity uidx^n$
]
#definition[ *Ordered indices*:
  $ oidx_k^n = {(i_1,dots,i_k) in NN^k: 1 lt.eq i_1 < dots < i_k lt.eq n} $
  $#h(1.2cm) oidx_0^n={emptyset} wide
   oidx^n = union.big_(p=0)^n oidx_k^n wide
   oidx=union.big_(n=0)^infinity oidx^n$
]
#definition[ *Multi-index notation*: $ idx(i)=(i_1,dots,i_k) =i_1 dots i_k arrow.double vv_idx(i)=vv_(i_1 dots i_k)=vv_(i_1)wedge dots wedge vv_(i_k) $
  - *Grade*: $|vv_idx(i)|=|vv_(i_1 dots i_k)|=|idx(i)|=|(i_1,dots,i_k)|=k$
  - *Sum*: $idx(i)=i_1 dots i_k arrow.double norm(idx(i))=i_1+dots+i_k$
  - *Reversion*: $idx(i)=i_1 dots i_k arrow.double rev(idx(i))=i_k dots i_1$
  ]
#definition[*Basis*
  $cal(B)_V#h(0cm)=#h(0cm){ee_i}_(i=1)^n #h(0cm)arrow.double#h(0cm) cal(B)_(ext^k V)#h(0cm)=#h(0cm){ee_idx(i)}_(idx(i)in oidx_k^n) #h(0cm)arrow.double#h(0cm) cal(B)_(ext V)#h(0cm)=#h(0cm){ee_idx(i)}_(i in oidx^n) $
]
#example[
  #move(dy:-1.3em,dx:3em,align(center)[
    #tablex(columns:5, auto-vlines: false, align:center+horizon,inset:.3em, auto-lines: false,
      [$k$],vlinex(),[0],[1],[2],[3],hlinex(),
      [basis for $ext^(k)(V)$],
      [{1}],
      [{$cvec(e)_1,cvec(e)_2,cvec(e)_3$}],
      [{$cvec(e)_(12), cvec(e)_(23), cvec(e)_(13)$}],
      [{$cvec(e)_(123)$}]
    )#v(-1.4em)
  ])
]
#definition[ *Dimension* $dim ext^k V #h(0em)= |cal(B)_(ext^k V)|$ and $dim ext V = |cal(B)_(ext V)|$]
#theorem[ $dim ext^k V = binom(n, k)$ and $dim ext V = sum_k binom(n, k) = 2^n$]

#definition[ *Pseudoscalar* $cal(B)_V = {ee_i}_(i=1)^n arrow.double ps(k) in ext^k V = ee_(1 dots k)$]

#theorem[ _Necessary_ and _sufficient_ conditions for a blade
  $ A eq.not 0 in ext^k V "is a blade" arrow.double.l.r A wedge A = 0 in ext^(2k) V arrow.double.l.r [A] lt.eq V $
]

#definition[*Grade projection* $(sum_i A_i)_k=A_k$]
#example[
  $ gproj(4
          +cvec(e)_2+3cvec(e)_3
          +34cvec(e)_(23) + 2cvec(e)_(12),2)
    = 34cvec(e)_(23) + 2cvec(e)_(12)$

  ]
#theorem[ _Outer prod. + grade projection_:
  $ A wedge B = #h(0em) limits(sum)_(k=0)^n limits(sum)_(l=0)^n (A)_k wedge (B)_l$
]
#theorem[ _Grade raising property_:
  $(A wedge B)_k=sum_(i=0)^k (A)_i wedge (B)_(k-i)$
]

#definition[ *Even projection*:
  $eproj(A) = sum_(k=0)^(floor(n/2)) gproj(A,2k) $
]
#definition[ *Odd projection*:
  $oproj(A) = sum_(k=0)^(ceil(n/2)-1) gproj(A,2k+1)$
]
#theorem[ $A = eproj(A)+oproj(A)$]

#definition[Blade *reversion*: $bl(A)=aa_(idx(i)) arrow.double rev(bl(A))=aa_(#h(.1em)rev(idx(i)))$]
#definition[$k$-Vector *reversion*: $A_k=sum_i bl(A)_i arrow.double rev(A)_k=sum_i rev(bl(A))_i$]

#theorem[_Antisymmetric prop. of reversion_: $rev(A)_k = (-1)^((k(k-1))/2)A_k$]
#note[#v(-.45cm)
  #align(center,tablex(columns: 10,inset: (top:.5em, rest:.3em),auto-lines: false, [k],vlinex(),0,1,2,3,4,5,6,7,$dots$,hlinex(),[$rev(A)\/A$],[+],[+],[-],[-],[+],[+],[-],[-],$dots$))
]
#definition[
  Multivector *reversion* $rev(A)=sum_k (#h(-.0em)A)^revalone_k$
]
#theorem[
  $rev(A)=sum_k (-1)^((k(k-1))/2)(A)_k$
]
#theorem[
  $(rev(A))^revalone = A, quad (A wedge B)^revalone = rev(B) wedge rev(A), quad (A)^revalone_k = \(rev(A)\)_k$
]
#definition[
  Blade *involution* $ invol(bl(A))_k = (aa_1 wedge dots wedge aa_k)^involalone = (-aa_1)wedge dots wedge (-aa_k) = (-1)^k bl(A)_k $
]
#definition[
  Multivector *involution* $invol(A) = sum_k (A)^involalone_k $
]
#theorem[
  $invol(A) = eproj(A) - oproj(A)$
]
#theorem[
  $\(invol(A)\)^involalone = A "and" (A wedge B)^involalone = invol(A)wedge invol(B)$
]
#note[#v(-.45cm)
  #align(center,tablex(columns: 10,inset: .3em,auto-lines: false, [k],vlinex(),0,1,2,3,4,5,6,7,$dots$,hlinex(),[$invol(bl(A))\/bl(A)$],[+],[-],[+],[-],[+],[-],[+],[-],$dots$))
]
#theorem[
  _Reversion and involution commute_: $invol(rev(A))=rev(invol(A))$
]
#definition[
  *Clifford conjugation* $cconj(A)=rev(invol(A))=invol(rev(A))$
]
#theorem[_Properties of Clifford conjugation_
  #grid(columns:(.3fr, .7fr),[
    - $cconj(cconj(A))=A$
    - $cconj((A)_k) = (cconj(A))_k$
    ],[
    - $cconj(A wedge B) = cconj(B) wedge cconj(A)$
    - $cconj(A) = sum_(k=0)^n (-1)^((k(k+1))/2)(A)_k$
  ])
]
#note[
  #align(center,move(dx:0em,dy:-1.5em,tablex(columns: 3,inset: (x:.2em, y:.3em), auto-lines: false,
      [*Operation*],vlinex(),[*Relative Weight*],vlinex(),[*Pattern*],hlinex(),
      [Reversion],$(-1)^(k(k-1)\/2)$,$++--++--$,
      [Grade involution],$(-1)^k$,$+-+-+-+-$,
      [Clifford conjug.],$(-1)^(k(k+1)\/2)$,$+--++--+$)))#v(-.6cm)
]

// Metric products

#definition[
  *Inner product*: bilinear form $inner(dot,dot)#h(0em):ext^k#h(0cm) V #h(0cm)times#h(0cm) ext^k#h(0cm) V #h(0.1em)arrow#h(0.1em) FF$, s.t.
  $ inner(aa_(1,dots,k),bb_(1 dots k))=det(inner(aa_i, bb_j)) quad "and" quad inner(alpha, beta)= overline(alpha) beta $
]

#theorem[
  _Inner product symmetry_:#h(-0.3em) $inner(B,A)#h(0.0em)=#h(0.0em)inner(A, B)#h(0.0em)=#h(0.0em)inner(rev(A), rev(B))#h(0.0em)=#h(0.0em)inner(rev(B), rev(A))$
]

#definition[
  *Scalar product* $bl(A) ast bl(B) = inner(rev(bl(A)),bl(B))$
]

#definition[
  *Norm* $norm(dot)$ *induced by* $inner(dot,dot)$: $norm(A)=sqrt(inner(A,A))=sqrt(rev(A) ast A)$
]

// Work a little bit with Mandolesi's angle paper
#theorem[
  *Angle* between blades: $inner(bl(A), bl(B)) = norm(bl(A))norm(bl(B))cos phi$
]
#definition[
  *Orthogonal* blades: $inner(bl(A),bl(B))=0 arrow.double.long.l.r A perp B $
]



#definition[
  Only the *left* ($N lcont M$) and *right* ($ M rcont N$) *contractions* of a 
  *contractor* $M h0 in h0 ext h0 V$ and a *contractee* $N h0  in h1  ext h0 V$, satisties, $forall X h0  in h0  ext h0 V$:
  $ inner(X,M lcont N) = inner(M wedge X, N) quad "and" quad inner(X,N rcont M) = inner(X wedge M, N) $
]
#note[$M lcont$ and $rcont M$ are the #text(myred,[_adjoint_]) operators of $M wedge$ and $wedge M$, resp.]

#proposition[
  For an orthonormal basis $(vv_1, dots, vv_n)$ and $idx(I),idx(J) in oidx^n$
  $ vv_idx(I) lcont vv_idx(J) =  $
]

#theorem[ Let $G in ext^p V, H in ext^q V$ and $M,N in ext V $
  + $G lcont H, H rcont G in ext^(q-p) V $
  + $p = q arrow.double A $
]


