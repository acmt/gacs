#import "base.typ": *
#show: thmrules
#show: doc => conf(
  title: [
    Diff Geom.-Lie Groups Cheatsheet
  ],
  authors: (
    (
      name: "Dr. Anderson Tavares",
      email: "acmt@outlook.com",
    ),
  ),
  doc,
)

#show_thm_legend #v(-1em)
#set heading(numbering: "1.1.")
#show outline.entry.where(
  level: 1
): it => {
  v(8pt, weak: true)
  strong(it)
}
#text(size:8.2pt,outline(depth:2,indent:1em, fill:none))

= The Matrix Exponential; Some Matrix Lie Groups
== The Exponential Map
#definition()[A *series* is an infinite sum $sum_(k=0)^infinity a_k, a_k in E$. The *partial sum* of the first $n+1$ elements is $S_n=sum_(k=0)^n a_k$]
#definition()[The series $sum_(k=0)^infinity a_k$ *converges* to the limit $a in E$ if the sequence $(S_n)$ converges to $a$, i.e., $forall epsilon.alt >0: exists N in NN_+,forall n lt.eq N: norm(S_n-a)<epsilon.alt$]
#theorem()[Let $A=[a_(i j)] h1 in h1 cal(M)_(m,n)$ 
  and let $mu=max{|a_(i j)|h0:1 lt.eq i,j lt.eq n}$.
  If $A^p = [a_(i j)^((p))]$, then $forall 1 lt.eq i,j lt.eq n: |a_(i j)^((p))| lt.eq (n mu)^p$
  ]
== Some Classical Lie Groups
== Symmetric and Other Special Matrices
== Exponential of Some Complex Matrices
== Hermitian and Other Special Matrices
== The Lie Group SE(n) and the Lie Algebra se(n)
== Problems
= Adjoint Representations and the Derivative of exp
== Adjoint Representations Ad and ad
== The Derivative of exp
== Problems
= Introduction to Manifolds and Lie Groups
== Introduction to Embedded Manifolds
== Linear Lie Groups
== Homomorphisms of Linear Lie groups and Lie Algebras
== Problems
= Groups and Group Actions
== Basic Concepts of Groups
== Group Actions: Part I, Definition and Examples 4.3 Group Actions: Part II, Stabilizers and Homogeneous Spaces
== The Grassmann and Stiefel Manifolds
== Topological Groups
== Problems
= The Lorentz Groups
== The Lorentz Groups O(n, 1), SO(n, 1) and SO0(n, 1)
== The Lie Algebra of the Lorentz Group SO0 (n, 1)
== The Surjectivity of exp : so(1, 3) → SO0 (1, 3)
== Problems
= The Structure of O(p, q) and SO(p, q)
== Polar Forms for Matrices in O(p, q)
== Pseudo-Algebraic Groups
== More on the Topology of O(p, q) and SO(p, q)
== Problems
= Manifolds, Tangent Spaces, Cotangent Spaces
== Charts and Manifolds
== Tangent Vectors, Tangent Spaces
== Tangent Vectors as Derivations
== Tangent and Cotangent Spaces Revisited
== Tangent Maps
== Submanifolds, Immersions, Embeddings
== Problems
= Construction of Manifolds From Gluing Data

== Sets of Gluing Data for Manifolds
== Parametric Pseudo-Manifolds
= Vector Fields, Integral Curves, Flows

== Tangent and Cotangent Bundles
== Vector Fields, Lie Derivative
== Integral Curves, Flows, One-Parameter Groups
== Log-Euclidean Polyaffine Transformations
== Fast Polyaffine Transforms
== Problems
= Partitions of Unity, Covering Maps

== Partitions of Unity
== Covering Maps and Universal Covering Manifolds
== Problems
= Basic Analysis: Review of Series and Derivatives

== Series and Power Series of Matrices
== The Derivative of a Function Between Normed Spaces
== Linear Vector Fields and the Exponential
== Problems

= A Review of Point Set Topology

== Topological Spaces
== Continuous Functions, Limits
== Connected Sets
== Compact Sets
== Quotient Spaces
== Problems

= Riemannian Metrics, Riemannian Manifolds

== Frames
== Riemannian Metrics
== Problems
= Connections on Manifolds

== Connections on Manifolds
== Parallel Transport
== Connections Compatible with a Metric
== Problems
= Geodesics on Riemannian Manifolds

== Geodesics, Local Existence and Uniqueness
== The Exponential Map
== Complete Riemannian Manifolds, Hopf-Rinow, Cut Locus
== Convexity, Convexity Radius
== Hessian of a Function on a Riemannian Manifold
== The Calculus of Variations Applied to Geodesics
== Problems
= Curvature in Riemannian Manifolds

== The Curvature Tensor
== Sectional Curvature
== Ricci Curvature
== The Second Variation Formula and the Index Form
== Jacobi Fields and Conjugate Points
== Jacobi Fields and Geodesic Variations
== Topology and Curvature
== Cut Locus and Injectivity Radius: Some Properties
== Problems
= Isometries, Submersions, Killing Vector Fields

==  Isometries and Local Isometries

==  Riemannian Covering Maps

==  Riemannian Submersions

==  Isometries and Killing Vector Fields

==  Problems

= Lie Groups, Lie Algebra, Exponential Map

== Lie Groups and Lie Algebras
== Left and Right Invariant Vector Fields, Exponential Map
== Homomorphisms, Lie Subgroups
== The Correspondence Lie Groups–Lie Algebras
== Semidirect Products of Lie Algebras and Lie Groups
== Universal Covering Groups ~
== The Lie Algebra of Killing Fields ~
== Problems
= The Derivative of exp and Dynkin's Formula

== The Derivative of the Exponential Map
== The Product in Logarithmic Coordinates
== Dynkin's Formula
== Problems
= Metrics, Connections, and Curvature on Lie Groups

== Left (resp. Right) Invariant Metrics
== Bi-Invariant Metrics
== Connections and Curvature of Left-Invariant Metrics
== Connections and Curvature of Bi-Invariant Metrics
== Simple and Semisimple Lie Algebras and Lie Groups
== The Killing Form
== Left-Invariant Connections and Cartan Connections
== Problems
= The Log-Euclidean Framework

== Introduction
== A Lie Group Structure on SPD(n)
== Log-Euclidean Metrics on SPD(n)
== A Vector Space Structure on SPD(n)
== Log-Euclidean Means
== Problems
= Manifolds Arising from Group Actions

== Proper Maps
== Proper and Free Actions

== Riemannian Submersions and Coverings

== Reductive Homogeneous Spaces

== Examples of Reductive Homogeneous Spaces

== Naturally Reductive Homogeneous Spaces

== Examples of Naturally Reductive Homogeneous Spaces

== A Glimpse at Symmetric Spaces

== Examples of Symmetric Spaces

== Types of Symmetric Spaces

== Problems
