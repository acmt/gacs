// Imports
  #import "@preview/polylux:0.3.1": *
  #import "@preview/ctheorems:1.1.2": *
  #import "@preview/tablex:0.0.8": tablex, gridx, hlinex, vlinex, colspanx, rowspanx

// Configuration

  #let wedge = $#h(0.05em) and #h(0.1em) $
  // #let wedge = $and$
  #let span = "span"
  #let gproj(elem, exp) = {
    if exp == none [
      $(elem)$
    ] else [
      $(elem)_exp$
    ]
  }
  #let myred = color.rgb("#990000")
  #let mylightred = color.rgb("#ffaaaa")
  #let mygreen = color.rgb("#006600")
  #let myblue = color.rgb("#000099")

  #let oidx = $cal(I)$
  #let uidx = $cal(M)$
  #let cconj(elem) = $overline(elem)$ //$ accent(elem, macron, size:#(200%+.25cm))$
  #let eproj(elem) = gproj(elem, $+$)
  #let oproj(elem) = gproj(elem, $-$)
  #let cp = (paint: red,thickness: 1pt)
  #let revalone = scale(x:130%, y: 110%, $tilde$)
  #let involalone = scale(x:150%,y:150%,$hat$)
  #let rev(symb) = {$accent(symb, tilde, size:#calc.max(50%+.3cm))$}
  #let invol(symb) = {$accent(symb, hat, size:#(50%+.3cm))$}
  #let cvec(elem)=$bold(upright(elem))$
  #let hvec(elem)=$cvec(tilde(elem, size:#100%))$
  #let uvec(elem)=$cvec(hat(elem, size:#160%))$
  #let blade(elem)=$cvec(elem)$
  #let bl(elem) = $blade(elem)$
  #let ord(elem)=$overline(elem)$
  #let pperp = [#h(0.5em)#scale(y:80%,move(dy:-.2em,[|]))#h(-.5em)#move(dy:.2em, scale(x:130%,[$tilde$]))#h(0.2em)]

  #let idx(elem)=$cvec(elem)$// $#text(font:"Latin Modern Sans", weight: "regular", elem)$
  #let ee = $cvec(e)$
  #let xx = $cvec(x)$
  #let aa = $cvec(a)$
  #let bb = $cvec(b)$
  #let cc = $cvec(c)$
  #let rfloor = $\u{230B}$
  #let lfloor = $\u{230A}$
  #let lcont = $rfloor$
  #let rcont = $lfloor$
  #let ext = $scripts(and.big)$
  #let hidden-slide(body)={}
  #let inner(val1,val2)=$angle.l val1,val2 angle.r$
  #let uni-colors = state("uni-colors", (:))
  #let uni-short-title = state("uni-short-title", none)
  #let uni-short-author = state("uni-short-author", none)
  #let uni-short-date = state("uni-short-date", none)
  #let uni-progress-bar = state("uni-progress-bar", true)
  #let psn = $bl(I)_n$
  #let ps(pwr) = $bl(I)_pwr$
  #let psninv = $bl(I)_n^(-1)$
  #let psinv(pwr) = $ps(pwr)^(-1)$
  #let dual(elem) = $elem^ast$
  #let undual(elem) = $elem^ast.circle$
  #let myline = line(length: 28%, stroke:  (paint:myred, dash: "dashed"))
  #let yy = $cvec(y)$
  #let adj(func)=$overline(func)$
  #let implies=$quad arrow.double.long quad$


  #let footer = {
    set text(size: 10pt)
    set align(center + bottom)
    let cell(fill: none, it) = rect(
      width: 100%, height: 100%, inset: 1mm, outset: 0mm, fill: fill, stroke: none,
      align(horizon, text(fill: white, it))
    )
    locate( loc => {
      let colors = uni-colors.at(loc)

      show: block.with(width: (100%+3.5cm), height: auto, fill: rgb("#660000"))
      grid(
        columns: ((25%+3.5cm), 1fr, 15%, 10%),
        rows: (1.5em, auto),
        move(dx:-3.5cm,cell(fill: rgb("#990000"), "Anderson Tavares")),
        cell("Ch 2: Exterior Algebra"),
        cell(uni-short-date.display()),
        cell(logic.logical-slide.display() + [~/~] + utils.last-slide-number)
      )
    })
  }
  #let acmt-theme(body) = {
    set page(paper: "presentation-16-9", margin: (top:.75cm,left:.25cm,right:.25cm,bottom:0cm), footer: footer, footer-descent: 0em)
    set text(size: 21pt, stretch: 30%)
    show heading: set text(rgb("#aa0000"))
    show par: set block(spacing: 1em)
    show math.equation: set block(spacing: 1em)
    show footnote.entry: set text(size: 1em)
    show: thmrules
    body
  } 
  #let color_t = rgb("#006600")
  #let color_p = rgb("#777700")
  #let color_c = rgb("#006600")
  #let color_d = rgb("#990000")
  #let color_l = rgb("#009900")
  #let color_e = rgb("#000099")
  #let color_n = rgb("#000000")
  
  #let titlefmt(color) = {
    let f(body) = {text(fill:color, weight: "bold", body)}
    return f
  }
  #let mybox(name, title, fill, stroke) = {thmbox(name, title, fill: fill, radius:0cm, inset: (x: .3em, y: .5em), stroke: (left:stroke + 2pt, top:(paint:stroke,thickness:.0pt,dash:"solid")), spacing:0em, titlefmt:titlefmt(stroke), padding: (top: 0em, bottom: 0em), separator: [#h(0.1em)],base_level:1).with(numbering: none)}

  // #let mybox(name, title, fill, stroke) = {thmbox(name, title, fill: fill, radius:0cm, inset: (x: .5em, y: .5em), stroke: (left:stroke + 2pt), spacing:0em, titlefmt:titlefmt(stroke), padding: (top: 0em, bottom: -1em),).with(numbering: none)}
  // #let theorem = mybox("theorem", "Theorem", rgb("#ddffdd"), rgb("#006600"))
  // #let proposition = mybox("proposition", "Proposition", rgb("#ffeebb"), rgb("#777700"))
  // #let corollary = mybox("corollary", "Corollary", rgb("#ddffdd"), rgb("#006600"))

  // #let lemma = mybox("lemma","Lemma", rgb("#eeffee"), rgb("#009900"))
  // #let definition = mybox("definition", "Definition", rgb("#ffeeee"), rgb("#990000"))
  // #let example = mybox("example", "Example", rgb("#eeeeff"), rgb("#000099"))
  // #let proof = thmproof("proof", "Proof").with(numbering: none)
  // #let note = thmplain("note", "Note", inset:(left:0em,right:0em)).with(numbering: none)

  #let theorem = mybox("theorem", "T", rgb("#ddffdd"), color_t)//.with(numbering:"1.")
  #let proposition = mybox("theorem", "P", rgb("#ffeebb"), color_p)
  #let corollary = mybox("theorem", "C", rgb("#ddffdd"), color_c)

  #let lemma = mybox("theorem","L", rgb("#eeffee"), color_l)
  #let definition = mybox("definition", "D", rgb("#ffeeee"), color_d)//.with(numbering:"1.")
  #let example = mybox("example", "E", rgb("#eeeeff"), color_e)
  #let note = mybox("note", "N", rgb("#f1f1f1"), color_n)

  
  #let slide(body) = {
    set page(margin: (bottom: .7cm, left: 3.5cm, rest:0em))
    let g = grid(columns: (0cm, 1fr), [

    
    // sidebar
      #place(top+right, float:true, rect(height: (100%+0.5em), width: 3.5cm, fill: rgb("#222222"), inset: 5pt, text(white,[
        #set text(size: 14pt, spacing: 2em, weight: "bold", font: "DejaVu Sans")
        #set align(center)
        Linear Algebra


        #set text(size: 10pt, spacing: .3em, weight: "regular")
        #set align(left)
        // #outline(title: none)
      ])))
      #v(-2cm)],pad(top:10pt, rest:5pt, body)
    )
    logic.polylux-slide(g)
  }

  #let title-slide(
    title: [],
    subtitle: none,
    authors: (),
    institution-name: "University",
    date: none,
    logo: none,
  ) = {
    let authors = if type(authors) ==  "array" { authors } else { (authors,) }

    let content = locate( loc => {
      let colors = uni-colors.at(loc)

      if logo != none {
        align(right, logo)
      }

      align(center + horizon, {
        block(
          inset: 0em,
          breakable: false,
          {
            text(size: 2em, fill: myred, strong(title))
            if subtitle != none {
              parbreak()
              text(size: 1.2em, fill: myred, subtitle)
            }
          }
        )
        set text(size: .8em)
        grid(
          columns: (1fr,) * calc.min(authors.len(), 3),
          column-gutter: 1em,
          row-gutter: 1em,
          ..authors.map(author => text(fill: black, author))
        )
        v(1em)
        if institution-name != none {
          parbreak()
          text(size: .9em, institution-name)
        }
        if date != none {
          parbreak()
          text(size: .8em, date)
        }
      })
    })

    logic.polylux-slide(content)
  }