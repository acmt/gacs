#import "acmt.typ": *

#show: acmt-theme.with()

/* START OF DOCUMENT */

// Status of the slide:
  // d definition
  // r corollary
  // t theorem
  // l lemma
  // n note
  // p proposition
  // g graphical pictures
  // e example
  // c complete
  // * ongoing

  #title-slide(title: "Geometric Algebra", subtitle: "Chapter 5: Linear Transformations of Subspaces", authors: "Anderson Tavares", institution-name: "", date: datetime.today().display())

  // [c, d, g, e] Linear transformations of vectors
  #slide[
    == Linear transformations of vectors
    #uncover("2-",definition([Linear transformation])[
      *Linear transformation*: $f:V arrow.r W$ where:
    $ f[alpha xx +beta yy] = alpha f[xx]+  beta f[yy] $
    #uncover("3-")[The set of all linear transformations $f:V arrow.r W$ is denoted as $cal(L)(V, W)$.]
    ])
    #uncover("4-",note([Properties of linear transformations])[#v(-1cm)
      #align(center)[
        #only("5",image("../gacs/figs/ext_alg_lin_transf_homog.svg",width: 12cm))
        #only("6-",image("../gacs/figs/ext_alg_lin_transf_homog_addit.svg",width: 12cm))
      ]

      #align(center, grid(columns: (5cm, 9cm), 
        uncover("5-")[$f[alpha xx] = alpha f[xx] $ \ *_Homogeneous_*],
        uncover("6-")[$f[xx + yy] = f[xx] + f[yy]$ \ *_Additive_*])
      )

      // #uncover("3-")[- #box(width: 5cm)[:] $#box(width: 3cm, $f[alpha xx]$) = alpha f[xx] $]
      // #uncover("4-")[- #box(width: 5cm)[:] $#box(width: 3cm, $f[xx + yy]$) = f[xx] + f[yy]$]
    ])
    #uncover("7-",example[Scaling (e.g., reflection), shearing, and rotation#uncover("8-")[, but *not* translation.]
    ])

  ]

  // [c, d, t] Outermorphisms: linear transformations of blades
  #slide[
    == Outermorphisms
    #only("1-2",rect(width: 100%, inset: .5em, stroke: myred)[
      #box(grid(columns: (20%,80%),
      text(myred)[#v(.1em)$cal(L)(ext(V),ext(W))$],
      uncover("2-")[$ f[alpha bl(A) + beta bl(B)] = alpha f[bl(A)] + beta f[bl(B)] $]
    ))])
    #only("3-",rect(width: 100%, inset: .5em, stroke: myred)[
      #box(grid(columns: (20%,40%,40%),
      text(myred)[#v(.1em)$cal(L)(ext(V),ext(W))$],
      uncover("4-")[$ f[xx wedge yy] = f[xx] wedge f[yy] $],
      uncover("5-")[$ g[xx wedge yy] eq.not g[xx] wedge g[yy] $]
    ))])
    #v(-.5cm)
    #uncover("6-",definition([Outermorphism])[
      #uncover("7-")[A _linear_ function $f:ext(V) arrow ext(W)$ is an *outermorphism*]#uncover("8-")[ if it is also an #text(myred)[_unital algebra homomorphism_]:]
      #uncover("9-")[$ f[bl(A) wedge bl(B)] &= f[bl(A)] wedge f[bl(B)], quad forall bl(A),bl(B) in ext(V) $
      #uncover("10-")[$
        g: V arrow W wide uncover("11-",f[aa] = g[aa])uncover("12-", wide f[aa wedge bb] = g[aa] wedge g[bb])
      $]
      ]
    //   #uncover("7-")[The extension of a linear map $f:V arrow W$ to an]
    //   #uncover("8-")[ *outermorphism*]
    //   #uncover("9-")[is the unique map $f:ext(V) arrow ext(W)$ satisfying:]
    // $ f(1)  &= 1 \ 
    //   f[bl(A) wedge bl(B)] &= f[bl(A)] wedge f[bl(B)] \ 
    //   f[bl(A) + bl(B)] &= f[bl(A)] + f[bl(B)]
    //   $
    //   Outermorphism is an #text(myred)[_unital algebra homomorphism_] between exterior algebras.
    ])
    #uncover("13-",theorem[ $f[alpha] = alpha$ and $f[bl(A)_1 wedge dots.c wedge bl(A)_k] = f[bl(A)_1]wedge dots.c wedge f[bl(A)_k]$
    ])
    #uncover("14-",theorem([Some geometrical properties])[
      #uncover("15-")[- #box(width:10cm)[*_Blades remain blades_*:] $V(bl(A))$ is a subspace $arrow.double$ $V(f[bl(A)])$ is a subspace]
      #uncover("16-")[- #box(width:10cm)[*_Grades are preserved_*:] $f[gproj(bl(A),k)]=gproj(f[bl(A)],k)$]// "grade"(f[bl(A)]) = "grade"(bl(A))$]
      #uncover("17-")[- #box(width:10cm)[*_Preservation of factorization_*:] $bl(C) subset.eq bl(A) sect bl(B) arrow.double f[bl(C)] subset.eq f[bl(A)] sect f[bl(B)]$]
    ])
  ]
  // [c, e, g] Examples of outermorphisms
  #slide[
    == Examples of outermorphisms
    #uncover("2-")[
      #example([Uniform scaling])[
      #uncover("3-")[$S[xx] = alpha xx$.]
      #uncover("4-")[For $bl(A)=aa_1wedge dots.c wedge aa_k$:]
      #uncover("5-")[$ S[bl(A)] = S[aa_1] wedge dots.c wedge S[aa_k] = alpha^k bl(A) $ #v(-.5em)]
      #uncover("6-")[#note[Orientation is preserved for even blades.]]
    ]
    #v(-4.2cm)#align(right,image("../gacs/figs/ext_alg_uniform_scaling.svg", width: 4.5cm))#v(-1em)
    ]
    #v(1cm)
    #uncover("7-")[
      #example([Parallel projection onto a line])[
        #uncover("8-")[Let $P[aa]=aa, P[bb]=0$]
        #uncover("9-")[$ P[xx] = P[alpha aa + beta bb] = alpha aa $]
        #uncover("10-")[$ P[aa wedge bb] = P[aa]wedge P[bb] = aa wedge 0 = 0 $]
      ]
    #v(-3.2cm)#align(right,image("../gacs/figs/ext_alg_parallel_projection.svg", width: 4.5cm))
    #v(-1em)
    ]
    #uncover("11-")[
      #example([Planar rotation])[#v(.3em)
      #uncover("12-")[$ R[cvec(u)wedge cvec(v)] = R[cvec(u)] wedge R[cvec(v)]=cvec(v) wedge cvec(w)uncover("13-",=cvec(u)wedge cvec(v)) $]
      #uncover("14-",note[The 1D subspace $lambda cvec(u) wedge cvec(v)$ is invariant to $R$]) #v(.3em)
    ]
    #v(-4.25cm)#align(right,image("../gacs/figs/ext_alg_planar_rotation.svg", width: 4.5cm))
    #v(-.7cm)
    ]
  ]
  
  // [c, e, g] Examples of outermorphism: point reflection, orthogonal projection
  #slide[
    == Examples of outermorphisms
    #uncover("2-")[
    #example([Point reflections])[
      #uncover("3-")[$S[bl(A)_k]=(-1)^k bl(A)_k$.] 
      #uncover("4-",note[All even blades (regardless of $dim V$) are invariants.])
    ]
    #v(-2.8cm)#align(right,image("../gacs/figs/ext_alg_point_reflection.svg", width: 6cm))
    #v(-.5cm)
    ]
    #let binv = $bl(B)^(-1)$
    #let blb = $bl(B)$
    #uncover("5-",example([Orthogonal projection])[
      #columns(2)[
      $ 
      uncover("6-",P_bl(B)[xx] wedge P_bl(B)[yy] &= P_bl(B)[xx] wedge ((yy lcont blb)lcont binv)) \ 
      uncover("7-",&= (P_bl(B)[xx] lcont (yy lcont blb)) lcont binv) \ 
      uncover("8-",&= ((P_bl(B)[xx]wedge yy) lcont blb)lcont binv) \
      uncover("9-",&= -((yy wedge P_bl(B)[xx])lcont blb) lcont binv)\ 
      uncover("10-",&= - (yy lcont (P_bl(B)[xx] lcont blb)) lcont binv) \
      $ #colbreak() $
      uncover("11-",&= - (yy lcont ((xx lcont blb) wedge (binv lcont blb)))lcont binv)\
      uncover("12-",&= - (yy lcont (xx lcont blb))lcont binv)\
      uncover("13-",&= - ((yy wedge xx)lcont blb)lcont binv)\
      uncover("14-",&= ((xx wedge yy) lcont blb) lcont binv)\
      uncover("15-",&= P_bl(B)[xx wedge yy])

      $
      ]
      
    ])
  ]

  // [c, d, e, g] Determinant of linear transformations
  #slide[
    == Determinant of a linear transformation

    #uncover("2-",definition([determinant of a linear transformation])[
      $ f[psn] = det(f) psn $
    ])
    #uncover("3-")[#example([Determinant of a rotation])[
      #uncover("4-")[$ R[ps(2)] = ps(2) arrow.double.long det(R) = 1$]
      #uncover("5-")[$ R[psn]uncover("6-",=R[ps(2)] wedge R[ps(n-2)]= ps(2)wedge) uncover("7-", ps(n-2))uncover("8-",=psn arrow.double.long det(R) = 1) $]
    ]
    #v(-3.7cm)#uncover("5-",align(right,image("../gacs/figs/ext_alg_rotation_n.svg",width: 3.5cm)))#v(-.8cm)
    ]
    #uncover("9-",example([Determinant of a point reflection])[
      #uncover("10-")[$ f[psn] = (-1)^n psn uncover("11-",quad arrow.double.long quad det(f)=cases(1 &"for even dimensions" ,-1 &"for odd dimensions")) $]
    ])
    #uncover("12-",example([Determinant of a projection onlo a line])[
      $ det(P) = cases(0 "if" n > 1, 1 "if" n = 1) $
    ])
  ]

  // [c, t, p, r] Determinant of a linear transformation
  #slide[
    == Determinant of a linear transformation
    #uncover("2-",theorem([Composition rule of determinants])[
      $det(g circle.tiny f) = det(g)det(f)$
    ])
    #uncover("3-",proof[#v(-1.3cm)
      $ det(g circle.tiny f)psn 
      uncover("4-",&= (g circle.tiny f)[psn])\ 
      uncover("5-",&= g[f[psn]])\ 
      uncover("6-",&= det(f)g[psn])\ 
      uncover("7-",&= det(g)det(f) psn) $#v(-1.5cm)
    ])
    #uncover("8-",corollary[
      $det(f^k)=det(f)^k$
    ])
    #uncover("9-",corollary[If $f^(-1)$ exists, then 
      $det(f)det(f^(-1))=1$
    ])
    #uncover("10-",corollary[
      $f[psninv]=det(f)psninv$
    ])
  ]

  

  // [c, t, d] Linear transformation of metric products
  #slide[
    == Linear transformation of metric products
    #uncover("1-",rect(width: 100%, inset: .5em, stroke: myred)[
      #columns(4, align(center)[
        $f[A wedge B]$
        #colbreak()
        #uncover("2-")[$f[A ast B]$]
        #colbreak()
        #uncover("3-")[$f[A lcont B]$]
        #colbreak()
        #uncover("4-")[$f[B rcont A]$]
      ])
    ])
    #uncover("5-",theorem([Linear transformation of scalar product])[
      $f[bl(A) ast bl(B)] = bl(A) ast bl(B)$
    ])
    #uncover("6-",definition([Coordinate map])[
      #uncover("7-")[The *coordinate map* $phi_B:V arrow K^n$ generates for each $cvec(v)=alpha_1 bb_1 + dots.c + alpha_n bb_n$]
      #uncover("8-")[its *coordinate matrix* w.r.t. ordered basis $B=(bb_1,dots,bb_n)$:]
      #v(-1cm)
      #uncover("9-")[$ phi_B (v)= mtx(v)_B = mat(delim:"[",alpha_1;dots.v;alpha_n) $]
    ])
  ]

  // [c,t] Coordinate and linear transformation matrices
  #slide[
    == Linear transformation matrices
    #show math.equation: set block(spacing: 0.7em)
    #show par: set block(spacing: 0em)
    
    
    #uncover("2-",theorem([Matrix of a linear transformation])[
      #uncover("3-")[Let $f in cal(L)(V, W)$ and let $B=(bb_i)_(i=1)^n$ and $C$ be ordered bases for $V$ and $W$, respectively. Then:]
      
      #uncover("4-")[$ mtx(f[cvec(v)])_C = mtx(f)_(C,B)mtx(cvec(v))_B $]
      #uncover("5-")[#v(-.5em)where #v(-.5em)]
    
      $ uncover("5-",mtx(f)_(C,B))uncover("6-", = (mtx(f[bb_1])_C | dots | mtx(f[bb_n])_C)) $
      
      #uncover("5-")[is called a *matrix of* $f$ *with respect to the bases* $B$ and $C$.] #uncover("7-")[When $V = W$ and $B=C$, we denote $mtx(f)_(B,B)$ by $mtx(f)_B$ and so
      #v(.5em)
      $ mtx(f[cvec(v)])_B = mtx(f)_B mtx(cvec(v))_B $]
    ])
  ]

  // [c, d, t, p] Adjoint of a linear transformation
  #slide[
    #show math.equation: set block(spacing: 0.7em)
    #show par: set block(spacing: 0em)
    
    == Adjoint of a linear transformation
    #uncover("2-",definition([Adjoint transformation])[Every $f in cal(L)(ext(V))$ has an *adjoint* $adj(f) in cal(L)(ext(V))$ such that:
      $ inner(adj(f)[aa], bb) = inner(aa, f[bb]) quad forall aa,bb in V $
    ])
    #uncover("3-",theorem[For any basis $B$ of $V$, $mtx(adj(f))_B = mtx(f)^top_B$])
    #uncover("4-",proof[
      $mtx(aa)^top mtx(f)mtx(bb) = \(mtx(adj(f))mtx(aa)\)^top mtx(bb) = mtx(aa)^top mtx(adj(f))^top mtx(bb) $
    ])
    #uncover("5-",definition([Adjoint transformation of blades])[
      $ adj(f)[bl(A)] ast bl(B) = bl(A) ast adj(bl(B)) $
    ])
    #uncover("6-",theorem[
      $adj(adj(f)) = f$
    ])
    #uncover("7-",theorem[
      $adj(f^(-1))=adj(f)^(-1)$
    ])
  ]

  // [c, e] Example of adjoint
  #slide[
    == Examples of adjoints
    #uncover("2-",example([Uniform scaling])[
      $f[xx]=alpha xx arrow.double inner(adj(f)[xx], aa)= inner(xx, alpha aa) = inner(alpha xx, aa)$ 
      $ uncover("3-",adj(f)=f)uncover("4-", quad arrow.double.long quad  adj(f)[bl(X)]=f[bl(X)]) $
    ])
    #uncover("5-",example([Point reflection])[
      $alpha=-1 implies f^(-1)=f$
    ])
    #uncover("6-",example([Projection])[
      $ P[xx] = aa (xx wedge bb)/(aa wedge bb)uncover("7-", = aa(xx wedge bb)lcont (aa wedge bb)^(-1))uncover("8-", implies adj(P)[xx] = inner(xx,aa)bb lcont (aa wedge bb)^(-1)) $
    ])
  ]

  // [c, l, p, t] Linear transformation of the contraction 
  #slide[
    == Linear transformation of the contraction

    #uncover("2-",lemma[
      $f[adj(f)[bl(A)] lcont bl(B)] = bl(A) lcont f[bl(B)]$
    ])
    #uncover("3-",proof[#v(-1.3cm)
      $ bl(X) ast (bl(A)lcont f[bl(B)]) &= (bl(X)wedge bl(A))ast f[bl(B)] \ 
      uncover("4-",&=adj(f)[bl(X) wedge bl(A)] ast bl(B)) \
      uncover("5-",&= (adj(f)[bl(X)]wedge adj(f)[bl(A)]) ast bl(B)) \
      uncover("6-",&= adj(f)[bl(X)] ast (adj(f)[bl(A)] lcont bl(B))) \
      uncover("7-",&= bl(X) ast f[adj(f)[bl(A)]lcont bl(B)])
      $#v(-1.5cm)
    ])
    #uncover("8-",theorem([Contraction transformation])[
      $f[bl(A) lcont bl(B)] = adj(f)^(-1)[bl(A)]lcont f[bl(B)]$
    ])
  ]

  // [c, d, t, e] Orthogonal transformation
  #slide[
    #show math.equation: set block(spacing: 0.7em)
    #show par: set block(spacing: 0em)
    
    == Orthogonal transformation

    #definition([Isometry])[
      #uncover("2-")[A bijective $ f:V arrow W$ is an *isometry* if]
      #uncover("3-")[$ inner(f[aa], f[bb]) = inner(aa,bb) $]
      #uncover("4-")[Isometry of nonsingular orthogonal geometry $V arrow$ *orthogonal transformation*.\ ]
      #uncover("5-")[*Orthogonal group* $cal(O)(V)$: set of all orthogonal transformations on $V$.\ ]
      #uncover("6-")[Isometry of nonsingular symplectic geometry $V arrow$ *symplectic transformation*.]
      #uncover("7-")[*Symplectic group* $"Sp"(V)$: set of all symplectic transformations on $V$.]
    ]
    #uncover("8-",theorem[
      For an orthogonal transformation $f$, then $adj(f)=f^(-1)$
    ])
    #uncover("9-",proof[
      Let $x=f[aa]$, then $inner(xx, f[bb])=inner(f^(-1)[xx], bb)$
    ])
    #v(-.3cm)
    #uncover("10-",theorem[
      If $f$ is an orthogonal transformation, then $f[bl(A)lcont bl(B)]=f[bl(A)] lcont f[bl(B)]$
      Therefore, $f$ is both an #text(myred)[_outermorphism_] and an #text(myred)[_innermorphism_].
    ])
    #uncover("11-",example[
      Reflections and rotations
    ])
  ]

  // [c, d, t, p, g, e] Transforming a dual representation
  #slide[
    == Transforming a dual representation
    #show math.equation: set block(spacing: 0.7em)
    #show par: set block(spacing: 0em)
    
    #uncover("5-",definition([Dual transformation])[$dual(f)[dual(bl(X))]=dual((f[bl(X)]))$])
    #uncover("6-",theorem[$dual(f)[dual(bl(X))] = 
     uncover("13-",det(f)adj(f)^(-1)[dual(bl(X))])$])
    #uncover("7-",proof[#v(-1.1cm)$ dual(f)[dual(bl(X))] 
    uncover("8-",&= dual((f[bl(X)]))) 
    uncover("9-",= f[dual(bl(X))lcont psn]lcont psninv) \
    uncover("10-",&= (adj(f)^(-1)[dual(bl(X))] lcont f[psn])lcont psninv) \
    uncover("11-",&= adj(f)^(-1)[dual(bl(X))] wedge (f[psn]lcont psninv))
    uncover("12-",= det(f) adj(f)^(-1)[dual(bl(X))])
    $#v(-1.3cm)])
    #v(-9cm)
    #only("2",align(right,image("../gacs/figs/ext_alg_dual_transformation_1.svg")))
    #only("3",align(right,image("../gacs/figs/ext_alg_dual_transformation_2.svg")))
    #only("4-",align(right,image("../gacs/figs/ext_alg_dual_transformation_3.svg")))
    #v(0.5cm)
    #uncover("14-",example([Scaling in $ee_1$])[$f[alpha ee_1 + beta ee_2]=alpha/2 ee_1 + beta ee_2$\
      $ uncover("15-",f[ee_1 wedge ee_2] = f[ee_1]wedge f[ee_2])uncover("16-",=ee_1/2 wedge ee_2 = 1/2 ps(2))uncover("17-", arrow.double det(f)=1/2) $

      $ uncover("18-",inner(adj(f)(alpha_1 ee_1 + alpha_2 ee_2), beta_1 ee_1 + beta_2 ee_2) &= inner(alpha_1 ee_1 + alpha_2 ee_2,f(beta_1 ee_1 + beta_2 ee_2))) \ 
      uncover("19-",&= inner(f(alpha_1 ee_1 + alpha_2 ee_2),beta_1 ee_1 + beta_2 ee_2))uncover("20-", arrow.double adj(f)=f)$
      // inner(adj(f)(alpha_1 ee_1),beta_1 ee_1) + inner(adj(f)(alpha_2 ee_2),beta_2 ee_2) = inner(alpha_1\/2 ee_1,beta_1 ee_1) + inner(beta_1 ee_1, beta_2 ee_2) arrow.double adj(f)=f $
    ])
  ]

  // [c, t, r, g] Transforming a dual representation
  #slide[
    == Transforming a dual representation
    #uncover("2-",theorem[If $f$ is an orthogonal transformation, then $dual(f)[dual(bl(X))]=plus.minus f[dual(bl(X))]$])
    #uncover("3-",corollary[If $f$ is a rotation, then $dual(f)[dual(bl(X))]= f[dual(bl(X))]$])
    #align(center)[
      #only("4",image("../gacs/figs/ext_alg_orthogonal_transformation_1.svg"))
      #only("5",image("../gacs/figs/ext_alg_orthogonal_transformation_2.svg"))
      #only("6",image("../gacs/figs/ext_alg_orthogonal_transformation_3.svg"))
    ]
  ]

  // [c, g] Linear transformation of the cross product
  #slide[
    == Linear transformation of the cross product

    #align(center,image("../gacs/figs/ext_alg_cross_product.svg"))
    #uncover("2-")[- Cross products as duals of blades]
    #uncover("3-")[$ aa times bb = dual((aa wedge bb)) = (aa wedge bb)lcont psinv(3) $]
    #uncover("4-")[- How $f$ maps cross products]
    #uncover("5-")[$ aa times bb arrow.bar det(f)adj(f)^(-1)[aa times bb] eq.not f[aa] times f[bb] $]

    #uncover("6-")[- #text(myred)[_Normal vectors_]]
    #uncover("7-")[$ det(f)mtx(adj(f)^(-1))=det(f)mtx(f)^(-top) $]
  ]

  // [c, l, p, t, r] Inverses of outermorphism
  #slide[
    == Inverses of outermorphism

    #uncover("2-",lemma[
      $det(adj(f)) = det(f) $
    ])
    #uncover("3-",proof[#v(-1.3cm)
      $ det(adj(f))uncover("4-",&=det(adj(f))psninv ast psn)uncover("5-", = adj(f)[psninv]ast psn)uncover("6-", = psninv ast f[psn])\ 
      uncover("7-",&=det(f)psninv ast psn = det(f)) $#v(-1.5cm)
    ])
    #uncover("8-",theorem([Inverse of outermorphism])[#v(-0.2cm)
      $ f^(-1)[bl(A)]=(adj(f)[bl(A)lcont psninv]lcont psn)/(det(f)) $
    ])
    #uncover("9-",proof[
      $adj(f)[bl(A)lcont psninv]uncover("10-", = f^(-1)[bl(A)] lcont adj(f)[psninv])uncover("11-", = det(adj(f))f^(-1)[bl(A)]lcont psninv) $
    ])
    #uncover("12-",corollary([Dual of an inverse])[#v(-0.3cm)
      $ dual(f^(-1)[bl(A)]) = (adj(f)[dual(bl(A))])/(det(f)) $
    ])

  ]

  #slide[
    == Examples of inverses of outermorphisms

    #uncover("2-",example([Pseudoscalar])[
      $ uncover("3-",f^(-1)[psn])uncover("4-",=(adj(f)[1]lcont psn)/(det(f)))uncover("5-", = 1/det(f) psn) $
    ])
    #uncover("6-",example([Scalar])[
      $ uncover("7-",f^(-1)[alpha])
      uncover("8-",=(adj(f)[alpha psninv]lcont psn)/(det(f)))
      uncover("9-",=(alpha det(adj(f))(psninv lcont psn))/(det(f)))
      uncover("10-",=(alpha det(adj(f)))/(det(f)))
      uncover("11-",=alpha) $
    ])
    #uncover("12-",example([Vectors])[
      $ uncover("13-",f^(-1)[aa])
      uncover("14-",=(adj(f)[aa lcont psninv]lcont psn)/(det(f))) $
    ])
  ]

  // [c, t, p] Matrix representations
  #slide[
    == Matrix representations
    #show math.equation: set block(spacing: 0.7em)
    #show par: set block(spacing: 0em)
    #uncover("2-",theorem([Linear transformation as matrices])[$ uncover("3-",mtx(f[xx])=mtx(f)mtx(xx))$])
    #uncover("4-",proof[
      #v(-1cm)
      #uncover("15-",move(dy:2cm,dx:2.83cm,rect(fill:mylightred, width: 2.2cm, height: .8cm)))#v(-2.2cm)
      #uncover("16-",move(dy:2.5cm,dx:7.5cm,rect(fill:mylightred, width: 10.5cm, height: .8cm)))#v(-2.2cm)
      #uncover("17-",move(dy:2.05cm,dx:20.975cm,rect(fill:mylightred, width: 1.75cm, height: 3.3cm)))#v(-3.7cm)
      #text(size:18pt,$ 
       text(myred,mtx(dot)^("row")_"col") wide & uncover("7-",mtx(f)_i^j = f_i^j = inner(f[bb_i], bb^j)) wide & & uncover("5-",mtx(xx)^j = x^j = inner(xx, bb^j)) \
      uncover("15-",mtx(f[xx])=mat(delim:"[",mtx(f[xx])^1;mtx(f[xx])^2;dots.v;mtx(f[xx])^n))
        &uncover("8-",mtx(f) = mat(delim:"[",
                  inner(f[bb_1], bb^1), inner(f[bb_2], bb^1), dots, inner(f[bb_n], bb^1);
                  inner(f[bb_1], bb^2), inner(f[bb_2], bb^2), dots, inner(f[bb_n], bb^2);
                  dots.v, dots.v,dots.down,dots.v;
                  inner(f[bb_1], bb^n), inner(f[bb_2], bb^n), dots, inner(f[bb_n], bb^n)
                  ))
          &&uncover("6-",mtx(xx) = mat(delim:"[",inner(xx,bb^1);inner(xx,bb^2);dots.v;inner(xx,bb^n))) $)
      #uncover("9-")[- Transforming $xx$:]
      #v(-.5cm)
      $ uncover("10-",f[xx] &= f[sum_(i=1)^n inner(xx, bb^i)bb_i]) 
        uncover("11-",= sum_(i=1)^n inner(xx, bb^i)f[bb_i]) \
        uncover("12-",&= sum_(i=1)^n uncover("13-",sum_(j=1)^n) underbrace(inner(xx, bb^i),mtx(x)^i))
        uncover("13-",underbrace(inner(f[bb_i],bb^j), mtx(f)^j_i) bb_j) 
        uncover("14-",= sum_(j=1)^n underbrace(text(myred,sum_(i=1)^n mtx(f)^j_i mtx(xx)^i), text(myred,mtx(f[xx])^j)) bb_j) $#v(-1cm)
    ])
  ]

  // [c] Matrix representations (contraction of pseudoscalar)
  #slide[
    == Matrix representations
    $ uncover("2-",mtx(f)_i^j)
     uncover("3-",&= inner(f[bb_i], bb^j))\ 
     uncover("4-",&= (-1)^(j-1) f[bb_i]lcont (bb_1 wedge dots wedge breve(bb)_j wedge dots wedge bb_n lcont psninv)) \
     uncover("5-",&= (-1)^(j-1)(f[bb_i]wedge bb_1 wedge dots wedge breve(bb)_j wedge dots wedge bb_n lcont psninv)) \
     uncover("6-",&= (bb_1 wedge dots wedge bb_(j-1)wedge f[bb_i] wedge bb_(j+1) wedge dots wedge bb_n)lcont psninv) $

    #uncover("7-")[- $mtx(f)_i^j uncover("8-",=psn lcont psninv=psn ast psninv)$ #uncover("9-")[after replacing $bb_j$ by $f[bb_i]$ in $psn=bb_1wedge dots wedge bb_n$]]
  ]

  // [c, d, t] Matrices for outermorphisms
  #slide[
    //#show math.equation: set block(spacing: 0.7em)
    #show par: set block(spacing: 0em)
    
    #let irev=$accent(I, tilde, size:#200%)$
    == Matrices for outermorphisms
    #uncover("2-")[- Notation for bivectors:]
    #v(-.5cm)
    $ uncover("3-",bl(B)_(i_1 i_2)=bb_i_1 wedge bb_i_2 = bl(B)_I)uncover("4-", quad arrow.double.long quad bl(X) = sum_I X^I bl(B)_I) $ #v(-.5cm)
    #uncover("5-",definition([Equality of indices])[
      $i_k=j_k$ for all $ k=1,2 arrow.double.long I=J$
    ])
    #uncover("6-",definition([Reversion of indices])[
      $I=i_1i_2 arrow.double.long irev=i_2i_1$
    ])
    #uncover("7-",definition([Reciprocal basis 2-blade])[
      $bl(B)^J=(bb^(j_1)wedge bb^(j_2))^revalone = bb^(j_2)wedge bb^(j_1)$
    ])
    #uncover("8-",theorem[$bl(B)_I ast bl(B)^J =delta_I^J$])
    #uncover("9-",proof[#v(-1.4cm)
    $ bl(B)_I ast bl(B)^J uncover("10-",&= bl(B)_(i_1 i_2) ast bl(B)^(j_1 j_2))uncover("11-", = (bb_(i_1)wedge bb_(i_2))ast(bb^(j_2) wedge bb^(j_1))) \ 
    uncover("12-",&=inner(bb_(i_1), bb^(j_1))inner(bb_(i_2),bb^(j_2))-inner(bb_(i_2),bb^(j_1))inner(bb_(i_1),bb^(j_2))) \
    uncover("13-",&=1 ("if" J=I)\, -1 ("if" J=irev) "or" 0 ("otherwise"))
    $#v(-1.5cm)])

  ]

  // [c, t] Matrices for outermorphisms
  #slide[
    == Matrices for outermorphisms
    #uncover("2-",theorem([Blade expansion on bases])[$bl(X) = sum_I (bl(X) ast bl(B)^I)bl(B)_I $])

    #uncover("3-",definition[$mtx(f)_I^J &= f[bl(B)_I] &ast& bl(B)^J $])

    #uncover("4-",theorem[$mtx(f)_(i_1 i_2)^(j_1 j_2)=mtx(f)^(j_1)_(i_1)mtx(f)^(j_2)_(i_2)-mtx(f)^(j_1)_(i_2)mtx(f)^(j_2)_(i_1)$])
    #uncover("5-",proof[$ mtx(f)_(i_1 i_2)^(j_1 j_2) &= f[bl(B)_(i_1 i_2)] ast bl(B)^(j_1 j_2)\ 
      uncover("6-",&= (f[bb_(i_1)] wedge f[bb_(i_2)]) ast (bb^(j_2)wedge bb^(j_1))) \
      uncover("7-",&= (f[bb_(i_1)]ast bb^(j_1))(f[bb_(i_2)]ast bb^(j_2)) - (f[bb_(i_2)]ast bb^(j_1))(f[bb_(i_1)]ast bb^(j_2))) $
    ])
  ]

  // [c] Summary
  #slide[
    #show math.equation: set block(spacing: 0.0em, above:.5em)
    #show par: set block(spacing: 0em)
    

    == Summary
    #uncover("2-")[- #table(columns: 3, stroke: none, [Linear transformation], uncover("3-",$arrow.double.long$), uncover("3-",text(myred)[_outermorphism_]), $f[alpha aa + beta bb]=alpha f[aa]+beta f[bb]$, [],uncover("3-",$f[aa wedge bb]=f[aa] wedge f[bb]$))]
    #uncover("4-")[- How products are transformed:]
    $ uncover("5-",f[bl(A) wedge bl(B)] &= f[bl(A)] wedge f[bl(B)]) \
      uncover("6-",f[bl(A) ast bl(B)] &= bl(A) ast bl(B))\
      uncover("7-",f[bl(A) lcont bl(B)] &= adj(f)^(-1)[bl(A)]lcont f[bl(B)])
    $
    #uncover("8-")[- #text(myred)[_Coordinate-free_] expression for inverse of linear transformation:]
    #uncover("9-")[$ f^(-1)[bl(A)] = (adj(f)[bl(A) lcont psninv]lcont psn) / det(f) $]
    #uncover("10-")[- Orthogonal transformation: $inner(f[aa],f[bb])=inner(aa,bb)$]
    #uncover("11-")[- Orthogonal transformations are #text(myred)[_innermorphisms_]: 
    $ adj(f)^(-1) = f arrow.double.long f[bl(A)lcont bl(B)]=f[bl(A)]lcont f[bl(B)] $
    ]
  ]