#import "base.typ": *

// PREAMBLE
  #show: thmrules
  #show: doc => conf(
    title: [
      Geometric Algebra Cheatsheet
    ],
    authors: (
      (
        name: "Dr. Anderson Tavares",
        email: "acmt@outlook.com",
      ),
    ),
    doc,
  )

  #show outline.entry.where(
    level: 1
  ): it => {
    v(8pt, weak: true)
    strong(it)
  }
  #let proof(..args)=myproof(..args)

// Document
// Pre-text
  // Legend
  #show_thm_legend #v(-0em)

  // List of symbols
  #grid(columns: 2,column-gutter: .3em, [
      #tablex(columns:2, auto-vlines: false, align:(left+horizon,center+horizon),inset:(x:.2em,y:.3em), auto-lines: false,
          hlinex(stroke:.5pt),
          [Scalar],vlinex(stroke:.5pt),[$alpha, beta$],
          [Coefficients],[$alpha_i, beta^i$],hlinex(stroke:.5pt),
          [Vectors],[$cvec(a), cvec(b)$],
          [Orthonormal Basis vec.],[$cvec(e)_i$],
          [General Basis vectors],[$cvec(b)_i$],hlinex(stroke:.5pt),
          [Blade],[$bl(A), bl(B), bl(C)$],
          [Blade of grade $k$],[$bl(A)_k, bl(B)_k$],
          [$k$-vector],[$F, G, H$],
          [General multivector],[$L, M, N$],hlinex(stroke:.5pt),
          [(Unit pseudoscalars)],[$bl(I)_n, bl(I)$],hlinex(stroke:.5pt),
          [Transformations],[$tau, sigma$],
          [Inverse trasform.],[$tau^(-1)$],
          [Adjoint transform.],[$adj(tau)$],
          [Determinant],[$det tau$],
          [Restriction to domain],[$f|_W$],
          [Identity map],[$iota$],
          [Kernel],[$ker tau$],
          [Image],[$im tau$],
          [Nullity],[$"null"(tau)$],
          [Rank],[$"rk"(tau)$],
          [Column rank],[$"crk"(tau)$],
          [Row rank],[$"rrk"(tau)$],
          [Projection],[$rho_(S,T)$],
          [Orthogonal projection],[$rho_(S,S^perp),Rho_S$],
          hlinex(stroke:.5pt),
        )
    ],[
      #tablex(columns:2, auto-vlines: false, align:(left+horizon,center+horizon),inset:(x:.2em,y:.3em), auto-lines: false,
          hlinex(stroke:.5pt),
          [Vector spaces],vlinex(stroke:.5pt),[$V, RR^n$],
          [Span of a set],[$span(A)$],
          [Subspace],[$S lt.eq V$],
          [Basis],[$B lt.dot V$],
          [Exterior power],[$ext^(k)V$],
          [Exterior algebra],[$ext V$],
          [Blade space],[$[A]$],
          [Clifford algebra],[$"Cl"(V,Q)$],hlinex(stroke:.5pt),
          [Grade projection],[$gproj(A, k)$],
          [Reversion],[$rev(bl(A))$],
          [Inverse],[$A^(-1)$],
          [Norm],[$norm(A)$],
          [Orthogonal pair],[$A perp B$],
          [Partially orthog.],[$A pperp B$],
          [Orthog. complem.],[$A^perp$],
          [Orthogonal set],[$A_perp$],
          [Orthonormal set],[$A_nperp$],
          [Left dual],[$ld(A),star_L$],
          [Right dual],[$rd(A),star_R$],
          [Left $B$-dual],[$ldd(A,B), star_(L B)$],
          [Right $B$-dual],[$rdd(A,B), star_(R B)$],
          [Grade involution],[$invol(bl(A))$],
          [Clifford conjug.],[$cconj(bl(A))$],hlinex(stroke:.5pt),
          [Inner product],[$inner(A,B)$],
          [Scalar product],[$A ast B$],
          [Tensor product],[$A otimes B$],
          [Exterior product],[$cvec(a) wedge cvec(b)$],
          [Left contraction],[$M lc N$],
          [Right contraction],[$M rc N$],
          [Geometric product],[$G H$],
          [Commutator],[$A times B$],
          [],[],
          [],[],
          hlinex(stroke:.5pt),
        )
  ])

  // Table of contents
  #block(inset:(right:.5em),text(size:8.2pt,outline(depth:2,indent:1em, fill:none)))

// Text
= Preliminaries
  == Multi-index formalism
    #let h8=h(.8em)
    #let h7=h(.8em)
    #let space=$quad$
    #definition[ *Unordered indices*: $idx(r),idx(s),idx(t) in uidx$
      $ uidx_k^(a,b) #h(0em) = {(i_1,dots,i_k) in NN^k: a lt.eq i_j lt.eq b, i_j eq.not i_k "if" j eq.not k}
      $
      #align(center)[
        $uidx_k^n h1=h1 uidx_k^(1,n) space
        uidx_0^n h1=h1{emptyset} space
        uidx^(a,b) h1=h1 union.big_(p=0)^(b-a+1)h0  uidx_k^(a,b) space
        uidx^n h1=h1 uidx^(1,n) space$
        $uidx h1=h1 union.big_(n=0)^infinity uidx^n$
      ]
      ]
    #definition[ *Ordered indices*: $idx(i), idx(j), idx(k) in oidx$
      $ oidx_k^(a,b) = {(i_1,dots,i_k) in NN^k: a lt.eq i_1 < dots < i_k lt.eq b} $
      #align(center)[
        $oidx_k^n = oidx_k^(1,n) space 
        oidx_0^n={emptyset} space
        oidx^(a,b) = union.big_(p=0)^(b-a+1) oidx_k^(a,b) space
        oidx^(n) = oidx^(1,n) space$

        $oidx=union.big_(n=0)^infinity oidx^n$
      ]
      ]
    #let abb = 2.4cm
    #definition[ _Unary index operations_ on $ idx(i)=(i_1,dots,i_k) =i_1 dots i_k$
    #grid(columns: (.4fr, .6fr),[
      - #box(width:1.8cm)[*Grade*:] $|idx(i)|=k$
      - #box(width:1.8cm)[*Reversion*:] $rev(idx(i))=i_k dots i_1$
    ],[
      - #box(width:abb)[*Sum*:] $ norm(idx(i))=i_1+dots+i_k$
      - #box(width:abb)[*Complement*:] $idx(i)'=setminus((1,dots,n),idx(i))$\ #h(2.5cm) $forall idx(i) in oidx^n$
    ])

      // #grid(columns: (.15fr, .35fr, .2fr, .3fr), inset: (top:.3em, rest:.2em), align: center, [*Grade*],[*Sum*],[*Reversion*],[*Complement*],
      // $|idx(i)|=k$, $ norm(idx(i))=i_1+dots+i_k$, $rev(idx(i))=i_k dots i_1$,[$idx(i)'=setminus((1,dots,n),idx(i))$\ $forall idx(i) in oidx^n$])
      // #columns(3,[3 #colbreak() a #colbreak() a #colbreak() ])
      // - #box(width:2.1cm)[*Grade*:] $|idx(i)|=k$
      // - #box(width:2cm)[*Sum*:] $ norm(idx(i))=i_1+dots+i_k$
      // - #box(width:2.28cm)[*Reversion*:] $rev(idx(i))=i_k dots i_1$
      #v(-1em)
      - $|emptyset|=norm(emptyset)=0$
      ]

    #definition[_Binary index operations_
        - For $idx(r), idx(s) in uidx quad idx(r)=r_1 dots r_k quad idx(s)=s_1 dots s_l $
          // - *Union* $idx(r) union idx(s) = i_1 dots i_k, i_j in idx(r)$
          - #box(width:abb)[*Difference*:] $setminus(idx(r), idx(s)) in uidx =$ removing from $idx(r)$ any indices of $idx(s)$
          - #box(width:abb)[*Disjoint*:] $idx(r) sect idx(s)=emptyset arrow.double.l.r r_i eq.not s_j$ (no common indices)
          - #box(width:abb)[*Concatenation*:] $idx(r)idx(s)=r_1 dots r_k s_1 dots s_l in uidx$ (if disjoint)
          - #box(width:abb)[*Subsequence*:] $idx(r) subset idx(s) quad arrow.l.r.double.long quad r_i in idx(r) arrow.double r_i in idx(s)$
          - #box(width:abb)[*Ordering* $idx(r)$:] $overline(idx(r)) in oidx_k^n$ (permutation *sign*: $signord_idx(r)$ ($signord_emptyset=1$))
          - #box(width:1*abb)[*Cartesian prod*] $idx(r)times idx(s)={(r,s): r in idx(r), s in idx(s)}$
          - #box(width:abb)[*Larger indices*:] $idx(r) > idx(s)={(r,s) in idx(r)times idx(s): r > s}$#v(.5em)

        - For $idx(i),idx(j) in oidx$
          - #box(width:abb)[*Intersection*] $idx(i) sect idx(j) = i_1 dots i_k in oidx, i_j in idx(i) "and" i_j in idx(j) $
          - #box(width:abb)[*Union*] $idx(i) union idx(j) = i_1 dots i_k in oidx, i_j in idx(i) "or" i_j in idx(j)$
          - #box(width:abb)[*Symmetric dif*:] $idx(i) triangle.t idx(j) = (setminus(idx(i),idx(j))) union (setminus(idx(j),idx(i)))$
        // - #box(width:1.5*abb)[*Complement* ($idx(i)in oidx^q $):] $idx(i)'=setminus((1,dots,q),idx(i))$
    ]
  == Projections

    #note[
      For $S lt.eq V$, $Rho_S h1= h1 rho_(S,S^perp) h0:V arrow S$ is an orthogonal projection.
    ]

// #include "geoalg_extalg_cheatsheet.typ"

= Clifford algebra

  == Definition

    #definition[*Geometric algebra* (v1):
      $cal(G)(V,Q) = T(V)\/I$ s.t.\
      #align(center,[$T(V)$: Tensor algebra on $V quad Q: V arrow FF: "quadratic form"$ \ //I$: two-sided ideal where \
        $I=span({cv(v) otimes cv(v) - Q(vv)1: vv in V} in T(V))_"ideal"
      // <{}>{A otimes (cv(v) otimes cv(v) - Q(vv)1) otimes B  h0: cv(v) h1 in h1 V, A,B in T(V)}
        $])
        The *clifford geometric product* is the coset 
        $ M N:=[M otimes N]=M otimes N + I $
        where $1_G=[1]=1+I$ is the *multiplicative identity*.
      ]
    #note[
      For a normed space $(V(RR),norm(dot))$ where the parallelogram identity $|x h1 + h1 y|^2+|x h1 - h1 y|^2=2|x|^2+|y|^2$ holds, then $Q(vv):=norm(v)^2$ is a quadratic form and $b_Q$ is an inner product on $V$.
    ]
    #theorem()[$vv^2 = Q(vv)$.]
    #proof()[$vv^2 = [vv otimes vv]=[vv otimes vv - Q(vv)1]+Q(vv)1_cal(G) = 0 + Q(vv)$ where we omitted $1_cal(G)=[1]$]

    #theorem[
      If $"char"(FF)eq.not 2$, then $forall vv,ww in V h0: vv ww + ww vv = 2 b_(Q)(vv,ww)1$
    ] <t:inner_product_bilinear_form>
    #proof[$2beta_(Q)(vv,ww)h0 &= h0 Q(vv h0 + h0 ww) h0 - h0 Q(vv) h0 - h0 Q(ww) h0 = h0 (vv h0 + h0 ww)^2-vv^2-ww^2 $]

    #theorem()[
      Let $(V(FF),Q)$ be an arbitrary quadratic space, and let $cal(A)$ be an associative algebra over $FF$ with unit $1_cal(A)$. If $f in cal(L)(V, cal(A))$ and 
      $ f(vv)^2 = Q(vv)1_(cal(A)), forall vv in V, $
      then there exists a unique $FF$-algebra homomorphism 
      $ F:cal(G)(V,Q) arrow cal(A) $ s.t. $F([vv])=f(vv) forall vv in V$.
    ] <t:universality_G>
    #note[From @t:universality_G, $cal(G)(V, Q)$ is the "freest" unital associative algebra generated by $V$ s.t
      $forall vv in V h0:vv^2 = Q(vv)1$.
    ]


    #theorem[
      $vv,ww in V "and" vv perp ww arrow.double vv ww = -ww vv $
    ]

    #example[$cal(G)(RR^2)$ is the *plane algebra*. If ${ee_1,ee_2} basis RR^2$, then $ee_1^2 = ee_2^2 = 1, ee_1 ee_2 = -ee_2 ee_1$, and $span({1, ee_1, ee_2, ee_1ee_2}) = cal(G)(RR^2)$.]
    #example()[$cal(G)(RR^3)$ is the *space algebra*]
    #theorem[$(ee_1,dots,ee_n) basis V arrow.double span((ee_(i_1)dots ee_(i_k): (i_1,dots,i_k) in oidx^n)) = cal(G)(V)$]
    #theorem[
      $cal(G)(V,0) isom ext V$ as an $FF$-algebra isomorphism.
    ]
    #corollary()[
      $cal(G)(V,Q) isom ext V$ as a (graded) vector space with the following canonical vector space isomorphism:
      $ ee_1 wedge dots wedge ee_k = ee_1 dots ee_k, quad (ee_1,dots,ee_n) basis V. $
    ]
    #theorem[
      For $(vv_1,dots,vv_p)_perp subset V $, $vv_1 dots vv_p=vv_1 wedge dots wedge vv_p$
    ]
    #theorem[
      For $(vv_1,dots,vv_p)_perp subset V $, $vv_(sigma(1)) dots vv_(sigma(p))="sg"(sigma)vv_1  dots  vv_p$
    ]
    Assume $inner(dot,dot)=b_(Q)(dot,dot)$ for $cal(G)(V,Q)$.
    #theorem()[
      $forall vv,ww in V$
      + $inner(vv, ww)=1/2(vv ww + ww vv)$
      + $vv wedge ww = 1/2(vv ww - ww vv)$
      + $vv ww=inner(vv,ww)+vv wedge ww$
    ]
    #proof[
      i. from @t:inner_product_bilinear_form ii. $wedge$ is bilinear and alternate ($vv wedge vv =0$). The geometric product is bilinear, and $1/2(vv vv - vv vv) = 0$. iii. $vv ww = 1/2 (vv ww + ww vv)+ 1/2(vv ww - ww vv)$
    ]
    #theorem[
      For $(vv_1,dots,vv_n)_nperp basis V $, 
      $ vv_idx(i)vv_idx(j)=(-1)^(|idx(i)>idx(j)|)vv_(idx(i) triangle.t idx(j)), quad "for" idx(i),idx(j) in oidx^n $ 
    ]
  == Combinatorial Clifford algebra
    #definition()[
      Let $X$ be any finite set and $R$ a commutative ring with unit. Let an arbitrary $r: X arrow R$ be a *signature* on $X$. The *Clifford algebra* over $(X,R,r)$ is defined as the set
      $ "Cl"(X,R,r) := plus.big_(cal(P)(X))R $
      i.e. the free $R$-module generated by $cal(P)(X)$. $R$ is the *scalars* of Cl.
    ]
    #lemma()[Given X a finite set and $r: X arrow R$, there exists a map $tau: cal(P)(X) times cal(P)(X)arrow R$ s.t., 
      + $tau({x},{x})=r(x) quad forall x in X$,
      + $tau({x},{y})=-tau({y},{x}) quad forall x,y in X: x eq.not y$
      + $tau(emptyset, A)=tau(A,emptyset) = 1 quad forall A in cal(P)(X)$
      + $tau(A,B)tau(A h1 triangle.t h1  B, C) = tau(A,B h1  triangle.t h1 C)tau(B,C), thick forall A,B,C h1 in h1 cal(P)(X)$
      + $tau(A,B) in {-1,1} quad "if" A sect B = emptyset$
    ]
    #let Cl = "Cl"
    #definition()[
      The *Clifford product* $Cl(X) times Cl(X) arrow Cl(X)$ is defined as $A B:= tau(A,B) A triangle.t B$ for $A,B in cal(P)(X)$ and extending linearly.
    ]
    #theorem()[
      If $A,B in cal(P)(X)$ then
      $ A B = (-1)^(1/2 |A|(|A|-1)+1/2|B|(|B|-1) + 1/2|A triangle.t B|(|A triangle.t B|-1))B A $
    ]
  == Equivalence between $"Cl"$ and $cal(G)$

    Consider a canonical embedding $i h0: h1 V h0 arrow h1 Cl$ where $i(ee_j) h1 := h1 ee_j, h0 forall j h1 in h1 NN^+_n$.
    #theorem()[$i(vv)^2 = Q(vv)emptyset$]
    #theorem([_Universality of_ $Cl$])[
      Let $(V(FF), Q)$ be a finite-dimensional quadratic space with char $FF h1 eq.not h1 2$, and let $cal(A)$ be an associative algebra over $FF$ with unit $1_(#h(-.2em)cal(A))$.
      Let $f h1 in h1 cal(L)(V #h(-.2em), h1 cal(A))$ where $f(vv)^2 h0 = h1 Q(vv)1_cal(A), forall vv h1 in h1 V$. Let any $E basis V$ be orthogonal and introduce $Cl(E,FF,Q|_E)$. Then there exists a unique $FF$-algebra homomorphism $F:Cl(E,FF,Q|_E)arrow cal(A)$ s.t. $F(i(vv))=f(vv),forall vv in V$.
    ]

    #theorem()[If $E basis (V,Q)$ is orthogonal then $Cl(E,FF,Q|_E)$ and $cal(G)(V,Q)$ are isomorphic as $FF$-algebras, and in particular
    $ dim_FF cal(G)(V,Q)=dim_FF Cl(E,FF,Q|_E) = 2^(dim V), $
    and the map $j:V arrow cal(G)(V,Q), vv arrow.bar [vv]$ is injective.
    ]
    #note[
      We will supress the unit $emptyset$, replace ${e}$ by $e, forall e in E$, and identify $im i = im j = V$. For $V=RR_3$, then $cal(G),Cl$ are spanned by
      $ span({1,ee_1,ee_2,ee_3,ee_1ee_2,ee_1ee_3,ee_2ee_3,ee_1ee_2ee_3}) $
    ]

    #definition()[*Subspace of k-vectors* in $Cl$, or *grade-$k$ part* of $Cl$:
    $ Cl^k(X,R,r):= plus.circle.big_(A in cal(P)(X):|A|=k) R $
    ]
    #definition()[*Even* and *odd subspaces*: $ Cl^(plus.minus)(X,R,r) := plus.big_(k "is" #stack(spacing: .05cm,[Even],[Odd])) Cl^k(X,R,r)  $ ]
    #note[$Cl(X)=Cl^plus plus.circle Cl^minus=Cl^0 plus.circle Cl^1 plus.circle dots plus.circle Cl^(|X|)$]
    #theorem[Under Clifford product, $Cl^(plus.minus)Cl^(plus.minus) subset Cl^(plus)$ and $Cl^(plus.minus)Cl^(minus.plus) subset.eq Cl^(minus)$]
    #corollary()[$Cl^plus$ form a subalgebra of $Cl$.]
    #note[
      $Cl$ is _filtered_ in the sense that 
      $ (plus.circle.big_(i lt.eq k) Cl^i)(plus.circle.big_(j lt.eq l) Cl^j)subset.eq plus.circle.big_(m lt.eq k+l)Cl^m $
    ]

  == Standard operations on $Cl$

    #definition()[
      For $A,B in cal(P)(X)$ we define
      #set par(leading: .3em)
      $ A wedge B &:= delta_(A sect B = emptyset)A B && "outer/exterior/wedge product"\
        A lc B &:= delta_(A subset.eq B) A B &&"left inner/interior product"\
        A rc B &:= delta_(A supset.eq B) A B &&"right inner/interior product"\
        A ast B &:= delta_(A = B) A B &&"scalar product"\
        gproj(A,k) &:= (|A|=k)A &&"projection on grade" k\
        invol(A) &:= (-1)^(|A|)A && "grade involution"\
        rev(A) &:= (-1)^(binom(|A|,2))A &#h(.4cm)& "grade reversion" \
        overline(A)&:=invol(rev(A))=rev(invol(A)) & &"Clifford conjugate"
      $
      and extend linearly to $Cl(X,R,r)$.

    ]
    #note[In $cal(G)(RR^(0,1)) isom CC$, the grade involution (or Clifford conjugate) corresponds to the complex conjugate.]
    #note[In $cal(G)(RR^(0,2)) isom HH$, the Clifford conjugate corresponds to quaternion conjugate.]


    #theorem[
      For $G in ext^p V$ and $H in ext^q V$, $G H$ can have components of grades $|q-p|,|q-p|+2,dots,p+q$, with $gproj(G H,p+q)=G wedge H$.
    ]
    #proof[#todo]
    #theorem[
      In a versor $M=vv_1 dots vv_p$ for $vv_i in V$, 
      $ invol(M)=(-1)^p M, quad breve(M)=(-1)^(p(n+1))M quad rev(M) eq.not (-1)^(p(p-1)/2)M $
    ]
    #proof[#todo]
    #theorem[
      $(M N)^involalone = invol(M) invol(N), (M N)^#scale(x:200%,$breve$)=breve(M)breve(N), (M N)^revalone=rev(N)rev(M)$
    ]
    #proof[#todo]
    #theorem()[
      $inner(M,N)=(rev(M) N)_0$ and $inner(L, M N)=inner(rev(M)L,N)=inner(L rev(N),M)$
    ]
    #proof[#todo]

    #theorem()[For $G in ext^p V$ and $H in ext^q V$,
    $ G lc H = gproj(rev(G)H,q-p) quad "and" quad G rc H = gproj(G rev(H),p-q) $]
    #proof[#todo]
    #theorem()[
      Let $vv, ww_1, dots, ww_q in V$ and $M,N in ext V$
      + $vv M = vv lc M + vv wedge M$
      + $vv lc M = 1/2 (vv M - invol(M)vv)$ and $vv wedge M = 1/2(vv M + invol(M)vv)$
      + $vv lc (M N)=(vv lc M)N + invol(M)(vv lc N)$
      + $vv lc (M N) =(vv wedge M)N - invol(M)(vv wedge N)$
      + $vv wedge (M N)=(vv lc M)N + invol(M)(vv wedge N)$
      + $vv wedge (M N) = (vv wedge M)N - invol(M)(vv lc N)$
      + $vv lc (ww_1 dots ww_q) = sum_(i = 1)^q (-1)^(i-1)inner(vv, ww_i) ww_1 dots ww'_i dots ww_q$
      + $(vv M) lc N = (vv lc M) lc N + M lc (vv lc N)$
    ]
    #proof[#todo]
    #corollary()[For $M,N in ext V$, $M N = M lc N + M wedge N$.]
    #theorem[
      For all $M,N in Cl(X)$ we have
      
        $ M wedge N &= sum_(k,l gt.eq 0) gproj(gproj(M,k) gproj(N,l),k+l) \
        M lc N &= sum_(0 lt.eq k lt.eq l) gproj(gproj(M,k) gproj(N, l), l-k) \
        M rc N &= sum_(0 lt.eq l lt.eq k) gproj(gproj(M,k) gproj(N, l), k-l) \
        #colbreak()
          M ast N &= gproj(M N, 0) \
          invol(M) &= sum_(0 lt.eq k)(-1)^n gproj(M,k)\
          rev(M) &= sum_(0 lt.eq k)(-1)^binom(n,2)gproj(M,k)
        $
    ]
    #proof[#todo]
    #corollary()[Let $S,T lt.eq V$, $M in ext S, N in ext T$
    + $L in ext(T^perp) arrow.double L lc (M N)=(L lc M) N$
    + $L in ext(T^perp) arrow.double L wedge (M N)=(L wedge M)N$
    + $H in ext^(p)(S^perp) arrow.double H lc (M N)=M^(involalone p)(H lc N)$
    + $H in ext^(p)(S^perp) arrow.double H wedge (M N)=M^(involalone p)(H wedge N)$
    ]
    #proof[#todo]
    #corollary()[
      For disjoint $S,T lt.eq V$, and $vv in V, M eq.not 0 in ext S, N in ext T$
      $ v lc (M N) = 0 arrow.double.l.r vv lc M = vv lc N = 0 $
    ]
    #proof[#todo]
    #theorem()[
      For $vv_1,dots,vv_p in V$ and $M,N in ext V$
      $ vv_(1 dots p) lc (M N) = sum_(idx(i) in oidx^p) sigma_(idx(i)idx(i)') (vv_(idx(i)') lc M^(involalone|idx(i)|))(vv_idx(i) lc N) \
        vv_(1 dots p) wedge (M N) = sum_(idx(i) in oidx^p) sigma_(idx(i)idx(i)') (rev(vv)_(idx(i)') lc M^(involalone|idx(i)'|))(vv_idx(i)' wedge N)
      $
    ]
    #proof[#todo]

= Star duality

  #definition()[
    An *orientation*, *unit pseudoscalar* or *volume element* of $V$ is a unit $bl(I) in ext^n V$, for $n=dim V$
  ]

  #note[
    A real space has 2 orientations $plus.minus I$. A complex space has a continuum of them, the unit circle in $ext^n V$.
  ]

  #definition()[
    Let $I$ be an orientation. The *left* and *right star operators* $star: ext V arrow ext V$ and *left* and *right duals* of $M in ext V$, resp., are
    $ ld(M)=I rc M quad rd(M)=M lc I $
  ]
  #note[$star_R$ and $star_L$ also refer to left and right stars.]
  #note[
    $star$ take precedence over produts, e.g., $ld(M)wedge rd(N) h0= h0 (ld(M))wedge(rd(N))$
  ]
  #note[
    In complex case, $star$ is conjugate-linear.
  ]

  #theorem()[
    For a $p$-blade $bl(B)eq.not 0$, $rd(bl(B))$ is the unique $(n-p)$-blade s.t. $[rd(bl(B))]=[bl(B)]^perp, norm(rd(bl(B)))=norm(bl(B))$, likewise for $ld(B)$.
  ]
  #proof()[#todo]
  #theorem()[
    For a $p$-blade $bl(B)eq.not 0$, $bl(B)wedge rd(bl(B))$ and $ld(bl(B))wedge bl(B)$ has orientation of $I$.
  ]
  #proof()[#todo]
  #note[$[rd(0)]=[0]eq.not [0]^perp$]

  #theorem()[
    $rd(vv_idx(i)) h0= h1 sigma(idx(i)idx(i)')vv_idx(i)'$ for $idx(i) in oidx^n$, #h(-.2em) $(vv_1,dots,vv_n)_nperp basis V$, and $I h1=h1 vv_(1,dots,n)$
  ]
  #proof()[#todo]

  #theorem()[
    Let $M in ext V$
    + $(rd(M))^breve = rd((breve(M)))$
    + $lrd(M)=M$
    + $ld(M)=rd(breve(M))$
  ]
  #proof()[#todo]

  #theorem[
    Let $M,N in ext V$
    + $rd((M wedge N)) = N lc rd(M)$ and $rd((M rc N)) = N wedge rd(M)$
    + $rd(M) rc M = M lc ld(N)$ and $ld(M) lc N = M rc rd(N)$
    + $inner(rd(M), rd(N)) = inner(N, M)$
  ]
  #proof()[#todo]

  #corollary()[
    $norm(rd(M))=norm(M)$, for $M in ext V$.
  ]
  #proof()[#todo]
  #corollary()[$G wedge rd(H) = inner(H, G)I$, for $G,H in ext^p V$]
  #proof()[#todo]
  #theorem()[Let $vv, vv_1,dots,vv_p in V$ and $M, N in ext V$
  + $rd(M)=rev(M)I$
  + $rd((M N)) = rev(N)rd(M)$
  + $rd((vv_1 dots vv_p))=vv_p dots vv_2 rd(vv_1)$
  + $(rd(M))N = rev(M)(ld(rev(N)))$
  + $rd((vv M)) = rd((vv lc M)) + M lc rd(v)$
  ]
  #proof()[#todo]
  #definition()[
    Let $B$ be an $p$-blade. The *left/right star op.* $star_B h0: ext h0 V h1 arrow h1 ext h0 h0 V$ and *left/right duals* of $M h1 in h1 ext h0 V$ w.r.t $B$ ($B$-*duals*), resp., are
    $ ldd(M,B)=B rc M quad rdd(M,B)=M lc B $
  ]
  #theorem[
    For a p-blade $A eq.not 0$
    + $rdd(A,B)$ is a $(q-p)$-blade
    + $rdd(A,B) eq.not 0 arrow.double.l.r [A] npperp [B]$
  ]
  #proof()[#todo]
  #theorem()[
    For $M in ext V$, 
    + $lrdd(M, B, B)=Rho_B M$
    + #h(.32cm) $rdd(M,B)=rdd((Rho_B M),B) = ldd(M,B)^(involalone (q+1))$.
    + $inner(rdd(M,B),rdd(N,B))=inner(Rho_B N, Rho_B M)$
  ]
  #proof()[#todo]

  #theorem()[Let $B$ be a unit blade, and $M,N in ext V$
    + $rd(M)= rdd(M,B) wedge rd(B)$, if $M in ext[B]$
    + $rdd((M wedge N),B)=N lc rdd(M,B)$
    + $rdd((A rc M),B)=(Rho_A M)wedge rdd(A,B)$, if $A subset.eq B$
    + $rdd((M rc N), B)=N wedge rdd(M,B)$, if $N in ext [B]$
  ]
  #proof()[#todo]

  == Regressive product

    #definition()[
      *Regressive product* $M vee N$ of $M,N in ext V$: 
      $ rd((M vee N))=rd(M) wedge rd(N) $
    ]
    #theorem[$vee$ is bilinear and associative]
    #proof()[#todo]
    #theorem()[For $G in ext^p V, H in ext^q V$ and $dim V = n$,
      $ G vee H = (-1)^((n-p)(n-q)) H vee G in ext^(p+q-n) V. $
    ]
    #proof()[#todo]
    #theorem[$p+q < n arrow.double G vee H = 0$.]
    #proof()[#todo]
    #theorem[
      Let $M, N in ext V$
      + $(M vee N)^breve = breve(M) vee breve(N)$
      + $rd((M wedge N)) = rd(M) vee rd(N)$
      + $M vee N = N rc rd(M)$
    ]
    #proof()[#todo]
    #theorem()[For $idx(i), idx(j)in oidx^n$, $(vv_1,dots,vv_n)_nperp basis V$, and orientation $vv_(1 dots n)$,
      $ vv_idx(i) vee vv_idx(j) = delta_(idx(i) union idx(j)=1 dots n) sigma_(idx(j)'idx(i)') vv_(idx(i)sect idx(j)). $
    ]
    #proof()[#todo]
    #theorem()[
      For nonzero blades $A in ext^p V, B in ext^q V$, 
      $ A wedge B=0 arrow.double.l.r [A]+[B] eq.not V. $
      If $A vee B eq.not 0$, it is the only $(p+q-n)$-blade with:
      + $[A vee B]=[A] sect [B]$;
      + $norm(A vee B)=norm(A)norm(B)cos Theta_([A]^perp, [B])$;
      + $A wedge ((A vee B) lc B)$ has the orientation of $I$.
    ]
    #proof()[#todo]

    === Join and meet

      #theorem()[
        Let $A, B in ext V$ be nonzero blades.
        + For join $J=A union B$, a meet is $A sect B = B lc rdd(A,J).$
        + For a meet $M=A sect B$, a join is $A union B = A wedge (M lc B).$
      ]

      #theorem()[
        $norm(A sect B)=norm(A)norm(B)cos Theta_([A]^perp sect [J],[B]).$
      ]
      #note[Its bilinear property remains valid if $A$ or $B$ changes but $[A]+[B]$ does not]



  == Outermorphisms and duality

    Let $tau in cal(L)(V,W)$ where $W$ has orientation $I_W$ and star $ast.circle$ ($star$ in V).
    #theorem()[$tau I eq.not 0 arrow.double.l.r tau$ is injective]
    #proof()[#todo]
    #theorem()[If $tau^(-1)$ exists, then $tau I = lambda_tau I_W$ for some $lambda_tau eq.not 0 in FF$]
    #proof()[#todo]
    #theorem()[If $Y=X$, then $tau I = (det tau)I$]
    #proof()[#todo]
    #theorem()[for $N in ext W$:
      $ tau(rd((adj(tau)N))) h1 = h1 N lc tau I h1 = h1 cases(
        norm(tau I)rdd(N,B) "with" B h1 = h1 (tau I)/norm(tau I)\, "if" tau "is injective",
        0 \, #h(3.24cm) "otherwise".
      ) $
    ]
    #proof()[#todo]
    #corollary()[For $M in ext V$ 
    + If $tau$ is an isometry then $tau(rd(M))=rdd(tau M, tau I)$;
    + If $tau$ is also invertible, then $tau(rd(M)) = lambda_tau rd(tau M)$ and $|lambda_tau|=1$
    ]
    #proof()[#todo]
    #corollary()[
      If $tau$ is invertible, the following diagram is commutative. Stars can be left or right, but must have equal (resp. opposite) sides for equal (resp. opposite) arrows
    ]
    #proof()[#todo]
    #corollary()[
      If $tau$ is invertible, then $tau^(-1) N = 1/lambda_tau ld(adj(tau)(rd(N)))$ for $N in ext(W)$
    ]
    #proof()[#todo]
    #corollary()[
      Let $tau in cal(L)(V)$, and $M in ext V$.
      + $tau(rd((adj(tau) M))) = (det tau)rd(M)$
      + $tau(rd(M)) = (det tau)rd(((adj(tau))^(-1)M))$ if $tau in "GL"(V)$
      + $tau(rd(M))=(det tau)rd((tau M))$ if $T in "U"(V)$
      + $tau(rd(M))=rd((tau M))$ if $tau in "SU"(V)$
    ]
    #proof()[#todo]
    #note[
      If $FF == RR$, then $upright(U)(V), "SU"(V)$ become $upright(O)(V), "SO"(V)$
    ]
    #theorem()[
      If $tau:V arrow W$ is invertible and $M, N in ext V$ then 
      $ tau(M vee N)=(1\/lambda_tau) tau M vee tau N $
      where the second $vee$ is w.r.t. $ast.circle$ (in $W$).
    ]
    #proof()[#todo]
    #corollary()[For $M,N in ext V$ and $tau in "GL"(V)$:
      $ tau(M vee N) = (1\/det tau) tau M vee tau N $
    ]
    #proof()[#todo]

= Inverse w.r.t Geometric Product

  #definition()[*Inverse* of blade $A eq.not 0$: $ A^(-1)
    =A/A^2
    =A/norm(A)^2
    =A/inner(A,A)
    =A/(A lc A)
    =A/(rev(A) ast A)
    =rev(A)/(A ast A) $
    ]<d:blade_inverse_geoprod>
  #theorem()[$A A^(-1)= A^(-1)A =1$.]
  #proof()[#v(-1.5em)$
    A A^(-1)= A lc A/(A lc A) + A wedge A/(A lc A)= (A lc A)/(A lc A) + (A wedge A)/(A lc A) = 1
  $#v(-1.5em)]
  #theorem()[Blade $A$ is invertible $arrow.double.l.r A^2 eq.not 0$.]
  #proof()[From @d:blade_inverse_geoprod]
  #definition()[
    Set of all invertible vectors:
    $V^times := {vv in V: vv^2 eq.not 0}$
  ]
  #definition()[*Versor*: $M=vv_1 dots vv_k$ where $vv_i in V^times$.]
  
= Classification of Geometric Algebras
  #theorem()[For all $s,t,n$ for which the following make sense, then:
    $ cal(G)(RR^(t,s-1)) isom cal(G)^(plus)(RR^(s,t)) isom cal(G)(RR^(s,t-1)) isom cal(G)^(plus)(RR^(t,s)) $
    $ cal(G)^(plus)(CC) isom cal(G)(CC^(n-1)) $
  ]
  #proof()[#todo]
  #theorem[ For all $s,t,n$ for which the following make sense, then:
    $ cal(G)(RR^(n+2, 0)) isom cal(G)(RR^(0,n)) otimes cal(G)(RR^(2,0)) $
    $ cal(G)(RR^(0, n+2)) isom cal(G)(RR^(n,0)) otimes cal(G)(RR^(0,2)) $
    $ cal(G)(RR^(s+1,t+1)) isom cal(G)(RR^(s,t)) otimes cal(G)(RR^(1,1)) $
  ]
  #proof()[#todo]
  #corollary()[$cal(G)(RR^(4,0)) isom cal(G)(RR^(0,4)) isom HH^(2,2)$]
  #proof()[#todo]
  #theorem()[
    If $s+t$ is odd and $I^2 = -1$ then
    $cal(G)(RR^(s,t)) isom cal(G)^(plus)(s,t) otimes CC isom cal(G)(CC^(s+t-1))$
  ]
  #proof()[#todo]
  #corollary()[For any $p gt.eq 0, q gt.eq 1$ s.t. $p+q=s+t$: 
    $ cal(G)(RR^(s,t)) isom cal(G)(RR^(p,q-1))otimes CC $
  ]
  #proof()[#todo]
  #theorem()[
    For all $n gt.eq 0$
  ]
  #figure(caption: [$cal(G)(RR^(s,t))$ in pos. $(x,y)=(s,t)$, where $FF_N=cal(M)_(N,N)(FF)$, $FF^n=FF oplus dots oplus FF $ (n times), $HH=$ quaternions, $CC=$ complex numbers, $RR=$ real numbers] )[
    
    #tablex(columns: 10, auto-lines:false, align:center+horizon, row-gutter:0pt,col-gutter:0pt,
    [*8*],[$RR_16$   ],[$(RR_16)^2$],[$RR_32$   ],[$CC_32$   ],[$HH_32$    ],[$(HH_32)^2$],[$HH_64$],[$CC_128$],[$RR_256$],
    [*7*],[$(RR_8)^2$],[$RR_16$    ],[$CC_16$   ],[$HH_16$   ],[$(HH_16)^2$],[$HH_32$],[$CC_64$],[$RR_128$],[$(RR_128)^2$],
    [*6*],[$RR_8$    ],[$CC_8$     ],[$HH_8$    ],[$(HH_8)^2$],[$HH_16$    ],[$CC_32$],[$RR_64$],[$(RR_64)^2$],[$RR_128$],
    [*5*],[$CC_4$    ],[$HH_4$     ],[$(HH_4)^2$],[$HH_8$    ],[$CC_16$    ],[$RR_32$],[$(RR_32)^2$],[$RR_64$],[$CC_64$],
    [*4*],[$HH_2$    ],[$(HH_2)^2$ ],[$HH_4$    ],[$CC_8$    ],[$RR_16$    ],[$(RR_16)^2$],[$RR_32$],[$CC_32$],[$HH_32$],
    [*3*],[$HH^2$    ],[$HH_2$     ],[$CC_4$    ],[$RR_8$    ],[$(RR_8)^2$ ],[$RR_16$],[$CC_16$],[$HH_16$],[$(HH_16)^2$],
    [*2*],[$HH$      ],[$CC_2$     ],[$RR_4$    ],[$(RR_4)^2$],[$RR_8$     ],[$CC_8$],[$HH_8$],[$(HH_8)^2$],[$HH_16$],
    [*1*],[$CC$      ],[$RR_2$     ],[$(RR_2)^2$],[$RR_4$    ],[$CC_4$     ],[$HH_4$],[$(HH_4)^2$],[$HH_8$],[$CC_16$],
    [*0*],[$RR$      ],[$RR^2$     ],[$RR_2$    ],[$CC_2$    ],[$HH_2$     ],[$(HH_2)^2$],[$HH_4$],[$CC_8$],[$RR_16$],hlinex(),
    [   ],vlinex(),[*0*               ],[*1*                 ],[*2*        ],[*3*],[*4*    ],[*5*    ],[*6*    ],[*7*    ],[*8*    ]
    )
  ]


= Groups
  #definition()[*Rotor* or *spinor*: versor $vv_1 dots vv_k$ where $k$ is even.]
  #definition([_Some interesting groups_])[

  + *Group of all invertible elements*:
    $ cal(G)^times:={M in cal(G):exists N in G: M N=N M=1} $
  + #box(width: 3.7cm)[*Lipschitz group*:]
   $rev(Gamma) := {M in cal(G)^times: invol(M) V M^(-1) subset.eq V } $
  + #box(width: 3.7cm)[*Versor group*]
   $Gamma := {vv_1 dots vv_k in cal(G): vv_i in V^times} $
  + #box(width: 3.7cm)[*Unit versor group*]
   $"Pin":={M in Gamma: M rev(M) in {-1,1}} $
  + #box(width: 3.7cm)[*Even unit versor group*]
   $"Spin":= "Pin" sect G^plus $
  + #box(width: 3.7cm)[*Rotor group*]
   $"Spin"^plus:= {M in "Spin": M rev(M)=1}$

  ]

  #definition([_Canonical products_])[
    + #box(width: 3.7cm)[*Left multiplication*:] $l_M in "end"(cal(G))$ s.t. $l_(M)(N)=M N$
    + #box(width: 3.7cm)[*Right multiplication*:] $r_M in "end"(cal(G))$ s.t. $r_(M)(N)=N M$
    + #box(width: 3.7cm)[*Adjoint mult.*:] $"ad"_M h1 in h1 "aut"(cal(G))$ s.t. $"ad"_(M)(N) h1 = h1 M N M^(-1)$
    + #box(width: 3.7cm)[*Twisted adjoint mult.*:] $invol("ad")_M h2 in h2 "aut"(cal(G))$ s.t. $invol("ad")_(M)(N) h1 = h1 invol(M) N M^(-1)$
  ]
  #definition([_Canonical actions_])[
    + #box(width: 3.7cm)[*Left action*:] $l: cal(G) arrow "end"(cal(G)), l(x)=l_x$
    + #box(width: 3.7cm)[*Right action*:] $r: cal(G) arrow "end"(cal(G)), r(x)=r_x$
    + #box(width: 3.7cm)[*Adjoint action*:] $"ad": cal(G) arrow "aut"(cal(G)), "ad"(x)="ad"_x$
    + #box(width: 3.7cm)[*Twisted adjoint action*:] $invol("ad"): cal(G) arrow "aut"(cal(G)), invol("ad")(x)=invol("ad")_x$
  ]

  #definition()[
    *Reflection along vector* $vv in V$: $R_(vv)(aa)=aa-2Rho_span(vv)(aa) $
  ]
  #definition()[
    *Reflection across subspace* $S lt.eq V$:
    $ R_(S)(aa)=2P_(S)(A)-aa$
  ]
  #theorem[
    $invol("ad")_vv$ is a reflection along $vv in V$.
  ]
  #proof()[Let $cv(a)=aa_perp h1 + h1 aa_parallel$, where $aa_parallel h1 = Rho_span(vv)(aa)$ and $aa_perp = aa h1 - h1 aa_parallel$. Then:
    $ invol("ad")_(vv)(aa)&=invol(vv)(aa_perp + aa_parallel)vv^(-1)=-vv aa_perp vv^(-1) -vv(lambda vv)vv^(-1) \ 
      &= aa_perp vv vv^(-1) -lambda vv vv vv^(-1)= aa_perp - lambda vv=aa_perp - aa_parallel \ 
      &= aa-2aa_parallel= R_vv(aa) $ #v(-1.3em)
  ]
  #theorem[$invol("ad")_vv in O(V,Q)$ (a linear isometry), i.e. $Q(invol("ad")_(vv)(uu)) = Q(uu).$
  ]
  #proof()[
    $Q(invol("ad")_vv(uu))h1=h1(invol("ad")_vv(uu))^2#h(-.2em)=h1 vv uu vv^(-1) vv uu vv^(-1)h1 =h1 uu^2 vv vv^(-1)h1 =h1 Q(uu)$.
  ]
  #theorem([_Cartan-Dieudonné_])[In a non-degenerate symmetric bilinear space where $"char"(FF)eq.not 2$,
    every orthogonal transformation is a product of at most $dim V$ reflections $R_i$, i.e., 
    $ tau in O(V,Q) arrow.double tau = R_1 compose dots compose R_k, quad k lt.eq dim V. $
  ]<t:cartan-dieudonne>
  #proof()[
    #todo
  ]
  #theorem()[
    $invol("ad"): Gamma arrow O(V,Q)$ is a homomorphism.
  ]
  #proof()[We show that $tad(uu_1 dots uu_k)h1=h1 tad_(uu_1) h0 compose h1 dots h1 compose tad_(uu_k)$:
    #set par(leading: .3em)
    $ tad_(uu_1 dots uu_k)(vv)h1&=h1 (uu_1 dots uu_k)^involalone vv (uu_1 dots uu_k)^(-1)h1=h1 invol(uu_1)dots invol(uu_k)vv uu_k^(-1)dots uu_1^(-1) \ 
    &=tad_(uu_1)compose dots compose tad_(uu_k)(vv) $#v(-1.3em)
  ]
  #corollary()[The homomorphism $invol("ad"):Gamma arrow O(V,Q)$ is surjective.]
  #proof()[
    From @t:cartan-dieudonne, $f in O(V,Q) arrow.double f=tad_(vv_1)compose dots compose tad_(vv_k)=tad_(vv_1 dots vv_k)$, where $vv_1 dots vv_k in Gamma$.
  ]
  #lemma()[$(xx,yy h1 in h1 V^times$) and $(xx^2 h1 = h1 yy^2) arrow.double (xx h1 + h1 yy in V^times)$ or $(xx h1 -h1 yy in V^times)$.]
  #lemma()[
    With the conditions 
    #set par(leading: .3em)
    $ xx+yy in V^times &arrow.double tad_(xx+yy)(xx)=-yy \
      xx-yy in V^times &arrow.double tad_(xx-yy)(xx)=yy
    $
  ]
  #proof()[#todo]
  #lemma()[Let ${ee_1,dots,ee_n},{ff_1,dots,ff_n} h2 in h2 cal(P)(V^times) h2 sect h2 cal(P)(V)_perp$ s.t. $ee^2 h1 = h1 ff^2$, $j=1,dots,n lt.eq dim V$. Then there exist explicit versors $M_1, dots, M_n in Gamma$ where each $M_j$ is either a $0$-versor ($M_j=1$), 1-versor ($M_j=vv in V^times$), or 2-versor ($M_j=vv_1 vv_2, vv_1, vv_2 in V^times$) s.t. $ tad_(M_n dots M_1)(ee_j)= ff_j, quad j=1,dots,n. $
  In case of definite signatures, only 0- or 1-versors are necessary.]
  #proof()[#todo]

  #theorem()[
    
  ]

= Projective Geometry
  == Cross ratio
    #definition()[
      *Cross ratio* of four points: $ D(a,b,c,d)=((a wedge b)(c wedge d))/(a wedge c)(b wedge d) $
    ]

#hid[
= Inner and outer spaces

  #definition[
    The *inner* and *outer spaces* of $M in ext V$ are, resp., $ins(M)={vv in V: vv wedge M = 0}$ and $ous(M)={vv in V: vv lc M = 0}^perp$.
  ]

  #theorem()[
    For a blade $B$, $ins(B)=ous(B)=[B]$.
  ]
  #note[0 is an exception: $ous(0)=[0]={0}$, but $ins(0)=V$.]
  #theorem()[For nonzero $M in ext V$, $ins(M) lt.eq ous(M) $.]
  #theorem()[For $M in ext V$, $ins(rd(M))=ous(M)^perp$ and $ous(rd(M))=ins(M)^perp$.]
  #theorem()[
    Let $H in ext^p V$.
    + $ous(H) = {F lc H: F in ext^(p-1) V}$
    + $ins(H) = {H lc G: G in ext^(p+1) V}^perp$
  ]
  #theorem()[
    $ous(M) = sect{S < V: M in ext S}$ is the smallest subspace whose exterior algebra contains $M in V$.
  ]
  #corollary()[
    $ins(M)= sum{S < V: rd(M) in ext(V^perp)}$ is the largest subspace whose orthogonal complement has $rd(M)$ is its exterior algebra.
  ]
  #corollary()[Given $M,N in ext V$ with $ous(M) sect ous(N) = {0}$, we have 
  $ M wedge N = 0 arrow.double.l.r M = 0 "or" N = 0 $]
  #theorem()[
    Let $M in ext V$
    + $ins(M) = {vv in V: vv = 0 "or" M = vv wedge N "for" N in ext V}$
    + $ins(M) = [B]$ for any blade $B eq.not 0$ of largest possible grade s.t. $M = B wedge N$ for $N in ext V$
  ]

  #corollary()[
    If $ins(M)=ous(M)$ then $M in ext V$ is a blade.
  ]

  #corollary()[
    For $M,N in ext V$, $M wedge N eq.not 0 arrow.double ins(M) sect ins(N) = {0}$
  ]
  #theorem()[Let $M in ext V$
    + $ous(M)={vv in V: vv = 0 "or" M = vv lc N "for" N in ext V}^perp$
    + $ous(M)=[B]$ for any blade $B$ of smallest possible grade s.t. $M = N lc B$ for $N in ext V$
  ]
  #theorem()[Let $tau in cal(L)(V(FF),W(FF))$, and $M in ext V$. Then $ins(tau M) gt.eq tau(ins(M))$ and $ous(tau M) lt.eq tau(ous(M))$, with equalities if $tau$ is invertible.]

  #theorem()[
    For $M,N in ext V$ we have the following, with equalities and $plus.circle$ if $M,N eq.not 0$ and $ous(M) sect ous(N) = {0}$
    + $ous(M wedge N) lt.eq ous(M)+ous(N)$
    + $ins(M wedge N) gt.eq ins(M)+ins(N)$
  ]

  #note[If $M wedge N eq.not 0$, then $ins(M) lt.eq ins(M wedge N) lt.eq ous(M wedge N)$. So, $[B] lt.eq ous(B wedge N)$ if $B$ is a blade and $B wedge N eq.not 0$. If $N eq.not 0$ and $ous(M) sect ous(N) = {0}$, $ous(M) lt.eq ous(M wedge N)$ ($M wedge N eq.not 0$ is not enough)]

  #theorem()[
    For $M, N in ext V$ we have the following, with equalities if $M,N eq.not 0$ and $ous(M) npperp ins(N)$
    + $ous(M lc N) lt.eq ins(M)^perp sect ous(N)$.
    + $ins(M lc N) gt.eq ous(M)^perp sect ins(N)$.
  ]
  #theorem()[
    Let $M,N in ext V$.
    + $ous(M N) lt.eq ous(M) + ous(N)$, with equality and $plus.circle$ if $M,N eq.not 0$ and $ous(M) sect ous(N) = {0}$
    + $ins(M N) gt.eq (ous(N)^perp sect ins(M)) + (ous(M)^perp sect ins(N))$. For $M, N eq.not 0$, $ins(M N) = ous(N)^perp sect ins(M)$ if $ous(N) npperp ins(M)$, or $ins(M N) = ous(M)^perp sect ins(N)$ if $ous(M) npperp ins(N)$.
  ]

  #note[
    If $M,N eq.not 0$, $ous(N) npperp ins(M)$ and $ous(M) npperp ins(N)$ then $ins(M N) = {0}$. But this only happens if $ins(M), ous(M), ins(N)$, and $ous(N)$ have same dimension, so $M$ and $N$ are same grade blades with $(M N)_0 = inner(rev(M), N) eq.not 0$.
  ]

  #theorem()[
    Let $M, N in ext V$
    + $ins(M) pperp ous(N) arrow.double M lc N = 0$
    + $M lc N = 0 arrow.double ous(M) pperp ins(N)$, for $M, N eq.not 0$
  ]
  #note[
    The converses do not hold, and $ous(M) perp ous(N) arrow.double.not M lc N = 0$
  ]

= Disassembling multivectors
  #definition()[#box(width: 3.5cm)[*Decomposition* of $M$:] $M = sum_i M_i$]
  #definition()[#box(width: 3.5cm)[*Factorization* of $M$:] $M = L wedge N$]
  #definition()[#box(width: 3.5cm)[*Carving*:] $M = L lc N$]
  == Balanced and minimal decompositions

    Let $M = sum_i M_i$ for $M, M_i in ext V$.
    #theorem()[$ins(M) gt.eq sect.big_i ins(M_i)$ and $ous(M) lt.eq sum_i ous(M_i)$. ]

    #definition()[The sum is *inner balanced* if $ins(M) h1= h1 sect.big_i ins(M_i)$, *outer balanced* if $ous(M) h1 =h1 sum_i ous(M_i)$, and *balanced* if both conditions hold.]

    #theorem[Inner balanced #box(baseline: 1.85em,$&arrow.double.l.r forall i(ins(M) lt.eq ins(M_i)) \
    & arrow.l.r.double forall vv in V (vv wedge M = 0 &arrow.double.l.r forall i(vv wedge M_i = 0)).$)]
    #theorem[Outer balanced #box(baseline: 1.85em,$&arrow.double.l.r forall i(ous(M) gt.eq ous(M_i)) \
    & arrow.l.r.double forall vv in V (vv lc M = 0 &arrow.double.l.r forall i(vv lc M_i = 0)).$)]

    #theorem()[Let $A$ be a blade and $L, M, N in ext V$.
      + $(A rc M) lc N = (Rho_A M) wedge (A lc N)$, if $[A] lt.eq ins(N)$;
      + $(M rc L) lc N = L wedge (M lc N)$, if $ous(L) lt.eq ins(N)$.
    ]

    #theorem()[$sum_i M_i$ is inner balanced $arrow.double.l.r sum_i rd(M_i)$ is outer balanced.]
    #proof()[#todo]
    #definition()[A blade decomposition of $M$ is *minimal* if no other blade decomposition of $M$ has fewer blades.]
    #theorem()[Any minimal blade decomposition is balanced.]
    #proof()[#todo]
  == Blade factorizations

    #definition()[*Inner blade* of $M$: blade $B eq.not 0$  with  $[B] lt.eq ins(M)$, being *maximal* if $[B] = ins(M)$.]
    #theorem()[Blade $B h1 eq.not h1 0$ is an inner blade of $M arrow.double.r.l exists N h1 in h1 ext h0 V h0: h1 M h1 = h1 B wedge N$. In this case, there is a unique such $N h1 in h1 ext S$ for any complement $S$ of $[B]$.]
    #proof()[#todo]

    #definition()[A *blade factorization* $M=B wedge N$, with blade $B eq.not 0$ and $N in ext V$, is:
      + *tight* if $ous(N) sect [B]={0}$;
      + *orthogonal* if $ous(N) lt.eq [B]^perp$;
      + *maximal* if $[B] = ins(M)$.
    ]

    #theorem()[
      Let $0 eq.not M = B wedge N$ be a blade factorization.
      + It is tight $arrow.double.l.r ous(M) = [B] plus.circle ous(N)$ and $ins(M)=[B] plus.circle ins(N)$.
      + It is orthogonal $arrow.double.l.r N = (B lc M)/(norm(B)^2)$
      + It is maximal $arrow.double ins(N) = {0}$. The converse holds if it is tight.
    ]
    #proof()[#todo]
    #theorem()[Let $0 eq.not M = B wedge N = B_m wedge N_m $ be blade factorizations, with $B_m wedge N_m$ being maximal. Then $[B] lt.eq [B_m]$, and if $B wedge N$ is:
      + tight then $[B_m]=[B]plus.circle ins(N)$;
      + orthogonal then $ins(N)=[B lc B_m]$;
      + maximal then $B = lambda B_m$ for some $lambda eq.not 0 in FF$.
    ]
    #proof()[#todo]

    #theorem()[Given a complement $S$ of $ins(M)$, there is a unique (up to sca-lars) maximal tight blade factorization $M h1 = h1 B wedge N$ with $N h1 in h1 ext S$.]
    #proof()[#todo]

    #theorem()[
      If a blade factorization $0 eq.not M = B wedge N$ is:
      + maximal then $M = sum_i B wedge N_i$ is inner balanced for any decomposition $N = sum_i N_i$;
      + tight then $M = sum_i B wedge N_i$ is outer balanced for any outer balanced decomposition $N = sum_i N_i$.
    ]
    #proof()[#todo]

  == Blade carvings

    #definition()[An *outer blade* of $M$ is a blade $B eq.not 0$ with $ous(M) lt.eq [B]$, being *minimal* if $[B]=ous(M)$.]
    #definition()[A *blade carving* $M=N lc B$, where $B eq.not 0$ is a blade and $N in ext V$, is:
      + *tight* if $ous(N) sect [B]^perp = {0}$;
      + *internal* if $ous(N) lt.eq [B]$;
      + *minimal* if $[B] = ous(M)$.
    ]

    #theorem()[Let $M, N in ext V$ and $B eq.not 0$ be a blade.
    + $B$ is an inner blade of $M arrow.double.l.r rd(B)$ is an outer blade of $rd(M)$. Moreover, $B$ is maximal $arrow.double.l.r rd(B)$ is minimal.
    + $M = B wedge N$ is a tight (resp. orthogonal, maximal) factorization $arrow.l.r.double rd(M)=N lc rd(B)$ is a tight (resp. internal, minimal) carving.
    ]
    #proof()[#todo]
    #theorem()[
      A blade $B eq.not 0$ is an outer blade of $M$ iff $M = N lc B$ for some $N in ext V$. In this case, for each complement $S$ of $[B]^perp$ there is a unique such $N in ext S$.
    ]
    #proof()[#todo]
    #theorem()[
      Let $0 eq.not M = N lc B$ be a blade carving.
      + If it is tight then $ous(M)=ins(N)^perp sect [B]$ and $ins(M)=ous(N)^perp sect [B]$.
      + It is internal $arrow.double.l.r N = (B rc M)/(norm(B)^2)$.
      + If it is minimal then $ins(n)={0}$. The converse holds if it is tight.
    ]
    #proof()[#todo]
    #theorem()[
      Let $0 eq.not M = N lc B = N_m lc B_m$ be blade carvings, with $N_m lc B_m$ being minimal. Then $[B_m] lt.eq [B]$, and if $N lc B$ is:
      + tight then $[B_m]=ins(N)^perp sect [B]$;
      + internal then $ins(N)=[B_m lc B]$;
      + minimal then $B = lambda B_m$ for a scalar $lambda eq.not 0$.
    ]
    #proof()[#todo]
    #theorem()[
      Given a complement $S$ of $ous(M)^perp$, there is a unique (up to scalars) minimal tight blade carving $M = N lc B$ with $N in ext V$.
    ]
    #proof()[#todo]
    #theorem()[
      If a blade carving $0 eq.not M = N lc B$ is:
      + minimal then $M = sum_i N_i lc B$ is outer balanced for any decomposition $N = sum_i N_i$;
      + tight then $M = sum_i N_i lc B$ is inner balanced for any outer balanced decomposition $N = sum_i N_i$.
    ]
    #proof()[#todo]
= Simplicity and grades
  == Generalized grades

  #definition()[
    The *inner*, *outer*, *bottom* and *top grades* of $M$ are $|M|_"in" = dim ins(M), |M|_"out" = dim ous(M), |M|_"bot" = min\{p:(M)_p eq.not 0\}$ and $|M|_"top" = max\{p:(M)_p eq.not 0\}$.
  ]
  #theorem()[
    $M$ is a homogeneous (k-vector) $arrow.double.l.r |M|_"bot"=|M|_"top"$
  ]
  #theorem()[
    $M$ is a blade $ h0 arrow.double.l.r h0 |M|_"in" h0 = h0 |M|_"out"$. In this case, all grades will coincide.
  ]

  #theorem()[
    Let $0 eq.not M in ext V$ and $n = dim V$.
    + $|M|_"in" lt.eq |M|_"bot" lt.eq |M|_"top" lt.eq |M|_"out" $;
    + $|M|_"in" + |rd(M)|_"out" = |M|_"bot" + |rd(M)|_"top" = n$.
  ]

  #theorem()[Let $b = max{|M|_"bot", |N|_"bot"}$ and $t = min{|M|_"top",|N|_"top"}$ for nonzero $M, N in ext V$
  + If $dim(ins(M)+ins(N)) gt.eq t+2$ then $M + N$ is inner balanced and has $|M + N|_"in" lt.eq b - 2$.
  + If $dim(ous(M) sect ous(N)) lt.eq b-2$ then $M + N$ is outer balanced and has $|M+N|_"out" gt.eq t + 2$.
  ]


  == Simplicity criteria
  #theorem()[For $M in ext V$, $M$ is a blade $arrow.l.r.double ins(M)=ous(M)$]
  #corollary()[$H in ext^p V$ is a blade $arrow.double.l.r$ the equivalent criteria below hold:
  + #box(width:4.3cm,align(right, $forall F in ext^(p-1)V:$)) $(F lc H) wedge H = 0$
  + #box(width:4.3cm,align(right, $forall G in ext^(p+1)V:$)) $(H lc G) lc H = 0$
  + #box(width:4.3cm,$forall F in ext^(p-1)V, G in ext^(p+1) V:$) $inner(F lc H, H lc G) = 0 $
  + #box(width:4.3cm,align(right, $forall F in ext^(n-p-1)V:$)) $(F wedge H) vee H = 0$
  + #box(width:4.3cm,align(right, $forall G in ext^(n-p+1)V:$)) $(G vee H) wedge H = 0$
  ]
  #corollary()[$H in ext^2 V$ is a blade $arrow.double.l.r H wedge H = 0$.]
  #theorem()[
    If $i = |M|_"in"$ and $o=|M|_"out"$ for $0 eq.not M in ext V$, then $gproj(M,i), gproj(M,i+1), gproj(M,o-1)$ and $gproj(M,o)$ are simple.
  ]
  #corollary()[If $gproj(M,p)$ is a blade then $|M|_"in" + 2 lt.eq p lt.eq |M|_"out" - 2$.]
  #corollary()[For $0 eq.not H in ext^p V$, there are 2 possibilities:
    + $H$ is a blade and $|H|_"in" = |H|_"out" = p$
    + $H$ is not a blade and $|H|_"in"+2 lt.eq p lt.eq |H|_"out"-2.$
  ]
  #corollary()[For nonzero $G,H in ext^p V$, if $dim(ous(G) sect ous(H)) lt.eq p - 2$ or dim]
  #theorem()[
    For nonzero $p$-blades $A$ and $B$ with $[A] eq.not [B]$, either:
    + $dim([A]sect[B])+1=p=dim([A]+[B])-1$ and $A+B$ is a blade but neither inner nor outer balanced
    + $dim([A]sect[B])+2 lt.eq p lt.eq dim([A]+[B])-2$ and $A+B$ is not a blade but is balanced.
  ]

  #theorem()[
    For $p gt.eq 3$ and $1 lt.eq r lt.eq p-2, H in ext^p V$ is a blade $arrow.double.l.r B lc H$ is a blade for every blade $B in ext^r V$.
  ]

  #theorem()[
    $H in ext^p V$ is a blade $arrow.double.l.r$ the equivalent criteria below holds:
    + #box(width:4.3cm,align(right, $forall F in ext^(p-2)V:$)) $(F lc H) wedge H = 0$
    + #box(width:4.3cm,align(right, $forall G in ext^(p+2)V:$)) $(H lc G) lc H = 0$
    + #box(width:4.3cm,$forall F in ext^(p-2)V, G in ext^(p+2) V:$) $inner(F lc H, H lc G) = 0 $
    + #box(width:4.3cm,align(right, $forall F in ext^(n-p-2)V:$)) $(F wedge H) vee H = 0$
    + #box(width:4.3cm,align(right, $forall G in ext^(n-p+2)V:$)) $(G vee H) wedge H = 0$
  ]

= Principal Angles and Angle Bivector
  == Principal Angles

  #definition()[
    Let nonzero $S,T h1 lt.eq h1 V#h(-.1em)$ and  $m h1 = h1 min{p=dim S,q=dim T}$.
    Their *principal angles* are $0 h1 lt.eq h1 theta_1 h1 lt.eq h1 dots h1 lt.eq h1 theta_m h1 lt.eq h1 pi/2$,
    with *associated principal bases* $cal(B)_S=(ee_1,dots,ee_p)_nperp basis S$, $cal(B)_T=(cv(f)_1,dots,cv(f)_q)_nperp basis T$ formed by *principal vectors*, if, for all $1 lt.eq i lt.eq p$ and $1 lt.eq j lt.eq q$,
    $ inner(ee_i, cv(f)_j) = delta_(i=j) cos theta_i. $
    We also say that $cal(B)_T$ is a principal basis of $T$ w.r.t., $S$.
  ]
  #theorem[The number of null principal angles for which $ee_i=cv(f)_i$, equals $dim(S sect T)$.]
  #proof()[#todo]
  #theorem()[$V perp W arrow.double.l.r theta_i = pi/2$ for all principal angles $theta_i$.]
  #proof()[#todo]
  #theorem()[$Rho_W ee_i=delta_(i > m)cv(f)_i cos theta_i$]
  #proof()[#todo]
  #theorem()[
    $theta_i h0 = h0 min{theta_(cv(s),cv(t))h0: h0 cv(s) h0 in h0 S h0 without h0 {0}, cv(t) h0 in h0 T h0 without h0  {0}, h0  inner(cv(s), h0 ee_j)h0=h0 inner(cv(t), h0 cv(f)_j)h0 =h1 0, h0 forall j h0 < h0 i}$, where $theta_(cv(s),cv(t))$ is the angle between $cv(s)$ and $cv(t)$.
  ]
  #proof()[#todo]
  == Partial Orthogonality

  #theorem()[For any subspaces $S,T lt.eq V$:
  + $S pperp T arrow.double.l.r dim S > dim T$ or a principal angle of $S$ and $T$ is $pi/2$
  + $S pperp T arrow.double.l.r dim S > dim Rho_(T)(S)$
  + If $dim S = dim T$ then $S pperp T arrow.double.l.r T pperp S$
  ]

  #definition()[Let $A,B in ext V$ be blades.
    + If $[A] pperp [B]$ we say $A$ is *partially orthogonal* to $B$ ($A pperp B$).
    + if $[A] perp [B]$ we say $A$ and $B$ is *completely orthogonal*.
  ]

  == Principal Decomposition and Relative Orientation

  #definition[
    A principal decomposition of A and B is 
    $A = sigma_A norm(A) ee_1 ee_2 dots ee_p \ 
    B = sigma_B norm(B) cv(f)_1 cv(f)_2 dots cv(f)_q$,
    where $sigma_A,sigma_B = plus.minus 1$. We also define $sigma_(A,B)=sigma_A sigma_B$, which we call *relative orientation* of $A$ and $B$ (w.r.t. $cal(B)_A$ and $cal(B)_B$).
  ]

  #lemma[
    If $p=q$ then $inner(A, B) = rev(A) ast B = sigma_(A,B) norm(A) norm(B) product_(i=1)^p cos theta_i$.
  ]

  #theorem()[
    For nonzero blades $A,B in ext^p V$, $A pperp B arrow.double.r.l A ast B = 0$.
  ]
  #proof()[#todo]
  == Asymmetric Angle of Subspaces

  #definition()[
    Let $p = dim S, U subset S $ be a $p$-dimensional parallelotope, and $"vol"_p$ be the $p$-dimensional volume. The *projection factor* of $S$ on $T$ is $pi_(S,T)=("vol"_p Rho_T(U))/("vol"_p U)$
  ]

  #definition()[
    The *asymmetric angle* $Theta_(S,T) in [0,pi/2]$ of $V$ with $W$ is $cos Theta_(S,T)=pi_(S,T)$.
  ]

  #theorem()[
    Given subspaces $S,T lt.eq V$ with principal angles $theta_1, dots, theta_m$, where $m=min{p=dim S,q=dim T}$, let $A,B in ext V$ be nonzero blades s.t. $[A]=S$ and $[B]=T$. Then:
    + $cos Theta_(S,T)=(norm(Rho_B A))/(norm(A))$
    + $Theta_(S,T)=Theta_(S,Rho_T(S))$
    + $Theta_(S,T)$ is the angle in $ext^p V$ between $ext^p S$ and $ext^p T$.
    + If $dim S = dim T$ then $cos Theta_(S,T)=(|A ast B|)/(norm(A)norm(B))$, and $Theta_(S,T)=Theta_(T,S)$
    + If $p > q$ then $Theta_(S,T)=pi/2$, otherwise
    $cos Theta_(S,T)=product_(i=1)^m cos theta_i$.
    + $Theta_(S,T)=0 arrow.double.l.r S lt.eq T$.
    + $Theta_(S,T)=pi/2 arrow.double.l.r S pperp T$.
  ]
  #proof()[#todo]
  === Related Angles

  #definition()[The *max-* and *min-symmetrized angles* are resp. $hat(Theta)_(S,T)=max{Theta_(S,T), Theta_(T,S)}$ and $caron(Theta)_(S,T)=min{Theta_(S,T), Theta_(T,S)}$]

  #definition()[The *complementary angle* is $Theta_(S,T)^perp = Theta_(S,T^perp)$]
  #theorem()[Given subspaces $S,T lt.eq V$ with principal angles $theta_1,dots,theta_m$, $m=min(dim S, dim T)$, we have
  $cos Theta_(S,T)^perp = product_(i=1)^m sin theta_i$.
  ]

  #theorem()[
    Let $S,T lt.eq V$ be subspaces.
    + $Theta^perp_(S,T) = 0 arrow.double.l.r S perp T$;
    + $Theta^perp_(S,T) = pi/2 arrow.double.l.r S sect T eq.not {0}$;
    + $Theta^perp_(S,T) = Theta^perp_(T,S)$.
  ]
  #proof()[#todo]

  === Oriented Angles

  #definition()[Let $[A]=S$, $[B]=T$ for blades $A,B in ext V$ and $S,T lt.eq V$. The *oriented asymmetric angle* $Theta_(A,B) in [0, pi]$ of $S$ with $T$ is $cos Theta_(A,B)=sigma_(A,B) cos Theta_(S,T)$.]

  #definition()[$pi_(A,B)=sigma_(A,B)pi_(S,T)$ is an *oriented projection factor*, and *oriented max-symmetrized* and *complementary angles* $hat(Theta)_(A,B), Theta^perp_(A,B) in [0,pi]$ are given by $cos hat(Theta)_(A,B)=sigma_(A,B)cos hat(Theta)_(S,T)$ and $cos Theta^perp_(A,B)=sigma_(A,B) cos Theta^perp_(S,T)$]

  == Angle Bivector of Subspaces

  Let $S,T lt.eq V$ be $p$-subspaces, having associated principal bases $cal(B)_S = (ee_1,dots,ee_p)$ and $cal(B)_T = (cv(f)_1,dots,cv(f)_p)$, and principal angles $theta_1 lt.eq dots lt.eq theta_p$. Also, let $d = dim(S sect T), E=ee_1 ee_2 dots ee_p$ and $F=cv(f)_1 cv(f)_2 dots cv(f)_p$.
  #definition()[
    For $1 lt.eq i lt.eq p$, $span({ee_i, cv(f)_i})$ is a *principal plane*, with *principal bivector* I_i (oriented from $S$ to $T$) given by $I_i = delta_(i > d) (ee_i wedge cv(f)_i)/(norm(ee_i wedge cv(f)_i))$. For $d < i lt.eq p$, $ee_i^perp = I_i cv(f)_i$ and $cv(f)_i^perp = ee_i I_i$ are *orthoprincipal vectors*.
  ]

  #definition()[
    $Phi_(S,T) = sum_(i=1)^p theta_i I_i$ is an *angle bivector* from $S$ to $T$.
  ]

  #theorem()[$Phi_(S,T)$ is uniquely defined $arrow.double.l.r theta_p eq.not pi/2$.]
  #proof()[#todo]

  #theorem()[
    $F = e^(-Phi/2) E ext^(Phi/2) = E e^Phi = e^(-Phi)E$, where $Phi=Phi_(S,T)$.
  ]
  #proof()[#todo]

  === Oriented Angle Bivector

  #definition[The *oriented principal angles and bivectors* of $S$ and $T$ are $theta^plus_i = theta_i$ and $I_i^plus = I_i$ for $i < p$ and also for $i=p$ if $sigma_(A, B) = 1$, otherwise $theta_p^plus = pi - theta p$ and $I_p^plus = -I_p$. The *oriented angle bivector* from $S$ to $T$ is
  $ Phi_(A,B)=sum_(i=1)^p theta_i^plus I_i^plus = Phi_(S,T) - delta_(sigma_(A,B)=-1)/2 pi I_p $
  ]
  #theorem()[$Phi_(A,B)$ is uniquely defined $arrow.double.l.r theta^plus_(p-1) + theta^plus_p eq.not pi$.]
  #proof()[#todo]
  #theorem()[$e^(Phi_(A,B)) = sigma_(A,B)e^(Phi_(S,T))$.]
  #proof()[#todo]
  #theorem()[If $norm(A)=norm(B) = 1$ then $B = e^(-Phi/2) A e^(Phi/2) = A e^Phi = e^(-Phi)A$, where $Phi=Phi_(A,B)$.]
  #proof()[#todo]
  // === Minimal Geodesics in the Grassmannians
  === Exponentials of Angle Bivectors

  #definition()[For $1 lt.eq i lt.eq p$, $R_i = ee_i cv(f)_i$ = e^(I_i theta_i)=cos theta_i + I_i sin theta_i is a principal rotor.]
  #theorem()[$e^(Phi_(S,T))=R_1 R_2 dots R_p$.]
  #proof()[#todo]
  #theorem()[Let $c_i = cos theta_i$ and s_i = sin theta_i. Then
  $ e^(Phi_(S,T)) &= c_1 c_2 dots c_p \
                    &+ s_1c_2 dots c_p I_1 + c_1 s_2 c_3 dots c_p I_2 + dots + c_1 dots c_(p-1)s_p I_p \
                    &+ h0 s_1 s_2 c_3 dots c_p I_1 I_2 h0 + h0 s_1 c_2 s_3 dots c_p I_1 I_3 h0 + h0 dots h0 + h0 c_1 dots s_(p-1)s_p I_(p-1)I_p \
                    & dots.v \
                    & + c_1 s_2 dots s_p I_2 dots I_p + h1 dots h1 + s_1 h1 dots h1 s_(p-1) c_p I_1 h1 dots h1 I_(p-1)\
                    & + s_1 s_2 dots s_p I_1 I_2 dots I_p.

    $
  ]
  #proof()[#todo]
  #definition()[
    Let $I_0 = 1$ and $I_idx(i) = I_(i_1) dots I_(i_k)$ for $idx(i) = (i_1,dots,i_k) in oidx^(d+1,p)$, where $d = dim (S sect T)$. For any $idx(i) in oidx^(d+1,p)$, let $F_i = I_idx(i)F$ and $T_idx(i)=[F_idx(i)]$, where $F=cv(f)_1 cv(f)_2 dots cv(f)_p$.
  ]

  #lemma()[For any $idx(i) in oidx^(d+1,p)$:
    + $F_idx(i)$ is the unit $p$-blade obtained from $F$ by replacing $cv(f)_i$ with $ee_i^perp$ for each $i in idx(i)$
    + $cal(B)_idx(i)={f_idx(i): i in.not idx(i)}union{ee_idx(i)^perp: i in idx(i)}$ is a principal basis of $T_idx(i)$ associated to $cal(B)_S$
    + The (unordered) principal angles of $S$ and $T_idx(i)$ are $theta_i$ for $i in.not idx(i)$ and $pi/2-theta_i$ for $i in idx(i)$
  ]
  #proof()[#todo]
  #theorem()[$e^(Phi_(S,T)) = sum_(idx(i) in oidx^(d+1,p)) cos Theta_(S,T_idx(i))I_idx(i) = sum_(idx(i)in oidx^(d+1,p)) pi_(S,T_idx(i))I_idx(i)$]
  #proof()[#todo]
  #theorem()[$gproj(e^(Phi_(S,T)),0) = cos Theta_(S,T) = pi_(S,T)$, and also $norm(gproj(e^(Phi_S,T),2p)) = cos Theta_(S,T)^perp = pi_(S,T^perp)$.]
  #proof()[#todo]
  #definition()[For $idx(i) in oidx^(d+1,p)$, we give $T_(idx(i))$ the orientation of $B_idx(i)=I_idx(i)B=sigma_B norm(B)F_idx(i)$, and set $sigma_(A,B_idx(i))$ in terms of $cal(B)_V$ and $cal(B)_(idx(i))$]

  #theorem()[$e^(Phi_(A,B)) = sum_(idx(i)in oidx^(d+1,p)) cos Theta_(A,B_idx(i))I_idx(i) = sum_(idx(i) in oidx^(d+1, p)) pi_(A,B_idx(i))I_idx(i)$.]
  #proof()[#todo]
  === Plucker Coordinates
  === Distinct Dimensions and Projective-Orthogonal Decomposition

  == Clifford Geometric Product

  #theorem[$rev(A)B = norm(A)norm(B) e^(Phi_(A,B))$ for same grade blades $A,B in ext^p V$.]

  === Plucker Coordinates in the Geometric Product

  === Distinct Grades

  === Principal Angles via Geometric Algebra



] // hid