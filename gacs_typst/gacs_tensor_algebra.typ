#import "acmt.typ": *

#show: acmt-theme.with()
/* START OF DOCUMENT */

// Status of the slide:
  // d definition
  // r corollary
  // t theorem
  // l lemma
  // n note
  // p proposition
  // g graphical pictures
  // e example
  // c complete
  // * ongoing

#title-slide(title: "Geometric Algebra", subtitle: "Chapter 2: Tensor Algebra", authors: "Anderson Tavares", institution-name: "", date: datetime.today().display())

// Quadratic form
  #slide[
    == Multilinear map
    #definition[
      *Multilinear*\/*$n$-linear* map $f:V_1 times dots.c times V_n arrow W$ is linear in each coordinate (let $u_(i:j)=u_i,u_(i+1),dots,u_j$)
      $ f(u_(1:k-1),alpha v + beta w, u_(k+1:n))=alpha f(u_(1:k-1), v, u_(k+1:n)) + beta f(u_(1:k-1), w, u_(k+1:n)) $
      for all $k=1,dots,n$.
      If $W=K$, then it is called a *multilinear form* or *$n$-form*.
    ]
    #definition[(Special multilinear maps)
      - *Symmetric*: \ #box(align(center)[$f(cvec(v)_1,dots,cvec(v)_i,dots,cvec(v)_j,dots,cvec(v)_n)=f(cvec(v)_1,dots,cvec(v)_j,dots,cvec(v)_i, dots,cvec(v)_n), forall i eq.not j $])
      - *Antisymmetric*\/*skew-symmetric*:\ $f(cvec(v)_1,dots,cvec(v)_i,dots,cvec(v)_j,dots,cvec(v)_n)=-f(cvec(v)_1,dots,cvec(v)_j,dots,cvec(v)_i,dots,cvec(v)_n), forall i eq.not j $
      - *Alternate*\/*alternating*: \ $v_i=v_j "for some " i eq.not j quad arrow.double.long quad f(cvec(v)_1,dots,cvec(v)_n) = 0$
    ]
  ]
  #slide[
    == Bilinear form
    #definition[A *bilinear form* is a 2-form.]
    #definition[A bilinear form $angle.l dot.c,dot.c angle.r$ is *positive definite* if
    $ angle.l x,x angle.r gt.eq 0 "and" angle.l x,x angle.r=0 arrow.l.r.double.long x = 0 $
    ]
    #definition[An *inner product* is a bilinear form which is either symmetric, skew-symmetric, or alternate.
    - *Orthogonal geometry* is a symmetric inner product space
    - *Symplectic geometry* is a alternate inner product space
    ]
    #theorem[
      - If char($K$) $=$ 2, then alternate $arrow.double.long$ symmetric $arrow.double.l.r.long$ skew-symmetric
      - If char($K$) $eq.not 2$, then alternate $arrow.double.r.l.long$ skew-symmetric
    ]
  ]
  #slide[
    == Quadratic form
    #definition[
      *Quadratic space* $(V,Q)$: vector space $V$ and *quadratic form* $Q:V arrow W$ where:
      - $Q(alpha v) = alpha^2 Q(v), forall alpha in K, v in V$
      - $angle.l u,v angle.r=Q(u+v)-Q(u)-Q(v)$ is bilinear
      *$n$-ary quadratic form*: homogeneous polynomial of degree 2 in $n$ variables:
      $ Q(x_1,dots,x_n)=sum_(i=1)^n sum_(j=1)^n a_(i j)x_i x_j=x^top A x, quad (A)_(i j)=a_(i j) in K. $
    ]
    #theorem[
      If char$(K) eq.not 2$ and if $angle.l dot.c, dot.c angle.r$ is a symmetric bilinear form on $V$, then the function $Q(x)=1/2 angle.l x, x angle.r$ is a quadratic form $Q$.
    ]
  ]
// Algebra
  #slide[
    == Algebra
  ]
// Tensor algebra
  #slide[
    == Tensor algebra
  ]