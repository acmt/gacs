
\section{Vector Space}

% Vector space
\begin{frame}<0>[t]
  \frametitle{Vector space}
  \framesubtitle{Vector space}
  \begin{definition}[Vector space]
    \kw{Vector space} over a field $K$ of \kw{scalars}: $(V, +, \cdot)$
    %A vector space over a field $K$ of scalars $\alpha \in K$ is a non-empty set $V$ of vectors $v\in V$ together with a binary operation $+:V\times V\rightarrow V$ called vector addition and a binary function $\cdot:K\times V\rightarrow V$ called scalar multiplication that satisfy the following eight axioms (let $u,v, w \in V$ and $\alpha, \beta \in K$):
    \begin{itemize}
      \item<2-> Set $V$ of \kw{vectors} $\cvec{v}\in V$
      \item<3-> \kw{Vector addition} $+:V\times V\rightarrow V$ (abelian group):
        \begin{enumerate}
          \item<4-> \eqmakebox[things][l]{Associativity: }
            $ \cvec{u}+(\cvec{v}+\cvec{w})=(\cvec{u}+\cvec{v})+\cvec{w} $
          \item<5-> \eqmakebox[things][l]{Commutativity: }
            $ \cvec{u}+\cvec{v} = \cvec{v}+\cvec{u} $
          \item<6-> \eqmakebox[things][l]{Identity: }
            $ \exists 0\in V:\cvec{v}+0=\cvec{v}, \forall \cvec{v}\in V $
          \item<7-> \eqmakebox[things][l]{Reverse: }
            $ \forall \cvec{v}\in V, \exists -\cvec{v}\in V: \cvec{v}+(-\cvec{v})=0 $
        \end{enumerate}
      \item<8-> \kw{Scalar multiplication} $\cdot: K\times V\rightarrow V$:% (Ring homomorphism from $K$ to $End((V, +)))$:
        \begin{enumerate}
          %\setcounter{enumi}{4}
          \item<9-> \eqmakebox[things][l]{Scalar/Field compatibility: }
            $\alpha(\beta \cvec{v})=(\alpha\beta)\cvec{v}$
          \item<10-> \eqmakebox[things][l]{Identity: }
            $1\cvec{v}=\cvec{v}$
          \item<11-> \eqmakebox[things][l]{Distributivity w.r.t. vector addition: }
            $\alpha(\cvec{u}+\cvec{v})=\alpha \cvec{u} + \alpha \cvec{v}$
          \item<12-> \eqmakebox[things][l]{Distributivity w.r.t. field addition: }
            $(\alpha+\beta)\cvec{v} = \alpha \cvec{v}+ \beta \cvec{v}$
        \end{enumerate}
    \end{itemize}
    \onslide<13->
    Axioms 1-4: abelian group under addition \quad 5-8: $K$-module.% ring homomorphism $h:K\rightarrow End(V)$.
  \end{definition}
\end{frame}

\subsection[Subsp Lin comb Span]{Subspace, linear combination, span}

% Subspace, linear combination, span
\begin{frame}<0>[t]
  \frametitle{Vector spaces}
  \framesubtitle{Subspace, linear combination, span}
  %------------------------
  % Subspace
  %------------------------
  \begin{definition}[Subspace]
    \begin{itemize}
      \item<2-> \kw{Subspace}: $S\subseteq V \text{ and } S \text{ is a vector space } \Leftrightarrow  S \le V $
      \item<3-> \kw{Proper subspace}: $S\le V \text{ and } S \neq V \Leftrightarrow S < V$
      \item<4-> \kw{Zero subspace}: $\{0\}$
    \end{itemize}
    %A subspace of a vector space $V$ is a subset $S$ of $V$ that is a vector space in its own right under the operations obtained by restricting the operations of $V$ to $S$. We use the notation $S\le V$ to indicate that $S$ is a subspace of $V$ and $S<V$ to indicate that $S$ is a \kw{proper subspace} of $V$, that is, $S\le V$ but $S\neq V$. The \kw{zero subpace} of $V$ is $\{0\}$.
  \end{definition}

  %------------------------
  % Linear Combination
  %------------------------
  \onslide<5->\vspace{-.1cm}
  \begin{definition}[Linear combination]
    %Let $V$ be a vector space over the field $K$. The linear combination of vectors $v_1,\dots,v_n\in V$ with coefficients $\alpha_1,\dots,\alpha_n\in K$ is the expression:
    \vspace*{-1em}
    \onslide<6->
    $$\alpha_1\cvec{v}_1+\alpha_2\cvec{v}_2+\dots+\alpha_n\cvec{v}_n$$
    \vspace*{-3em}
    \begin{itemize}
      \begin{columns}
        \begin{column}[t]{.25\columnwidth}
          \item \visible<7->{$\alpha_i \in K$ and $\cvec{v}_i\in V$.}
        \end{column}
        \begin{column}[t]{.25\columnwidth}
          \item \visible<8->{\kw{Trivial}: $\forall i, \alpha_i = 0$}
        \end{column}
        \begin{column}[t]{.3\columnwidth}
          \item \visible<9->{\kw{Nontrivial} otherwise}
        \end{column}
      \end{columns}
    \end{itemize}
    %A linear combination is \kw{trivial} if every coefficient $\alpha_i$ is zero. Otherwise it is \kw{nontrivial}.
  \end{definition}
  %------------------------
  % Subspace spanned (span)
  %------------------------
  \onslide<10->\vspace{-.1cm}
  \begin{definition}[Span]\vspace*{-.9em}
    %The subspace spanned (or zsubspace generated) by a set $S$ of vectors in $V$ is the set of all linear combinations of vectors from $S$
    $$\lan S \ran = span(S) = \{\alpha_1\cvec{v}_1+\dots+\alpha_n\cvec{v}_n \mid \alpha_i \in K, \cvec{v}_i \in S\}$$\vspace*{-1em}
    %When $S=\{v_1, \dots,v_n\}$ is a finite set, we use the notation $\lan v_1,\dots,v_n\ran$, or $span(v_1,\dots,v_n)$. 
    %A set $S$ of vectors in $V$ is said to span $V$, or generate $V$, if $V=span(S)$, that is, if every vector $v\in V$ can be written in the form $$v=\alpha_1v_1+\dots+\alpha_nv_n$$ for some scalars $\alpha_1,\dots,\alpha_n$ and vectors $v_1,\dots,v_n$.
  \end{definition}
\end{frame}

\subsection[Lin indep Basis]{Linear independence, basis}

% Linear independence, basis
\begin{frame}<0>[t]
  \frametitle{Vector spaces}
  \framesubtitle{Linear independence, basis}
  %------------------------
  % Linear independence
  %------------------------
  \begin{definition}[Linear independence]\vspace*{-1em}
    %A nonempty set S of vectors in V is linearly independent if for any $v_1,\dots,v_n\in S$, we have
    \onslide<2->$$\alpha_1\cvec{v}_1+\dots+\alpha_n\cvec{v}_n=0\visible<3->{\quad\Rightarrow\quad \alpha_1=\dots=\alpha_n=0}$$
    \onslide<4->Not linearly independent $\Rightarrow$ \kw{linearly dependent}
    %If a set of vectors is not linearly independent, it is said to be linearly dependent.
  \end{definition}
  \onslide<5->
  %------------------------
  % Basis
  %------------------------
  \begin{theorem}[Equivalent conditions on linear independence]\label{thm:spanningset_linearindependence}
    %Let S be a set of vectors in V. The following are equivalent:
    \begin{enumerate}
      \item<6-> $S$ is linearly independent and spans $V$.
      \item<7-> $\forall \cvec{v}\in V, \exists! \cvec{v}_1,\dots,\cvec{v}_n\in S, \alpha_1,\dots,\alpha_n\in K:$
        \vspace{-.25cm}$$\cvec{v} = \alpha_1\cvec{v}_1+\dots+\alpha_n\cvec{v}_n\vspace{-.25cm}$$
      \item<8-> $S$ is a minimal spanning set.
      \item<9-> $S$ is a maximal linearly independent set.
    \end{enumerate}
    \onslide<10->If $S$ satisfies one (and hence all) those conditions, then $S$ is a \onslide<11->\kw{basis} for $V$
  \end{theorem}
\end{frame}

\subsection[Dim, Inner prod]{Dimension, inner product}

% Dimension, inner product
\begin{frame}<0>[t]
  \frametitle{Vector spaces}
  \framesubtitle{Dimension, inner product, inner product space}
  % %------------------------
  % % Basis
  % %------------------------
  % \begin{definition}[Basis]
  %   A set of vectors in $V$ that satisfies any (and hence all) of the equivalent conditions in Theorem~\ref{thm:spanningset_linearindependence} is called a basis for $V$.
  % \end{definition}

  %------------------------
  % Dimension
  %------------------------
  \begin{definition}[Dimension]
    \visible<2->{\kw{Finite dimensional}:} \visible<3->{$\{0\}$} \visible<4->{or has a finite basis} \visible<5->{(otw, \kw{infinite dimensional})}.
    \onslide<6->\kw{Dimension}: cardinality $\kappa$ of any basis for $V$ \visible<7->{($\kappa$-dimensional $\rightarrow\dim(V)=\kappa$)}.
    \begin{itemize}
      \item<8-> $\dim(\{0\}) = 0$
    \end{itemize}

    %A vector space $V$ is \kw{finite-dimensional} if it is the zero space {0}, or if it has a finite basis. All other vector spaces are \kw{infinite-dimensional}. The \kw{dimension} of the zero space is 0 and the \kw{dimension} of any nonzero vector space $V$ is the cardinality of any basis for $V$. If a vector space $V$ has a basis of cardinality $\kappa$, we say that $V$ is $\kappa$\kw{-dimensional} and write $\dim(V)=\kappa$.
  \end{definition}

  %------------------------
  % Inner product
  %------------------------
  \onslide<9->
  \begin{definition}[Inner product, inner product space]
    \kw{Inner product space} $(V, \lan\cdot,\cdot\ran)$\visible<10->{: vector space $V$}\visible<11->{ and \kw{inner product} $ \lan \cdot,\cdot \ran:V\times V\rightarrow K $:}
    \begin{enumerate}
      \item<12-> \makebox[5.5cm]{\emph{Linearity in the first argument}:\hfill} $ \lan \alpha\cvec{x}+\beta\cvec{y},\cvec{z}\ran = \alpha\lan\cvec{x},\cvec{y}\ran +\beta\lan \cvec{y},\cvec{z}\ran$.
      \item<13-> \makebox[5.5cm]{\emph{Hermitian symmetry}:\hfill} $ \lan\cvec{x},\cvec{y}\ran=\lan\cvec{y},\cvec{x}\ran^* $.
      \item<14-> \makebox[5.5cm]{\emph{Positive definiteness}:\hfill} $ \lan\cvec{x},\cvec{x}\ran \ge 0 $, $ \lan\cvec{x},\cvec{x}\ran = 0\Leftrightarrow \cvec{x}=\cvec{0} $.
    \end{enumerate}
  \end{definition}
\end{frame}

\subsection[linear transf.]{Linear transformation}

\begin{frame}<0>[t]
  \frametitle{Vector space}
  \framesubtitle{Linear transformations}
  \setalllength{2pt}
  \begin{definition}[Linear transformation, operator, functional]
    \kw{Linear transformation}: $\tau:V\rightarrow W$ where:
    $$\tau(\alpha\cvec{u}+\beta\cvec{v}) = \alpha\tau(\cvec{u})+ \beta\tau(\cvec{v})$$

    \vspace{-0cm}
    \begin{itemize}
      \setlength\itemsep{1pt}
      \item<3-> \makebox[4.5cm]{\kw{Linear operator}:\hfill} $\tau: V\rightarrow V$.
        \begin{itemize}
          \item<4-> \makebox[3.75cm]{\kw{Real operator}:\hfill} $\tau$ linear operator and $K=\mathbb{R}$.
          \item<5-> \makebox[3.75cm]{\kw{Complex operator}:\hfill} $\tau$ linear operator and $K=\mathbb{C}$.
        \end{itemize}
      \item<6-> \makebox[4.5cm]{\kw{Linear functional}:\hfill} $\tau: V\rightarrow K$.
        \begin{itemize}
          \item<7-> \makebox[3.75cm]{\kw{Dual space}:\hfill} $V^\ast$: set of all linear functionals on $V$.
        \end{itemize}
      \item<8-> \makebox[4.5cm]{\kw{Homomorphism}:\hfill} for linear transformation
      \item<9-> \makebox[4.5cm]{\kw{Endomorphism}:\hfill} for linear operator
      \item<10-> \makebox[4.5cm]{\kw{Monomorphism}:\hfill} for {\color{myred}injective} linear transformation
      \item<11-> \makebox[4.5cm]{\kw{Epimorphism}:\hfill} for  {\color{myred}surjective} linear transformation
      \item<12-> \makebox[4.5cm]{\kw{Isomorphism}:\hfill} for  {\color{myred}bijective} linear transformation
    \end{itemize}
  \end{definition}

\end{frame}

\subsection[Normed/metric spa]{Normed space, metric space}

% Norm, normed space, metric, metric space
\begin{frame}<0>[t]
  \frametitle{Vector spaces}
  \framesubtitle{Norm, normed space, metric, metric space}
  %------------------------
  % Norm, normed space
  %------------------------
  \begin{definition}[Norm and Normed space]
    \kw{Normed space} $(V, \|\cdot\|)$: \onslide<2->vector space $V$ \onslide<3->and \kw{norm} $\|\cdot\|:V\rightarrow K$ with conditions:
    \begin{itemize}
      \item<4-> \makebox[4cm]{\emph{Triangle inequality}:\hfill} $\|\cvec{x}+\cvec{y}\|\le\|\cvec{x}\|+\|\cvec{y}\|$
      \item<5-> \makebox[4cm]{\emph{Absolute homogeneity}:\hfill} $\|\alpha\cvec{x}\|=|\alpha|\|\cvec{x}\|$
      \item<6-> \makebox[4cm]{\emph{Positive definiteness}:\hfill} $\|\cvec{x}\|=0\Rightarrow \cvec{x}=0  $
    \end{itemize}
  \end{definition}

  %------------------------
  % Metric, metric space
  %------------------------
  \onslide<7->
  \begin{definition}[Metric and Metric space]
    %\kw{Metric}: $\text{d}(\cvec{x}, \cvec{y})=\|\cvec{x}-\cvec{y}\|$
    \kw{Metric space}: vector space $(V, d)$ and \kw{metric}/\kw{distance} $d:V\times V\rightarrow K$ with conditions:
    \begin{itemize}
      \item<8-> \makebox[4cm]{\emph{Triangle inequality}:\hfill} $\forall\cvec{x},\cvec{y},\cvec{z}\in V, d(\cvec{x},\cvec{y})\le d(\cvec{x},\cvec{z})+d(\cvec{z},\cvec{y})$
      \item<9-> \makebox[4cm]{\emph{Symmetry}:\hfill} $\cvec{x},\cvec{y}\in V, d(\cvec{x},\cvec{y})=d(\cvec{y},\cvec{z})$
      \item<10-> \makebox[4cm]{\emph{Positive definiteness}:\hfill} $\forall\cvec{x},\cvec{y}\in V, d(\cvec{x},\cvec{y})\le 0$ and $d(\cvec{x},\cvec{y})=0\Leftrightarrow \cvec{x}=\cvec{y}$
    \end{itemize}
  \end{definition}
\end{frame}

\subsection{Quotient space}

% Cosets, Quotient space
\begin{frame}<0>[t]
  \frametitle{Vector spaces}
  \framesubtitle{Quotient space}

  \begin{theorem}
    Let $S\le V$. The binary relation
    \vspace{-1em}
    \visible<2->{$${\color{myred}\cvec{u}\equiv\cvec{v}\Leftrightarrow\cvec{u}-\cvec{v}\in S}\vspace{-.7em}$$}
    \visible<3->{is an equivalence relation on $V$, whose equivalence classes are the }\visible<4->{\kw{cosets}}
    \visible<5->{\vspace{-.5em}$$[\cvec{v}]=\cvec{v}+S=\{\cvec{v}+\cvec{s}\mid \cvec{s}\in S\}\vspace{-.7em}$$}
    \visible<5->{of $S$ in $V$.\vspace{.5em}}
  \end{theorem}
  \vspace{-.2cm}
  \onslide<6->
  \begin{definition}[Quotient space]
    \kw{Quotient space} of $V$ modulo $S$: \visible<7->{vector space $V/S$ of all cosets of $S$ in $V$, where}

    \vspace{-1.5em}
    \begin{align*}
      \visible<8->{\alpha(\cvec{u}+S)        & =\alpha\cvec{u}+S}      \\
      \visible<9->{(\cvec{u}+S)+(\cvec{v}+S) & =(\cvec{u}+\cvec{v})+S}
    \end{align*}

    \vspace{-.6em}
    \visible<10->{Zero vector of $V/S$: $0+S=S$.}
  \end{definition}

\end{frame}

